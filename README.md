[![Build status](https://ci.appveyor.com/api/projects/status/581084nj03nft84v?svg=true)](https://ci.appveyor.com/project/sgraves67/repertory)
[![Build Status](https://ci.fifthgrid.com/buildStatus/icon?job=Blockstorage%2Frepertory_OS_X)](https://ci.fifthgrid.com/job/Blockstorage/job/repertory_OS_X/)
[![CircleCI](https://circleci.com/bb/blockstorage/repertory.svg?style=svg)](https://circleci.com/bb/blockstorage/repertory)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/f2de14f2560a49a8b0261004fe61f1db)](https://www.codacy.com/app/sgraves67/repertory?utm_source=sgraves67@bitbucket.org&amp;utm_medium=referral&amp;utm_content=blockstorage/repertory&amp;utm_campaign=Badge_Grade)

# Repertory
Repertory allows you to mount Sia and/or SiaPrime blockchain storage solutions via FUSE on FreeBSD/FreeNAS/Linux/OS X or via WinFSP on Windows.

# Details and Features
* Optimized for [Plex Media Server](https://www.plex.tv/)
* Single application to mount Sia and/or SiaPrime
* Only 1 Sia and 1 SiaPrime mount per user is supported
* Cross-platform support (FreeBSD/FreeNAS 64-bit, Linux 64-bit, OS X, Windows 64-bit)
* Requires Sia 1.4.1+ and/or SiaPrime 1.4.0+

# Known Issues
* Windows
    * Executables do not run from the mounted location. I have had success launching from the root folder but the root cause is still being researched. 
    * Unable to share the mounted disk for network access. Requires additional research as to whether or not this is a limitation of WinFSP. 

# Downloads

### GUI
  * [**Repertory UI**](https://bitbucket.org/blockstorage/repertory-ui)
      * NOTE: FreeBSD/FreeNAS are not supported in `repertory-ui`. Please use the standalone version or compile from source.
    
### Standalone
* [**Bitbucket Downloads**](https://bitbucket.org/blockstorage/repertory/downloads/)
    * NOTE: Windows standalone version requires the following dependencies to be installed:
        * [WinFSP 1.5B2](https://github.com/billziss-gh/winfsp/releases/download/v1.5B2/winfsp-1.5.19192.msi)
        * [Microsoft Visual C++ Redistributable for Visual Studio 2015, 2017 and 2019](https://aka.ms/vs/16/release/vc_redist.x64.exe)
    * NOTE: OS X standalone version requires the following dependency to be installed:
        * [FUSE for macOS v3.10.0](https://github.com/osxfuse/osxfuse/releases/download/osxfuse-3.10.0/osxfuse-3.10.0.dmg)

# Additional Information
Consult [CHANGELOG.md](https://bitbucket.org/blockstorage/repertory/src/master/CHANGELOG.md) for details of each release. Some relevant information may not be present in this document. 

# Current Version
* FreeBSD/FreeNAS, Linux, macOS and Windows: 
    * `1.1.3-release`
* FreeBSD/FreeNAS 64-bit release support:
    * FreeBSD 11.2 / FreeNAS 11.2
    * FreeBSD 11.3
    * FreeBSD 12.0
* Linux 64-bit distribution support:
    * Antergos
        * Use `Ubuntu 18.10` binaries for compatibility
    * Arch Linux
    * Bodhi 5.0.0
    * CentOS 7
    * Debian 9 
    * Debian 10
    * Elementary OS 5.0
    * Fedora 28
    * Fedora 29
    * Fedora 30
    * Linux Mint 19
    * Linux Mint 19.1
    * Manjaro
        * Use `Ubuntu 18.10` binaries for compatibility
    * OpenSUSE Leap 15.0
    * OpenSUSE Leap 15.1
    * OpenSUSE Tumbleweed
    * Solus       
    * Ubuntu 18.04
    * Ubuntu 18.10
    * Ubuntu 19.04

# Issues/Suggestions
Please submit [here](https://bitbucket.org/blockstorage/repertory/issues?status=new&status=open).

# Upcoming Features
* NTFS ADS support
* NTFS permissions
* Windows mount-at-boot support
* Random-write/partial upload support

# Usage

## FreeBSD/FreeNAS/Linux

### Sia Mount
```repertory -o big_writes ~/sia```

### SiaPrime Mount
```repertory -o big_writes,sp ~/siaprime```

### FreeBSD/FreeNAS
The `fusefs-libs` package is required for FUSE operations. To install, execute the following as `root`:
```
pkg install fusefs-libs
```
Make sure the `fuse` kernel module is being loaded at boot. If `/dev/fuse` is not present, add the following line to `/boot/loader.conf`:
```
fuse_load="YES"
```

You can manually load the `fuse` kernel module by issuing the following command as `root`:
```
kldload fuse
```

If you receive the error message `Operation not permittied` when attempting to mount, you will need to enable user mounts. 
Also, be sure your normal user account is a member of the group `operator`.

As `root`, execute the following command:
```
sysctl vfs.usermount=1
```

To make this setting persistent after reboot, edit `/etc/sysctl.conf` and add the following line:
```
vfs.usermount=1
```

## Mac OS X

### Sia Mount
```repertory -o big_writes ~/sia```

### SiaPrime Mount
```repertory -o big_writes,sp ~/siaprime```

## Windows

### Sia Mount
```repertory.exe T:```

### SiaPrime Mount
```repertory.exe -sp T:```

# Plex Media Server on FreeBSD/FreeNAS/Linux
Most Linux distributions will create a service user account `plex`. Repertory will need to be mounted as the `plex` user for media discovery and playback. The following is an example of how to configure and mount Repertory for use in Plex:


```bash
sudo mkdir /home/plex
sudo chown plex.plex /home/plex
sudo -u plex repertory -o uid=<plex uid>,gid=<plex gid>,big_writes /mnt/location
```

Alternatively, you can mount as root and allow default permissions. This will create a FUSE mount that mimics a normal disk. Once mounted, you will need to set filesystem permissions according to your requirements. The following is an example of how to setup a Repertory mount in `/etc/fstab`:

```bash
sudo ln -s <path to 'repertory'> /sbin/mount.repertory
```
Example `/etc/fstab` entries for Sia:
```
none /mnt/sia repertory defaults,_netdev,default_permissions,allow_other 0 0
```
```
repertory /mnt/sia fuse defaults,_netdev,default_permissions,allow_other 0 0
```
Example `/etc/fstab` entries for SiaPrime:
```
none /mnt/siaprime repertory defaults,sp,_netdev,default_permissions,allow_other 0 0
```
```
repertory /mnt/siaprime fuse defaults,sp,_netdev,default_permissions,allow_other 0 0
```
Please note that the Sia and/or SiaPrime daemon's must also be launched at boot time for `fstab` mounts to work.

# Data Locations
You will find all relevant configuration and logging files in the root data directory of repertory. Consult the list below to determine this location for your operating system.

* FreeBSD/FreeNAS/Linux
    * ~/.local/repertory/sia
    * ~/.local/repertory/siaprime
* Mac OS X
    * ~/Library/Application Support/repertory/sia
    * ~/Library/Application Support/repertory/siaprime
* Windows
    * %LOCALAPPDATA%\repertory\sia
    * %LOCALAPPDATA%\repertory\siaprime

# Configuration Details
If you have enabled an API password in Sia or SiaPrime, it is recommended to pre-create repertory configuration prior to mounting.

## FreeBSD/FreeNAS/Linux

### Generate Sia Configuration
* ```repertory -gc```

### Generate SiaPrime Configuration
* ```repertory -gc -sp```

### Setting API Password
* For Sia edit:
    * ```~/.local/repertory/sia/config.json``` 
* For SiaPrime edit:
    * ```~/.local/repertory/siaprime/config.json``` 
    
## Mac OS X

### Generate Sia Configuration
* ```repertory -gc```

### Generate SiaPrime Configuration
* ```repertory -gc -sp```

### Setting API Password
* For Sia edit:
    * ```~/Libray/Application Support/repertory/sia/config.json``` 
* For SiaPrime edit:
    * ```~/Libray/Application Support/repertory/siaprime/config.json``` 
    
## Windows

### Generate Sia Configuration
* ```repertory.exe -gc```

### Generate SiaPrime Configuration
* ```repertory.exe -gc -sp```

### Setting API Password
* For Sia edit:
    * ```%LOCALAPPDATA%\repertory\sia\config.json``` 
* For SiaPrime edit:
    * ```%LOCALAPPDATA%\repertory\siaprime\config.json``` 
    
## Sample Sia Configuration
```
{
  "ApiAuth": "password",
  "ApiPort": 11101,
  "ApiUser": "repertory",
  "ChunkDownloaderTimeoutSeconds": 30,
  "ChunkSize": 4096,
  "EnableDriveEvents": false,
  "EnableMaxCacheSize": true,
  "EventLevel": "Normal",
  "EvictionDelayMinutes": 30,
  "HighFreqIntervalSeconds": 30,
  "HostConfig": {
    "AgentString": "Sia-Agent",
    "ApiPassword": "",
    "ApiPort": 9980,
    "HostNameOrIp": "localhost",
    "TimeoutMs": 60000
  },
  "LowFreqIntervalSeconds": 900,
  "MaxCacheSizeBytes": 10737418240,
  "MinimumRedundancy": 2.5,
  "OnlineCheckRetrySeconds": 60,
  "OrphanedFileRetentionDays": 15,
  "PreferredDownloadType": "Fallback",
  "ReadAheadCount": 4,
  "RingBufferFileSize": 512,
  "StorageByteMonth": "0",
  "Version": 4
}
```

## Sample SiaPrime Configuration
```
{
  "ApiAuth": "password",
  "ApiPort": 11102,
  "ApiUser": "repertory",
  "ChunkDownloaderTimeoutSeconds": 30,
  "ChunkSize": 4096,
  "EnableDriveEvents": false,
  "EnableMaxCacheSize": true,
  "EventLevel": "Normal",
  "EvictionDelayMinutes": 30,
  "HighFreqIntervalSeconds": 30,
  "HostConfig": {
    "AgentString": "SiaPrime-Agent",
    "ApiPassword": "",
    "ApiPort": 4280,
    "HostNameOrIp": "localhost",
    "TimeoutMs": 60000
  },
  "LowFreqIntervalSeconds": 900,
  "MaxCacheSizeBytes": 10737418240,
  "MinimumRedundancy": 2.5,
  "OnlineCheckRetrySeconds": 60,
  "OrphanedFileRetentionDays": 15,
  "PreferredDownloadType": "Fallback",
  "ReadAheadCount": 4,
  "RingBufferFileSize": 512,
  "StorageByteMonth": "0",
  "Version": 4
}
```

## Configuration Settings
Configuration settings that are not documented below should be ignored for now as they are used internally.

`StorageByteMonth` should never be altered. On start-up, this value is used to calculate estimated storage space. 

You can change or retrieve these values using the `repertory` executable:


`repertory -get EnableDriveEvents`

`repertory -set EnableDriveEvents false`

### ApiAuth
Password used to connect to Repertory API. Auto-generated by default.

### ApiPort
Repertory API port to use for JSON-RPC requests. 

* Sia default: `11101`
* SiaPrime default: `11102`

### ApiUser
Username used to connect to Repertory API. Default is 'repertory'.

### ChunkDownloaderTimeoutSeconds
Files that are not cached locally will download data in `ChunkSize` chunks when a read or write operation occurs. This timeout value specifies the amount of time chunks should continue downloading after the last file handle has been closed.

### ChunkSize
This is the minimum data size (converted to KiB - value of 8 means 8KiB) used for downloads. This value cannot be less than 8 and should also be a multiple of 8. Default is 2048. 

### EnableDriveEvents
When set to `true`, additional logging for FUSE on UNIX or WinFSP on Windows will occur. It's best to leave this value set to 'false' unless troubleshooting an issue as enabling it may have an adverse affect on performance.

### EnableMaxCacheSize
If set to `true`, files will begin to be removed from the local cache as soon as `MaxCacheSizeBytes` and `MinimumRedundancy` have been met. This does not mean further attempts to write will fail when `MaxCacheSizeBytes` is reached. Writes will continue as long as there is enough local drive space to accommodate the operation.

If set to `false`, files will begin to be removed from the local cache as soon as `MinimumRedundancy` has been met.

In both cases, files that do not have any open handles will be chosen by oldest modification date for removal.

### EventLevel
Internally, events are fired during certain operations. This setting determines which events should be logged to `repertory.log`. Valid values are `Error`, `Warn`, `Normal`, `Debug`, and `Verbose`.

### EvictionDelayMinutes
Number of minutes to wait after all file handles are closed before allowing file to be evicted from cache.

### MaxCacheSizeBytes
This value specifies the maximum amount of local space to consume before files are removed from cache. `EnableMaxCacheSize` must also be set to `true` for this value to take affect.

### MinimumRedundancy
Files are elected for removal once this value has been reached. Be aware that this value cannot be set to  less than 1.5x.

### OnlineCheckRetrySeconds
Number of seconds to wait for Sia/SiaPrime daemon to become available/connectable.

### OrphanedFileRetentionDays
Repertory attempts to keep modifications between Sia-UI and the mounted location in sync as much as possible. In the event a file is removed from Sia-UI, it will be marked as orphaned in Repertory and moved into an 'orphaned' directory within Repertory's data directory. This setting specifies the number of days this file is retained before final deletion.

### PreferredDownloadType
Repertory supports 3 download modes for reading files that are not cached locally: full file allocation, ring buffer mode and direct mode.

Full file allocation mode pre-allocates the entire file prior to downloading. This mode is required for writes but also ensures the best performance when reading data.

Ring buffer mode utilizes a fixed size file buffer to enable a reasonable amount of seeking. This alleviates the need to fully allocate a file. By default, it is 512MiB. When the buffer is full, it attempts to maintain the ability to seek 50% ahead or behind the current read location without the need to re-download data from Sia/SiaPrime.

Direct mode utilizes no disk space. All data is read directly from Sia/SiaPrime.

Preferred download type modes are:

* _Fallback_ - If there isn't enough local space to allocate the full file, ring buffer mode is used. If there isn't enough local space to create the ring buffer's file, then direct mode is used.
* _RingBuffer_ - Full file allocation is always bypassed; however, if there isn't enough space to create the ring buffer's file, then direct mode will be chosen.
* _Direct_ - All files will be read directly from Sia/SiaPrime.

### ReadAheadCount
This value specifies the number of read-ahead chunks to use when downloading a file. This is a per-open file setting and will result in the creation of an equal number of threads.

### RingBufferFileSize
The size of the ring buffer file in MiB. Default is 512. Valid values are: 64, 128, 256, 512, 1024.

### Version
Configuration file version. This is used internally and should not be modified.

# Compiling

## FreeBSD/FreeNAS/Linux 64-bit
* Requires [CMake](https://cmake.org) 3.1 or above
* Requires ```fuse``` 2.9.x binary/kernel module
* Requires ```libfuse``` 2.9.x binary and development package
* Requires ```openssl``` binary and development package
* Requires ```zlib``` binary and development package
* Execute the following from the ```repertory``` source directory:
    * ```mkdir build && cd build```
    * Debug build:
        * ```cmake -DCMAKE_BUILD_TYPE=Debug ..```
    * Release build:
        * ```cmake -DCMAKE_BUILD_TYPE=Release ..```
    * ```make```
    * Optionally:
        * ```sudo make install```
        
## Mac OS X
* Requires XCode from Apple Store
* Requires [CMake](https://cmake.org) 3.1 or above
* Requires [FUSE for macOS 3.10.0](https://github.com/osxfuse/osxfuse/releases/download/osxfuse-3.10.0/osxfuse-3.10.0.dmg)
* Execute the following from the ```repertory``` source directory:
    * ```mkdir build && cd build```
    * Debug build:
        * ```cmake -DCMAKE_BUILD_TYPE=Debug ..```
    * Release build:
        * ```cmake -DCMAKE_BUILD_TYPE=Release ..```
    * ```make```
    
## Windows 7 or above 64-bit
* Install [cmake-3.15.2](https://github.com/Kitware/CMake/releases/download/v3.15.2/cmake-3.15.2-win64-x64.msi)
* Install [Win32 OpenSSL](https://slproweb.com/products/Win32OpenSSL.html) 64-bit version
* Install [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/)
    * During installation, make sure to install:
        * C++ development support
        * Windows 10 SDK
* Install [WinFSP 1.5B2](https://github.com/billziss-gh/winfsp/releases/download/v1.5B2/winfsp-1.5.19192.msi)
* Execute Windows build script
    * Debug build
        * `scripts\build_win64_debug.cmd`
    * Release build
        * `scripts\build_win64_release.cmd`
    * NOTE: `cmake.exe` must be in Windows search path. If not, modify the following line in `build_win64.cmd`:
        * `set CMAKE="<Full path to cmake.exe>"`
        
# Credits
* [boost c++ libraries](https://www.boost.org/)
* [C++ REST SDK](https://github.com/microsoft/cpprestsdk)
* [curl](https://curl.haxx.se/)
* [FUSE for macOS](https://osxfuse.github.io/)
* [JSON for Modern C++](https://github.com/nlohmann/json)
* [jsonrpcpp - C++ JSON-RPC 2.0 library](https://github.com/badaix/jsonrpcpp)
* [libfuse](https://github.com/libfuse/libfuse)
* [OpenSSL](https://www.openssl.org/)
* [OSSP uuid](http://www.ossp.org/pkg/lib/uuid/)
* [Pixeldrain - Free File Sharing](https://pixeldrain.com/)
* [RocksDB](https://rocksdb.org/)
* [Sia Decentralized Cloud Storage](https://sia.tech/)
* [SQLiteC++](https://github.com/SRombauts/SQLiteCpp)
* [ttmath - Bignum C++ library](https://www.ttmath.org/)
* [WinFSP - FUSE for Windows](https://github.com/billziss-gh/winfsp)
* [zlib](https://zlib.net/)

# Tips
* BTC: 1CMvhGaJfH95VS4CTS6dJYDs8kwYXTUCEA
* SC: c5510db5c2e85794b9cb81851daa57d6dfd5b4e42cd02789442e8789cd6492b07c754b2056ed

# Developer Public Key
```
-----BEGIN PUBLIC KEY-----
MIIEIjANBgkqhkiG9w0BAQEFAAOCBA8AMIIECgKCBAEKfZmq5mMAtD4kSt2Gc/5J
H+HHTYtUZE6YYvsvz8TNG/bNL67ZtNRyaoMyhLTfIN4rPBNLUfD+owNS+u5Yk+lS
ZLYyOuhoCZIFefayYqKLr42G8EeuRbx0IMzXmJtN0a4rqxlWhkYufJubpdQ+V4DF
oeupcPdIATaadCKVeZC7A0G0uaSwoiAVMG5dZqjQW7F2LoQm3PhNkPvAybIJ6vBy
LqdBegS1JrDn43x/pvQHzLO+l+FIG23D1F7iF+yZm3DkzBdcmi/mOMYs/rXZpBym
2/kTuSGh5buuJCeyOwR8N3WdvXw6+KHMU/wWU8qTCTT87mYbzH4YR8HgkjkLHxAO
5waHK6vMu0TxugCdJmVV6BSbiarJsh66VRosn7+6hlq6AdgksxqCeNELZBS+LBki
tb5hKyL+jNZnaHiR0U7USWtmnqZG6FVVRzlCnxP7tZo5O5Ex9AAFGz5JzOzsFNbv
xwQ0zqaTQOze+MJbkda7JfRoC6TncD0+3hoXsiaF4mCn8PqUCn0DwhglcRucZlST
ZvDNDo1WAtxPJebb3aS6uymNhBIquQbVAWxVO4eTrOYEgutxwkHE3yO3is+ogp8d
xot7f/+vzlbsbIDyuZBDe0fFkbTIMTU48QuUUVZpRKmKZTHQloz4EHqminbfX1sh
M7wvDkpJEtqbc0VnG/BukUzP6e7Skvgc7eF1sI3+8jH8du2rivZeZAl7Q2f+L9JA
BY9pjaxttxsud7V5jeFi4tKuDHi21/XhSjlJK2c2C4AiUEK5/WhtGbQ5JjmcOjRq
yXFRqLlerzOcop2kbtU3Ar230wOx3Dj23Wg8++lV3LU4U9vMR/t0qnSbCSGJys7m
ax2JpFlTwj/0wYuTlVFoNQHZJ1cdfyRiRBY4Ou7XO0W5hcBBKiYsC+neEeMMHdCe
iTDIW/ojcVTdFovl+sq3n1u4SBknE90JC/3H+TPE1s2iB+fwORVg0KPosQSNDS0A
7iK6AZCDC3YooFo+OzHkYMt9uLkXiXMSLx70az+qlIwOzVHKxCo7W/QpeKCXUCRZ
MMdlYEUs1PC8x2qIRUEVHuJ0XMTKNyOHmzVLuLK93wUWbToh+rdDxnbhX+emuESn
XH6aKiUwX4olEVKSylRUQw8nVckZGVWXzLDlgpzDrLHC8J8qHzFt7eCqOdiqsxhZ
x1U5LtugxwSWncTZ7vlKl0DuC/AWB7SuDi7bGRMSVp2n+MnD1VLKlsCclHXjIciE
W29n3G3lJ/sOta2sxqLd0j1XBQddrFXl5b609sIY81ocHqu8P2hRu5CpqJ/sGZC5
mMH3segHBkRj0xJcfOxceRLj1a+ULIIR3xL/3f8s5Id25TDo/nqBoCvu5PeCpo6L
9wIDAQAB
-----END PUBLIC KEY-----
```
