#!/bin/bash

DISTNAME=unknown
DISTVER=

resetDistVer() {
  DISTNAME=unknown
  DISTVER=
}

if [ -f /etc/centos-release ]; then
  . /etc/os-release
  if [ "$VERSION_ID" == "7" ]; then
    DISTNAME=centos
    DISTVER=7
  else
    resetDistVer
  fi
elif [ -f /etc/fedora-release ]; then
  . /etc/os-release
  if [ "$VERSION_ID" == "30" ]; then
    DISTNAME=fedora
    DISTVER=30
  elif [ "$VERSION_ID" == "29" ]; then
    DISTNAME=fedora
    DISTVER=29
  elif [ "$VERSION_ID" == "28" ]; then
    DISTNAME=fedora
    DISTVER=28
  else
    resetDistVer
  fi
elif [ -f /etc/solus-release ]; then
  DISTNAME=solus
elif [ -f /etc/lsb-release ]; then
  . /etc/lsb-release
  DISTNAME=$(echo ${DISTRIB_ID} | awk '{print tolower($0)}')
  DISTVER=${DISTRIB_RELEASE}
  if [ "$DISTNAME" != "ubuntu" ]; then
    if [ "$DISTNAME" = "linuxmint" ]; then
      if [ "$DISTVER" = "19" ] || [ "$DISTVER" = "19.1" ]; then
        DISTNAME=ubuntu
        DISTVER=18.04
      else
        resetDistVer
      fi
    elif [ "$DISTNAME" = "elementary" ]; then
      if [ "$DISTVER" = "5.0" ]; then
        DISTNAME=ubuntu
        DISTVER=18.04
      else
        resetDistVer
      fi
    else
      resetDistVer
    fi
  fi
elif [ -f /etc/debian_version ]; then
  DISTNAME=debian
  DISTVER=$(head -1 /etc/debian_version|awk -F. '{print $1}')
  if [ "$DISTVER" != "9" ]; then
    if [ "$DISTVER" != "10" ]; then
      resetDistVer
    fi
  fi
fi  

if [ "$DISTNAME" == "unknown" ]; then
  if [ -f /etc/os-release ]; then
    . /etc/os-release
    if [ "$ID" == "arch" ]; then
      DISTNAME=arch
    elif [ "$ID" == "antergos" ] || [ "$ID" == "manjaro" ]; then
      DISTNAME=ubuntu
      DISTVER=18.10
    elif [ "$ID" == "opensuse-leap" ]; then
      if [ "$VERSION_ID" == "15.0" ]; then
        DISTNAME=opensuse
        DISTVER=15
      elif [ "$VERSION_ID" == "15.1" ]; then
        DISTNAME=opensuse
        DISTVER=15.1
      else
        resetDistVer
      fi
    elif [ "$ID" == "opensuse-tumbleweed" ]; then
      DISTNAME=tumbleweed
      DISTVER=
    else
      resetDistVer
    fi
  else
    resetDistVer
  fi
fi

echo ${DISTNAME}${DISTVER}
