#!/bin/bash

beginsWith() { case $2 in "$1"*) true;; *) false;; esac; }

TAG=$1
ENABLE_UPLOAD=$2
BITBUCKET_AUTH=$3
CURRENT_DIR=$(pwd)
TAG_BUILDS_ROOT=${CURRENT_DIR}/tag_builds
mkdir -p ${TAG_BUILDS_ROOT}

if beginsWith darwin "$OSTYPE"; then
  PLATFORM_NAME=darwin
elif [ "$(uname)" = "FreeBSD" ]; then
  PLATFORM_NAME=freebsd
else
  PLATFORM_NAME=$(sh "${CURRENT_DIR}/detect_linux.sh")
fi
NUM_JOBS=$(getconf _NPROCESSORS_ONLN 2>/dev/null || getconf NPROCESSORS_ONLN 2>/dev/null || echo 1)

if [ "$PLATFORM_NAME" = "darwin" ]; then
  BASE64_EXEC=base64
  JQ_EXEC=jq-osx-amd64
  SHA256_EXEC="shasum -a 256 -b"
elif [ "$PLATFORM_NAME" = "freebsd" ]; then
  BASE64_EXEC=base64
  JQ_EXEC=jq
  SHA256_EXEC="shasum -a 256 -b"
else
  BASE64_EXEC="base64 -w0"
  JQ_EXEC=jq-linux64
  SHA256_EXEC="sha256sum -b"
fi

REPERTORY_ROOT=${TAG_BUILDS_ROOT}/repertory
TAG_BUILD=${TAG_BUILDS_ROOT}/${PLATFORM_NAME}/${TAG}

rm -f ${TAG_BUILDS_ROOT}/${PLATFORM_NAME}.error

split_string() {
  IFS=$1
  DATA=$2
  read -ra SPLIT_STRING <<< "$DATA"
  IFS=' '
}

exit_script() {
  echo $1 > ${TAG_BUILDS_ROOT}/${PLATFORM_NAME}.error
  echo $1
  cd "${CURRENT_DIR}"
  exit 1
}

upload_to_bitbucket() {
  SOURCE_FILE=$1
  curl --fail -u "${BITBUCKET_AUTH}" -X POST https://api.bitbucket.org/2.0/repositories/blockstorage/repertory/downloads -F files=@${SOURCE_FILE} > upload_response.json || exit_script "Upload to Bitbucket failed: ${SOURCE_FILE}"
}

set_temp_ld_library_path() {
  if [ ! "$PLATFORM_NAME" = "freebsd" ]; then
    LD_LIBRARY_PATH=${TAG_BUILD}/external/lib:${TAG_BUILD}/external/lib64
    export LD_LIBRARY_PATH
  fi
}

clear_temp_ld_library_path() {
  if [ ! "$PLATFORM_NAME" = "freebsd" ]; then
    LD_LIBRARY_PATH=
    export LD_LIBRARY_PATH
  fi
}

if [ "$PLATFORM_NAME" = "unknown" ]; then
  exit_script "Unknown build platform"
elif [ -z "$TAG" ]; then
  exit_script "Branch/tag not supplied"
else
  split_string '_' ${TAG}
  APP_VER=${SPLIT_STRING[0]}

  split_string '-' ${APP_VER}
  APP_RELEASE_TYPE=${SPLIT_STRING[1]}

  split_string '.' ${APP_RELEASE_TYPE}
  APP_RELEASE_TYPE=${SPLIT_STRING[0]}
  APP_RELEASE_TYPE="$(tr '[:lower:]' '[:upper:]' <<< ${APP_RELEASE_TYPE:0:1})${APP_RELEASE_TYPE:1}"
  if [ "$APP_RELEASE_TYPE" = "Rc" ]; then
    APP_RELEASE_TYPE="RC"
  fi
  mkdir -p "${TAG_BUILD}"

  rm -f "${TAG_BUILD}/repertory.sig"
  rm -f "${TAG_BUILD}/repertory.sha256"

  if [ ! -f ${TAG_BUILDS_ROOT}/blockstorage_dev_public.pem ]; then
    cp -f blockstorage_dev_public.pem ${TAG_BUILDS_ROOT}
  fi

  if [ ! -f ${TAG_BUILDS_ROOT}/blockstorage_dev_private.pem ]; then
    cp -f ../blockstorage_dev_private.pem ${TAG_BUILDS_ROOT}
  fi

  if [ "$PLATFORM_NAME" = "debian9" ]; then
    CMAKE_OPTS="-DCMAKE_CXX_STANDARD=14"
  fi

  if [ "$PLATFORM_NAME" = "centos7" ]; then
    if [ ! -d "${TAG_BUILDS_ROOT}/cmake-3.14.2-Linux-x86_64" ]; then
      cd "${TAG_BUILDS_ROOT}"
      wget https://github.com/Kitware/CMake/releases/download/v3.14.2/cmake-3.14.2-Linux-x86_64.tar.gz || exit_script "Download cmake failed"
      tar xvzf cmake-3.14.2-Linux-x86_64.tar.gz || exit_script "Extract cmake failed"
      rm -f cmake-3.14.2-Linux-x86_64.tar.gz
      cd -
    fi
    PATH="${TAG_BUILDS_ROOT}/cmake-3.14.2-Linux-x86_64/bin:$PATH"
  fi

  cd "${TAG_BUILDS_ROOT}"
  git clone https://bitbucket.org/blockstorage/repertory.git 2>/dev/null

  cd "${REPERTORY_ROOT}"
  git clean -f || exit_script "Git clean failed"
  git clean -f -d || exit_script "Git clean failed"
  git tag -d ${TAG} 1>/dev/null 2>&1
  (git checkout . && git checkout master && git pull && git checkout ${TAG}) || exit_script "Git checkout failed"

  GIT_REV=$(git rev-parse --short HEAD)
  OUT_FILE=repertory_${TAG}_${GIT_REV}_${PLATFORM_NAME}.zip
  OUT_ZIP=${TAG_BUILDS_ROOT}/${OUT_FILE}

  PATH="${REPERTORY_ROOT}/bin:$PATH"
  export PATH

  if [ ! "$PLATFORM_NAME" = "freebsd" ]; then
    chmod +x "${REPERTORY_ROOT}/bin/${JQ_EXEC}" || exit_script "chmod +x ${JQ_EXEC} failed"
  fi

  if [ -f "${OUT_ZIP}" ]; then
    echo ${PLATFORM_NAME} already exists
  else
    cd "${TAG_BUILD}"
    if [ ! -f "${TAG_BUILDS_ROOT}/releases.json" ]; then
      cp -f ${REPERTORY_ROOT}/releases.json ${TAG_BUILDS_ROOT}/releases.json || exit_script "Copy releases.json failed"
    fi

    if (cmake "${REPERTORY_ROOT}" ${CMAKE_OPTS} && make clean && make -j ${NUM_JOBS}) || (cmake "${REPERTORY_ROOT}" ${CMAKE_OPTS} && make -j ${NUM_JOBS}); then
      PATH=${TAG_BUILD}/external/bin:$PATH
      export PATH

      if [ "$PLATFORM_NAME" = "freebsd" ]; then
        strip ./repertory
      fi
      ${SHA256_EXEC} ./repertory>repertory.sha256 || exit_script "SHA-256 failed for repertory"

      set_temp_ld_library_path
      openssl dgst -sha256 -sign ${TAG_BUILDS_ROOT}/blockstorage_dev_private.pem -out repertory.sig repertory || exit_script "Create signature failed for repertory"
      openssl dgst -sha256 -verify ${TAG_BUILDS_ROOT}/blockstorage_dev_public.pem -signature repertory.sig repertory || exit_script "Verify signature failed for repertory"
      clear_temp_ld_library_path

      zip "${OUT_ZIP}" repertory repertory.sha256 repertory.sig || exit_script "Create zip failed"

      cd "${TAG_BUILDS_ROOT}"
      ${SHA256_EXEC} ./${OUT_FILE}>${OUT_FILE}.sha256 || exit_script "SHA-256 failed for zip file"

      set_temp_ld_library_path
      openssl dgst -sha256 -sign blockstorage_dev_private.pem -out ${OUT_FILE}.sig ${OUT_FILE} || exit_script "Create signature failed for zip file"
      openssl dgst -sha256 -verify blockstorage_dev_public.pem -signature ${OUT_FILE}.sig ${OUT_FILE} >${OUT_FILE}.status 2>&1 || exit_script "Verify signature failed for zip file"
      clear_temp_ld_library_path

      if [ "${PLATFORM_NAME}" = "freebsd" ]; then
        ${BASE64_EXEC} ${OUT_FILE}.sig | tr -d '\n' > ${OUT_FILE}.sig.b64 || exit_script "Create base64 signature failed for zip file"
      else
        ${BASE64_EXEC} ${OUT_FILE}.sig > ${OUT_FILE}.sig.b64 || exit_script "Create base64 signature failed for zip file"
      fi

      if [ "${ENABLE_UPLOAD}" = "1" ]; then
        APP_SIG=$(cat ${OUT_FILE}.sig.b64)
        APP_SHA256=$(cat ${OUT_FILE}.sha256 | awk '{print $1;}')
        rm -f upload_response.json 1>/dev/null 2>&1
        curl --fail -F name="${OUT_FILE}" -F anonymous=true -F file="@${OUT_FILE}" https://pixeldrain.com/api/file > upload_response.json || exit_script "Upload to Pixeldrain failed"

        PIXEL_SUCCESS=$(${JQ_EXEC} .success upload_response.json)
        if [ "${PIXEL_SUCCESS}" = "false" ]; then
          PIXEL_MESSAGE=$(${JQ_EXEC} .message upload_response.json)
          exit_script "${PIXEL_MESSAGE}"
        else
          PIXEL_ID=$(${JQ_EXEC} .id upload_response.json|sed s/\"//g)
          PIXEL_LOCATION=https://pixeldrain.com/api/file/${PIXEL_ID}

          upload_to_bitbucket "${OUT_FILE}"
          upload_to_bitbucket "${OUT_FILE}.sha256"
          upload_to_bitbucket "${OUT_FILE}.sig"
          BITBUCKET_LOCATION=https://bitbucket.org/blockstorage/repertory/downloads/${OUT_FILE}

          ${JQ_EXEC} ".Versions.${APP_RELEASE_TYPE}[\"${PLATFORM_NAME}\"]|=(.+ [\"${APP_VER}\"]|unique)" ${TAG_BUILDS_ROOT}/releases.json > releases_temp.json || exit_script "Update releases.json Versions failed"
          ${JQ_EXEC} ".Locations[\"${PLATFORM_NAME}\"].\"${APP_VER}\".sig=\"${APP_SIG}\"" releases_temp.json > releases.json || exit_script "Update releases.json sig failed"
          ${JQ_EXEC} ".Locations[\"${PLATFORM_NAME}\"].\"${APP_VER}\".sha256=\"${APP_SHA256}\"" releases.json > releases_temp.json || exit_script "Update releases.json sha256 failed"
          ${JQ_EXEC} ".Locations[\"${PLATFORM_NAME}\"].\"${APP_VER}\".urls=[\"${PIXEL_LOCATION}\",\"${BITBUCKET_LOCATION}\"]" releases_temp.json > releases.json || exit_script "Update releases.json URL failed"

          rm -f releases_temp.json
        fi
      fi
    else
       exit_script "Build failed"
    fi
  fi
  cd "${CURRENT_DIR}"
fi
