# Changelog

## 1.1.3-release
* Fix premature download end when seeking using `CBufferedReader`

## 1.1.2-release
* \#81: Fix leak in `GetSecurityByName()`
* \#82: Error reading created file while write is occurring

## 1.1.1-release
* Fixed looping on download error

## 1.1.1-rc
* \#75: Unable to save new file to mount location via Notepad
* \#77: Copying folder structure returns an error

## 1.1.0-release
* \#64: Complete FreeBSD support
* \#70: Implement file-based ring buffer reader
    * \#72: Implement timeout handling in `CRingDownload`
* \#71: Implement cache-less direct Sia reads
    * \#73: Implement timeout handling in `CDirectDownload`
* \#76: Nested directory rename is failing to delete original directory
* Upgraded WinFSP to v1.5B2
* Require VisualStudio 2019
* Support WinFSP uninstall/reboot for upgrade/downgrade
* Changed 'EvictionDelaySeconds' to 'EvictionDelayMinutes'
* JSON for Modern C++ to v3.7.0
* Reduced default `ReadAheadCount` to `4`
* Increased default `ChunkSize` to `4096`
* Removed directory caching
* Fix cache files being prematurely deleted due to Sia reporting `ondisk` as `false` during `siad` startup.

## 1.0.1-release
* \#68: Windows 7 is failing to mount: RES|-1073741591
* Downgrade RocksDB to 6.0.2 on Windows

## 1.0.0-release
* \#63: Throttle write queue to prevent excessive memory usage
* Set 'EnableChunkDownloaderTimeout' to 'true', by default
* Set 'HostConfig.TimeoutMs' to '60000', by default
* Fix high CPU usage on download
* Check files for eviction eve  ry 60s when 'EnableMaxCacheSize' is 'false'
* Removed experimental support for remote Sia uploads

## 1.0.0-rc
* \#55: Fix download timeout
    * Downloads should no longer timeout if open handles are present
* \#56: File not evicted from cache when source path doesn't match siapath
* \#57: Rename is failing when '&' is part of path name
* \#58: Excessive re-uploads when copying files using Finder
* \#59: Slow directory browsing on Windows
* \#60: Used storage space isn't being updated correctly for externally added files
* \#61: Used storage space isn't being updated correctly for externally deleted files
* Log unmount command result
* OpenSSL to v1.1.c
* RocksDB to v6.1.2
* Added Bitbucket as backup download source
* Additional Linux distribution support:
    * Debian 10
    * OpenSUSE Leap 15.0
    * OpenSUSE Leap 15.1
    * OpenSUSE Tumbleweed
* Fix potential for incorrect used cache space calculation
* Added 'getDriveInformation' RPC and '-di,--drive_information' CLI options
* Added 'getOpenFiles' RPC and '-of,--open_files' CLI options
* Completed processing of orphaned files (files removed in Sia-UI or other external client(s) that still reside in `repertory` cache directory)
* Handle `nullptr` `struct timespec` when calling FUSE `utimens`

## 1.0.0-beta.2
* Fixed macOS unmount hang

## 1.0.0-beta
* Linux distribution support
    * Arch Linux
    * Bodhi 5.0.0
    * CentOS 7
    * Debian 9 
    * Elementary OS 5.0
    * Fedora 28
    * Fedora 29
    * Fedora 30
    * Linux Mint 19
    * Linux Mint 19.1
    * Solus       
    * Ubuntu 18.04
    * Ubuntu 18.10
    * Ubuntu 19.04
* Fixed file/directory Properties display on Windows
* Erase directory cache when new file/directory is discovered
* Support for Sia partial downloads
* Fix importing Sia files that where added externally (via Sia-UI)
    * Affects OS X and Linux only
* Removed ClearDownloadHistory()
* Use OSSP uuid library for Linux
* RocksDB to 6.0.1
* Support Sia uploads to remote `siad` instances (EXPERIMENTAL)
    * Mounts can now be performed on remote systems
    * Enabled via `EnableUploadManager` configuration setting
* Static linking for all support libraries (excluding WinFSP/FUSE and cpprestsdk on Windows)
* Improved random reads and download processing  
* Added single retry for API read failures
* Added download progress logging
* Removed Hyperspace support
    
## 1.0.0-alpha.4
* OS X support
* FUSE extended attributes
* Fast active download cancel
* Used drive space calculated in real-time
* C++ REST SDK to v2.10.6
* SiaPrime support
* Added minimum daemon version check
    * Hyperspace >=0.2.3
    * Sia >=1.4.0
    * SiaPrime >=1.3.5.2
* Load provider API password from file
* Added basic authentication to JSON-RPC
* JSON for Modern C++ to v3.5.0
* Added Windows automated build scripts
    * `build_win64_debug.cmd`
    * `build_win64_release.cmd`
* Fixed Windows file time precision
* WinFSP to v2019.1
* Added 'OnlineCheckRetrySeconds' configuration option
* Prefer provider times (accessed, creation, modified, changed) for discovered files
* Sia 1.4.0 directory API support
* Moved source path to RocksDB
* Removed SQLite database dependency from Sia/SiaPrime provider's 
* Added install target to CMakeLists.txt for Linux and macOS

## 1.0.0-alpha.3
* Fixed GetSecurityByName() crash on nullptr
* Fix download looping when file is removed
* Fix crash on exit
* Chunk reader threads created per open file handle
* Debian 8 compilation support 
    * Requires 'gcc-5.5.0' to be compiled/installed
    * Requires 'cpprestsdk' to be compiled/installed 
        * Must be compiled using 'gcc-5.5.0'
    * If 'gcc-5.5.0' is installed into `/usr/local`, you will need to do one of the following prior to executing `repertory`:
        * Temporary solution:
            * `export LD_LIBRARY_PATH=/usr/local/lib64:$LD_LIBRARY_PATH`
            * `./reportory`
        * Permanent solution performed as `root`:
            * `echo "/usr/local/lib64">/etc/ld.so.conf.d/gcc-5.conf`
            * `ldconfig`
            * `./repertory`
* Debian 9 compilation support
    * `apt install git gcc g++ libcurl4-openssl-dev libssl-dev libboost-dev libcpprest-dev libboost-atomic1.62 libboost-system1.62 libboost-filesystem1.62 libboost-chrono1.62 libboost-date-time1.62 libboost-random1.62 libboost-thread1.62 libboost-regex1.62 cmake pkg-config libfuse-dev libfuse2 uuid-dev libcpprest-dev`
* Changed minimum gcc version to 5.5
* Changed minimum cmake version to 3.1
* Partial OS X support

## 1.0.0-alpha.2
* Fix directory create/rename on Windows
* Added initial json-rpc API for configuration
* Added CLI options for configuration
* Clean-up buffer files on start-up
* RocksDB for file meta (replaced SQLite)
* Various fixes

## 1.0.0-alpha.1
* Initial release
* Windows support
