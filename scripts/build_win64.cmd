@echo off
setlocal EnableDelayedExpansion

set ROOT=%~dp0%\..
pushd "%ROOT%"

set ROOT=%CD%
set PATH=%ROOT%\bin;%ROOT%\bin\curl\bin;%PATH%
set CMAKE=cmake
set VS_CMAKE_GENERATOR=Visual Studio 16 2019
set TARGET_MODE=%1
set /a ENABLE_PACKAGING=%2
set /a DISABLE_TESTING=%3
set /a DISABLE_PAUSE=%4
set /a APPVEYOR_BUILD=%5
set /a ENABLE_UPLOAD=%6
set BITBUCKET_AUTH=%7

set ERROR_EXIT=0
set OPENSSL_BIN="c:\OpenSSL-Win64\bin\openssl.exe"
if NOT EXIST %OPENSSL_BIN% (
  set OPENSSL_BIN="c:\Program Files\OpenSSL-Win64\bin\openssl.exe"
)

set PRIVATE_KEY="c:\src\cert\blockstorage_dev_private.pem"
set PUBLIC_KEY="%ROOT%\blockstorage_dev_public.pem"

if "%TARGET_MODE%" NEQ "Debug" (
  if "%TARGET_MODE%" NEQ "Release" (
    call :ERROR "[Debug|Release] not specified"
  )
)

if "%APPVEYOR_BUILD%" == "1" (
  set CMAKE_OPTS=-DOPENSSL_ROOT_DIR=C:\OpenSSL-v111-Win64
)

mkdir build >NUL 2>&1
mkdir build\%TARGET_MODE% >NUL 2>&1
pushd build\%TARGET_MODE% >NUL 2>&1
  for /F "tokens=*" %%f in ('git rev-parse --short HEAD') do (set GIT_REV=%%f)
  
  echo Building [%TARGET_MODE%]
  ((%CMAKE% ../.. %CMAKE_OPTS% -A x64 -DCMAKE_BUILD_TYPE=%TARGET_MODE% -DCMAKE_GENERATOR="%VS_CMAKE_GENERATOR%" -DCMAKE_CONFIGURATION_TYPES="%TARGET_MODE%") && (%CMAKE% --build . --config %TARGET_MODE%)) || (
    popd
    call :ERROR "Compile Error"
  )

  for /f "tokens=*" %%i in ('grep -m1 -a REPERTORY_VERSION repertory.vcxproj ^| sed -e "s/.*REPERTORY_VERSION=\"//g" -e "s/\".*//g"') do (
    set APP_VER=%%i
  )
  
  for /f "tokens=2 delims=-" %%a in ("%APP_VER%") do (
    set APP_RELEASE_TYPE=%%a
  )
  if "%APP_RELEASE_TYPE%"=="alpha" set APP_RELEASE_TYPE=Alpha
  if "%APP_RELEASE_TYPE%"=="beta" set APP_RELEASE_TYPE=Beta
  if "%APP_RELEASE_TYPE%"=="rc" set APP_RELEASE_TYPE=RC
  if "%APP_RELEASE_TYPE%"=="release" set APP_RELEASE_TYPE=Release   

  echo Repertory Version: %APP_VER%
  set TARGET_MODE_LOWER=%TARGET_MODE%
  call :LOWERCASE TARGET_MODE_LOWER

  set OUT_NAME=repertory_%APP_VER%_%GIT_REV%_!TARGET_MODE_LOWER!_win64.zip
  set OUT_FILE=..\..\%OUT_NAME%

  pushd %TARGET_MODE%
    if "%DISABLE_TESTING%" NEQ "1" (
      echo Testing [%TARGET_MODE%]
      del /q unittests.err 1>NUL 2>&1
      start "Unittests" /wait /min cmd /v:on /c "unittests.exe>unittests.log 2>&1 || echo ^!errorlevel^!>unittests.err"
      if EXIST unittests.err (
        call :ERROR "Unittests Failed"
      )
    )

    if "%APPVEYOR_BUILD%" NEQ "1" (
      echo Signing Application
      del /q repertory.exe.sha256 1>NUL 2>&1
      del /q repertory.exe.sig 1>NUL 2>&1
      ((certutil -hashfile repertory.exe SHA256 | sed -e "1d" -e "$d" -e "s/\ //g") > repertory.exe.sha256) || (call :ERROR "Create repertory sha-256 failed")
      (%OPENSSL_BIN% dgst -sha256 -sign "%PRIVATE_KEY%" -out repertory.exe.sig repertory.exe) || (call :ERROR "Create repertory signature failed")
      (%OPENSSL_BIN% dgst -sha256 -verify "%PUBLIC_KEY%" -signature repertory.exe.sig repertory.exe) || (call :ERROR "Verify repertory signature failed")
    )

    if "%ENABLE_PACKAGING%"=="1" (
      echo Creating Archive [%OUT_FILE%]
      if "!TARGET_MODE_LOWER!" == "debug" (
        (7za u "%OUT_FILE%" repertory.exe repertory.exe.sha256 repertory.exe.sig cpprest142_2_10d.dll winfsp-x64.dll) || (call :ERROR "Create repertory zip failed")
      ) else (
        (7za u "%OUT_FILE%" repertory.exe repertory.exe.sha256 repertory.exe.sig cpprest142_2_10.dll winfsp-x64.dll) || (call :ERROR "Create repertory zip failed")
      )

      echo Signing Archive [%OUT_FILE%]
      (certutil -hashfile "%OUT_FILE%" SHA256 | sed -e "1d" -e "$d" -e "s/\ //g" > "%OUT_FILE%.sha256") || (call :ERROR "Create zip sha-256 failed")
      (%OPENSSL_BIN% dgst -sha256 -sign "%PRIVATE_KEY%" -out "%OUT_FILE%.sig" "%OUT_FILE%") || (call :ERROR "Create zip signature failed")
      (%OPENSSL_BIN% dgst -sha256 -verify "%PUBLIC_KEY%" -signature "%OUT_FILE%.sig" "%OUT_FILE%") || (call :ERROR "Verify zip signature failed")
      (b64.exe -e "%OUT_FILE%.sig" "%OUT_FILE%.sig.b64") || (call :ERROR "Create zip base64 signature failed")
      for /f "delims=" %%i in ('type %OUT_FILE%.sig.b64') do set APP_SIG=!APP_SIG!%%i
      for /f "delims=" %%i in ('type %OUT_FILE%.sha256') do set APP_SHA256=!APP_SHA256!%%i

      if "%ENABLE_UPLOAD%"=="1" (
        if "!TARGET_MODE_LOWER!" == "release" (
          del /q upload_response.json 1>NUL 2>&1

          echo Uploading %OUT_NAME% to Pixeldrain
          (curl --fail -F name="%OUT_NAME%" -F anonymous=true -F file="@%OUT_FILE%" https://pixeldrain.com/api/file > upload_response.json) || (
            call :PIXEL_RESPONSE 0
          )
          call :PIXEL_RESPONSE 1
          set PIXEL_LOCATION=https://pixeldrain.com/api/file/!PIXEL_ID!

          echo Updating Releases
          del /q ..\..\releases.json 1>NUL 2>&1

          call :UPLOAD_TO_BITBUCKET "%OUT_FILE%"
          call :UPLOAD_TO_BITBUCKET "%OUT_FILE%.sha256"
          call :UPLOAD_TO_BITBUCKET "%OUT_FILE%.sig"
          set BITBUCKET_LOCATION=https://bitbucket.org/blockstorage/repertory/downloads/%OUT_NAME%

          (jq-win64.exe ".Versions.%APP_RELEASE_TYPE%.win32|=(.+ ["""%APP_VER%"""]|unique)" ..\..\..\releases.json>..\..\releases_temp.json && move /Y ..\..\releases_temp.json ..\..\releases.json) || (call :ERROR "Update releases.json Versions failed")
          (jq-win64.exe ".Locations.win32."""%APP_VER%""".sig="""!APP_SIG!"""" ..\..\releases.json>..\..\releases_temp.json && move /Y ..\..\releases_temp.json ..\..\releases.json) || (call :ERROR "Update releases.json sig failed")
          (jq-win64.exe ".Locations.win32."""%APP_VER%""".sha256="""!APP_SHA256!"""" ..\..\releases.json>..\..\releases_temp.json && move /Y ..\..\releases_temp.json ..\..\releases.json) || (call :ERROR "Update releases.json sha256 failed")
          (jq-win64.exe ".Locations.win32."""%APP_VER%""".urls=["""!PIXEL_LOCATION!""","""!BITBUCKET_LOCATION!"""]" ..\..\releases.json>..\..\releases_temp.json && move /Y ..\..\releases_temp.json ..\..\releases.json) || (call :ERROR "Update releases.json URL failed")
        )
      )
    ) 
  popd
popd

goto :END

:PIXEL_RESPONSE
  set PIXEL_RESPONSE=
  if %1==1 (
    for /f "delims=" %%i in ('jq-win64.exe .success upload_response.json') do (
      if "%%i" == "false" (
        for /f "delims=" %%i in ('jq-win64.exe .message upload_response.json') do (
          set PIXEL_RESPONSE=!PIXEL_RESPONSE!%%i
        )
        call :ERROR "Upload to pixeldrain failed: !PIXEL_RESPONSE!"
      ) else (
        for /f "delims=" %%i in ('jq-win64.exe .id upload_response.json') do (
          set PIXEL_ID=%%i
          call :NO_QUOTES PIXEL_ID
        )
      )
    )
  ) else (
    for /f "delims=" %%i in ('type upload_response.json') do (
      set PIXEL_RESPONSE=!PIXEL_RESPONSE!%%i
    )
    call :ERROR "Upload to pixeldrain failed: !PIXEL_RESPONSE!"
  )
goto :EOF

:UPLOAD_TO_BITBUCKET
  set SOURCE_FILE=%1
  call :NO_QUOTES SOURCE_FILE
  call :NO_QUOTES BITBUCKET_AUTH
  echo "Uploading !SOURCE_FILE! to Bitbucket"
  (curl --fail -u "!BITBUCKET_AUTH!" -X POST https://api.bitbucket.org/2.0/repositories/blockstorage/repertory/downloads -F files="@!SOURCE_FILE!" > upload_response.json) || (call :ERROR "Upload to Bitbucket failed: %SOURCE_FILE%")
goto :EOF

:ERROR
  echo %1
  set ERROR_EXIT=1
  if "%DISABLE_PAUSE%" NEQ "1" (
    pause
  )
goto :END

:NO_QUOTES
  set %~1=!%~1:"=!
goto :EOF

:LOWERCASE
  set %~1=!%~1:A=a!
  set %~1=!%~1:B=b!
  set %~1=!%~1:C=c!
  set %~1=!%~1:D=d!
  set %~1=!%~1:E=e!
  set %~1=!%~1:F=f!
  set %~1=!%~1:G=g!
  set %~1=!%~1:H=h!
  set %~1=!%~1:I=i!
  set %~1=!%~1:J=j!
  set %~1=!%~1:K=k!
  set %~1=!%~1:L=l!
  set %~1=!%~1:M=m!
  set %~1=!%~1:N=n!
  set %~1=!%~1:O=o!
  set %~1=!%~1:P=p!
  set %~1=!%~1:Q=q!
  set %~1=!%~1:R=r!
  set %~1=!%~1:S=s!
  set %~1=!%~1:T=t!
  set %~1=!%~1:U=u!
  set %~1=!%~1:V=v!
  set %~1=!%~1:W=w!
  set %~1=!%~1:X=x!
  set %~1=!%~1:Y=y!
  set %~1=!%~1:Z=z!
goto :EOF

:END
popd
if "!ERROR_EXIT!" NEQ "0" (
  exit !ERROR_EXIT!
)
