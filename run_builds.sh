#!/bin/bash

RESULT=0
CURRENT_DIR=$(pwd)
TAG_BUILDS_ROOT=${CURRENT_DIR}/tag_builds
DISTRO=$(./detect_linux.sh)

set_failure() {
  NAME=$1
  if [ -f "${TAG_BUILDS_ROOT}/${NAME}.error" ]; then
    RESULT=1
  fi
}

exit_on_failure() {
  if [ "${RESULT}" != "0" ]; then
    echo "One or more builds failed"
    exit ${RESULT}
  fi
}

if [ "$DISTRO" != "solus" ]; then
  echo "Builds must be run on 'Solus Linux'"
  exit 1
elif [ "$UID" != 0 ]; then
  echo "Builds must be run as root"
  exit 1
else
  TAG=$1
  ENABLE_UPLOAD=$2
  BITBUCKET_AUTH=$3
  if [ -z "$TAG" ]; then
    echo "Branch/tag not supplied"
    exit 1
  else
    bash ./compile_tag.sh ${TAG} ${ENABLE_UPLOAD} ${BITBUCKET_AUTH}
    set_failure solus

    for FILE in $(pwd)/docker/*; do
      DISTRONAME=$(basename ${FILE})
      docker stop repertory_${DISTRONAME}
      docker rm repertory_${DISTRONAME}
      docker build -t repertory:${DISTRONAME} - < docker/${DISTRONAME} && \
        docker run -itd --device /dev/fuse --cap-add SYS_ADMIN --name repertory_${DISTRONAME} -v $(pwd):/mnt repertory:${DISTRONAME} && \
        docker exec repertory_${DISTRONAME} bash -c "cd /mnt && bash compile_tag.sh ${TAG} ${ENABLE_UPLOAD} ${BITBUCKET_AUTH}"
      docker stop repertory_${DISTRONAME}
      docker rm repertory_${DISTRONAME}
      set_failure ${DISTRONAME}
    done

    exit_on_failure
  fi
fi