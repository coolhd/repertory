#ifndef REPERTORY_UTILS_TEST_H
#define REPERTORY_UTILS_TEST_H

#include <testcommon.h>
#include <utils/utils.h>

using namespace Repertory;

#ifdef _WIN32
TEST(Utils, ConvertApiTime) {
  LARGE_INTEGER li{};
  li.QuadPart = Utils::ConvertApiDate("2019-02-21T03:24:37.653091916-06:00");

  SYSTEMTIME st{};
  FileTimeToSystemTime((FILETIME*)&li, &st);

  SYSTEMTIME lt{};
  SystemTimeToTzSpecificLocalTime(nullptr, &st, &lt);

  EXPECT_EQ(2019, lt.wYear);
  EXPECT_EQ(2, lt.wMonth);
  EXPECT_EQ(21, lt.wDay);

  EXPECT_EQ(3, lt.wHour);
  EXPECT_EQ(24, lt.wMinute);
  EXPECT_EQ(37, lt.wSecond);
  EXPECT_EQ(653, lt.wMilliseconds);
}
#endif

TEST(Utils, CreateUUIDString) {
  const auto uuid1 = Utils::CreateUUIDString();
  const auto uuid2 = Utils::CreateUUIDString();
  ASSERT_EQ(36, uuid1.size());
  ASSERT_EQ(36, uuid2.size());
  ASSERT_STRNE(&uuid1[0], &uuid2[0]);
}

#endif //REPERTORY_UTILS_TEST_H
