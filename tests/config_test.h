#ifndef REPERTORY_CONFIG_TEST_H
#define REPERTORY_CONFIG_TEST_H

#include <testcommon.h>
#include <config.h>
#include <utils/fileutils.h>
#include <utils/stringutils.h>

using namespace Repertory;

const auto DEFAULT_SIA_RELEASE_CONFIG = "{\n"
                                        "  \"ApiAuth\": \"\",\n"
                                        "  \"ApiPort\": 11101,\n"
                                        "  \"ApiUser\": \"repertory\",\n"
                                        "  \"ChunkDownloaderTimeoutSeconds\": 30,\n"
                                        "  \"ChunkSize\": 4096,\n"
                                        "  \"EnableChunkDownloaderTimeout\": true,\n"
                                        "  \"EnableDriveEvents\": false,\n"
                                        "  \"EnableMaxCacheSize\": true,\n"
                                        "  \"EventLevel\": \"Normal\",\n"
                                        "  \"EvictionDelayMinutes\": 30,\n"
                                        "  \"HighFreqIntervalSeconds\": 30,\n"
                                        "  \"HostConfig\": {\n"
                                        "    \"AgentString\": \"Sia-Agent\",\n"
                                        "    \"ApiPassword\": \"\",\n"
                                        "    \"ApiPort\": 9980,\n"
                                        "    \"HostNameOrIp\": \"localhost\",\n"
                                        "    \"TimeoutMs\": 60000\n"
                                        "  },\n"
                                        "  \"LowFreqIntervalSeconds\": 900,\n"
                                        "  \"MaxCacheSizeBytes\": 21474836480,\n"
                                        "  \"MinimumRedundancy\": 2.5,\n"
                                        "  \"OnlineCheckRetrySeconds\": 60,\n"
                                        "  \"OrphanedFileRetentionDays\": 15,\n"
                                        "  \"PreferredDownloadType\": \"Fallback\",\n"
                                        "  \"ReadAheadCount\": 4,\n"
                                        "  \"RingBufferFileSize\": 512,\n"
                                        "  \"StorageByteMonth\": \"0\",\n"
                                        "  \"Version\": " + std::to_string(REPERTORY_CONFIG_VERSION) + "\n"
                                        "}";

const auto DEFAULT_SIA_DEBUG_CONFIG = "{\n"
                                      "  \"ApiAuth\": \"\",\n"
                                      "  \"ApiPort\": 11101,\n"
                                      "  \"ApiUser\": \"repertory\",\n"
                                      "  \"ChunkDownloaderTimeoutSeconds\": 30,\n"
                                      "  \"ChunkSize\": 4096,\n"
                                      "  \"EnableChunkDownloaderTimeout\": true,\n"
                                      "  \"EnableDriveEvents\": true,\n"
                                      "  \"EnableMaxCacheSize\": true,\n"
                                      "  \"EventLevel\": \"Debug\",\n"
                                      "  \"EvictionDelayMinutes\": 30,\n"
                                      "  \"HighFreqIntervalSeconds\": 30,\n"
                                      "  \"HostConfig\": {\n"
                                      "    \"AgentString\": \"Sia-Agent\",\n"
                                      "    \"ApiPassword\": \"\",\n"
                                      "    \"ApiPort\": 9980,\n"
                                      "    \"HostNameOrIp\": \"localhost\",\n"
                                      "    \"TimeoutMs\": 60000\n"
                                      "  },\n"
                                      "  \"LowFreqIntervalSeconds\": 900,\n"
                                      "  \"MaxCacheSizeBytes\": 21474836480,\n"
                                      "  \"MinimumRedundancy\": 2.5,\n"
                                      "  \"OnlineCheckRetrySeconds\": 60,\n"
                                      "  \"OrphanedFileRetentionDays\": 15,\n"
                                      "  \"PreferredDownloadType\": \"Fallback\",\n"
                                      "  \"ReadAheadCount\": 4,\n"
                                      "  \"RingBufferFileSize\": 512,\n"
                                      "  \"StorageByteMonth\": \"0\",\n"
                                      "  \"Version\": " + std::to_string(REPERTORY_CONFIG_VERSION) + "\n"
                                      "}";


const auto DEFAULT_SIAPRIME_RELEASE_CONFIG = "{\n"
                                             "  \"ApiAuth\": \"\",\n"
                                             "  \"ApiPort\": 11102,\n"
                                             "  \"ApiUser\": \"repertory\",\n"
                                             "  \"ChunkDownloaderTimeoutSeconds\": 30,\n"
                                             "  \"ChunkSize\": 4096,\n"
                                             "  \"EnableChunkDownloaderTimeout\": true,\n"
                                             "  \"EnableDriveEvents\": false,\n"
                                             "  \"EnableMaxCacheSize\": true,\n"
                                             "  \"EventLevel\": \"Normal\",\n"
                                             "  \"EvictionDelayMinutes\": 30,\n"
                                             "  \"HighFreqIntervalSeconds\": 30,\n"
                                             "  \"HostConfig\": {\n"
                                             "    \"AgentString\": \"SiaPrime-Agent\",\n"
                                             "    \"ApiPassword\": \"\",\n"
                                             "    \"ApiPort\": 4280,\n"
                                             "    \"HostNameOrIp\": \"localhost\",\n"
                                             "    \"TimeoutMs\": 60000\n"
                                             "  },\n"
                                             "  \"LowFreqIntervalSeconds\": 900,\n"
                                             "  \"MaxCacheSizeBytes\": 21474836480,\n"
                                             "  \"MinimumRedundancy\": 2.5,\n"
                                             "  \"OnlineCheckRetrySeconds\": 60,\n"
                                             "  \"OrphanedFileRetentionDays\": 15,\n"
                                             "  \"PreferredDownloadType\": \"Fallback\",\n"
                                             "  \"ReadAheadCount\": 4,\n"
                                             "  \"RingBufferFileSize\": 512,\n"
                                             "  \"StorageByteMonth\": \"0\",\n"
                                             "  \"Version\": " + std::to_string(REPERTORY_CONFIG_VERSION) + "\n"
                                             "}";

const auto DEFAULT_SIAPRIME_DEBUG_CONFIG = "{\n"
                                           "  \"ApiAuth\": \"\",\n"
                                           "  \"ApiPort\": 11102,\n"
                                           "  \"ApiUser\": \"repertory\",\n"
                                           "  \"ChunkDownloaderTimeoutSeconds\": 30,\n"
                                           "  \"ChunkSize\": 4096,\n"
                                           "  \"EnableChunkDownloaderTimeout\": true,\n"
                                           "  \"EnableDriveEvents\": true,\n"
                                           "  \"EnableMaxCacheSize\": true,\n"
                                           "  \"EventLevel\": \"Debug\",\n"
                                           "  \"EvictionDelayMinutes\": 30,\n"
                                           "  \"HighFreqIntervalSeconds\": 30,\n"
                                           "  \"HostConfig\": {\n"
                                           "    \"AgentString\": \"SiaPrime-Agent\",\n"
                                           "    \"ApiPassword\": \"\",\n"
                                           "    \"ApiPort\": 4280,\n"
                                           "    \"HostNameOrIp\": \"localhost\",\n"
                                           "    \"TimeoutMs\": 60000\n"
                                           "  },\n"
                                           "  \"LowFreqIntervalSeconds\": 900,\n"
                                           "  \"MaxCacheSizeBytes\": 21474836480,\n"
                                           "  \"MinimumRedundancy\": 2.5,\n"
                                           "  \"OnlineCheckRetrySeconds\": 60,\n"
                                           "  \"OrphanedFileRetentionDays\": 15,\n"
                                           "  \"PreferredDownloadType\": \"Fallback\",\n"
                                           "  \"ReadAheadCount\": 4,\n"
                                           "  \"RingBufferFileSize\": 512,\n"
                                           "  \"StorageByteMonth\": \"0\",\n"
                                           "  \"Version\": " + std::to_string(REPERTORY_CONFIG_VERSION) + "\n"
                                           "}";
#ifdef _DEBUG
#define DEFAULT_SIA_CONFIG DEFAULT_SIA_DEBUG_CONFIG
#define DEFAULT_SIAPRIME_CONFIG DEFAULT_SIAPRIME_DEBUG_CONFIG
#else
#define DEFAULT_SIA_CONFIG DEFAULT_SIA_RELEASE_CONFIG
#define DEFAULT_SIAPRIME_CONFIG DEFAULT_SIAPRIME_RELEASE_CONFIG
#endif

TEST(Config, SiaDefaultSettings) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);

  for (int i = 0; i < 2; i++) {
    CConfig config(ProviderType::Sia, "./data");
    config.SetAPIAuth("");
    config.SetValueByName("HostConfig.ApiPassword", "");
    json data;
    Utils::File::ReadJsonFile(configFile, data);
    EXPECT_STREQ(DEFAULT_SIA_CONFIG.c_str(), data.dump(2).c_str());
    EXPECT_TRUE(Utils::File::IsDirectory("./data/sia/cache"));
    EXPECT_TRUE(Utils::File::IsDirectory("./data/sia/logs"));
  }
}

TEST(Config, SiaPrimeDefaultSettings) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/siaprime", "config.json"));
  Utils::File::DeleteAsFile(configFile);

  for (int i = 0; i < 2; i++) {
    CConfig config(ProviderType::SiaPrime, "./data");
    config.SetAPIAuth("");
    config.SetValueByName("HostConfig.ApiPassword", "");
    json data;
    Utils::File::ReadJsonFile(configFile, data);
    EXPECT_STREQ(DEFAULT_SIAPRIME_CONFIG.c_str(), data.dump(2).c_str());
    EXPECT_TRUE(Utils::File::IsDirectory("./data/siaprime/cache"));
    EXPECT_TRUE(Utils::File::IsDirectory("./data/siaprime/logs"));
  }
}

TEST(Config, DefaultAPIAuth) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::string originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetAPIAuth();
    EXPECT_EQ(48, originalValue.size());
  }
}

TEST(Config, GetAndSetAPIAuth) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::string originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetAPIAuth();
    config.SetAPIAuth(originalValue.substr(0, 20));
    EXPECT_EQ(originalValue.substr(0, 20), config.GetAPIAuth());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue.substr(0, 20), config.GetAPIAuth());
  }
}

TEST(Config, GetAndSetAPIPort) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint16_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetAPIPort();
    config.SetAPIPort(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetAPIPort());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetAPIPort());
  }
}

TEST(Config, GetAndSetAPIAUser) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::string originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetAPIUser();
    config.SetAPIUser(originalValue.substr(0, 2));
    EXPECT_EQ(originalValue.substr(0, 2), config.GetAPIUser());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue.substr(0, 2), config.GetAPIUser());
  }
}

TEST(Config, GetAndSetChunkDownloaderTimeoutSeconds) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint8_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetChunkDownloaderTimeoutSeconds();
    config.SetChunkDownloaderTimeoutSeconds(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetChunkDownloaderTimeoutSeconds());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetChunkDownloaderTimeoutSeconds());
  }
}

TEST(Config, GetAndSetChunkSize) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint64_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetChunkSize();
    config.SetChunkSize(static_cast<std::uint16_t>(originalValue + 8u));
    EXPECT_EQ(originalValue + 8, config.GetChunkSize());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 8, config.GetChunkSize());
  }
}

TEST(Config, GetAndSetEnableChunkDownloaderTimeout) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  bool originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetEnableChunkDownloaderTimeout();
    config.SetEnableChunkDownloaderTimeout(not originalValue);
    EXPECT_EQ(not originalValue, config.GetEnableChunkDownloaderTimeout());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(not originalValue, config.GetEnableChunkDownloaderTimeout());
  }
}

TEST(Config, GetAndSetEnableDriveEvents) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  bool originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetEnableDriveEvents();
    config.SetEnableDriveEvents(not originalValue);
    EXPECT_EQ(not originalValue, config.GetEnableDriveEvents());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(not originalValue, config.GetEnableDriveEvents());
  }
}

TEST(Config, GetAndSetEnableMaxCacheSize) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  bool originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetEnableMaxCacheSize();
    config.SetEnableMaxCacheSize(not originalValue);
    EXPECT_EQ(not originalValue, config.GetEnableMaxCacheSize());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(not originalValue, config.GetEnableMaxCacheSize());
  }
}

TEST(Config, GetAndSetEventLevel) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  {
    CConfig config(ProviderType::Sia, "./data");
    config.SetEventLevel(EventLevel::Debug);
    EXPECT_EQ(EventLevel::Debug, config.GetEventLevel());
    config.SetEventLevel(EventLevel::Warn);
    EXPECT_EQ(EventLevel::Warn, config.GetEventLevel());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(EventLevel::Warn, config.GetEventLevel());
  }
}

TEST(Config, GetAndSetEvictionDelayMinutes) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint32_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetEvictionDelayMinutes();
    config.SetEvictionDelayMinutes(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetEvictionDelayMinutes());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetEvictionDelayMinutes());
  }
}

TEST(Config, GetAndSetHighFreqIntervalSeconds) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint8_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetHighFreqIntervalSeconds();
    config.SetHighFreqIntervalSeconds(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetHighFreqIntervalSeconds());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetHighFreqIntervalSeconds());
  }
}

TEST(Config, GetAndSetLowFreqIntervalSeconds) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint32_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetLowFreqIntervalSeconds();
    config.SetLowFreqIntervalSeconds(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetLowFreqIntervalSeconds());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetLowFreqIntervalSeconds());
  }
}

TEST(Config, GetAndSetMaxCacheSizeBytes) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint64_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetMaxCacheSizeBytes();
    config.SetMaxCacheSizeBytes(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetMaxCacheSizeBytes());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetMaxCacheSizeBytes());
  }
}

TEST(Config, GetAndSetMinimumRedundancy) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  double originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetMinimumRedundancy();
    config.SetMinimumRedundancy(originalValue + 0.1);
    EXPECT_EQ(originalValue + 0.1, config.GetMinimumRedundancy());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 0.1, config.GetMinimumRedundancy());
  }
}

TEST(Config, GetAndSetOnlineCheckRetrySeconds) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint16_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetOnlineCheckRetrySeconds();
    config.SetOnlineCheckRetrySeconds(originalValue + 1);
    EXPECT_EQ(originalValue + 1, config.GetOnlineCheckRetrySeconds());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 1, config.GetOnlineCheckRetrySeconds());
  }
}

TEST(Config, OnlineCheckRetrySecondsMinimumValue) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  {
    CConfig config(ProviderType::Sia, "./data");
    config.SetOnlineCheckRetrySeconds(14);
    EXPECT_EQ(15, config.GetOnlineCheckRetrySeconds());
  }
}

TEST(Config, GetAndSetOrphanedFileRetentionDays) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint16_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetOrphanedFileRetentionDays();
    config.SetOrphanedFileRetentionDays(originalValue + 1);
    EXPECT_EQ(originalValue + 1, config.GetOrphanedFileRetentionDays());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 1, config.GetOrphanedFileRetentionDays());
  }
}

TEST(Config, OrphanedFileRetentionDaysMinimumValue) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  {
    CConfig config(ProviderType::Sia, "./data");
    config.SetOrphanedFileRetentionDays(0);
    EXPECT_EQ(1, config.GetOrphanedFileRetentionDays());
  }
}

TEST(Config, OrphanedFileRetentionDaysMaximumValue) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  {
    CConfig config(ProviderType::Sia, "./data");
    config.SetOrphanedFileRetentionDays(32);
    EXPECT_EQ(31, config.GetOrphanedFileRetentionDays());
  }
}

TEST(Config, GetAndSetReadAheadCount) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint8_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetReadAheadCount();
    config.SetReadAheadCount(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetReadAheadCount());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetReadAheadCount());
  }
}

TEST(Config, GetAndStorageByteMonth) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  ApiCurrency originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetStorageByteMonth();
    config.SetStorageByteMonth(originalValue + 5);
    EXPECT_EQ(originalValue + 5, config.GetStorageByteMonth());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5, config.GetStorageByteMonth());
  }
}

TEST(Config, GetCacheDirectory) {
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_STREQ(Utils::Path::Absolute("./data/sia/cache").c_str(), config.GetCacheDirectory().c_str());
  }

  {
    CConfig config(ProviderType::SiaPrime, "./data");
    EXPECT_STREQ(Utils::Path::Absolute("./data/siaprime/cache").c_str(), config.GetCacheDirectory().c_str());
  }
}

TEST(Config, GetConfigFilePath) {
  {
    const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
    Utils::File::DeleteAsFile(configFile);
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_STREQ(configFile.c_str(), config.GetConfigFilePath().c_str());
  }

  {
    const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/siaprime", "config.json"));
    Utils::File::DeleteAsFile(configFile);
    CConfig config(ProviderType::SiaPrime, "./data");
    EXPECT_STREQ(configFile.c_str(), config.GetConfigFilePath().c_str());
  }
}

TEST(Config, GetDataDirectory) {
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_STREQ(Utils::Path::Absolute("./data/sia").c_str(), config.GetDataDirectory().c_str());
  }

  {
    CConfig config(ProviderType::SiaPrime, "./data");
    EXPECT_STREQ(Utils::Path::Absolute("./data/siaprime").c_str(), config.GetDataDirectory().c_str());
  }
}

TEST(Config, GetLogDirectory) {
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_STREQ(Utils::Path::Absolute("./data/sia/logs").c_str(), config.GetLogDirectory().c_str());
  }

  {
    CConfig config(ProviderType::SiaPrime, "./data");
    EXPECT_STREQ(Utils::Path::Absolute("./data/siaprime/logs").c_str(), config.GetLogDirectory().c_str());
  }
}

TEST(Config, GetAndSetRingBufferFileSize) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  std::uint16_t originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetRingBufferFileSize();
    config.SetRingBufferFileSize(originalValue + 5u);
    EXPECT_EQ(originalValue + 5u, config.GetRingBufferFileSize());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(originalValue + 5u, config.GetRingBufferFileSize());
  }
}

TEST(Config, RingBufferFileSizeMinimum) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  {
    CConfig config(ProviderType::Sia, "./data");
    config.SetRingBufferFileSize(63u);
    EXPECT_EQ(64u, config.GetRingBufferFileSize());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(64u, config.GetRingBufferFileSize());
  }
}

TEST(Config, RingBufferFileSizeMaximum) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  {
    CConfig config(ProviderType::Sia, "./data");
    config.SetRingBufferFileSize(1025u);
    EXPECT_EQ(1024u, config.GetRingBufferFileSize());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(1024u, config.GetRingBufferFileSize());
  }
}

TEST(Config, GetAndSetPreferredDownloadType) {
  const auto configFile = Utils::Path::Absolute(Utils::Path::Combine("./data/sia", "config.json"));
  Utils::File::DeleteAsFile(configFile);
  DownloadType originalValue;
  {
    CConfig config(ProviderType::Sia, "./data");
    originalValue = config.GetPreferredDownloadType();
    config.SetPreferredDownloadType(DownloadType::RingBuffer);
    EXPECT_NE(originalValue, config.GetPreferredDownloadType());
  }
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_NE( originalValue, config.GetPreferredDownloadType());
  }
}

TEST(Config, GetDefaultAgentName) {
  EXPECT_STREQ("Sia-Agent", CConfig::GetDefaultAgentName(ProviderType::Sia).c_str());
  EXPECT_STREQ("SiaPrime-Agent", CConfig::GetDefaultAgentName(ProviderType::SiaPrime).c_str());
}

TEST(Config, GetDefaultApiPort) {
  EXPECT_EQ(9980u, CConfig::GetDefaultApiPort(ProviderType::Sia));
  EXPECT_EQ(4280u, CConfig::GetDefaultApiPort(ProviderType::SiaPrime));
}

TEST(Config, GetDefaultDataDirectory) {
  const std::string dataDirs[] = {
    CConfig::GetDefaultDataDirectory(ProviderType::Sia),
    CConfig::GetDefaultDataDirectory(ProviderType::SiaPrime)
  };

#ifdef _WIN32
  const auto localAppData = Utils::EnvironmentVariable("localappdata");
#endif
#if __linux__ || IS_FREEBSD
  const auto localAppData = Utils::Path::Combine(Utils::EnvironmentVariable("HOME"), ".local");
#endif
#ifdef __APPLE__
  const auto localAppData = Utils::Path::Combine(Utils::EnvironmentVariable("HOME"), "Library/Application Support");
#endif
  auto expectedDir = Utils::Path::Combine(localAppData, "/repertory/sia");
  EXPECT_STREQ(&expectedDir[0], &dataDirs[0][0]);
  expectedDir = Utils::Path::Combine(localAppData, "/repertory/siaprime");
  EXPECT_STREQ(&expectedDir[0], &dataDirs[1][0]);
}

TEST(Config, GetDefaultRPCPort) {
  EXPECT_EQ(11101u, CConfig::GetDefaultRPCPort(ProviderType::Sia));
  EXPECT_EQ(11102u, CConfig::GetDefaultRPCPort(ProviderType::SiaPrime));
}

TEST(Config, GetProviderDisplayName) {
  EXPECT_STREQ("Sia", CConfig::GetProviderDisplayName(ProviderType::Sia).c_str());
  EXPECT_STREQ("SiaPrime", CConfig::GetProviderDisplayName(ProviderType::SiaPrime).c_str());
}

TEST(Config, GetProviderMinimumVersion) {
  EXPECT_STREQ(MIN_SIA_VERSION, CConfig::GetProviderMinimumVersion(ProviderType::Sia).c_str());
  EXPECT_STREQ(MIN_SP_VERSION, CConfig::GetProviderMinimumVersion(ProviderType::SiaPrime).c_str());
}

TEST(Config, GetProviderName) {
  EXPECT_STREQ("sia", CConfig::GetProviderName(ProviderType::Sia).c_str());
  EXPECT_STREQ("siaprime", CConfig::GetProviderName(ProviderType::SiaPrime).c_str());
}

TEST(Config, GetProviderPathName) {
  EXPECT_STREQ("siapath", CConfig::GetProviderPathName(ProviderType::Sia).c_str());
  EXPECT_STREQ("siapath", CConfig::GetProviderPathName(ProviderType::SiaPrime).c_str());
}

TEST(Config, GetVersion) {
  {
    CConfig config(ProviderType::Sia, "./data");
    EXPECT_EQ(REPERTORY_CONFIG_VERSION, config.GetVersion());
  }

  {
    CConfig config(ProviderType::SiaPrime, "./data");
    EXPECT_EQ(REPERTORY_CONFIG_VERSION, config.GetVersion());
  }
}

TEST(Config, VersionUpgrade) {
  ASSERT_TRUE(Utils::File::DeleteAsFile(Utils::Path::Absolute("./data/sia/config.json")));
  const auto OLD_SIA_RELEASE_CONFIG = "{"
                                      "  \"ChunkSize\": 128,"
                                      "  \"EnableChunkDownloaderTimeout\": false,"
                                      "  \"HostConfig\": {"
                                      "    \"TimeoutMs\": 10000"
                                      "  },"
                                      "  \"ReadAheadCount\": 3,"
                                      "  \"EvictionDelaySeconds\": 300"
                                      "}";
  Utils::File::WriteJsonToFile("./data/sia/config.json", json::parse(OLD_SIA_RELEASE_CONFIG));

  CConfig config(ProviderType::Sia, "./data");
  EXPECT_EQ(REPERTORY_CONFIG_VERSION, config.GetVersion());
  EXPECT_EQ(4096, config.GetChunkSize());
  EXPECT_EQ(4, config.GetReadAheadCount());
  EXPECT_EQ(true, config.GetEnableChunkDownloaderTimeout());
  EXPECT_EQ(60000, config.GetHostConfig().TimeoutMs);
  EXPECT_EQ(5, config.GetEvictionDelayMinutes());

  json fileData;
  ASSERT_TRUE(Utils::File::ReadJsonFile("./data/sia/config.json", fileData));
  EXPECT_EQ(REPERTORY_CONFIG_VERSION, fileData["Version"].get<std::uint64_t>());
  EXPECT_EQ(4096, fileData["ChunkSize"].get<std::uint64_t>());
  EXPECT_EQ(4, fileData["ReadAheadCount"].get<std::uint8_t >());
  EXPECT_EQ(true, fileData["EnableChunkDownloaderTimeout"].get<bool>());
  EXPECT_EQ(60000, fileData["HostConfig"]["TimeoutMs"].get<std::uint32_t>());
  EXPECT_EQ(5, fileData["EvictionDelayMinutes"].get<std::uint32_t>());
}
#endif //REPERTORY_CONFIG_TEST_H
