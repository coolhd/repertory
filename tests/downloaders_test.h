#ifndef REPERTORY_DOWNLOADERS_TEST_H
#define REPERTORY_DOWNLOADERS_TEST_H

#include <testcommon.h>
#include <config.h>
#include <events/consumers/consoleconsumer.h>
#include <download/directdownload.h>
#include <download/download.h>
#include <download/ringdownload.h>
#include <mocks/mockopenfiletable.h>

using namespace Repertory;

static const std::size_t READ_SIZE = 1ull*2048ull*1024ull;
static const std::uint8_t DOWNLOADER_COUNT = 3;

static NativeFilePtr GenerateRandomFileBytesAndOpen2(const std::string& path, const size_t& size) {
  NativeFilePtr nativeFile;
  if (CNativeFile::CreateOrOpen(path, nativeFile) == ApiFileError::Success) {
    EXPECT_TRUE(nativeFile->Truncate(0));

    size_t offset = 0;
    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 255);
    std::vector<char> buf;
    buf.resize(size);

    auto d = static_cast<char>(dist(rd));
    while (offset < size) {
      buf[offset++] = d++;
    }
    size_t bytesWritten;
    nativeFile->WriteBytes(&buf[0], buf.size(), 0, bytesWritten);
    nativeFile->Flush();
    std::uint64_t curSize;
    EXPECT_TRUE(Utils::File::GetSize(path, curSize));
    EXPECT_EQ(size, curSize);
  }

  return std::move(nativeFile);
}

static std::shared_ptr<IDownload> CreateDownload(const std::uint8_t& index, CConfig& config, FileSystemItem& fsi, ApiReader& apiReader, IOpenFileTable& openFileTable) {
  return (index == 0) ?
         std::dynamic_pointer_cast<IDownload>(std::make_shared<CRingDownload>(config, fsi, apiReader, 6ull * READ_SIZE)) :
         (index == 1) ?
         std::dynamic_pointer_cast<IDownload>(std::make_shared<CDirectDownload>(config, fsi, apiReader)) :
         std::dynamic_pointer_cast<IDownload>(std::make_shared<CDownload>(config, fsi, apiReader, 6ull * READ_SIZE, openFileTable));
}

static void RunFullFileTest(const std::size_t& maxReadSize, const std::string& sourceFilePath, const std::uint64_t& sourceFileSize) {
  CConsoleConsumer c;
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./ringreader_data"));
  CConfig config(ProviderType::Sia, "./ringreader_data");

  Utils::File::DeleteAsFile(sourceFilePath);

  auto sourceFile = GenerateRandomFileBytesAndOpen2(sourceFilePath, sourceFileSize);
  EXPECT_NE(sourceFile, nullptr);
  if (sourceFile) {
    FileSystemItem fsi{};
    fsi.ApiFilePath = "/Test.dat";
    fsi.Size = sourceFileSize;
    fsi.ApiParent = "/";
    fsi.Directory = false;
    fsi.ItemLock = std::make_shared<std::recursive_mutex>();
    fsi.LegacyChunks = false;

    ApiReader apiReader = [&](const std::string& path, const std::size_t& size, const std::uint64_t& offset, std::vector<char>& data, const bool& stopRequested) -> ApiFileError {
      data.resize(size);
      size_t bytesRead;
      auto ret = sourceFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
      if (ret != ApiFileError::Success) {
        std::cout << Utils::GetLastErrorCode() << std::endl;
      }
      EXPECT_EQ(ApiFileError::Success, ret);
      EXPECT_EQ(bytesRead, data.size());
      return ret;
    };

    EventSystem::Instance().Start();
    for (auto i = 0; i < DOWNLOADER_COUNT; i++) {
      CMockOpenFileTable openFileTable;
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      auto download = CreateDownload(i, config, fsi, apiReader, openFileTable);

      auto sizeRemain = sourceFileSize;
      auto resultCode = ApiFileError::Success;
      auto offset = 0ull;
      while ((resultCode == ApiFileError::Success) && (sizeRemain > 0)) {
        const auto readSize = std::min(maxReadSize, static_cast<std::size_t>(sizeRemain));
        std::vector<char> data;
        if ((resultCode = download->ReadBytes(readSize, offset, data)) == ApiFileError::Success) {
          EXPECT_EQ(readSize, data.size());
          if (data.size() == readSize) {
            std::size_t bytesRead = 0;
            std::vector<char> data2(data.size());
            resultCode = sourceFile->ReadBytes(&data2[0], data2.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
            EXPECT_EQ(ApiFileError::Success, resultCode);
            EXPECT_EQ(data.size(), bytesRead);
            const auto res = memcmp(&data[0], &data2[0], data.size());
            EXPECT_EQ(0, res);
          }

          sizeRemain -= readSize;
          offset += readSize;
        }
        EXPECT_EQ(ApiFileError::Success, resultCode);
      }
    }
    sourceFile->Close();
    Utils::File::DeleteAsFile(sourceFilePath);
  }
  EventSystem::Instance().Stop();
}

TEST(Downloaders, ReadFullFile) {
  const auto maxReadSize = READ_SIZE;
  const auto sourceFilePath = "./ringreader_source_full.dat";
  const auto sourceFileSize = 20ull * READ_SIZE;
  RunFullFileTest(maxReadSize, sourceFilePath, sourceFileSize);
}

TEST(Downloaders, ReadFullFileWithOverlappingChunks) {
  const auto maxReadSize = READ_SIZE + (READ_SIZE / 2);
  const auto sourceFilePath = "./ringreader_source_ovr.dat";
  const auto sourceFileSize = 20ull * READ_SIZE;
  RunFullFileTest(maxReadSize, sourceFilePath, sourceFileSize);
}

TEST(Downloaders, ReadFullFileWithPartialChunk) {
  const auto maxReadSize = READ_SIZE;
  const auto sourceFilePath = "./ringreader_source_pc.dat";
  const auto sourceFileSize = (20ull * READ_SIZE) + 252ull;
  RunFullFileTest(maxReadSize, sourceFilePath, sourceFileSize);
}

TEST(Downloaders, ReadFullFileWithPartialReads) {
  const auto maxReadSize = 32ull * 1024ull;
  const auto sourceFilePath = "./ringreader_source_pr.dat";
  const auto sourceFileSize = 20ull * READ_SIZE;
  RunFullFileTest(maxReadSize, sourceFilePath, sourceFileSize);
}

TEST(Downloaders, ReadFullFileWithPartialOverlappingReads) {
  const auto maxReadSize = (READ_SIZE / 2) + 20ull;
  const auto sourceFilePath = "./ringreader_source_povr.dat";
  const auto sourceFileSize = 20ull * READ_SIZE;
  RunFullFileTest(maxReadSize, sourceFilePath, sourceFileSize);
}

TEST(Downloaders, ReadPastFullBuffer) {
  CConsoleConsumer c;
  const auto maxReadSize = READ_SIZE;
  const auto sourceFilePath = "./ringreader_source_rpfb.dat";
  const auto sourceFileSize = 20ull * READ_SIZE; 
  
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./ringreader_data"));
  CConfig config(ProviderType::Sia, "./ringreader_data");

  Utils::File::DeleteAsFile(sourceFilePath);

  auto sourceFile = GenerateRandomFileBytesAndOpen2(sourceFilePath, sourceFileSize);
  EXPECT_NE(sourceFile, nullptr);
  if (sourceFile) {
    FileSystemItem fsi{};
    fsi.ApiFilePath = "/Test.dat";
    fsi.Size = sourceFileSize;
    fsi.ApiParent = "/";
    fsi.Directory = false;
    fsi.ItemLock = std::make_shared<std::recursive_mutex>();
    fsi.LegacyChunks = false;

    ApiReader apiReader = [&](const std::string& path, const std::size_t& size, const std::uint64_t& offset, std::vector<char>& data, const bool& stopRequested) -> ApiFileError {
      data.resize(size);
      size_t bytesRead;
      auto ret = sourceFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
      EXPECT_EQ(ApiFileError::Success, ret);
      EXPECT_EQ(bytesRead, data.size());
      return ret;
    };

    EventSystem::Instance().Start();
    for (auto i = 0; i < DOWNLOADER_COUNT; i++) {
      CMockOpenFileTable openFileTable;
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      auto download = CreateDownload(i, config, fsi, apiReader, openFileTable);
      
      auto sizeRemain = sourceFileSize;
      auto resultCode = ApiFileError::Success;
      auto offset = 0ull;
      while ((resultCode == ApiFileError::Success) && (sizeRemain > 0)) {
        const auto readSize = std::min(maxReadSize, static_cast<std::size_t>(sizeRemain));
        std::vector<char> data;
        if ((resultCode = download->ReadBytes(readSize, offset, data)) == ApiFileError::Success) {
          EXPECT_EQ(readSize, data.size());
          if (data.size() == readSize) {
            std::size_t bytesRead = 0;
            std::vector<char> data2(data.size());
            resultCode = sourceFile->ReadBytes(&data2[0], data2.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
            EXPECT_EQ(ApiFileError::Success, resultCode);
            EXPECT_EQ(data.size(), bytesRead);
            EXPECT_EQ(0, memcmp(&data[0], &data2[0], data.size()));
          }

          if (sizeRemain == sourceFileSize) {
            sizeRemain -= (readSize + (6ull * READ_SIZE));
            offset += (readSize + (6ull * READ_SIZE));
          } else {
            sizeRemain -= readSize;
            offset += readSize;
          }
        }

        EXPECT_EQ(ApiFileError::Success, resultCode);
      }
    }
    sourceFile->Close();
    Utils::File::DeleteAsFile(sourceFilePath);
  }
  EventSystem::Instance().Stop();
}

TEST(Downloaders, ReadWithSeekBehind) {
  CConsoleConsumer c;
  const auto maxReadSize = READ_SIZE;
  const auto sourceFilePath = "./ringreader_source_rsb.dat";
  const auto sourceFileSize = 20ull * READ_SIZE;

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./ringreader_data"));
  CConfig config(ProviderType::Sia, "./ringreader_data");

  Utils::File::DeleteAsFile(sourceFilePath);

  auto sourceFile = GenerateRandomFileBytesAndOpen2(sourceFilePath, sourceFileSize);
  EXPECT_NE(sourceFile, nullptr);
  if (sourceFile) {
    FileSystemItem fsi{};
    fsi.ApiFilePath = "/Test.dat";
    fsi.Size = sourceFileSize;
    fsi.ApiParent = "/";
    fsi.Directory = false;
    fsi.ItemLock = std::make_shared<std::recursive_mutex>();
    fsi.LegacyChunks = false;

    ApiReader apiReader = [&](const std::string& path, const std::size_t& size, const std::uint64_t& offset, std::vector<char>& data, const bool& stopRequested) -> ApiFileError {
      data.resize(size);
      size_t bytesRead;
      auto ret = sourceFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
      EXPECT_EQ(ApiFileError::Success, ret);
      EXPECT_EQ(bytesRead, data.size());
      return ret;
    };

    EventSystem::Instance().Start();

    for (auto i = 0; i < DOWNLOADER_COUNT; i++) {
      CMockOpenFileTable openFileTable;
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      auto download = CreateDownload(i, config, fsi, apiReader, openFileTable);

      auto sizeRemain = sourceFileSize;
      auto resultCode = ApiFileError::Success;
      auto offset = 0ull;
      auto readCount = 0;
      while ((resultCode == ApiFileError::Success) && (sizeRemain > 0)) {
        const auto readSize = std::min(maxReadSize, static_cast<std::size_t>(sizeRemain));
        std::vector<char> data;
        if ((resultCode = download->ReadBytes(readSize, offset, data)) == ApiFileError::Success) {
          EXPECT_EQ(readSize, data.size());
          if (data.size() == readSize) {
            std::size_t bytesRead = 0;
            std::vector<char> data2(data.size());
            resultCode = sourceFile->ReadBytes(&data2[0], data2.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
            EXPECT_EQ(ApiFileError::Success, resultCode);
            EXPECT_EQ(data.size(), bytesRead);
            EXPECT_EQ(0, memcmp(&data[0], &data2[0], data.size()));
          }

          if (++readCount == 3) {
            sizeRemain += (readSize * 2);
            offset -= (readSize * 2);
          } else {
            sizeRemain -= readSize;
            offset += readSize;
          }
        }
        EXPECT_EQ(ApiFileError::Success, resultCode);
      }
    }
    sourceFile->Close();
    Utils::File::DeleteAsFile(sourceFilePath);
  }
  EventSystem::Instance().Stop();
}

TEST(Downloaders, SeekBeginToEndToBegin) {
  CConsoleConsumer c;
  const auto maxReadSize = READ_SIZE;
  const auto sourceFilePath = "./ringreader_source_beb.dat";
  const auto sourceFileSize = 20ull * READ_SIZE;

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./ringreader_data"));
  CConfig config(ProviderType::Sia, "./ringreader_data");

  Utils::File::DeleteAsFile(sourceFilePath);

  auto sourceFile = GenerateRandomFileBytesAndOpen2(sourceFilePath, sourceFileSize);
  EXPECT_NE(sourceFile, nullptr);
  if (sourceFile) {
    FileSystemItem fsi{};
    fsi.ApiFilePath = "/Test.dat";
    fsi.Size = sourceFileSize;
    fsi.ApiParent = "/";
    fsi.Directory = false;
    fsi.ItemLock = std::make_shared<std::recursive_mutex>();
    fsi.LegacyChunks = false;

    ApiReader apiReader = [&](const std::string& path, const std::size_t& size, const std::uint64_t& offset, std::vector<char>& data, const bool& stopRequested) -> ApiFileError {
      data.resize(size);
      size_t bytesRead;
      auto ret = sourceFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
      EXPECT_EQ(ApiFileError::Success, ret);
      EXPECT_EQ(bytesRead, data.size());
      return ret;
    };

    EventSystem::Instance().Start();

    for (auto i = 0; i < DOWNLOADER_COUNT; i++) {
      CMockOpenFileTable openFileTable;
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      auto download = CreateDownload(i, config, fsi, apiReader, openFileTable);

      auto sizeRemain = sourceFileSize;
      auto resultCode = ApiFileError::Success;
      auto offset = 0ull;
      auto readCount = 0;
      while ((resultCode == ApiFileError::Success) && (readCount < 4)) {
        const auto readSize = std::min(maxReadSize, static_cast<std::size_t>(sizeRemain));
        std::vector<char> data;
        if ((resultCode = download->ReadBytes(readSize, offset, data)) == ApiFileError::Success) {
          EXPECT_EQ(readSize, data.size());
          if (data.size() == readSize) {
            std::size_t bytesRead = 0;
            std::vector<char> data2(data.size());
            resultCode = sourceFile->ReadBytes(&data2[0], data2.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
            EXPECT_EQ(ApiFileError::Success, resultCode);
            EXPECT_EQ(data.size(), bytesRead);
            EXPECT_EQ(0, memcmp(&data[0], &data2[0], data.size()));
          }

          ++readCount;
          if (readCount == 1) {
            sizeRemain = readSize;
            offset = sourceFileSize - readSize;
          } else if (readCount == 2) {
            std::this_thread::sleep_for(10ms);
            sizeRemain = sourceFileSize;
            offset = 0ull;
          } else {
            sizeRemain -= readSize;
            offset += readSize;
          }
        }
        EXPECT_EQ(ApiFileError::Success, resultCode);
      }
    }
    sourceFile->Close();
    Utils::File::DeleteAsFile(sourceFilePath);
  }
  EventSystem::Instance().Stop();
}
#endif //REPERTORY_DOWNLOADERS_TEST_H
