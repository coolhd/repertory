#ifndef REPERTORY_MOCKOPENFILETABLE_H
#define REPERTORY_MOCKOPENFILETABLE_H

#include <testcommon.h>
#include <download/downloadmanager.h>
#include <drives/iopenfiletable.h>

namespace Repertory {
class CMockOpenFileTable :
  public virtual IOpenFileTable {
  public:
    explicit CMockOpenFileTable(CDownloadManager* dm = nullptr, FileSystemItem* fsi = nullptr) :
    _dm(dm),
    _fsi(fsi) {
    }

  private:
    CDownloadManager* _dm;
    FileSystemItem* _fsi;

  public:
    MOCK_CONST_METHOD1(GetOpenCount, std::uint64_t(const std::string& apiFilePath));
    MOCK_CONST_METHOD0(CheckNoOpenFileHandles, bool());
    MOCK_METHOD1(EvictFile, bool(const std::string &apiFilePath));
    MOCK_METHOD2(GetOpenFile, bool(const std::string& apiFilePath, FileSystemItem*& fileSystemItem));
    MOCK_METHOD0(GetOpenFiles, std::unordered_map<std::string, std::size_t>());
    void PerformLockedOperation(LockedOperation operation) override {
      if (_fsi && _dm) {
        _fsi->SourceFilePath = _dm->GetSourceFilePath(_fsi->ApiFilePath);
      }
    }
};
}
#endif //REPERTORY_MOCKOPENFILETABLE_H
