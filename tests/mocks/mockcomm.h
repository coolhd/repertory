#ifndef REPERTORY_MOCKCOMM_H
#define REPERTORY_MOCKCOMM_H

#include <testcommon.h>
#include <comm/icomm.h>
#include <deque>

namespace Repertory {
class CMockComm :
  public virtual IComm {
  private:
    typedef struct {
      ApiError apiError;
      json result;
      json error;
      bool persist = false;
    } Data;
    std::unordered_map<std::string, std::deque<Data>> _returnLookup;
    
  public:
    void PushReturn(const std::string& op, const std::string& path, const json& result, const json& error, const ApiError& apiError, bool persist = false) {
      const auto lookupPath = op + path;
      if (_returnLookup.find(lookupPath) == _returnLookup.end()) {
        _returnLookup[lookupPath] = std::deque<Data>();
      };
      Data data = {apiError, result, error, persist};
      _returnLookup[lookupPath].emplace_back(data);
    }

    void RemoveReturn(const std::string& op, const std::string& path) {
      const auto lookupPath = op + path;
      _returnLookup.erase(lookupPath);
    }

  private:
    ApiError Process(const std::string& op, const std::string &path, json &data, json &error) {
      const auto lookupPath = op + path;
      if ((_returnLookup.find(lookupPath) == _returnLookup.end()) || _returnLookup[lookupPath].empty()) {
        EXPECT_STREQ("", ("unexpected path: " + lookupPath).c_str());
        return ApiError::CommError;
      }
      error = _returnLookup[lookupPath].front().error;
      data = _returnLookup[lookupPath].front().result;
      const auto ret = _returnLookup[lookupPath].front().apiError;
      if (not _returnLookup[lookupPath].front().persist) {
        _returnLookup[lookupPath].pop_front();
      }
      return ret;
    }

  public:
    ApiError Get(const std::string &path, json &data, json &error) override {
      return Process("get", path, data, error);
    }

    ApiError Get(const std::string &path, const HttpParameters& httpParameters, json &data, json &error) override {
      return Process("get_params", path, data, error);
    }

    ApiError GetRaw(const std::string& path, const HttpParameters& parameters, std::vector<char>& data, json& error, const bool& stopRequested) override {
      throw std::runtime_error("not implemented: get_raw");
      return ApiError::CommError;
    }

    ApiError Post(const std::string &path, json &data, json &error) override {
      return Process("post", path, data, error);
    }

    ApiError Post(const std::string &path, const HttpParameters& parameters, json &data, json &error) override {
      return Process("post_params", path, data, error);
    }

    ApiError PostFile(const std::string &path, const std::string &sourcePath, const HttpParameters& parameters, OSHandle handle, json &error, bool& cancel) override {
      return ApiError::GeneralError;
    }
};
}

#endif //REPERTORY_MOCKCOMM_H
