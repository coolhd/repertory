#ifndef REPERTORY_MOCKPROVIDER_H
#define REPERTORY_MOCKPROVIDER_H

#include <testcommon.h>
#include <providers/iprovider.h>

namespace Repertory {

class CMockProvider :
  public virtual IProvider {
  public:
    MOCK_METHOD2(CreateDirectory, ApiFileError(const std::string& apiFilePath, const ApiMetaMap& meta));
    MOCK_METHOD2(CreateDirectoryCloneSourceMeta, ApiFileError(const std::string& sourceApiPath, const std::string& apiFilePath));
    MOCK_METHOD2(CreateFile, ApiFileError(const std::string &apiFilePath, const ApiMetaMap &meta));
    MOCK_CONST_METHOD1(GetFileList, ApiError(ApiFileList& fileList));
    MOCK_CONST_METHOD1(GetDirectoryItemCount, std::uint64_t(const std::string& apiFilePath));
    MOCK_CONST_METHOD2(GetDirectoryItems, ApiFileError(const std::string& apiFilePath, DirectoryItemList& directoryItemList));
    MOCK_CONST_METHOD2(GetFile, ApiError(const std::string& apiFilePath, ApiFile& apiFile));
    MOCK_CONST_METHOD2(GetFileSize, ApiError(const std::string& apiFilePath, std::uint64_t& fileSize));
    MOCK_CONST_METHOD3(GetFileSystemItem, ApiFileError(const std::string &apiFilePath, const bool& directory, FileSystemItem& fileSystemItem));
    MOCK_CONST_METHOD2(GetFileSystemItemFromSourcePath, ApiFileError(const std::string& sourceFilePath, FileSystemItem& fileSystemItem));
    MOCK_CONST_METHOD2(GetItemMeta, ApiFileError(const std::string &apiFilePath, ApiMetaMap &meta));
    MOCK_CONST_METHOD3(GetItemMeta, ApiFileError(const std::string &apiFilePath, const std::string &key, std::string &value));
    MOCK_CONST_METHOD0(GetTotalDriveSpace, std::uint64_t());
    MOCK_CONST_METHOD0(GetTotalItemCount, std::uint64_t());
    MOCK_CONST_METHOD0(GetUsedDriveSpace, std::uint64_t());
    MOCK_CONST_METHOD1(IsDirectory, bool(const std::string& apiFilePath));
    MOCK_CONST_METHOD1(IsFile, bool(const std::string& apiFilePath));
    MOCK_CONST_METHOD0(IsOnline, bool());
    MOCK_METHOD5(ReadFileBytes, ApiFileError(const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested));
    MOCK_METHOD1(RemoveDirectory, ApiFileError(const std::string& apiFilePath));
    MOCK_METHOD1(RemoveFile, ApiFileError(const std::string& apiFilePath));
    MOCK_METHOD2(RenameFile, ApiFileError(const std::string& fromApiPath, const std::string& toApiPath));
    MOCK_METHOD2(RemoveItemMeta, ApiFileError(const std::string &apiFilePath, const std::string &key));
    MOCK_METHOD3(SetItemMeta, ApiFileError(const std::string &apiFilePath, const std::string &key, const std::string &value));
    MOCK_METHOD2(SetItemMeta, ApiFileError(const std::string &apiFilePath, const ApiMetaMap &meta));
    MOCK_METHOD2(SetSourcePath, ApiFileError(const std::string& apiFilePath, const std::string& sourcePath));
    MOCK_METHOD2(Start, void(ApiItemAdded apiItemAdded, IOpenFileTable* openFileTable));
    MOCK_METHOD0(Stop, void());
    MOCK_METHOD2(UploadFile, ApiFileError(const std::string& apiFilePath, const std::string& sourcePath));
};

}
#endif //REPERTORY_MOCKPROVIDER_H
