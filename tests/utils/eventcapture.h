#ifndef REPERTORY_EVENTCAPTURE_H
#define REPERTORY_EVENTCAPTURE_H

#include <common.h>
#include <events/eventsystem.h>
#include <utils/utils.h>

namespace Repertory {
class CEventCapture {
    E_CONSUMER();

  public:
    explicit CEventCapture(std::vector<std::string> eventNames, std::vector<std::string> nonFiredEventNames = {}) :
      _eventNames(std::move(eventNames)),
      _nonFiredEventNames(std::move(nonFiredEventNames)) {
      E_SUBSCRIBE_ALL(ProcessEvent);
    }

    ~CEventCapture() {
      EXPECT_TRUE(_eventNames.empty());
    }

  private:
    std::vector<std::string> _eventNames;
    std::vector<std::string> _firedEventNames;
    std::vector<std::string> _nonFiredEventNames;
    std::mutex _eventMutex;
    std::condition_variable _eventNotify;

  public:
    void ProcessEvent(const Event &event) {
      UniqueMutexLock l(_eventMutex);
      Utils::RemoveElement(_eventNames, event.GetName());
      _firedEventNames.emplace_back(event.GetName());
      _eventNotify.notify_all();
      l.unlock();

      for (const auto& name : _nonFiredEventNames) {
        EXPECT_EQ(_nonFiredEventNames.end(), std::find(_nonFiredEventNames.begin(), _nonFiredEventNames.end(), event.GetName()));
      }
    }

    bool WaitForEvent(const std::string& eventName) {
      auto missing = true;
      auto count = 0u;
      UniqueMutexLock l(_eventMutex);
      while ((count++ != 30) && (missing = (std::find(_firedEventNames.begin(), _firedEventNames.end(), eventName) == _firedEventNames.end()))) {
        _eventNotify.wait_for(l, 1s);
      }
      l.unlock();
      return not missing;
    }
};
}

#endif //REPERTORY_EVENTCAPTURE_H
