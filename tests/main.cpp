#include <sqlite3.h>
#include <config_test.h>
#include <downloaders_test.h>
#include <downloadmanager_test.h>
#include <version_test.h>
#include <utils_test.h>
#include <fuse_test.h>
#include <winfsp_test.h>
#include <platform/unixplatform.h>
#include <utils/polling.h>

INITIALIZE_SINGLETONS();

#ifdef _WIN32
#pragma data_seg("SHARED")
  char g_siaMountLocation[3] = {0};
  std::int64_t g_siaPID = (-1);
  char g_siaPrimeMountLocation[3] = {0};
  std::int64_t g_siaPrimePID = (-1);
#pragma data_seg()
#pragma comment(linker, "/section:SHARED,RWS")
#endif

int main(int argc, char **argv) {
  sqlite3_initialize();
  curl_global_init(CURL_GLOBAL_DEFAULT);

  InitGoogleTest(&argc, argv);
  const auto ret = RUN_ALL_TESTS();

  curl_global_cleanup();
  sqlite3_shutdown();
  return ret;
}