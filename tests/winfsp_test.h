#ifdef _WIN32
#include <testcommon.h>
#include <fixtures/winfsp_fixture.h>

using namespace Repertory;

E_SIMPLE1(TestBegin, Normal, false,
  std::string, TestName, TN, E_STRING
);
#define TEST_HEADER(func) \
  EventSystem::Instance().Raise<TestBegin>(std::string(func) + "\r\n***********************\r\n***********************")

static auto MountSetup(WinFSPTest *winFSPTest, std::string &mountPoint) {
  // Mock communicator setup
  winFSPTest->_mockComm.PushReturn("get", "/wallet", {}, {}, ApiError::Success, true); 
  winFSPTest->_mockComm.PushReturn("get", "/renter/dir/", {
    { "directories", {{
    { "numfiles", 0 },
    { "numsubdirs", 0 }}}} }, {}, ApiError::Success, true);
  winFSPTest->_mockComm.PushReturn("get", "/renter/file/", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  winFSPTest->_mockComm.PushReturn("get", "/renter/dir/autorun.inf", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  winFSPTest->_mockComm.PushReturn("get", "/renter/file/autorun.inf", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  winFSPTest->_mockComm.PushReturn("get", "/renter/dir/AutoRun.inf", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  winFSPTest->_mockComm.PushReturn("get", "/renter/file/AutoRun.inf", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  json files;
  files["files"] = std::vector<json>();
  winFSPTest->_mockComm.PushReturn("get", "/renter/files", files, {}, ApiError::Success, true);
  winFSPTest->_mockComm.PushReturn("get", "/renter/prices", {}, {{"message", "offline"}}, ApiError::CommApiError, true);
  winFSPTest->_mockComm.PushReturn("get", "/daemon/version", {{"version", CConfig::GetProviderMinimumVersion(ProviderType::Sia)}}, {}, ApiError::Success, true);

  // Mount setup
  mountPoint = "J:";
  return std::vector<std::string>({"unittests",
                                   "-f",
                                   mountPoint});
}

static void ExecuteMount(WinFSPTest *winFSPTest, const std::vector<std::string> &driveArgs, std::thread &th) {
  ASSERT_EQ(0, winFSPTest->_drive->Main(driveArgs));
  th.join();
}

static void Unmount(WinFSPTest *winFSPTest, const std::string& mountPoint) {
  winFSPTest->_drive->Shutdown();
  auto mounted = Utils::File::IsDirectory(mountPoint);
  for (auto i = 0; mounted && (i < 50); i++) {
    std::this_thread::sleep_for(100ms);
    mounted = Utils::File::IsDirectory(mountPoint);
  }
  EXPECT_FALSE(Utils::File::IsDirectory(mountPoint));
}

static void TestRootCreation(CMockComm& mockComm, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  WIN32_FILE_ATTRIBUTE_DATA ad{};
  EXPECT_TRUE(::GetFileAttributesEx(&mountPoint[0], GetFileExInfoStandard, &ad));
  EXPECT_EQ(FILE_ATTRIBUTE_DIRECTORY, ad.dwFileAttributes);
  EXPECT_EQ(0, ad.nFileSizeHigh);
  EXPECT_EQ(0, ad.nFileSizeLow);
  mockComm.RemoveReturn("get", "/renter/dir/");
}

static auto TestCreate(CMockComm& mockComm, IProvider& provider, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/test_create.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  mockComm.PushReturn("post", "/renter/delete/test_create.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  mockComm.PushReturn("post_params", "/renter/upload/test_create.txt", {}, {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", RENTER_FILE_DATA("test_create.txt", 0), {}, ApiError::Success, true);

  const auto file = Utils::Path::Combine(mountPoint, "test_create.txt");
  auto handle = ::CreateFile(&file[0], GENERIC_READ, FILE_SHARE_READ, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
  EXPECT_NE(INVALID_HANDLE_VALUE, handle);
  EXPECT_TRUE(::CloseHandle(handle));

  EXPECT_TRUE(Utils::File::IsFile(file));
  EXPECT_TRUE(Utils::File::IsFile(Utils::String::ToUpper(file)));

  std::uint64_t fileSize;
  EXPECT_TRUE(Utils::File::GetSize(file, fileSize));
  EXPECT_EQ(0, fileSize);

  std::string attr;
  EXPECT_EQ(ApiFileError::Success, provider.GetItemMeta("/test_create.txt", META_ATTRIBUTES, attr));
  EXPECT_EQ(FILE_ATTRIBUTE_NORMAL, Utils::String::ToUInt32(attr));

  EventSystem::Instance().Stop();
  EventSystem::Instance().Start();

  mockComm.RemoveReturn("get", "/renter/dir/test_create.txt");
  mockComm.RemoveReturn("post", "/renter/delete/test_create.txt");
  mockComm.RemoveReturn("post_params", "/renter/upload/test_create.txt");
  mockComm.RemoveReturn("get", "/renter/file/test_create.txt");

  return file;
}

static void TestDeleteFile(CMockComm& mockComm, const std::string& file) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/test_create.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", RENTER_FILE_DATA("test_create.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", RENTER_FILE_DATA("test_create.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", RENTER_FILE_DATA("test_create.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", RENTER_FILE_DATA("test_create.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_create.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  mockComm.PushReturn("post", "/renter/delete/test_create.txt", {}, {}, ApiError::Success);
  {
    CEventCapture eventCapture({ "FileRemoved" });
    EXPECT_TRUE(Utils::File::DeleteAsFile(Utils::String::ToUpper(file)));
    EXPECT_FALSE(Utils::File::IsFile(file));
    EventSystem::Instance().Stop();
    EventSystem::Instance().Start();
  }

  mockComm.RemoveReturn("post", "/renter/delete/test_create.txt");
}

static void TestCreateDirectory(CMockComm& mockComm, const std::string& directory) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/" + directory.substr(3), {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {
    { "directories", {{
    { "numfiles", 0 },
    { "numsubdirs", 0 }}}} }, {}, ApiError::Success, true);
  mockComm.PushReturn("post_params", "/renter/dir/" + directory.substr(3), {}, {}, ApiError::Success);

  EXPECT_FALSE(::PathIsDirectory(&directory[0]));
  EXPECT_TRUE(::CreateDirectory(&directory[0], nullptr));
  EXPECT_TRUE(::PathIsDirectory(&Utils::String::ToUpper(directory)[0]));

  mockComm.RemoveReturn("get", "/renter/dir/" + directory.substr(3));
  mockComm.RemoveReturn("get", "/renter/file/" + directory.substr(3));
  mockComm.RemoveReturn("post_params", "/renter/dir/" + directory.substr(3));
}

static void TestRemoveDirectory(CMockComm& mockComm, const std::string& directory) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {
    { "directories", {{
                        { "numfiles", 0 },
                        { "numsubdirs", 0 }}}} }, {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {
    { "directories", {{
                        { "numfiles", 0 },
                        { "numsubdirs", 0 }}}} }, {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {
    { "directories", {{
                        { "numfiles", 0 },
                        { "numsubdirs", 0 }}}} }, {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {
    { "directories", {{
                        { "numfiles", 0 },
                        { "numsubdirs", 0 }}}} }, {}, ApiError::Success);
  mockComm.PushReturn("post_params", "/renter/dir/" + directory.substr(3), {}, {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/dir/" + directory.substr(3), {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/" + directory.substr(3), {}, { {"message", "no file known"} }, ApiError::CommApiError, true);

  {
    CEventCapture eventCapture({ "DirectoryRemoved" });
    EXPECT_TRUE(::PathIsDirectory(&directory[0]));
    EXPECT_TRUE(::RemoveDirectory(&Utils::String::ToUpper(directory)[0]));
    EXPECT_FALSE(::PathIsDirectory(&directory[0]));
    EventSystem::Instance().Stop();
    EventSystem::Instance().Start();
  }

  mockComm.RemoveReturn("post_params", "/renter/dir/" + directory.substr(3));
}

static void TestWriteFile(CMockComm& mockComm, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/", {
    { "directories", {{
    { "numfiles", 0 },
    { "numsubdirs", 0 }}}} }, {}, ApiError::Success, true);
  mockComm.PushReturn("get", "/renter/dir/test_write.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/test_write.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  mockComm.PushReturn("post_params", "/renter/upload/test_write.txt", {}, {}, ApiError::Success);
  mockComm.PushReturn("post_params", "/renter/upload/test_write.txt", {}, {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_write.txt", RENTER_FILE_DATA("test_write.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_write.txt", RENTER_FILE_DATA("test_write.txt", 10), {}, ApiError::Success, true);

  const auto file = Utils::Path::Combine(mountPoint, "test_write.txt");
  auto handle = ::CreateFile(&file[0], GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
  EXPECT_NE(INVALID_HANDLE_VALUE, handle);
  const std::string data = "0123456789";
  DWORD dwWritten = 0;
  EXPECT_TRUE(::WriteFile(handle, &data[0], static_cast<DWORD>(data.size()), &dwWritten, nullptr));
  EXPECT_EQ(10, dwWritten);
  EXPECT_TRUE(::CloseHandle(handle));

  EXPECT_TRUE(Utils::File::IsFile(file));

  std::uint64_t fileSize;
  EXPECT_TRUE(Utils::File::GetSize(Utils::String::ToUpper(file), fileSize));
  EXPECT_EQ(10, fileSize);

  EventSystem::Instance().Stop();
  EventSystem::Instance().Start();

  mockComm.RemoveReturn("get", "/renter/dir/");
  mockComm.RemoveReturn("post_params", "/renter/upload/test_write.txt");
}

static void TestReadFile(CMockComm& mockComm, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/", {
    { "directories", {{
    { "numfiles", 0 },
    { "numsubdirs", 0 }}}} }, {}, ApiError::Success, true);
  mockComm.PushReturn("get", "/renter/dir/test_read.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/test_read.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  mockComm.PushReturn("post_params", "/renter/upload/test_read.txt", {}, {}, ApiError::Success, true);
  mockComm.PushReturn("get", "/renter/file/test_read.txt", RENTER_FILE_DATA("test_read.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_read.txt", RENTER_FILE_DATA("test_read.txt", 10), {}, ApiError::Success, true);

  const auto file = Utils::Path::Combine(mountPoint, "test_read.txt");
  auto handle = ::CreateFile(&file[0], GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
  EXPECT_NE(INVALID_HANDLE_VALUE, handle);
  const std::string data = "0123456789";
  DWORD dwWritten = 0;
  EXPECT_TRUE(::WriteFile(handle, &data[0], static_cast<DWORD>(data.size()), &dwWritten, nullptr));
  EXPECT_EQ(10, dwWritten);

  std::vector<char> data2;
  data2.resize(10);
  DWORD dwRead = 0;
  EXPECT_EQ(0, ::SetFilePointer(handle, 0, nullptr, FILE_BEGIN));
  EXPECT_TRUE(::ReadFile(handle, &data2[0], static_cast<DWORD>(data2.size()), &dwRead, nullptr));
  EXPECT_EQ(10, dwRead);
  for (auto i = 0; i < data.size(); i++) {
    EXPECT_EQ(data[i], data2[i]);
  }
  EXPECT_TRUE(::CloseHandle(handle));

  EventSystem::Instance().Stop();
  EventSystem::Instance().Start();
}

static void TestRenameFile(CMockComm& mockComm, IProvider& provider, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/rename_file.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/dir/rename_file2.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError, true);
  mockComm.PushReturn("post_params", "/renter/upload/rename_file.txt", {}, {}, ApiError::Success);
  mockComm.PushReturn("post_params", "/renter/rename/rename_file.txt", {}, {}, ApiError::Success);

  mockComm.PushReturn("get", "/renter/file/rename_file.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/rename_file.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);

  mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/rename_file2.txt", RENTER_FILE_DATA("rename_file2.txt", 0), {}, ApiError::Success, true);

  const auto file = Utils::Path::Combine(mountPoint, "rename_file.txt");
  const auto file2 = Utils::Path::Combine(mountPoint, "rename_file2.txt");
  auto handle = ::CreateFile(&file[0], GENERIC_READ, FILE_SHARE_READ, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
  EXPECT_NE(INVALID_HANDLE_VALUE, handle);
  EXPECT_TRUE(::CloseHandle(handle));

  ApiMetaMap meta1{};
  EXPECT_EQ(ApiFileError::Success, provider.GetItemMeta("/rename_file.txt", meta1));

  EXPECT_TRUE(::MoveFile(&Utils::String::ToUpper(file)[0], &file2[0]));
  // Timing issue - check exists twice
  EXPECT_TRUE(Utils::File::IsFile(file2) || Utils::File::IsFile(file2) || Utils::File::IsFile(file2));
  EXPECT_FALSE(Utils::File::IsFile(file));

  ApiMetaMap meta2{};
  EXPECT_EQ(ApiFileError::Success, provider.GetItemMeta("/rename_file2.txt", meta2));
  EXPECT_STREQ(meta1[META_SOURCE].c_str(), meta2[META_SOURCE].c_str());

  FileSystemItem fsi{};
  EXPECT_EQ(ApiFileError::Success, provider.GetFileSystemItem("/rename_file2.txt", false, fsi));
  EXPECT_STREQ(meta1[META_SOURCE].c_str(), fsi.SourceFilePath.c_str());

  FileSystemItem fsi2{};
  EXPECT_EQ(ApiFileError::Success, provider.GetFileSystemItemFromSourcePath(fsi.SourceFilePath, fsi2));
  EXPECT_STREQ("/rename_file2.txt", fsi2.ApiFilePath.c_str());

  mockComm.RemoveReturn("post_params", "/renter/upload/rename_file.txt");
  mockComm.RemoveReturn("post_params", "/renter/rename/rename_file.txt");
}

static void TestGetSetBasicInfo(CMockComm& mockComm, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/setbasicinfo_file.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/setbasicinfo_file.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  mockComm.PushReturn("post_params", "/renter/upload/setbasicinfo_file.txt", {}, {}, ApiError::Success, true);

  mockComm.PushReturn("get", "/renter/file/setbasicinfo_file.txt", RENTER_FILE_DATA("setbasicinfo_file.txt", 0), {}, ApiError::Success, true);

  const auto file = Utils::Path::Combine(mountPoint, "setbasicinfo_file.txt");
  auto handle = ::CreateFile(&file[0], GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, nullptr);
  EXPECT_NE(INVALID_HANDLE_VALUE, handle);

  SYSTEMTIME st{};
  ::GetSystemTime(&st);
  st.wMinute = 0;

  FILETIME testChTime{};
  st.wMinute++;
  ::SystemTimeToFileTime(&st, &testChTime);

  FILETIME testCrTime{};
  st.wMinute++;
  ::SystemTimeToFileTime(&st, &testCrTime);

  FILETIME testLaTime{};
  st.wMinute++;
  ::SystemTimeToFileTime(&st, &testLaTime);

  FILETIME testLwTime{};
  st.wMinute++;
  ::SystemTimeToFileTime(&st, &testLwTime);

  FILE_BASIC_INFO fbi{};
  fbi.FileAttributes = FILE_ATTRIBUTE_HIDDEN;
  fbi.ChangeTime.HighPart = testChTime.dwHighDateTime;
  fbi.ChangeTime.LowPart = testChTime.dwLowDateTime;
  fbi.CreationTime = *(LARGE_INTEGER*)&testCrTime;
  fbi.LastAccessTime = *(LARGE_INTEGER*)&testLaTime;
  fbi.LastWriteTime = *(LARGE_INTEGER*)&testLwTime;

  EXPECT_TRUE(::SetFileInformationByHandle(handle, FileBasicInfo, &fbi, sizeof(FILE_BASIC_INFO)));

  FILE_BASIC_INFO fbi2{};
  EXPECT_TRUE(::GetFileInformationByHandleEx(handle, FileBasicInfo, &fbi2, sizeof(FILE_BASIC_INFO)));

  EXPECT_EQ(0, memcmp(&fbi, &fbi2, sizeof(FILE_BASIC_INFO)));

  std::cout << fbi.FileAttributes << " " << fbi.ChangeTime.QuadPart << " " << fbi.CreationTime.QuadPart << " " << fbi.LastAccessTime.QuadPart << " " << fbi.LastWriteTime.QuadPart << std::endl;
  std::cout << fbi2.FileAttributes << " " << fbi2.ChangeTime.QuadPart << " " << fbi2.CreationTime.QuadPart << " " << fbi2.LastAccessTime.QuadPart << " " << fbi2.LastWriteTime.QuadPart << std::endl;

  EXPECT_TRUE(::CloseHandle(handle));
}

static void TestOverwriteFile(CMockComm& mockComm, const std::string& mountPoint) {
  TEST_HEADER(__FUNCTION__);
  mockComm.PushReturn("get", "/renter/dir/test_overwrite.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  mockComm.PushReturn("get", "/renter/file/test_overwrite.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/test_overwrite.txt", RENTER_FILE_DATA("test_overwrite.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_overwrite.txt", RENTER_FILE_DATA("test_overwrite.txt", 0), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_overwrite.txt", RENTER_FILE_DATA("test_overwrite.txt", 10), {}, ApiError::Success);
  mockComm.PushReturn("get", "/renter/file/test_overwrite.txt", {}, { {"message", "no file known"} }, ApiError::CommApiError);
  mockComm.PushReturn("get", "/renter/file/test_overwrite.txt", RENTER_FILE_DATA("test_overwrite.txt", 10), {}, ApiError::Success, true);
  mockComm.PushReturn("post_params", "/renter/upload/test_overwrite.txt", {}, {}, ApiError::Success, true);
  mockComm.PushReturn("post", "/renter/delete/test_overwrite.txt", {}, {}, ApiError::Success, true);

  const auto file = Utils::Path::Combine("./", "test_overwrite.txt");
  auto handle = ::CreateFile(&file[0], GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
  EXPECT_NE(INVALID_HANDLE_VALUE, handle);
  if (handle != INVALID_HANDLE_VALUE) {
    const std::string data = "0123456789";
    DWORD dwWritten = 0;
    EXPECT_TRUE(::WriteFile(handle, &data[0], static_cast<DWORD>(data.size()), &dwWritten, nullptr));
    EXPECT_EQ(10, dwWritten);
    EXPECT_TRUE(::CloseHandle(handle));

    if (dwWritten == 10) {
      const auto file2 = Utils::Path::Combine(mountPoint, "test_overwrite.txt");
      EXPECT_TRUE(::CopyFile(&file[0], &file2[0], TRUE));
      EXPECT_TRUE(::CopyFile(&file[0], &file2[0], FALSE));
      EXPECT_FALSE(::CopyFile(&file[0], &file2[0], TRUE));
    }
  }
}

TEST_F(WinFSPTest, AllTests) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      TestRootCreation(_mockComm, mountPoint);
      {
        const auto file = TestCreate(_mockComm, *_provider, mountPoint);
        TestDeleteFile(_mockComm, file);
      }
      {
        const auto testDir = Utils::Path::Combine(mountPoint, "TestDir");
        TestCreateDirectory(_mockComm, testDir);
        TestRemoveDirectory(_mockComm, testDir);
      }
      TestWriteFile(_mockComm, mountPoint);
      TestReadFile(_mockComm, mountPoint);
      TestRenameFile(_mockComm, *_provider, mountPoint);
      TestOverwriteFile(_mockComm, mountPoint);
      TestGetSetBasicInfo(_mockComm, mountPoint);
    }

    Unmount(this, mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}
#endif