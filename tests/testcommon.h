#ifndef REPERTORY_MAIN_H
#define REPERTORY_MAIN_H
#include <common.h>
// Disable DLL-interface warnings
#pragma warning(disable: 4251)
#pragma warning(disable: 4275)

#include <json.hpp>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <fstream>

#define COMMA ,
#define RENTER_FILE_DATA(apiFilePath, size) \
  {{"file" COMMA { \
  {"filesize" COMMA size} COMMA \
  {"siapath" COMMA apiFilePath} COMMA \
  {"redundancy" COMMA 1.0} COMMA \
  {"available" COMMA false} COMMA \
  {"expiration" COMMA 0} COMMA \
  {"ondisk" COMMA true} COMMA \
  {"recoverable" COMMA false} COMMA \
  {"renewing" COMMA false} COMMA \
  {"localpath" COMMA ".\\" } COMMA \
  {"uploadedbytes" COMMA 0 } COMMA \
  {"uploadprogress" COMMA 0 } COMMA \
  {"accesstime" COMMA "2019-02-21T02:24:37.653091916-06:00" } COMMA \
  {"changetime" COMMA "2019-02-21T02:24:37.653091916-06:00" } COMMA \
  {"createtime" COMMA "2019-02-21T02:24:37.653091916-06:00" } COMMA \
  {"modtime" COMMA "2019-02-21T02:24:37.653091916-06:00" } COMMA \
  }}}

using ::testing::_;
using namespace ::testing;

#endif