#ifndef REPERTORY_VERSION_TEST_H
#define REPERTORY_VERSION_TEST_H

#include <testcommon.h>
#include <utils/utils.h>

using namespace Repertory;

TEST(Version, Equal) {
  EXPECT_EQ(0, Utils::CompareVersionStrings("1.0", "1.0"));
  EXPECT_EQ(0, Utils::CompareVersionStrings("1.0.0", "1.0"));
  EXPECT_EQ(0, Utils::CompareVersionStrings("1.0.0.0", "1.0"));
  EXPECT_EQ(0, Utils::CompareVersionStrings("1.0.0.0", "1.0.0"));
}

TEST(Version, Greater) {
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1", "1.0"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1", "1.0.0"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1", "1.0.0.0"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1.0", "1.0"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1.0", "1.0.0"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1.0", "1.0.0.0"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0", "0.9.9"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1", "0.9.9"));
  EXPECT_EQ(1, Utils::CompareVersionStrings("1.0.1.0", "0.9.9"));
}

TEST(Version, Less) {
  EXPECT_EQ(-1, Utils::CompareVersionStrings("0.9.9", "1.0"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("0.9.9", "1.0.1"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("0.9.9", "1.0.1.0"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("1.0", "1.0.1"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("1.0", "1.0.1.0"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("1.0.0", "1.0.1"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("1.0.0", "1.0.1.0"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("1.0.0.0", "1.0.1"));
  EXPECT_EQ(-1, Utils::CompareVersionStrings("1.0.0.0", "1.0.1.0"));
}

#endif //REPERTORY_VERSION_TEST_H
