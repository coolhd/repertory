#ifndef REPERTORY_DOWNLOADMANAGER_TEST_H
#define REPERTORY_DOWNLOADMANAGER_TEST_H

#include <testcommon.h>
#include <config.h>
#include <download/downloadmanager.h>
#include <events/consumers/consoleconsumer.h>
#include <mocks/mockopenfiletable.h>
#include <utils/eventcapture.h>
#include <utils/fileutils.h>
#include <utils/stringutils.h>
#include <utils/utils.h>

#define TEST_CHUNK_SIZE 65536
using namespace Repertory;

NativeFilePtr GenerateRandomFileBytesAndOpen(const std::string& path, const size_t& size) {
  NativeFilePtr nativeFile;
  if (CNativeFile::CreateOrOpen(path, nativeFile) == ApiFileError::Success) {
    EXPECT_TRUE(nativeFile->Truncate(0));

    size_t offset = 0;
    std::random_device rd;
    std::uniform_int_distribution<int> dist(0, 255);
    std::vector<char> buf;
    buf.resize(size);

    auto d = static_cast<char>(dist(rd));
    while (offset < size) {
      buf[offset++] = d++;
    }
    size_t bytesWritten;
    nativeFile->WriteBytes(&buf[0], buf.size(), 0, bytesWritten);
    nativeFile->Flush();
    std::uint64_t curSize;
    EXPECT_TRUE(Utils::File::GetSize(path, curSize));
    EXPECT_EQ(size, curSize);
  }

  return std::move(nativeFile);
}

TEST(DownloadManager, DownloadFailNoHang) {
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");

  for (int i = 0; i < 2; i++) {
    CConsoleConsumer consoleConsumer;
    EventSystem::Instance().Start();

    CEventCapture eventCapture({"DownloadBegin", "DownloadEnd"}, {"DownloadTimeout"});
    const auto testSource = Utils::Path::Absolute("./test_chunk_src");
    Utils::File::DeleteAsFile(testSource);

    const auto chunkSize = (i ? TEST_CHUNK_SIZE : (config.GetChunkSize() * 1024ull));
    const auto fileSize = (chunkSize * 5ull) + 2;
    const auto testDest = Utils::Path::Absolute("./test_chunk_dest");
    Utils::File::DeleteAsFile(testDest);

    auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
    EXPECT_NE(nativeFile, nullptr);
    if (nativeFile) {
      ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)->ApiFileError {
#ifdef _WIN32
        ::SetLastError(ERROR_ACCESS_DENIED);
#else
        Utils::SetLastErrorCode(EACCES);
#endif
        return ApiFileError::OSErrorCode;
      };

      FileSystemItem fsi{};
      fsi.ApiFilePath = "/test_chunk" + std::to_string(i);
      fsi.Directory = false;
      fsi.ItemLock = std::make_shared<std::recursive_mutex>();
      fsi.LegacyChunks = !!i;
      fsi.SourceFilePath = testDest;
      fsi.Size = fileSize;

      CDownloadManager downloadManager(config, apiReader, true);
      CMockOpenFileTable openFileTable(&downloadManager, &fsi);
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      downloadManager.Start(&openFileTable);

      EXPECT_EQ(ApiFileError::OSErrorCode, downloadManager.Download(1, fsi));

      downloadManager.Stop();
      EXPECT_EQ(0, downloadManager.GetDownloadCount());
      nativeFile->Close();

      Utils::File::DeleteAsFile(fsi.SourceFilePath);
    }

    Utils::File::DeleteAsFile(testSource);
    EventSystem::Instance().Stop();
  }

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}

TEST(DownloadManager, SingleChunkNoOffset) {
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");

  for (int i = 0; i < 2; i++) {
    CConsoleConsumer consoleConsumer;
    EventSystem::Instance().Start();

    CEventCapture eventCapture({"DownloadBegin", "DownloadEnd", "DownloadProgress"}, {"DownloadTimeout"});
    const auto testSource = Utils::Path::Absolute("./test_chunk_src");
    Utils::File::DeleteAsFile(testSource);

    const auto chunkSize = (i ? TEST_CHUNK_SIZE : (config.GetChunkSize() * 1024ull));
    const auto fileSize = (chunkSize * 5ull);
    auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
    EXPECT_NE(nativeFile, nullptr);
    if (nativeFile) {
      ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool &stopRequested) -> ApiFileError {
        data.resize(size);
        size_t bytesRead;
        auto ret = nativeFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
        EXPECT_EQ(ApiFileError::Success, ret);
        EXPECT_EQ(bytesRead, data.size());
        return ret;
      };

      FileSystemItem fsi{};
      fsi.ApiFilePath = "/test_chunk" + std::to_string(i);
      fsi.Directory = false;
      fsi.LegacyChunks = !!i;
      fsi.ItemLock = std::make_shared<std::recursive_mutex>();
      fsi.SourceFilePath = testSource + std::to_string(i);
      fsi.Size = fileSize;

      CDownloadManager downloadManager(config, apiReader, true);
      CMockOpenFileTable openFileTable(&downloadManager, &fsi);
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      downloadManager.Start(&openFileTable);

      std::vector<char> readBuffer;
      EXPECT_EQ(ApiFileError::Success, downloadManager.ReadBytes(1, fsi, chunkSize, 0, readBuffer));
      EXPECT_EQ(chunkSize, readBuffer.size());

      std::size_t bytesRead;
      std::vector<char> sourceBuffer;
      sourceBuffer.resize(chunkSize);
      EXPECT_TRUE(nativeFile->ReadBytes(&sourceBuffer[0], sourceBuffer.size(), 0, bytesRead));
      EXPECT_EQ(readBuffer.size(), bytesRead);
      EXPECT_EQ(readBuffer.size(), sourceBuffer.size());
      if (readBuffer.size() == sourceBuffer.size()) {
        EXPECT_EQ(0, memcmp(&readBuffer[0], &sourceBuffer[0], readBuffer.size()));
      }

      downloadManager.Stop();
      EXPECT_EQ(0, downloadManager.GetDownloadCount());
      nativeFile->Close();

      Utils::File::DeleteAsFile(fsi.SourceFilePath);
    }

    Utils::File::DeleteAsFile(testSource);
    EventSystem::Instance().Stop();
  }

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}

TEST(DownloadManager, SingleChunkOffsetOverlap) {
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");

  for (int i = 0; i < 2; i++) {
    CConsoleConsumer consoleConsumer;
    EventSystem::Instance().Start();

    CEventCapture eventCapture({"DownloadBegin", "DownloadEnd", "DownloadProgress"}, {"DownloadTimeout"});
    const auto testSource = Utils::Path::Absolute("./test_chunk_src");
    Utils::File::DeleteAsFile(testSource);
    const auto chunkSize = (i ? TEST_CHUNK_SIZE : (config.GetChunkSize() * 1024ull));
    const auto fileSize = (chunkSize * 5ull);
    auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
    EXPECT_NE(nativeFile, nullptr);
    if (nativeFile) {
      ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)->ApiFileError {
        data.resize(size);
        size_t bytesRead;
        auto ret = nativeFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
        EXPECT_EQ(ApiFileError::Success, ret);
        EXPECT_EQ(bytesRead, data.size());
        return ret;
      };

      FileSystemItem fsi{};
      fsi.ApiFilePath = "/test_chunk" + std::to_string(i);
      fsi.Directory = false;
      fsi.LegacyChunks = !!i;
      fsi.ItemLock = std::make_shared<std::recursive_mutex>();
      fsi.SourceFilePath = testSource + std::to_string(i);
      fsi.Size = fileSize;

      CDownloadManager downloadManager(config, apiReader, true);
      CMockOpenFileTable openFileTable(&downloadManager, &fsi);
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      downloadManager.Start(&openFileTable);

      std::vector<char> readBuffer;
      EXPECT_EQ(ApiFileError::Success, downloadManager.ReadBytes(1, fsi, chunkSize, chunkSize / 2, readBuffer));
      EXPECT_EQ(chunkSize, readBuffer.size());

      std::size_t bytesRead;
      std::vector<char> sourceBuffer;
      sourceBuffer.resize(chunkSize);
      EXPECT_TRUE(nativeFile->ReadBytes(&sourceBuffer[0], sourceBuffer.size(), chunkSize / 2, bytesRead));
      EXPECT_EQ(readBuffer.size(), bytesRead);
      EXPECT_EQ(readBuffer.size(), sourceBuffer.size());
      if (readBuffer.size() == sourceBuffer.size()) {
        EXPECT_EQ(0, memcmp(&readBuffer[0], &sourceBuffer[0], readBuffer.size()));
      }

      downloadManager.Stop();
      EXPECT_EQ(0, downloadManager.GetDownloadCount());
      nativeFile->Close();

      Utils::File::DeleteAsFile(fsi.SourceFilePath);
    }

    Utils::File::DeleteAsFile(testSource);
    EventSystem::Instance().Stop();
  }

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}

TEST(DownloadManager, CheckOverflowOnReadGreaterThanFileSize) {
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");

  for (int i = 0; i < 2; i++) {
    CConsoleConsumer consoleConsumer;
    EventSystem::Instance().Start();

    CEventCapture eventCapture({}, {"DownloadBegin", "DownloadEnd", "DownloadTimeout"});
    const auto testSource = Utils::Path::Absolute("./test_chunk_src");
    Utils::File::DeleteAsFile(testSource);

    const auto chunkSize = (i ? TEST_CHUNK_SIZE : (config.GetChunkSize() * 1024ull));
    const auto fileSize = (chunkSize * 5ull);
    auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
    EXPECT_NE(nativeFile, nullptr);
    if (nativeFile) {
      ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)->ApiFileError {
        data.resize(size);
        size_t bytesRead;
        auto ret = nativeFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
        EXPECT_EQ(ApiFileError::Success, ret);
        EXPECT_EQ(bytesRead, data.size());
        return ret;
      };

      FileSystemItem fsi{};
      fsi.ApiFilePath = "/test_chunk" + std::to_string(i);
      fsi.Directory = false;
      fsi.LegacyChunks = !!i;
      fsi.ItemLock = std::make_shared<std::recursive_mutex>();
      fsi.SourceFilePath = testSource + std::to_string(i);
      fsi.Size = fileSize;
      CDownloadManager downloadManager(config, apiReader, true);
      CMockOpenFileTable openFileTable(&downloadManager, &fsi);
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      downloadManager.Start(&openFileTable);

      std::vector<char> data;
      EXPECT_EQ(ApiFileError::Success, downloadManager.ReadBytes(1, fsi, fileSize * 2, fileSize, data));
      EXPECT_EQ(0, data.size());

      downloadManager.Stop();
      EXPECT_EQ(0, downloadManager.GetDownloadCount());
      nativeFile->Close();

      Utils::File::DeleteAsFile(fsi.SourceFilePath);
    }

    Utils::File::DeleteAsFile(testSource);
    EventSystem::Instance().Stop();
  }

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}

TEST(DownloadManager, CheckReadSizeGreaterThanFileSize) {
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");

  for (int i = 0; i < 2; i++) {
    CConsoleConsumer consoleConsumer;
    EventSystem::Instance().Start();

    CEventCapture eventCapture({"DownloadBegin", "DownloadEnd", "DownloadProgress"}, {"DownloadTimeout"});
    const auto testSource = Utils::Path::Absolute("./test_chunk_src");
    Utils::File::DeleteAsFile(testSource);

    const auto chunkSize = (i ? TEST_CHUNK_SIZE : (config.GetChunkSize() * 1024ull));
    const auto fileSize = (chunkSize * 5ull);
    auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
    EXPECT_NE(nativeFile, nullptr);
    if (nativeFile) {
      ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)->ApiFileError {
        data.resize(size);
        size_t bytesRead;
        auto ret = nativeFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
        EXPECT_EQ(ApiFileError::Success, ret);
        EXPECT_EQ(bytesRead, data.size());
        return ret;
      };

      FileSystemItem fsi{};
      fsi.ApiFilePath = "/test_chunk" + std::to_string(i);
      fsi.Directory = false;
      fsi.ItemLock = std::make_shared<std::recursive_mutex>();
      fsi.LegacyChunks = !!i;
      fsi.SourceFilePath = testSource + std::to_string(i);
      fsi.Size = fileSize;

      CDownloadManager downloadManager(config, apiReader, true);
      CMockOpenFileTable openFileTable(&downloadManager, &fsi);
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      downloadManager.Start(&openFileTable);

      std::vector<char> data;
      EXPECT_EQ(ApiFileError::Success, downloadManager.ReadBytes(1, fsi, fileSize * 2, 0, data));
      EXPECT_EQ(fileSize, data.size());

      downloadManager.Stop();
      EXPECT_EQ(0, downloadManager.GetDownloadCount());
      nativeFile->Close();

      Utils::File::DeleteAsFile(fsi.SourceFilePath);
    }

    Utils::File::DeleteAsFile(testSource);
    EventSystem::Instance().Stop();
  }

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}

TEST(DownloadManager, DownloadFile) {
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");

  for (int i = 0; i < 2; i++) {
    CConsoleConsumer consoleConsumer;
    EventSystem::Instance().Start();

    CEventCapture eventCapture({"DownloadBegin", "DownloadEnd", "DownloadProgress"}, {"DownloadTimeout"});
    const auto testSource = Utils::Path::Absolute("./test_chunk_src");
    Utils::File::DeleteAsFile(testSource);

    const auto chunkSize = (i ? TEST_CHUNK_SIZE : (config.GetChunkSize() * 1024ull));
    const auto fileSize = (chunkSize * 5ull + 8ull);
    const auto testDest = Utils::Path::Absolute("./test_chunk_dest");
    Utils::File::DeleteAsFile(testDest);

    auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
    EXPECT_NE(nativeFile, nullptr);
    if (nativeFile) {
      ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)->ApiFileError {
        data.resize(size);
        size_t bytesRead;
        auto ret = nativeFile->ReadBytes(&data[0], data.size(), offset, bytesRead) ? ApiFileError::Success : ApiFileError::OSErrorCode;
        EXPECT_EQ(ApiFileError::Success, ret);
        EXPECT_EQ(bytesRead, data.size());
        return ret;
      };

      FileSystemItem fsi{};
      fsi.ApiFilePath = "/test_chunk" + std::to_string(i);
      fsi.Directory = false;
      fsi.ItemLock = std::make_shared<std::recursive_mutex>();
      fsi.LegacyChunks = !!i;
      fsi.SourceFilePath = testDest;
      fsi.Size = fileSize;

      CDownloadManager downloadManager(config, apiReader, true);
      CMockOpenFileTable openFileTable(&downloadManager, &fsi);
      EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(1ull));
      downloadManager.Start(&openFileTable);

      EXPECT_EQ(ApiFileError::Success, downloadManager.Download(1, fsi));

      std::uint64_t sourceSize;
      EXPECT_TRUE(Utils::File::GetSize(testSource, sourceSize));
      std::uint64_t curSize;
      EXPECT_TRUE(Utils::File::GetSize(fsi.SourceFilePath, curSize));
      EXPECT_EQ(sourceSize, curSize);

      EXPECT_STRCASEEQ(Utils::File::GenerateSha256(testSource).c_str(), Utils::File::GenerateSha256(fsi.SourceFilePath).c_str());

      downloadManager.Stop();
      EXPECT_EQ(0, downloadManager.GetDownloadCount());
      nativeFile->Close();

      Utils::File::DeleteAsFile(fsi.SourceFilePath);
    }

    Utils::File::DeleteAsFile(testSource);
    EventSystem::Instance().Stop();
  }

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}

TEST(DownloadManager, DownloadTimeout) {
  CConsoleConsumer consoleConsumer;
  EventSystem::Instance().Start();

  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
  CConfig config(ProviderType::Sia, "./chunk_data");
  config.SetChunkDownloaderTimeoutSeconds(5);
  config.SetEnableChunkDownloaderTimeout(true);
  
  CEventCapture eventCapture({"FileSystemItemClosed", "DownloadTimeout", "DownloadBegin", "DownloadEnd"});
  const auto testSource = Utils::Path::Absolute("./test_chunk_src");
  Utils::File::DeleteAsFile(testSource);

  const auto chunkSize = (config.GetChunkSize() * 1024ull);
  const auto fileSize = (chunkSize * 5ull) + 2;
  const auto testDest = Utils::Path::Absolute("./test_chunk_dest");
  Utils::File::DeleteAsFile(testDest);

  auto nativeFile = GenerateRandomFileBytesAndOpen(testSource, fileSize);
  EXPECT_NE(nativeFile, nullptr);
  if (nativeFile) {
    FileSystemItem* pfsi = nullptr;
    auto sent = false;
    ApiReader apiReader = [&](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)->ApiFileError {
      if (not sent) {
        sent = true;
        EventSystem::Instance().Raise<FileSystemItemClosed>(pfsi->ApiFilePath, pfsi->SourceFilePath, false, false);
      }
      while (not stopRequested) {
        std::this_thread::sleep_for(1ms);
      }
      return ApiFileError::DownloadFailed;
    };

    FileSystemItem fsi{};
    fsi.ApiFilePath = "/test_chunk";
    fsi.Directory = false;
    fsi.ItemLock = std::make_shared<std::recursive_mutex>();
    fsi.LegacyChunks = true;
    fsi.SourceFilePath = testDest;
    fsi.Size = fileSize;
    pfsi = &fsi;

    CDownloadManager downloadManager(config, apiReader, true);
    CMockOpenFileTable openFileTable;
    EXPECT_CALL(openFileTable, GetOpenCount(_)).WillRepeatedly(Return(0ull));
    downloadManager.Start(&openFileTable);

    EXPECT_EQ(ApiFileError::DownloadTimeout, downloadManager.Download(1, fsi));

    downloadManager.Stop();
    EXPECT_EQ(0, downloadManager.GetDownloadCount());
    nativeFile->Close();

    Utils::File::DeleteAsFile(fsi.SourceFilePath);

    Utils::File::DeleteAsFile(testSource);
  }

  EventSystem::Instance().Stop();
  Utils::File::RecursiveDeleteDirectory(Utils::Path::Absolute("./chunk_data"));
}
#endif