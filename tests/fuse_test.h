#ifndef _WIN32
#include <testcommon.h>
#include <fixtures/fuse_fixture.h>
#include <utils/fileutils.h>

using namespace Repertory;

static void RetryUnlink(const std::string& file) {
  int ret = 0;
  std::this_thread::sleep_for(100ms);
  for (auto i = 0; ((ret = unlink(&file[0])) != 0) && (i < 20); i++) {
    std::this_thread::sleep_for(100ms);
  }
  EXPECT_EQ(0, ret);
}

static auto MountSetup(FUSETest *fuseTest, std::string &mountPoint) {
  // Mock communicator setup
  auto uid = std::to_string(getuid());
  fuseTest->_mockComm.PushReturn("get", "/wallet", {}, {}, ApiError::Success, true);
  json files;
  files["files"] = std::vector<json>();
  fuseTest->_mockComm.PushReturn("get", "/renter/files", files, {}, ApiError::Success, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/.localized", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/.localized", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/.hidden", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/.hidden", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/.DS_Store", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/.DS_Store", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/BDMV", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/BDMV", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/.xdg-volume-info", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/.xdg-volume-info", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/autorun.inf", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/autorun.inf", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/.Trash", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/.Trash", {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/dir/.Trash-" + uid, {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/file/.Trash-" + uid, {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("post", "/renter/downloads/clear", files, {}, ApiError::Success, true);
  fuseTest->_mockComm.PushReturn("get", "/renter/prices", {}, {{"message", "offline"}}, ApiError::CommApiError, true);
  fuseTest->_mockComm.PushReturn("get", "/daemon/version", {{"version", CConfig::GetProviderMinimumVersion(ProviderType::Sia)}}, {}, ApiError::Success, true);

  // Mount setup
  static long i = 0;
  mountPoint = Utils::Path::Absolute("./fuse_mount" + std::to_string(i++));
  Utils::File::CreateFullDirectoryPath(mountPoint);
  return std::vector<std::string>({"unittests",
                                   "-f",
                                   mountPoint});
}

static void ExecuteMount(FUSETest *fuseTest, const std::vector<std::string> &driveArgs, std::thread &th) {
  ASSERT_EQ(0, fuseTest->_drive->Main(driveArgs));
  th.join();
}

static void Unmount(const std::string &mountPoint) {
  auto unmounted = false;
  for (int i = 0; not unmounted && (i < 10); i++) {
#if __APPLE__ || IS_FREEBSD
    unmounted = (system(("umount \"" + mountPoint + "\"").c_str()) == 0);
#else
    unmounted = (system(("fusermount -u \"" + mountPoint + "\"").c_str()) == 0);
#endif
    if (not unmounted) {
      std::this_thread::sleep_for(100ms);
    }
  }

  EXPECT_TRUE(unmounted);
  rmdir(&mountPoint[0]);
}

TEST_F(FUSETest, MountAndUnMount) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted", "DriveUnMounted", "DriveUnMountPending", "DriveMountResult"});

  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      EXPECT_EQ(0, system(("mount|grep \"" + mountPoint + "\"").c_str()));
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);

  EventSystem::Instance().Stop();
  EventSystem::Instance().Start();
}

TEST_F(FUSETest, RootCreation) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      struct stat st{};
      EXPECT_EQ(0, stat(&mountPoint[0], &st));
      EXPECT_EQ(getuid(), st.st_uid);
      EXPECT_EQ(getgid(), st.st_gid);
      EXPECT_TRUE(S_ISDIR(st.st_mode));
      EXPECT_EQ(S_IRUSR | S_IWUSR | S_IXUSR, ACCESSPERMS & st.st_mode);
      EXPECT_EQ(0, st.st_size);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Chmod) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      EXPECT_EQ(0, chmod(&mountPoint[0], S_IRUSR | S_IWUSR));
      std::string mode;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/", META_MODE, mode));
      EXPECT_EQ(S_IRUSR | S_IWUSR, ACCESSPERMS & Utils::String::ToUInt32(mode));
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, ChownUID) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      EXPECT_EQ(0, chown(&mountPoint[0], 0, -1));

      std::string uid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/", META_UID, uid));
      EXPECT_STREQ("0", uid.c_str());

      std::string gid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/", META_GID, gid));
      if (not gid.empty()) {
        EXPECT_EQ(getgid(), Utils::String::ToUInt32(gid));
      }
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, ChownGID) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      EXPECT_EQ(0, chown(&mountPoint[0], -1, 0));

      std::string uid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/", META_UID, uid));
      EXPECT_EQ(getuid(), Utils::String::ToUInt32(uid));

      std::string gid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/", META_GID, gid));
      EXPECT_STREQ("0", gid.c_str());
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Mkdir) {
  _mockComm.PushReturn("post_params", "/renter/dir/new_dir", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/new_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/new_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/new_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._new_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/._new_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/new_dir",  {{"directories", {{{"siapath", "new_dir"}, {"numfiles", 0}, {"numsubdirs", 0}}}}, {"files", {}}}, {}, ApiError::Success, true);

  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto newDir = Utils::Path::Combine(mountPoint, "new_dir");
      EXPECT_EQ(0, mkdir(&newDir[0], S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP));

      EXPECT_TRUE(Utils::File::IsDirectory(newDir));
      EXPECT_FALSE(Utils::File::IsFile(newDir));

      std::string uid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/new_dir", META_UID, uid));
      EXPECT_EQ(getuid(), Utils::String::ToUInt32(uid));

      std::string gid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/new_dir", META_GID, gid));
      EXPECT_EQ(getgid(), Utils::String::ToUInt32(gid));

      std::string mode;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/new_dir", META_MODE, mode));
      EXPECT_EQ(S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP, ACCESSPERMS & (Utils::String::ToUInt32(mode)));
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Rmdir) {
  _mockComm.PushReturn("post_params", "/renter/dir/rm_dir", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/rm_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rm_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/rm_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._rm_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/._rm_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/rm_dir",  {{"directories", {{{"siapath", "rm_dir"}, {"numfiles", 0}, {"numsubdirs", 0}}}}, {"files", {}}}, {}, ApiError::Success);

  _mockComm.PushReturn("post_params", "/renter/dir/rm_dir", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/dir/rm_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);

  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      CEventCapture eventCapture2({"DirectoryRemoved"});

      const auto newDir = Utils::Path::Combine(mountPoint, "rm_dir");
      EXPECT_EQ(0, mkdir(&newDir[0], S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP));

      EXPECT_TRUE(Utils::File::IsDirectory(newDir));
      EXPECT_EQ(0, rmdir(&newDir[0]));
      EXPECT_FALSE(Utils::File::IsDirectory(newDir));
      ApiMetaMap meta;
      EXPECT_EQ(ApiFileError::ItemNotFound, _provider->GetItemMeta("/rm_dir", meta));
      EXPECT_EQ(0, meta.size());

      EventSystem::Instance().Stop();
      EventSystem::Instance().Start();
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, CreateNewFile) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/create_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/create_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/create_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/create_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._create_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._create_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/upload/create_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/create_file.txt", RENTER_FILE_DATA("create_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      CEventCapture eventCapture2({"FileUploadBegin", "FileUploadEnd", "FileSystemItemHandleClosed", "FileSystemItemClosed"});

      const auto file = Utils::Path::Combine(mountPoint, "create_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_TRUE(Utils::File::IsFile(file));
      EXPECT_FALSE(Utils::File::IsDirectory(file));

      std::uint64_t fileSize;
      EXPECT_TRUE(Utils::File::GetSize(file, fileSize));
      EXPECT_EQ(0, fileSize);

      std::string uid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/create_file.txt", META_UID, uid));
      EXPECT_EQ(getuid(), Utils::String::ToUInt32(uid));

      std::string gid;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/create_file.txt", META_GID, gid));
      EXPECT_EQ(getgid(), Utils::String::ToUInt32(gid));

      std::string mode;
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/create_file.txt", META_MODE, mode));
      EXPECT_EQ(S_IRUSR | S_IWUSR | S_IRGRP, ACCESSPERMS & (Utils::String::ToUInt32(mode)));

      EXPECT_EQ(0, close(fd));

      EventSystem::Instance().Stop();
      EventSystem::Instance().Start();
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Unlink) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/upload/unlink_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt", RENTER_FILE_DATA("unlink_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt", RENTER_FILE_DATA("unlink_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt", RENTER_FILE_DATA("unlink_file.txt", 0), {}, ApiError::Success);
#if __APPLE__ || IS_FREEBSD
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt", RENTER_FILE_DATA("unlink_file.txt", 0), {}, ApiError::Success);
#endif
#ifdef IS_FREEBSD
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt", RENTER_FILE_DATA("unlink_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt", RENTER_FILE_DATA("unlink_file.txt", 0), {}, ApiError::Success);
#endif
  _mockComm.PushReturn("post", "/renter/delete/unlink_file.txt", {}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/file/unlink_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      CEventCapture eventCapture2({"FileRemoved"});

      const auto file = Utils::Path::Combine(mountPoint, "unlink_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      FileSystemItem fsi{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItem("/unlink_file.txt", false, fsi));
      EXPECT_TRUE(Utils::File::IsFile(file));
      EXPECT_TRUE(Utils::File::IsFile(fsi.SourceFilePath));

      RetryUnlink(file);

      EXPECT_FALSE(Utils::File::IsFile(file));
      EXPECT_FALSE(Utils::File::IsFile(fsi.SourceFilePath));

      EventSystem::Instance().Stop();
      EventSystem::Instance().Start();
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Write) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/write_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/write_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/write_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/write_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._write_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._write_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/upload/write_file.txt", {}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/file/write_file.txt", RENTER_FILE_DATA("write_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/write_file.txt", RENTER_FILE_DATA("write_file.txt", 8), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "write_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);

      std::string data = "TestData";
      EXPECT_EQ(data.size(), write(fd, &data[0], data.size()));

      EXPECT_EQ(0, close(fd));

      FileSystemItem fsi{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItem("/write_file.txt", false, fsi));
      EXPECT_EQ(data.size(), fsi.Size);

      std::uint64_t fileSize;
      EXPECT_TRUE(Utils::File::GetSize(fsi.SourceFilePath, fileSize));
      EXPECT_EQ(data.size(), fileSize);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Read) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/read_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/read_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/read_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/read_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._read_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._read_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/upload/read_file.txt", {}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/file/read_file.txt", RENTER_FILE_DATA("read_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/read_file.txt", RENTER_FILE_DATA("read_file.txt", 8), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "read_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);

      std::string data = "TestData";
      EXPECT_EQ(data.size(), write(fd, &data[0], data.size()));

      EXPECT_EQ(0, lseek(fd, 0, SEEK_SET));

      std::vector<char> readData;
      readData.resize(data.size());
      EXPECT_EQ(data.size(), read(fd, &readData[0], readData.size()));

      EXPECT_EQ(0, memcmp(&data[0], &readData[0], data.size()));

      EXPECT_EQ(0, close(fd));
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Rename) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/rename_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/rename_file2.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._rename_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._rename_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
#if __APPLE__ || IS_FREEBSD
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt", RENTER_FILE_DATA("rename_file.txt", 0), {}, ApiError::Success);
#endif
  _mockComm.PushReturn("post_params", "/renter/upload/rename_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/rename_file2.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._rename_file2.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._rename_file2.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/rename/rename_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/rename_file.txt", {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/rename_file2.txt", RENTER_FILE_DATA("rename_file2.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "rename_file.txt");
      const auto newFile = Utils::Path::Combine(mountPoint, "rename_file2.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);

      EXPECT_EQ(0, close(fd));

      ApiMetaMap meta1{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/rename_file.txt", meta1));

      EXPECT_EQ(0, rename(&file[0], &newFile[0]));

      ApiMetaMap meta2{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetItemMeta("/rename_file2.txt", meta2));
      EXPECT_STREQ(meta1[META_SOURCE].c_str(), meta2[META_SOURCE].c_str());

      FileSystemItem fsi{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItem("/rename_file2.txt", false, fsi));
      EXPECT_STREQ(meta1[META_SOURCE].c_str(), fsi.SourceFilePath.c_str());

      FileSystemItem fsi2{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItemFromSourcePath(fsi.SourceFilePath, fsi2));
      EXPECT_STREQ("/rename_file2.txt", fsi2.ApiFilePath.c_str());

      EXPECT_EQ(-1, unlink(&file[0]));
      EXPECT_EQ(ENOENT, errno);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Truncate) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/truncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._truncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._truncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/upload/truncate_file.txt", {}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt", RENTER_FILE_DATA("truncate_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt", RENTER_FILE_DATA("truncate_file.txt", 0), {}, ApiError::Success);
#if __APPLE__ || IS_FREEBSD
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt", RENTER_FILE_DATA("truncate_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt", RENTER_FILE_DATA("truncate_file.txt", 0), {}, ApiError::Success);
#endif
  _mockComm.PushReturn("get", "/renter/file/truncate_file.txt", RENTER_FILE_DATA("truncate_file.txt", 16), {}, ApiError::Success, true);
  
  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "truncate_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      EXPECT_EQ(0, truncate(&file[0], 16));

      std::uint64_t fileSize;
      EXPECT_TRUE(Utils::File::GetSize(&file[0], fileSize));
      EXPECT_EQ(16, fileSize);

      FileSystemItem fsi{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItem("/truncate_file.txt", false, fsi));

      fileSize = 0;
      EXPECT_TRUE(Utils::File::GetSize(fsi.SourceFilePath, fileSize));
      EXPECT_EQ(16, fileSize);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, Ftruncate) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/ftruncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/ftruncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/ftruncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/ftruncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/._ftruncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._ftruncate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("post_params", "/renter/upload/ftruncate_file.txt", {}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/file/ftruncate_file.txt", RENTER_FILE_DATA("ftruncate_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/ftruncate_file.txt", RENTER_FILE_DATA("ftruncate_file.txt", 16), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "ftruncate_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);

      EXPECT_EQ(0, ftruncate(fd, 16));

      std::uint64_t fileSize;
      EXPECT_TRUE(Utils::File::GetSize(&file[0], fileSize));
      EXPECT_EQ(16, fileSize);

      EXPECT_EQ(0, close(fd));

      FileSystemItem fsi{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItem("/ftruncate_file.txt", false, fsi));

      fileSize = 0;
      EXPECT_TRUE(Utils::File::GetSize(fsi.SourceFilePath, fileSize));
      EXPECT_EQ(16, fileSize);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

#ifdef __APPLE__
TEST_F(FUSETest, Fallocate) {

}
#else
TEST_F(FUSETest, Fallocate) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/fallocate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/fallocate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/fallocate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/fallocate_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/fallocate_file.txt", {}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/file/fallocate_file.txt", RENTER_FILE_DATA("fallocate_file.txt", 0), {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/fallocate_file.txt", RENTER_FILE_DATA("fallocate_file.txt", 16), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "fallocate_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
  #ifdef IS_FREEBSD
      EXPECT_EQ(0, posix_fallocate(fd, 0, 16));
  #else
      EXPECT_EQ(0, fallocate(fd, 0, 0, 16));
  #endif
      std::uint64_t fileSize;
      EXPECT_TRUE(Utils::File::GetSize(&file[0], fileSize));
      EXPECT_EQ(16, fileSize);

      EXPECT_EQ(0, close(fd));

      FileSystemItem fsi{};
      EXPECT_EQ(ApiFileError::Success, _provider->GetFileSystemItem("/fallocate_file.txt", false, fsi));

      fileSize = 0;
      EXPECT_TRUE(Utils::File::GetSize(fsi.SourceFilePath, fileSize));
      EXPECT_EQ(16, fileSize);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}
#endif

#if !__APPLE__ && HAS_SETXATTR
TEST_F(FUSETest, CreateAndGetXAttr) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), XATTR_CREATE));

      std::string val;
      val.resize(attr.size());
      EXPECT_EQ(attr.size(), getxattr(&file[0], "user.test_attr", &val[0], val.size()));
      EXPECT_STREQ(&attr[0], &val[0]);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, ReplaceXAttr) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), XATTR_CREATE));

      attr = "cow";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), XATTR_REPLACE));

      std::string val;
      val.resize(attr.size());
      EXPECT_EQ(attr.size(), getxattr(&file[0], "user.test_attr", &val[0], val.size()));
      EXPECT_STREQ(&attr[0], &val[0]);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, DefaultCreateXAttr) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), 0));

      std::string val;
      val.resize(attr.size());
      EXPECT_EQ(attr.size(), getxattr(&file[0], "user.test_attr", &val[0], val.size()));
      EXPECT_STREQ(&attr[0], &val[0]);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, DefaultReplaceXAttr) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), 0));

      attr = "cow";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), 0));

      std::string val;
      val.resize(attr.size());
      EXPECT_EQ(attr.size(), getxattr(&file[0], "user.test_attr", &val[0], val.size()));
      EXPECT_STREQ(&attr[0], &val[0]);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, XAttrCreateFailsIfExists) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), 0));
      EXPECT_EQ(-1, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), XATTR_CREATE));
      EXPECT_EQ(EEXIST, errno);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, XAttrReplaceFailsIfNotExists) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(-1, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), XATTR_REPLACE));
      EXPECT_EQ(ENODATA, errno);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

TEST_F(FUSETest, RemoveXAttr) {
  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("post_params", "/renter/upload/xattr_file.txt", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/file/xattr_file.txt", RENTER_FILE_DATA("xattr_file.txt", 0), {}, ApiError::Success, true);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto file = Utils::Path::Combine(mountPoint, "xattr_file.txt");
      auto fd = open(&file[0], O_CREAT | O_RDWR, S_IRUSR | S_IWUSR | S_IRGRP);
      EXPECT_LE(1, fd);
      EXPECT_EQ(0, close(fd));

      std::string attr = "moose";
      EXPECT_EQ(0, setxattr(&file[0], "user.test_attr", &attr[0], attr.size(), XATTR_CREATE));

      EXPECT_EQ(0, removexattr(&file[0], "user.test_attr"));

      std::string val;
      val.resize(attr.size());
      EXPECT_EQ(-1, getxattr(&file[0], "user.test_attr", &val[0], val.size()));
      EXPECT_EQ(ENODATA, errno);
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}
#endif

TEST_F(FUSETest, RecreateDirAfterDelete) {
  _mockComm.PushReturn("post_params", "/renter/dir/rc_dir", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/dir/",  {{"directories", {{{"siapath", ""}, {"numfiles", 0}, {"numsubdirs", 0}, {"aggregatenumfiles", 1}}}}, {"files", {}}}, {}, ApiError::Success, true);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/file/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/._rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/file/._rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError, true);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {{"directories", {{{"siapath", "rc_dir"}, {"numfiles", 0}, {"numsubdirs", 0}}}}, {"files", {}}}, {}, ApiError::Success);

  _mockComm.PushReturn("post_params", "/renter/dir/rc_dir", {}, {}, ApiError::Success);
  _mockComm.PushReturn("post_params", "/renter/dir/rc_dir", {}, {}, ApiError::Success);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {}, {{"message", "no file known"}}, ApiError::CommApiError);
  _mockComm.PushReturn("get", "/renter/dir/rc_dir",  {{"directories", {{{"siapath", "rc_dir"}, {"numfiles", 0}, {"numsubdirs", 0}}}}, {"files", {}}}, {}, ApiError::Success, true);

  std::string mountPoint;
  const auto driveArgs = MountSetup(this, mountPoint);

  CEventCapture eventCapture({"DriveMounted"});
  std::thread th([&] {
    const auto mounted = eventCapture.WaitForEvent("DriveMounted");
    EXPECT_TRUE(mounted);
    if (mounted) {
      const auto newDir = Utils::Path::Combine(mountPoint, "rc_dir");
      EXPECT_EQ(0, mkdir(&newDir[0], S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP));

      EXPECT_TRUE(Utils::File::IsDirectory(newDir));
      EXPECT_EQ(0, rmdir(&newDir[0]));
      EXPECT_FALSE(Utils::File::IsDirectory(newDir));

      ApiMetaMap meta;
      EXPECT_EQ(ApiFileError::ItemNotFound, _provider->GetItemMeta("/rc_dir", meta));
      EXPECT_EQ(0, meta.size());

      EXPECT_EQ(0, mkdir(&newDir[0], S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP));
      EXPECT_TRUE(Utils::File::IsDirectory(newDir));
    }

    Unmount(mountPoint);
  });

  ExecuteMount(this, driveArgs, th);
}

#endif