#ifndef REPERTORY_FUSE_FIXTURE_H
#define REPERTORY_FUSE_FIXTURE_H

#ifndef _WIN32
#include <testcommon.h>
#include <config.h>
#include <drives/fusedrive/fusedrive.h>
#include <mocks/mockcomm.h>
#include <providers/sia/siaprovider.h>

namespace Repertory {
class FUSETest :
  public ::testing::Test {
  public:
    CMockComm _mockComm;
    std::unique_ptr<CConfig> _config;
    std::unique_ptr<CFuseDrive> _drive;
    std::unique_ptr<IProvider> _provider;

  protected:
    void SetUp() override {
      Utils::File::DeleteAsFile("./fuse_test/sia/files.db3");
      const auto path = Utils::Path::Absolute("./fuse_test/");
      system(("/bin/sh -c \'rm -rf \"" + path + "\"\'").c_str());
      _config = std::make_unique<CConfig>(ProviderType::Sia, "./fuse_test");
      _config->SetEnableDriveEvents(true);
      _config->SetEventLevel(EventLevel::Verbose);
      _config->SetAPIPort(11115);
      _provider = std::make_unique<CSiaProvider>(*_config, _mockComm);
      _drive = std::make_unique<CFuseDrive>(*_provider, *_config);
      EventSystem::Instance().Start();
    }

    void TearDown() override {
      _drive.reset();
      EventSystem::Instance().Stop();
      _provider.reset();
      _config.reset();
      Utils::File::DeleteAsFile("./fuse_test/sia/files.db3");
      const auto path = Utils::Path::Absolute("./fuse_test/");
      system(("/bin/sh -c \'rm -rf \"" + path + "\"\'").c_str());
    }
};
}
#endif
#endif //REPERTORY_FUSE_FIXTURE_H
