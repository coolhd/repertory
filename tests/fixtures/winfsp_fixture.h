#ifndef REPERTORY_WINFSP_FIXTURE_H
#define REPERTORY_WINFSP_FIXTURE_H

#ifdef _WIN32
#include <testcommon.h>
#include <config.h>
#include <drives/winfspdrive/winfspdrive.h>
#include <mocks/mockcomm.h>
#include <providers/sia/siaprovider.h>

namespace Repertory {
class WinFSPTest :
  public ::testing::Test {
  public:
    CMockComm _mockComm;
    std::unique_ptr<CConfig> _config;
    std::unique_ptr<CWinFSPDrive> _drive;
    std::unique_ptr<IProvider> _provider;

  protected:
    void SetUp() override {
      Utils::File::RecursiveDeleteDirectory("./winfsp_test");
      _config = std::make_unique<CConfig>(ProviderType::Sia, "./winfsp_test");
      _config->SetEnableDriveEvents(true);
      _config->SetEventLevel(EventLevel::Verbose);
      _config->SetAPIPort(11115);
      _provider = std::make_unique<CSiaProvider>(*_config, _mockComm);
      _drive = std::make_unique<CWinFSPDrive>(*_provider, *_config);
      EventSystem::Instance().Start();
    }

    void TearDown() override {
      _drive.reset();
      EventSystem::Instance().Stop();
      _provider.reset();
      _config.reset();
      Utils::File::RecursiveDeleteDirectory("./winfsp_test");
    }
};
}
#endif
#endif //REPERTORY_WINFSP_FIXTURE_H
