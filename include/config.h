#ifndef REPERTORY_CONFIG_H
#define REPERTORY_CONFIG_H

#include <common.h>
#include <events/eventsystem.h>
#include <utils/fileutils.h>
#include <utils/utils.h>

namespace Repertory {
class CConfig final {
  public:
    static inline std::string GetDefaultAgentName(const ProviderType &providerType) {
      static const std::vector<std::string> PROVIDER_AGENT_NAMES = {
        "Sia-Agent",
        "SiaPrime-Agent"
      };
      return PROVIDER_AGENT_NAMES[(int)providerType];
    }

    static inline std::uint16_t GetDefaultApiPort(const ProviderType &providerType) {
      static const std::vector<std::uint16_t> PROVIDER_API_PORTS = {
        9980u,
        4280u
      };
      return PROVIDER_API_PORTS[(int)providerType];
    }

    static inline std::string GetDefaultDataDirectory(const ProviderType& providerType) {
#ifdef _WIN32
      auto dataDirectory = Utils::Path::Combine(Utils::GetLocalAppDataDirectory(), std::string("/repertory/") + CConfig::GetProviderName(providerType));
#else
#ifdef __APPLE__
      auto dataDirectory = Utils::Path::Resolve(std::string("~/Library/Application Support/repertory/") + CConfig::GetProviderName(providerType));
#else
      auto dataDirectory = Utils::Path::Resolve(std::string("~/.local/repertory/") + CConfig::GetProviderName(providerType));
#endif
#endif
      return std::move(dataDirectory);
    }

    static inline std::uint16_t GetDefaultRPCPort(const ProviderType &providerType) {
      static const std::vector<std::uint16_t> PROVIDER_RPC_PORTS = {
        11101u,
        11102u
      };
      return PROVIDER_RPC_PORTS[(int)providerType];
    }

    static inline std::string GetProviderAPIPassword(const ProviderType& providerType) {
#ifdef _WIN32
      const auto apiFile = Utils::Path::Combine(Utils::Path::Combine(Utils::GetLocalAppDataDirectory(), GetProviderDisplayName(providerType)), "apipassword");
#else
#ifdef __APPLE__
      const auto apiFile = Utils::Path::Combine(Utils::Path::Resolve(std::string("~/Library/Application Support/") + GetProviderDisplayName(providerType)), "apipassword");
#else
      const auto apiFile = Utils::Path::Combine(Utils::Path::Resolve(std::string("~/.") + GetProviderName(providerType)), "apipassword");
#endif
#endif
      auto lines = Utils::File::ReadLines(apiFile);
      return lines.empty() ? "" : Utils::String::Trim(lines[0]);
    }

    static inline std::string GetProviderDisplayName(const ProviderType& providerType) {
      static const std::vector<std::string> PROVIDER_DISPLAY_NAMES = {
        "Sia",
        "SiaPrime"
      };
      return PROVIDER_DISPLAY_NAMES[(int)providerType];
    }

    static inline std::string GetProviderMinimumVersion(const ProviderType& providerType) {
      static const std::vector<std::string> PROVIDER_MIN_VERSIONS = {
        MIN_SIA_VERSION,
        MIN_SP_VERSION
      };
      return PROVIDER_MIN_VERSIONS[(int)providerType];
    }

    static inline std::string GetProviderName(const ProviderType& providerType) {
      static const std::vector<std::string> PROVIDER_NAMES = {
        "sia",
        "siaprime"
      };
      return PROVIDER_NAMES[(int)providerType];
    }

    static inline std::string GetProviderPathName(const ProviderType& providerType) {
      static const std::vector<std::string> PROVIDER_PATH_NAMES = {
        "siapath",
        "siapath"
      };
      return PROVIDER_PATH_NAMES[(int)providerType];
    }

  public:
    explicit CConfig(const ProviderType &providerType, const std::string& dataDir = "") :
      _providerType(providerType),
      _apiAuth(Utils::GenerateRandomString(48)),
      _apiPort(GetDefaultRPCPort(providerType)),
      _apiUser("repertory"),
      _chunkSize(4096),
      _configChanged(false),
      _dataDirectory(dataDir.empty() ? GetDefaultDataDirectory(providerType) : Utils::Path::Absolute(Utils::Path::Combine(dataDir, GetProviderName(providerType)))),
      _downloadTimeoutSeconds(30),
      _enableChunkDownloaderTimeout(true),
#ifdef _DEBUG
      _enableDriveEvents(true),
#else
      _enableDriveEvents(false),
#endif
      _enableMaxCacheSize(true),
#ifdef _DEBUG
      _eventLevel(EventLevel::Debug),
#else
      _eventLevel(EventLevel::Normal),
#endif
      _evictionDelayMinutes(30),
      _highFreqIntervalSeconds(30),
      _lowFreqIntervalSeconds(60 * 15),
      _maxCacheSizeBytes(20 * 1024 * 1024 * 1024ULL),
      _minDownloadTimeoutSeconds(5),
      _minimumRedundancy(2.5),
      _onlineCheckRetrySeconds(60),
      _orphanedFileRetentionDays(15),
      _preferredDownloadType(Utils::DownloadTypeToString(DownloadType::Fallback)),
      _readAheadCount(4),
      _ringBufferFileSize(512),
      _storageByteMonth("0") {
      _cacheDirectory = Utils::Path::Combine(_dataDirectory, "cache");
      _logDirectory = Utils::Path::Combine(_dataDirectory, "logs");

      _hostConfig.AgentString  = GetDefaultAgentName(_providerType);
      _hostConfig.ApiPassword  = GetProviderAPIPassword(_providerType);
      _hostConfig.ApiPort      = GetDefaultApiPort(_providerType);
      _hostConfig.HostNameOrIp = "localhost";
      _hostConfig.TimeoutMs    = 60000;

      if (not Utils::File::CreateFullDirectoryPath(_dataDirectory))
        throw StartupException("Unable to create: " + _dataDirectory);

      if (not Utils::File::CreateFullDirectoryPath(_cacheDirectory))
        throw StartupException("Unable to create: " + _cacheDirectory);

      if (not Utils::File::CreateFullDirectoryPath(_logDirectory))
        throw StartupException("Unable to create: " + _logDirectory);

      if (not LoadConfiguration()) {
        SaveConfiguration();
      }
    }

    ~CConfig() {
      SaveConfiguration();
    }

  public:
    static json CreateTemplate(const ProviderType& providerType) {
      return {
        {"ApiAuth", {{"type", "string"}}},
        {"ApiPort", {{"type", "uint16"}}},
        {"ApiUser", {{"type", "string"}}},
        {"ChunkDownloaderTimeoutSeconds", {{"type", "uint8"}}},
        {"ChunkSize", {{"type", "list"}, {"items", {"8", "16", "32", "64", "128", "256", "512", "1024", "2048", "3072", "4096", "5120", "6144", "7168", "8192"}}}},
        {"EnableChunkDownloaderTimeout", {{"type", "bool"}}},
        {"EnableDriveEvents", {{"type", "bool"}}},
        {"EnableMaxCacheSize", {{"type", "bool"}}},
        {"EventLevel", {{"type", "list"}, {"items", {"Error", "Warn", "Normal", "Debug", "Verbose"}}}},
        {"EvictionDelayMinutes", {{"type", "uint32"}}},
        {"HighFreqIntervalSeconds", {{"type", "uint8"}, {"advanced", true}}},
        {"HostConfig", {
          {"type", "object"},
          {"template", {
            {"AgentString", {{"type", "string"}}},
            {"ApiPassword", {{"type", "string"}}},
            {"ApiPort", {{"type", "uint16"}}},
            {"HostNameOrIp", {{"type", "string"}}},
            {"TimeoutMs", {{"type", "uint32"}}},
          }}
        }},
        {"LowFreqIntervalSeconds", {{"type", "uint32"}, {"advanced", true}}},
        {"MaxCacheSizeBytes", {{"type", "uint64"}}},
        {"MinimumRedundancy", {{"type", "double"}}},
        {"OnlineCheckRetrySeconds", {{"type", "uint16"}}},
        {"OrphanedFileRetentionDays", {{"type", "list"}, {"items", {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"}}}},
        {"PreferredDownloadType", {{"type", "list"}, {"items", {"Fallback", "RingBuffer", "Direct"}}}},
        {"ReadAheadCount", {{"type", "uint8"}}},
        {"RingBufferFileSize", {{"type", "list"}, {"items", {"64", "128", "256", "512", "1024"}}}},
      };
    }

  private:
    const ProviderType _providerType;
    std::string _apiAuth;
    std::uint16_t _apiPort;
    std::string _apiUser;
    std::uint16_t _chunkSize;
    bool _configChanged;
    const std::string _dataDirectory;
    std::uint8_t _downloadTimeoutSeconds;
    bool _enableChunkDownloaderTimeout;
    bool _enableDriveEvents;
    bool _enableMaxCacheSize;
    EventLevel _eventLevel;
    std::uint32_t _evictionDelayMinutes;
    std::uint8_t _highFreqIntervalSeconds;
    std::uint32_t _lowFreqIntervalSeconds;
    std::uint64_t _maxCacheSizeBytes;
    std::uint8_t _minDownloadTimeoutSeconds;
    double _minimumRedundancy;
    std::uint16_t _onlineCheckRetrySeconds;
    std::uint16_t _orphanedFileRetentionDays;
    std::string _preferredDownloadType;
    std::uint8_t _readAheadCount;
    std::uint16_t _ringBufferFileSize;
    ApiCurrency _storageByteMonth;
    std::uint64_t _version = REPERTORY_CONFIG_VERSION;
    std::string _cacheDirectory;
    HostConfig _hostConfig;
    std::string _logDirectory;
    std::recursive_mutex _readWriteMutex;

  private:
    bool LoadConfiguration() {
      auto ret = false;

      const auto configFilePath = GetConfigFilePath();
      RMutexLock l(_readWriteMutex);
      if (Utils::File::IsFile(configFilePath)) {
        try {
          std::ifstream configFile(&configFilePath[0]);
          if (configFile.is_open()) {
            std::stringstream ss;
            ss << configFile.rdbuf();
            const auto jsonTxt = ss.str();
            configFile.close();
            if ((ret = not jsonTxt.empty())) {
              const auto jsonDoc = json::parse(jsonTxt);

              StandardGetValue(jsonDoc, "ApiAuth", _apiAuth, ret);
              StandardGetValue(jsonDoc, "ApiPort", _apiPort, ret);
              StandardGetValue(jsonDoc, "ApiUser", _apiUser, ret);
              StandardGetValue(jsonDoc, "ChunkDownloaderTimeoutSeconds", _downloadTimeoutSeconds, ret);
              StandardGetValue(jsonDoc, "ChunkSize", _chunkSize, ret);
              StandardGetValue(jsonDoc, "EvictionDelayMinutes", _evictionDelayMinutes, ret);
              StandardGetValue(jsonDoc, "EnableChunkDownloaderTimeout", _enableChunkDownloaderTimeout, ret);
              StandardGetValue(jsonDoc, "EnableDriveEvents", _enableDriveEvents, ret);
              std::string eventLevel;
              if (StandardGetValue(jsonDoc, "EventLevel", eventLevel, ret)) {
                _eventLevel = EventLevelFromString(eventLevel);
              }

              if (jsonDoc.find("HostConfig") != jsonDoc.end()) {
                auto hostConfig = jsonDoc["HostConfig"];
                auto hc = _hostConfig;
                StandardGetValue(hostConfig, "AgentString", hc.AgentString, ret);
                StandardGetValue(hostConfig, "ApiPassword", hc.ApiPassword, ret);
                StandardGetValue(hostConfig, "ApiPort", hc.ApiPort, ret);
                StandardGetValue(hostConfig, "HostNameOrIp", hc.HostNameOrIp, ret);
                StandardGetValue(hostConfig, "TimeoutMs", hc.TimeoutMs, ret);
                _hostConfig = hc;
              } else {
                ret = false;
              }

              if (_hostConfig.ApiPassword.empty()) {
                _hostConfig.ApiPassword = GetProviderAPIPassword(_providerType);
                if (_hostConfig.ApiPassword.empty()) {
                  ret = false;
                }
              }

              StandardGetValue(jsonDoc, "ReadAheadCount", _readAheadCount, ret);
              StandardGetValue(jsonDoc, "RingBufferFileSize", _ringBufferFileSize, ret);
              StandardGetValue(jsonDoc, "MinimumRedundancy", _minimumRedundancy, ret);
              StandardGetValue(jsonDoc, "EnableMaxCacheSize", _enableMaxCacheSize, ret);
              StandardGetValue(jsonDoc, "MaxCacheSizeBytes", _maxCacheSizeBytes, ret);
              StandardGetValue(jsonDoc, "OnlineCheckRetrySeconds", _onlineCheckRetrySeconds, ret);
              StandardGetValue(jsonDoc, "HighFreqIntervalSeconds", _highFreqIntervalSeconds, ret);
              StandardGetValue(jsonDoc, "LowFreqIntervalSeconds", _lowFreqIntervalSeconds, ret);
              StandardGetValue(jsonDoc, "OrphanedFileRetentionDays", _orphanedFileRetentionDays, ret);
              StandardGetValue(jsonDoc, "PreferredDownloadType", _preferredDownloadType, ret);
              std::string storageByteMonth;
              if (StandardGetValue(jsonDoc, "StorageByteMonth", storageByteMonth, ret)) {
                _storageByteMonth = storageByteMonth;
              }
              std::uint64_t version = 0;
              StandardGetValue(jsonDoc, "Version", version, ret);
              // Handle configuration defaults for new config versions
              if (version != REPERTORY_CONFIG_VERSION) {
                if (version > REPERTORY_CONFIG_VERSION) {
                  version = 0;
                }

                do {
                  switch (version) {
                  case 0: {
                    _chunkSize = 2048u;
                    _readAheadCount = 5u;
                    Utils::File::RecursiveDeleteDirectory(Utils::Path::Combine(GetDataDirectory(), "meta_rdb"));
                    Utils::File::RecursiveDeleteDirectory(Utils::Path::Combine(GetDataDirectory(), "meta_rdb2"));
                  }
                  break;

                  case 1: {
                    _enableChunkDownloaderTimeout = true;
                    _hostConfig.TimeoutMs = 60000;
                  }
                  break;

                  case 2: {
                    std::uint32_t oldDelay = 0;
                    auto ignore = true;
                    if (StandardGetValue(jsonDoc, "EvictionDelaySeconds", oldDelay, ignore)) {
                      _evictionDelayMinutes = oldDelay / 60u;
                    }
                  }
                  break;

                  case 3: {
                    if (_chunkSize == 2048u) {
                      _chunkSize = 4096u;
                    }

                    if (_readAheadCount == 5) {
                      _readAheadCount = 4;
                    }
                  }
                  break;

                  default:
                    break;
                  }
                } while (++version != REPERTORY_CONFIG_VERSION);

                _version = version;
                ret = false;
              }
            }
          }

          if (not ret) {
            _configChanged = true;
          }
        } catch (const std::exception& ex) {
          EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, ex.what());
          ret = false;
        }
      }

      return ret;
    }

    template<typename Dest>
    bool StandardGetValue(const json& jsonDoc, const std::string& name, Dest& dest, bool& successFlag) {
      auto ret = false;
      try {
        if (jsonDoc.find(name) != jsonDoc.end()) {
          dest = jsonDoc[name].get<Dest>();
          ret = true;
        } else {
          successFlag = false;
        }
      } catch (const json::exception& ex) {
        EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, ex.what());
        successFlag = false;
        ret = false;
      }

      return ret;
    }

    template <typename Dest, typename Source>
    bool StandardSetValue(Dest &dest, const Source &src) {
      auto ret = false;
      RMutexLock l(_readWriteMutex);
      if (dest != src) {
        dest = src;
        _configChanged = true;
        SaveConfiguration();
        ret = true;
      }

      return ret;
    }

  public:
    inline std::string GetAPIAuth() const {
      return _apiAuth;
    }

    inline std::uint16_t GetAPIPort() const {
      return _apiPort;
    }

    inline std::string GetAPIUser() const {
      return _apiUser;
    }

    inline std::string GetCacheDirectory() const {
      return _cacheDirectory;
    }

    inline std::uint8_t GetChunkDownloaderTimeoutSeconds() const {
      return std::max(_minDownloadTimeoutSeconds, _downloadTimeoutSeconds);
    }

    inline std::uint64_t GetChunkSize() const {
      return std::max((std::uint16_t)8, (std::uint16_t)((_chunkSize / 8u) * 8u));
    }

    inline std::string GetConfigFilePath() const {
      const auto configFilePath = Utils::Path::Combine(_dataDirectory, "config.json");
      return configFilePath;
    }

    inline std::string GetDataDirectory() const {
      return _dataDirectory;
    }

    inline bool GetEnableChunkDownloaderTimeout() const {
      return _enableChunkDownloaderTimeout;
    }

    inline bool GetEnableDriveEvents() const {
      return _enableDriveEvents;
    }

    inline bool GetEnableMaxCacheSize() const {
      return _enableMaxCacheSize;
    }

    inline EventLevel GetEventLevel() const {
      return _eventLevel;
    }

    inline std::uint32_t GetEvictionDelayMinutes() const {
      return _evictionDelayMinutes;
    }

    inline std::uint8_t GetHighFreqIntervalSeconds() const {
      return std::max((std::uint8_t)1, _highFreqIntervalSeconds);
    }

    inline HostConfig GetHostConfig() const {
      return _hostConfig;
    }

    inline json GetJson() const {
      return {
        {"ApiAuth", _apiAuth},
        {"ApiPort", _apiPort},
        {"ApiUser", _apiUser},
        {"ChunkDownloaderTimeoutSeconds", _downloadTimeoutSeconds},
        {"ChunkSize", _chunkSize},
        {"EnableChunkDownloaderTimeout", _enableChunkDownloaderTimeout},
        {"EnableDriveEvents", _enableDriveEvents},
        {"EnableMaxCacheSize", _enableMaxCacheSize},
        {"EventLevel", EventLevelToString(_eventLevel)},
        {"EvictionDelayMinutes", _evictionDelayMinutes},
        {"HighFreqIntervalSeconds", _highFreqIntervalSeconds},
        {"HostConfig", {
          {"AgentString", _hostConfig.AgentString},
          {"ApiPassword", _hostConfig.ApiPassword},
          {"ApiPort", _hostConfig.ApiPort},
          {"HostNameOrIp", _hostConfig.HostNameOrIp},
          {"TimeoutMs", _hostConfig.TimeoutMs}}
        },
        {"LowFreqIntervalSeconds", _lowFreqIntervalSeconds},
        {"MaxCacheSizeBytes", _maxCacheSizeBytes},
        {"MinimumRedundancy", _minimumRedundancy},
        {"OnlineCheckRetrySeconds", _onlineCheckRetrySeconds},
        {"OrphanedFileRetentionDays", _orphanedFileRetentionDays},
        {"PreferredDownloadType", _preferredDownloadType},
        {"ReadAheadCount", _readAheadCount},
        {"RingBufferFileSize", _ringBufferFileSize},
        {"StorageByteMonth", _storageByteMonth.ToString()},
        {"Version", _version}
      };
    }

    inline std::string GetLogDirectory() const {
      return _logDirectory;
    }

    inline std::uint32_t GetLowFreqIntervalSeconds() const {
      return std::max((std::uint32_t)1, _lowFreqIntervalSeconds);
    }

    inline std::uint64_t GetMaxCacheSizeBytes() const {
      const auto maxSpace = std::max((std::uint64_t)100*1024*1024, _maxCacheSizeBytes);
      return std::min(Utils::File::GetAvailableDriveSpace(GetCacheDirectory()), maxSpace);
    }

    inline double GetMinimumRedundancy() const {
      return std::max(1.5, _minimumRedundancy);
    }

    inline std::uint16_t GetOnlineCheckRetrySeconds() const {
      return std::max(std::uint16_t(15), _onlineCheckRetrySeconds);
    }

    inline std::uint16_t GetOrphanedFileRetentionDays() const {
      return std::min((std::uint16_t)31, std::max((std::uint16_t)1, _orphanedFileRetentionDays));
    }

    inline DownloadType GetPreferredDownloadType() const {
      return Utils::DownloadTypeFromString(_preferredDownloadType, DownloadType::Fallback);
    }

    inline ProviderType GetProviderType() const {
      return _providerType;
    }

    inline std::uint8_t GetReadAheadCount() const {
      return std::max((std::uint8_t)1, _readAheadCount);
    }

    inline std::uint16_t GetRingBufferFileSize() const {
      return std::max((std::uint16_t)64, std::min((std::uint16_t)1024, _ringBufferFileSize));
    }

    inline ApiCurrency GetStorageByteMonth() const {
      return _storageByteMonth;
    }

    std::string GetValueByName(const std::string& name) {
      try {
        if (name == "ApiAuth") {
          return _apiAuth;
        } else if (name == "ApiPort") {
          return std::to_string(GetAPIPort());
        } else if (name == "ApiUser") {
          return _apiUser;
        } else if (name == "ChunkDownloaderTimeoutSeconds") {
          return std::to_string(GetChunkDownloaderTimeoutSeconds());
        } else if (name == "ChunkSize") {
          return std::to_string(GetChunkSize());
        } else if (name == "EnableChunkDownloaderTimeout") {
          return std::to_string(GetEnableChunkDownloaderTimeout());
        } else if (name == "EnableDriveEvents") {
          return std::to_string(GetEnableDriveEvents());
        } else if (name == "EnableMaxCacheSize") {
          return std::to_string(GetEnableMaxCacheSize());
        } else if (name == "EventLevel") {
          return EventLevelToString(GetEventLevel());
        } else if (name == "EvictionDelayMinutes") {
          return std::to_string(GetEvictionDelayMinutes());
        } else if (name == "HighFreqIntervalSeconds") {
          return std::to_string(GetHighFreqIntervalSeconds());
        } else if (name == "HostConfig.AgentString") {
          return _hostConfig.AgentString;
        } else if (name == "HostConfig.ApiPassword") {
          return _hostConfig.ApiPassword;
        } else if (name == "HostConfig.ApiPort") {
          return std::to_string(_hostConfig.ApiPort);
        } else if (name == "HostConfig.HostNameOrIp") {
          return _hostConfig.HostNameOrIp;
        } else if (name == "HostConfig.TimeoutMs") {
          return std::to_string(_hostConfig.TimeoutMs);
        } else if (name == "LowFreqIntervalSeconds") {
          return std::to_string(GetLowFreqIntervalSeconds());
        } else if (name == "MaxCacheSizeBytes") {
          return std::to_string(GetMaxCacheSizeBytes());
        } else if (name == "MinimumRedundancy") {
          return std::to_string(GetMinimumRedundancy());
        } else if (name == "OnlineCheckRetrySeconds") {
          return std::to_string(GetOnlineCheckRetrySeconds());
        } else if (name == "OrphanedFileRetentionDays") {
          return std::to_string(GetOrphanedFileRetentionDays());
        } else if (name == "PreferredDownloadType") {
          return Utils::DownloadTypeToString(Utils::DownloadTypeFromString(_preferredDownloadType, DownloadType::Fallback));
        }else if (name == "ReadAheadCount") {
          return std::to_string(GetReadAheadCount());
        } else if (name == "RingBufferFileSize") {
          return std::to_string(GetRingBufferFileSize());
        }
      } catch (const std::exception&) {

      }
      return "";
    }

    inline std::uint64_t GetVersion() const {
      return _version;
    }

    void SaveConfiguration() {
      const auto configFilePath = GetConfigFilePath();
      RMutexLock l(_readWriteMutex);
      if (_configChanged || not Utils::File::IsFile(configFilePath)) {
        if (not Utils::File::IsDirectory(_dataDirectory)) {
          Utils::File::CreateFullDirectoryPath(_dataDirectory);
        }
        _configChanged = false;
        json data = GetJson();
        auto success = false;
        for (auto i = 0; not success && (i < 5); i++) {
          if (not(success = Utils::File::WriteJsonToFile(configFilePath, data))) {
            std::this_thread::sleep_for(1s);
          }
        }
      }
    }

    inline void SetAPIAuth(const std::string &apiAuth) {
      StandardSetValue(_apiAuth, apiAuth);
    }

    inline void SetAPIPort(const std::uint16_t &apiPort) {
      StandardSetValue(_apiPort, apiPort);
    }

    inline void SetAPIUser(const std::string &apiUser) {
      StandardSetValue(_apiUser, apiUser);
    }

    inline void SetChunkDownloaderTimeoutSeconds(const std::uint8_t& downloadTimeoutSeconds) {
      StandardSetValue(_downloadTimeoutSeconds, downloadTimeoutSeconds);
    }

    inline void SetChunkSize(const std::uint16_t& chunkSize) {
      StandardSetValue(_chunkSize, chunkSize);
    }

    inline void SetEnableChunkDownloaderTimeout(const bool& enableChunkDownloaderTimeout) {
      StandardSetValue(_enableChunkDownloaderTimeout, enableChunkDownloaderTimeout);
    }

    inline void SetEnableDriveEvents(const bool& enableDriveEvents) {
      StandardSetValue(_enableDriveEvents, enableDriveEvents);
    }

    inline void SetEnableMaxCacheSize(const bool& enableMaxCacheSize) {
      StandardSetValue(_enableMaxCacheSize, enableMaxCacheSize);
    }

    inline void SetEventLevel(const EventLevel& eventLevel) {
      if (StandardSetValue(_eventLevel, eventLevel)) {
        EventSystem::Instance().Raise<EventLevelChanged>(EventLevelToString(eventLevel));
      }
    }

    inline void SetEvictionDelayMinutes(const std::uint32_t& evictionDelayMinutes) {
      StandardSetValue(_evictionDelayMinutes, evictionDelayMinutes);
    }

    inline void SetHighFreqIntervalSeconds(const std::uint8_t& intervalSeconds) {
      StandardSetValue(_highFreqIntervalSeconds, intervalSeconds);
    }

    inline void SetLowFreqIntervalSeconds(const std::uint32_t& intervalSeconds) {
      StandardSetValue(_lowFreqIntervalSeconds, intervalSeconds);
    }

    inline void SetMaxCacheSizeBytes(const std::uint64_t& maxCacheSizeBytes) {
      StandardSetValue(_maxCacheSizeBytes, maxCacheSizeBytes);
    }

    inline void SetMinimumRedundancy(const double& minimumRedundancy) {
      StandardSetValue(_minimumRedundancy, minimumRedundancy);
    }

    inline void SetOnlineCheckRetrySeconds(const std::uint16_t& onlineCheckRetrySeconds) {
      StandardSetValue(_onlineCheckRetrySeconds, onlineCheckRetrySeconds);
    }

    inline void SetOrphanedFileRetentionDays(const std::uint16_t& days) {
      StandardSetValue(_orphanedFileRetentionDays, days);
    }

    inline void SetPreferredDownloadType(const DownloadType& downloadType) {
      StandardSetValue(_preferredDownloadType, Utils::DownloadTypeToString(downloadType));
    }

    inline void SetReadAheadCount(const std::uint8_t& readAheadCount) {
      StandardSetValue(_readAheadCount, readAheadCount);
    }

    inline void SetRingBufferFileSize(const std::uint16_t& ringBufferFileSize) {
      StandardSetValue(_ringBufferFileSize, ringBufferFileSize);
    }

    inline void SetStorageByteMonth(const ApiCurrency& storageByteMonth) {
      if ((storageByteMonth > 0) && (_storageByteMonth != storageByteMonth)) {
        _storageByteMonth = storageByteMonth;
        _configChanged = true;
      }
    }

    std::string SetValueByName(const std::string& name, const std::string& value) {
      try {
        if (name == "ApiAuth") {
          SetAPIAuth(value);
          return GetAPIAuth();
        } else if (name == "ApiPort") {
          SetAPIPort(Utils::String::ToUInt8(value));
          return std::to_string(GetAPIPort());
        } else if (name == "ApiUser") {
          SetAPIUser(value);
          return GetAPIUser();
        } else if (name == "ChunkDownloaderTimeoutSeconds") {
          SetChunkDownloaderTimeoutSeconds(Utils::String::ToUInt8(value));
          return std::to_string(GetChunkDownloaderTimeoutSeconds());
        } else if (name == "ChunkSize") {
          SetChunkSize(Utils::String::ToUInt16(value));
          return std::to_string(GetChunkSize());
        } else if (name == "EnableChunkDownloaderTimeout") {
          SetEnableChunkDownloaderTimeout(Utils::String::ToBool(value));
          return std::to_string(GetEnableChunkDownloaderTimeout());
        } else if (name == "EnableDriveEvents") {
          SetEnableDriveEvents(Utils::String::ToBool(value));
          return std::to_string(GetEnableDriveEvents());
        } else if (name == "EnableMaxCacheSize") {
          SetEnableMaxCacheSize(Utils::String::ToBool(value));
          return std::to_string(GetEnableMaxCacheSize());
        } else if (name == "EventLevel") {
          SetEventLevel(EventLevelFromString(value));
          return EventLevelToString(GetEventLevel());
        } else if (name == "EvictionDelayMinutes") {
          SetEvictionDelayMinutes(Utils::String::ToUInt32(value));
          return std::to_string(GetEvictionDelayMinutes());
        } else if (name == "HighFreqIntervalSeconds") {
          SetHighFreqIntervalSeconds(Utils::String::ToUInt8(value));
          return std::to_string(GetHighFreqIntervalSeconds());
        } else if (name == "HostConfig.AgentString") {
          StandardSetValue(_hostConfig.AgentString, value);
          return _hostConfig.AgentString;
        } else if (name == "HostConfig.ApiPassword") {
          StandardSetValue(_hostConfig.ApiPassword, value);
          return _hostConfig.ApiPassword;
        } else if (name == "HostConfig.ApiPort") {
          StandardSetValue(_hostConfig.ApiPort, Utils::String::ToUInt16(value));
          return std::to_string(_hostConfig.ApiPort);
        } else if (name == "HostConfig.HostNameOrIp") {
          StandardSetValue(_hostConfig.HostNameOrIp, value);
          return _hostConfig.HostNameOrIp;
        } else if (name == "HostConfig.TimeoutMs") {
          StandardSetValue(_hostConfig.TimeoutMs, Utils::String::ToUInt32(value));
          return std::to_string(_hostConfig.TimeoutMs);
        } else if (name == "LowFreqIntervalSeconds") {
          SetLowFreqIntervalSeconds(Utils::String::ToUInt32(value));
          return std::to_string(GetLowFreqIntervalSeconds());
        } else if (name == "MaxCacheSizeBytes") {
          SetMaxCacheSizeBytes(Utils::String::ToUInt64(value));
          return std::to_string(GetMaxCacheSizeBytes());
        } else if (name == "MinimumRedundancy") {
          SetMinimumRedundancy(Utils::String::ToDouble(value));
          return std::to_string(GetMinimumRedundancy());
        } else if (name == "OnlineCheckRetrySeconds") {
          SetOnlineCheckRetrySeconds(Utils::String::ToUInt16(value));
          return std::to_string(GetOnlineCheckRetrySeconds());
        } else if (name == "OrphanedFileRetentionDays") {
          SetOrphanedFileRetentionDays(Utils::String::ToUInt16(value));
          return std::to_string(GetOrphanedFileRetentionDays());
        } else if (name == "PreferredDownloadType") {
          SetPreferredDownloadType(Utils::DownloadTypeFromString(value, DownloadType::Fallback));
          return Utils::DownloadTypeToString(GetPreferredDownloadType());
        } else if (name == "ReadAheadCount") {
          SetReadAheadCount(Utils::String::ToUInt8(value));
          return std::to_string(GetReadAheadCount());
        } else if (name == "RingBufferFileSize") {
          SetRingBufferFileSize(Utils::String::ToUInt16(value));
          return std::to_string(GetRingBufferFileSize());
        }
      } catch (const std::exception&) {

      }
      return "";
    }
};
}
#endif //REPERTORY_CONFIG_H
