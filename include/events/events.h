#ifndef REPERTORY_EVENTS_H
#define REPERTORY_EVENTS_H

#include <common.h>
#include <curl/curl.h>
#include <utils/utils.h>

namespace Repertory {
E_SIMPLE3(CacheFileProcessed, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING,
  ApiFileError, Result, RES, E_FROM_API_FILE_ERROR
);

E_SIMPLE1(CommGetBegin, Verbose, true,
  std::string, URL, URL, E_STRING
);

E_SIMPLE4(CommGetEnd, Verbose, true,
  std::string, URL, URL, E_STRING,
  CURLcode, CURL, CURL, E_FROM_INT32,
  int, HTTP, HTTP, E_FROM_INT32,
  std::string, Result, RES, E_STRING
);

E_SIMPLE1(CommGetRawBegin, Verbose, true,
  std::string, URL, URL, E_STRING
);

E_SIMPLE4(CommGetRawEnd, Verbose, true,
  std::string, URL, URL, E_STRING,
  CURLcode, CURL, CURL, E_FROM_INT32,
  int, HTTP, HTTP, E_FROM_INT32,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE2(CommPostBegin, Verbose, true,
  std::string, URL, URL, E_STRING,
  std::string, Fields, FIELDS, E_STRING
);

E_SIMPLE4(CommPostEnd, Verbose, true,
  std::string, URL, URL, E_STRING,
  CURLcode, CURL, CURL, E_FROM_INT32,
  int, HTTP, HTTP, E_FROM_INT32,
  std::string, Result, RES, E_STRING
);

E_SIMPLE1(CommPostFileBegin, Verbose, true,
  std::string, URL, URL, E_STRING
);

E_SIMPLE4(CommPostFileEnd, Verbose, true,
  std::string, URL, URL, E_STRING,
  CURLcode, CURL, CURL, E_FROM_INT32,
  int, HTTP, HTTP, E_FROM_INT32,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE1(DirectoryRemoved, Normal, true,
  std::string, ApiFilePath, AP, E_STRING
);

E_SIMPLE2(DirectoryRemoveFailed, Error, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE2(DriveMountFailed, Error, true,
  std::string, Location, LOC, E_STRING,
  std::string, Result, RES, E_STRING
);

E_SIMPLE1(DriveMounted, Normal, true,
  std::string, Location, LOC, E_STRING
);

E_SIMPLE1(DriveMountResult, Normal, true,
  std::string, Result, RES, E_STRING
);

E_SIMPLE1(DriveUnMountPending, Normal, true,
  std::string, Location, LOC, E_STRING
);

E_SIMPLE1(DriveUnMounted, Normal, true,
  std::string, Location, LOC, E_STRING
);

E_SIMPLE1(EventLevelChanged, Normal, true,
  std::string, NewEventLevel, LEVEL, E_STRING
);

E_SIMPLE1(FailedUploadQueued, Error, true,
  std::string, ApiFilePath, AP, E_STRING
);

E_SIMPLE1(FailedUploadRemoved, Warn, true,
  std::string, ApiFilePath, AP, E_STRING
);

E_SIMPLE1(FailedUploadRetry, Normal, true,
  std::string, ApiFilePath, AP, E_STRING
);

E_SIMPLE1(FileGetApiListFailed, Error, true,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE3(FileReadBytesFailed, Error, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Error, ERR, E_STRING,
  std::uint8_t, Retry, RETRY, E_FROM_UINT8
);

E_SIMPLE1(FileRemoved, Normal, true,
  std::string, ApiFilePath, AP, E_STRING
);

E_SIMPLE2(FileRemovedExternally, Warn, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE2(FileRemoveFailed, Error, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE3(FileRenameFailed, Error, true,
  std::string, FromApiFilePath, FROM, E_STRING,
  std::string, ToApiFilePath, TO, E_STRING,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE2(FileSizeGetFailed, Error, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE3(FileSystemItemAdded, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Parent, PARENT, E_STRING,
  bool, Directory, DIR, E_FROM_BOOL
);

E_SIMPLE4(FileSystemItemClosed, Verbose, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING,
  bool, Directory, DIR, E_FROM_BOOL,
  bool, Changed, CHANGED, E_FROM_BOOL
);

E_SIMPLE5(FileSystemItemHandleClosed, Verbose, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::uint64_t, Handle, HANDLE, E_FROM_UINT64,
  std::string, Source, SRC, E_STRING,
  bool, Directory, DIR, E_FROM_BOOL,
  bool, Changed, CHANGED, E_FROM_BOOL
);


E_SIMPLE2(FileSystemItemEvicted, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE2(FileSystemItemGetFailed, Error, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE3(FileSystemItemOpened, Verbose, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING,
  bool, Directory, DIR, E_FROM_BOOL
);

E_SIMPLE2(FileUploadBegin, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE3(FileUploadEnd, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING,
  ApiFileError, Result, RES, E_FROM_API_FILE_ERROR
);

E_SIMPLE3(FileUploadFailed, Error, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING,
  std::string, Error, ERR, E_STRING
);

E_SIMPLE2(FileUploadQueued, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE2(FileUploadRetry, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE(ItemScanBegin, Normal, true);
E_SIMPLE(ItemScanEnd, Normal, true);

E_SIMPLE1(OrphanedFileDeleted, Warn, true,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE1(OrphanedFileDetected, Warn, true,
  std::string, Source, SRC, E_STRING
);

E_SIMPLE2(OrphanedFileProcessed, Warn, true,
  std::string, Source, SRC, E_STRING,
  std::string, Dest, DEST, E_STRING
);

E_SIMPLE3(OrphanedFileProcessingFailed, Error, true,
  std::string, Source, SRC, E_STRING,
  std::string, Dest, DEST, E_STRING,
  std::string, Result, RES, E_STRING
);

E_SIMPLE1(PollingItemBegin, Debug, true,
  std::string, ItemName, NAME, E_STRING
);

E_SIMPLE1(PollingItemEnd, Debug, true,
  std::string, ItemName, NAME, E_STRING
);

E_SIMPLE2(ProviderIsOffline, Error, true,
  std::string, HostNameOrIP, HOST, E_STRING,
  std::uint16_t, Port, PORT, E_FROM_UINT16
);

E_SIMPLE2(RepertoryException, Error, true,
  std::string, Function, FUNC, E_STRING,
  std::string, Message, MSG, E_STRING
);

E_SIMPLE1(RPCServerException, Error, true,
  std::string, Exception, EXCEPTION, E_STRING
);

E_SIMPLE(UnmountRequested, Normal, true);
#ifndef _WIN32
E_SIMPLE2(UnmountResult, Normal, true,
  std::string, Location, LOC, E_STRING,
  std::string, Result, RES, E_STRING
);
#endif
}
#endif //REPERTORY_EVENTS_H
