#ifndef REPERTORY_LOGGINGCONSUMER_H
#define REPERTORY_LOGGINGCONSUMER_H

#include <events/eventsystem.h>
#include <utils/fileutils.h>

namespace Repertory {
class CLoggingConsumer {
  E_CONSUMER();
  public:
    CLoggingConsumer(const std::string &logDirectory, const EventLevel &eventLevel) :
      _eventLevel(eventLevel),
      _logDirectory(Utils::Path::Finalize(logDirectory)),
      _logPath(Utils::Path::Combine(logDirectory, "repertory.log")) {
      Utils::File::CreateFullDirectoryPath(_logDirectory);
      CheckLogRoll(0);
      ReOpenLogFile();
      E_SUBSCRIBE_ALL(ProcessEvent);
      E_SUBSCRIBE_EXACT(EventLevelChanged, [this](const EventLevelChanged& e) {
        _eventLevel = EventLevelFromString(e.GetNewEventLevel().get<std::string>());
      });
      _loggingThread = std::make_unique<std::thread>([this]() {
        this->LoggingThread();
      });
    }

  public:
    ~CLoggingConsumer() {
      _eventConsumers.clear();
      {
        MutexLock l(_logMutex);
        _loggingActive = false;
        _logNotify.notify_all();
      }
      _loggingThread->join();
      _loggingThread.reset();
      LoggingThread(true);
      CloseLogFile();
    }

  private:
    const std::uint8_t MAX_LOG_FILES = 5;
    const std::uint64_t MAX_LOG_FILE_SIZE = (1024 * 1024 * 5);

  private:
    EventLevel _eventLevel = EventLevel::Normal;
    const std::string _logDirectory;
    const std::string _logPath;
    bool _loggingActive = true;
    std::mutex _logMutex;
    std::condition_variable _logNotify;
    std::deque<std::shared_ptr<Event>> _eventQueue;
    std::unique_ptr<std::thread> _loggingThread;
    FILE *_logFile = nullptr;

  private:
    void CheckLogRoll(const size_t &count) {
      std::uint64_t fileSize;
      const auto success = Utils::File::GetSize(_logPath, fileSize);
      if (success && (fileSize + count) >= MAX_LOG_FILE_SIZE) {
        CloseLogFile();
        for (std::uint8_t i = MAX_LOG_FILES; i > 0; i--) {
          const auto tempLogPath = Utils::Path::Combine(_logDirectory, "repertory." + Utils::String::FromUInt8(i) + ".log");
          if (Utils::File::IsFile(tempLogPath)) {
            if (i == MAX_LOG_FILES) {
              Utils::File::DeleteAsFile(tempLogPath);
            } else {
              const auto nextFp = Utils::Path::Combine(_logDirectory, "repertory." + Utils::String::FromUInt8(i + std::uint8_t(1)) + ".log");
              Utils::File::MoveAsFile(tempLogPath, nextFp);
            }
          }
        }
        Utils::File::MoveAsFile(_logPath, Utils::Path::Combine(_logDirectory, "repertory.1.log"));
        ReOpenLogFile();
      }
    }

    void CloseLogFile() {
      if (_logFile) {
        fclose(_logFile);
        _logFile = nullptr;
      }
    }

    void LoggingThread(const bool &drain = false) {
      do {
        std::deque<std::shared_ptr<Event>> events;
        {
          UniqueMutexLock l(_logMutex);
          if (_eventQueue.empty() && not drain) {
            _logNotify.wait_for(l, 2s);
          } else {
            events.insert(events.end(), _eventQueue.begin(), _eventQueue.end());
            _eventQueue.clear();
          }
        }

        while (not events.empty()) {
          auto event = events.front();
          events.pop_front();

          if (event->GetEventLevel() <= _eventLevel) {
            const std::string msg = ([&]() -> std::string {
              struct tm localTime{};
              Utils::GetLocalTime(localTime);

              std::stringstream ss;
              ss <<
                std::put_time(&localTime, "%F %T") <<
                "|" << EventLevelToString(event->GetEventLevel()).c_str() << "|" <<
                event->GetSingleLine().c_str() <<
                std::endl;
              return ss.str();
            })();

            CheckLogRoll(msg.length());
            auto retry = true;
            for (int i = 0; retry && (i < 2); i++) {
              retry = (not _logFile || (fwrite(&msg[0], 1, msg.length(), _logFile) != msg.length()));
              if (retry) {
                ReOpenLogFile();
              }
            }

            if (_logFile) {
              fflush(_logFile);
            }
          }
        }
      } while (_loggingActive);
    }

    void ProcessEvent(const Event &event) {
      {
        MutexLock l(_logMutex);
        _eventQueue.push_back(event.Clone());
        _logNotify.notify_all();
      }
    }

    void ReOpenLogFile() {
      CloseLogFile();
#ifdef _WIN32
      _logFile = _fsopen(&_logPath[0], "a+", _SH_DENYWR);
#else
      _logFile = fopen(&_logPath[0], "a+");
#endif
    }
};
}

#endif //REPERTORY_LOGGINGCONSUMER_H
