#ifndef REPERTORY_CONSOLECONSUMER_H
#define REPERTORY_CONSOLECONSUMER_H

#include <events/eventsystem.h>
#include <iostream>

namespace Repertory {
class CConsoleConsumer {
  E_CONSUMER();

  public:
    CConsoleConsumer() {
      E_SUBSCRIBE_ALL(ProcessEvent);
    }

  public:
    ~CConsoleConsumer() = default;

  private:
    void ProcessEvent(const Event &event) {
#ifdef _WIN32
#ifdef _DEBUG
      OutputDebugString((event.GetSingleLine() + "\n").c_str());
#endif
#endif
      if (event.GetEventLevel() == EventLevel::Error) {
        std::cerr << event.GetSingleLine() << std::endl;
      } else {
        std::cout << event.GetSingleLine() << std::endl;
      }
    }
};
}

#endif //REPERTORY_CONSOLECONSUMER_H
