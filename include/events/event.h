#ifndef REPERTORY_EVENT_H
#define REPERTORY_EVENT_H

#include <sstream>

namespace Repertory {
enum class EventLevel {
    Error,
    Warn,
    Normal,
    Debug,
    Verbose
};

static EventLevel EventLevelFromString(const std::string &eventLevel) {
  if (eventLevel == "Debug" || eventLevel == "EventLevel::Debug") {
    return EventLevel::Debug;
  } else if (eventLevel == "Warn" || eventLevel == "EventLevel::Warn") {
    return EventLevel::Warn;
  } else if (eventLevel == "Normal" || eventLevel == "EventLevel::Normal") {
    return EventLevel::Normal;
  } else if (eventLevel == "Error" || eventLevel == "EventLevel::Error") {
    return EventLevel::Error;
  } else if (eventLevel == "Verbose" || eventLevel == "EventLevel::Verbose") {
    return EventLevel::Verbose;
  }
  return EventLevel::Normal;
}

static std::string EventLevelToString(const EventLevel &eventLevel) {
  switch (eventLevel) {
  case EventLevel::Debug:
    return "Debug";
  case EventLevel::Error:
    return "Error";
  case EventLevel::Normal:
    return "Normal";
  case EventLevel::Warn:
    return "Warn";
  case EventLevel::Verbose:
    return "Verbose";
  default:
    return "Normal";
  }
}

class Event {
  protected:
    explicit Event(const bool &allowAsync) :
      _allowAsync(allowAsync) {
    }

    Event(const std::stringstream &ss, json j, const bool &allowAsync) :
      _allowAsync(allowAsync),
      _ss(ss.str()),
      _j(std::move(j)) {
    }

  public:
    virtual ~Event() = default;

  private:
    const bool _allowAsync;

  protected:
    std::stringstream _ss;
    json _j;

  public:
    virtual std::shared_ptr <Event> Clone() const = 0;

    inline bool GetAllowAsync() const {
      return _allowAsync;
    }

    virtual EventLevel GetEventLevel() const = 0;

    inline json GetJson() const {
      return _j;
    }

    virtual std::string GetName() const = 0;
    virtual std::string GetSingleLine() const = 0;
};
}

#endif //REPERTORY_EVENT_H
