#ifndef REPERTORY_EVENTSYSTEM_H
#define REPERTORY_EVENTSYSTEM_H

#include <common.h>
#include <events/event.h>
#include <events/teventsystem.h>

namespace Repertory {
typedef TEventSystem<Event> EventSystem;
typedef EventSystem::EventConsumer EventConsumer;

#define E_CAST(t) ((std::string)t)
#define E_DOUBLE(d) Utils::String::FromDouble(d)
#define E_DOUBLE_PRECISE(d) ([](const double& d)->std::string {\
  std::stringstream ss;\
  ss << std::fixed << std::setprecision(2) << d;\
  return ss.str();\
})(d)
#define E_FROM_BOOL(t) Utils::String::FromBool(t)
#define E_FROM_EXCEPTION(e) std::string(e.what() ? e.what() : "")
#define E_FROM_INT32(t) Utils::String::FromInt32(t)
#define E_FROM_INT64(t) Utils::String::FromInt64(t)
#define E_FROM_UINT16(t) Utils::String::FromUInt16(t)
#define E_FROM_STRING_ARRAY(a) ([](const auto& array)->std::string {\
  std::stringstream ret;\
  for (const auto& item : array) {\
    ret << (std::string(item)+ " ");\
  }\
  return std::string(ret).TrimRight();\
})(a)

#define E_PERCENT(d) ([](const double& d)->std::string {\
  std::stringstream ss;\
  ss << std::fixed << std::setprecision(2) << d;\
  ss << "%";\
  return ss;\
})(d)
#define E_STRING(t) t
#define E_FROM_UINT8(t) Utils::String::FromUInt8(t)
#define E_FROM_UINT32(t) Utils::String::FromUInt32(t)
#define E_FROM_UINT64(t) Utils::String::FromUInt64(t)
#define E_FROM_API_FILE_ERROR(e) E_FROM_INT32((std::int32_t)e)

#define E_PROP(type, name, short_name, ts) \
  private:\
    inline void Init_##short_name(const type& tv) {\
      _ss << "|" << #short_name << "|" << ts(tv);\
      _j[#name] = ts(tv);\
    }\
  public:\
    json Get##name() const { \
      return _j[#name]; \
    }

#define E_BEGIN(name, event_level) class name final : public virtual Event { \
  private:\
    name(const std::stringstream& ss, const json& j, const bool& allowAsync) : Event(ss, j, allowAsync) {\
    }\
  public:\
    ~name() override = default;\
  public:\
    static const EventLevel Level = EventLevel::event_level;\
  public:\
    inline std::string GetName() const override { return #name; }\
    inline EventLevel GetEventLevel() const override { return name::Level; }\
    inline std::string GetSingleLine() const override {\
      const auto s = _ss.str();\
      return GetName() + (s.empty() ? "" : s);\
    }\
    inline std::shared_ptr<Event> Clone() const override { return std::shared_ptr<name>(new name(_ss, _j, GetAllowAsync())); }

#define E_END() }

#define E_SIMPLE(event_name, level, allow_async) \
E_BEGIN(event_name, level) \
  public:\
    event_name() : Event(allow_async) { }\
E_END()

#define E_SIMPLE1(event_name, level, allow_async, type, name, short_name, tc) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv) : Event(allow_async) { \
      Init_##short_name(tv); \
    } \
  E_PROP(type, name, short_name, tc)\
E_END()

#define E_SIMPLE2(event_name, level, allow_async, type, name, short_name, tc, type2, name2, short_name2, tc2) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv, const type2& tv2) : Event(allow_async) { \
      Init_##short_name(tv); \
      Init_##short_name2(tv2);\
    } \
  E_PROP(type, name, short_name, tc)\
  E_PROP(type2, name2, short_name2, tc2)\
E_END()

#define E_SIMPLE3(event_name, level, allow_async, type, name, short_name, tc, type2, name2, short_name2, tc2, type3, name3, short_name3, tc3) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv, const type2& tv2, const type3& tv3) : Event(allow_async) { \
      Init_##short_name(tv);\
      Init_##short_name2(tv2);\
      Init_##short_name3(tv3);\
    } \
  E_PROP(type, name, short_name, tc)\
  E_PROP(type2, name2, short_name2, tc2)\
  E_PROP(type3, name3, short_name3, tc3)\
E_END()

#define E_SIMPLE4(event_name, level, allow_async, type, name, short_name, tc, type2, name2, short_name2, tc2, type3, name3, short_name3, tc3, type4, name4, short_name4, tc4) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv, const type2& tv2, const type3& tv3, const type4& tv4) : Event(allow_async) { \
      Init_##short_name(tv);\
      Init_##short_name2(tv2);\
      Init_##short_name3(tv3);\
      Init_##short_name4(tv4);\
    } \
  E_PROP(type, name, short_name, tc)\
  E_PROP(type2, name2, short_name2, tc2)\
  E_PROP(type3, name3, short_name3, tc3)\
  E_PROP(type4, name4, short_name4, tc4)\
E_END()

#define E_SIMPLE5(event_name, level, allow_async, type, name, short_name, tc, type2, name2, short_name2, tc2, type3, name3, short_name3, tc3, type4, name4, short_name4, tc4, type5, name5, short_name5, tc5) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv, const type2& tv2, const type3& tv3, const type4& tv4, const type5& tv5) : Event(allow_async) { \
      Init_##short_name(tv);\
      Init_##short_name2(tv2);\
      Init_##short_name3(tv3);\
      Init_##short_name4(tv4);\
      Init_##short_name5(tv5);\
    } \
  E_PROP(type, name, short_name, tc)\
  E_PROP(type2, name2, short_name2, tc2)\
  E_PROP(type3, name3, short_name3, tc3)\
  E_PROP(type4, name4, short_name4, tc4)\
  E_PROP(type5, name5, short_name5, tc5)\
E_END()

#define E_SIMPLE6(event_name, level, allow_async, type, name, short_name, tc, type2, name2, short_name2, tc2, type3, name3, short_name3, tc3, type4, name4, short_name4, tc4, type5, name5, short_name5, tc5, type6, name6, short_name6, tc6) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv, const type2& tv2, const type3& tv3, const type4& tv4, const type5& tv5, const type6& tv6) : Event(allow_async) { \
      Init_##short_name(tv);\
      Init_##short_name2(tv2);\
      Init_##short_name3(tv3);\
      Init_##short_name4(tv4);\
      Init_##short_name5(tv5);\
      Init_##short_name6(tv6);\
    } \
  E_PROP(type, name, short_name, tc)\
  E_PROP(type2, name2, short_name2, tc2)\
  E_PROP(type3, name3, short_name3, tc3)\
  E_PROP(type4, name4, short_name4, tc4)\
  E_PROP(type5, name5, short_name5, tc5)\
  E_PROP(type6, name6, short_name6, tc6)\
E_END()

#define E_SIMPLE7(event_name, level, allow_async, type, name, short_name, tc, type2, name2, short_name2, tc2, type3, name3, short_name3, tc3, type4, name4, short_name4, tc4, type5, name5, short_name5, tc5, type6, name6, short_name6, tc6, type7, name7, short_name7, tc7) \
E_BEGIN(event_name, level) \
    explicit event_name(const type& tv, const type2& tv2, const type3& tv3, const type4& tv4, const type5& tv5, const type6& tv6, const type7& tv7) : Event(allow_async) { \
      Init_##short_name(tv);\
      Init_##short_name2(tv2);\
      Init_##short_name3(tv3);\
      Init_##short_name4(tv4);\
      Init_##short_name5(tv5);\
      Init_##short_name6(tv6);\
      Init_##short_name7(tv7);\
    } \
  E_PROP(type, name, short_name, tc)\
  E_PROP(type2, name2, short_name2, tc2)\
  E_PROP(type3, name3, short_name3, tc3)\
  E_PROP(type4, name4, short_name4, tc4)\
  E_PROP(type5, name5, short_name5, tc5)\
  E_PROP(type6, name6, short_name6, tc6)\
  E_PROP(type7, name7, short_name7, tc7)\
E_END()

#define E_CONSUMER() \
  private:\
    std::vector<std::shared_ptr<Repertory::EventConsumer>> _eventConsumers;

#define E_SUBSCRIBE(name, callback) \
  _eventConsumers.emplace_back(std::make_shared<Repertory::EventConsumer>(#name, [this](const Event& e) {callback(e);}))

#define E_SUBSCRIBE_EXACT(name, callback) \
  _eventConsumers.emplace_back(std::make_shared<Repertory::EventConsumer>(#name, [this](const Event& e) {callback(dynamic_cast<const name&>(e));}))

#define E_SUBSCRIBE_ALL(callback) \
  _eventConsumers.emplace_back(std::make_shared<Repertory::EventConsumer>([this](const Event& e) {callback(e);}))
}
#include <events/events.h>
#endif //REPERTORY_EVENTSYSTEM_H

