#ifndef REPERTORY_TEVENTSYSTEM_H
#define REPERTORY_TEVENTSYSTEM_H

#include <condition_variable>
#include <deque>
#include <future>
#include <mutex>
#include <thread>
#include <utils/utils.h>

namespace Repertory {
template<typename TEvent>
class TEventSystem final {
  public:
    TEventSystem(const TEventSystem &) = delete;
    TEventSystem(TEventSystem &&) = delete;
    TEventSystem &operator=(const TEventSystem &) = delete;
    TEventSystem &operator=(TEventSystem &&) = delete;

  protected:
    TEventSystem() = default;

    ~TEventSystem() {
      Stop();
    }

  public:
    class EventConsumer final {
      public:
        explicit EventConsumer(std::function<void(const Event &)> callback) :
          _callback(callback) {
          TEventSystem::Instance().Attach(this);
        }

        EventConsumer(const std::string &eventName, std::function<void(const Event &)> callback) :
          _callback(callback) {
          TEventSystem::Instance().Attach(eventName, this);
        }

        ~EventConsumer() {
          TEventSystem::Instance().Release(this);
        }

      private:
        std::function<void(const Event &)> _callback;

      public:
        inline void NotifyEvent(const Event &event) {
          _callback(event);
        }
    };

  private:
    static TEventSystem _eventSystem;

  public:
    static inline TEventSystem &Instance() {
      return _eventSystem;
    }

  private:
    std::unordered_map<std::string, std::deque<EventConsumer *>> _eventConsumers;
    std::recursive_mutex _consumerMutex;
    std::vector<std::shared_ptr<TEvent>> _eventList;
    std::condition_variable _eventNotify;
    std::mutex _eventMutex;
    std::unique_ptr<std::thread> _eventThread;
    std::mutex _runMutex;
    bool _stopRequested = false;

  private:
    void ProcessEvents() {
      std::vector<std::shared_ptr<TEvent>> events;
      {
        UniqueMutexLock l(_eventMutex);
        if (not _stopRequested && _eventList.empty()) {
          _eventNotify.wait_for(l, 5s);
        }

        if (not _eventList.empty()) {
          events.insert(events.end(), _eventList.begin(), _eventList.end());
          _eventList.clear();
        }
      }

      auto NotifyEvents = [this](const std::string &name, const TEvent &event) {
        std::deque<std::future<void>> futures;
        RMutexLock l(_consumerMutex);
        if (_eventConsumers.find(name) != _eventConsumers.end()) {
          for (auto *ec : _eventConsumers[name]) {
            if (event.GetAllowAsync()) {
              futures.emplace_back(std::async(std::launch::async, [ec, &event]() {
                ec->NotifyEvent(event);
              }));
            } else {
              ec->NotifyEvent(event);
            }
          }
        }

        while (not futures.empty()) {
          futures.front().get();
          futures.pop_front();
        }
      };

      for (const auto &event : events) {
        NotifyEvents("", *event.get());
        NotifyEvents(event->GetName(), *event.get());
      }
    }

    void QueueEvent(TEvent *event) {
      MutexLock l(_eventMutex);
      _eventList.emplace_back(std::shared_ptr<TEvent>(event));
      _eventNotify.notify_all();
    }

  public:
    void Attach(EventConsumer *eventConsumer) {
      RMutexLock l(_consumerMutex);
      _eventConsumers[""].push_back(eventConsumer);
    }

    void Attach(const std::string &eventName, EventConsumer *eventConsumer) {
      RMutexLock l(_consumerMutex);
      _eventConsumers[eventName].push_back(eventConsumer);
    }

    template<typename T, typename... Args>
    inline void Raise(Args &&... args) {
      QueueEvent(new T(std::forward<Args>(args)...));
    }

    void Release(EventConsumer *eventConsumer) {
      RMutexLock l(_consumerMutex);
      auto it = std::find_if(_eventConsumers.begin(), _eventConsumers.end(), [&](const auto &kv) -> bool {
        return std::find(kv.second.begin(), kv.second.end(), eventConsumer) != kv.second.end();
      });

      if (it != _eventConsumers.end()) {
        auto &q = (*it).second;
        Utils::RemoveElement(q, eventConsumer);
      }
    }

    void Start() {
      MutexLock l(_runMutex);
      if (not _eventThread) {
        _stopRequested = false;
        _eventThread = std::make_unique<std::thread>([this]() {
          while (not _stopRequested) {
            ProcessEvents();
          }
        });
      }
    }

    void Stop() {
      MutexLock l(_runMutex);
      if (_eventThread) {
        _stopRequested = true;
        _eventNotify.notify_all();
        _eventThread->join();
        _eventThread.reset();
        ProcessEvents();
        _stopRequested = false;
      }
    }
};
}
#endif //REPERTORY_TEVENTSYSTEM_H
