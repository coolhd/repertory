#ifndef REPERTORY_RINGDOWNLOAD_H
#define REPERTORY_RINGDOWNLOAD_H

#ifdef U
#undef U
#endif
#include <common.h>
#include <boost/dynamic_bitset.hpp>
#include <config.h>
#include <download/bufferedreader.h>
#include <download/events.h>
#include <download/idownload.h>
#include <events/eventsystem.h>
#include <utils/nativefile.h>
#include <utils/pathutils.h>
#include <utils/utils.h>

namespace Repertory {
class CRingDownload final :
  public virtual IDownload {
  private:
    typedef struct __IoAction {
      __IoAction(std::mutex& mtx, std::condition_variable& cv, std::function<void()> action) :
        Mutex(mtx),
        Notify(cv),
        Action(action) {
      }
      std::mutex& Mutex;
      std::condition_variable& Notify;
      std::function<void()> Action;
    } IoAction;

  public:
    CRingDownload(CConfig& config, FileSystemItem fileSystemItem, ApiReader& apiReader, const std::size_t& ringBufferSize=536870912ull) :
      _config(config),
      _fileSystemItem(std::move(fileSystemItem)),
      _apiReader(apiReader),
      _chunkSize(_config.GetChunkSize() * 1024ull),
      _ringState(ringBufferSize / (_config.GetChunkSize() * 1024ull)) {
      *const_cast<std::size_t*>(&_totalChunks) = Utils::DivideWithCeiling(static_cast<std::size_t>(fileSystemItem.Size), _chunkSize);
      const auto bufferDir = Utils::Path::Combine(_config.GetDataDirectory(), "buffer");
      if (Utils::File::CreateFullDirectoryPath(bufferDir)) {
        _bufferFilePath = Utils::Path::Combine(bufferDir, Utils::CreateUUIDString());
        if ((_apiFileError = CNativeFile::CreateOrOpen(_bufferFilePath, _bufferFile)) == ApiFileError::Success) {
          if (_bufferFile->Allocate(ringBufferSize)) {
            _timeoutThread = std::make_unique<std::thread>([this]() {
              this->TimeoutThread();
            });
          } else {
            _apiFileError = ApiFileError::OSErrorCode;
          }
        }
      } else {
        _apiFileError = ApiFileError::OSErrorCode;
      }
    }

    ~CRingDownload() override {
      UniqueMutexLock timeoutLock(_timeoutMutex);
      _timeoutActive = false;
      _timeoutNotify.notify_all();
      timeoutLock.unlock();

      Stop();

      timeoutLock.lock();
      _timeoutNotify.notify_all();
      timeoutLock.unlock();

      if (_timeoutThread) {
        _timeoutThread->join();
        _timeoutThread.reset();
      }

      if (_bufferFile) {
        _bufferFile->Close();
        Utils::File::DeleteAsFile(_bufferFilePath);
      }
    }

  private:
    CConfig& _config;
    const FileSystemItem _fileSystemItem;
    ApiReader& _apiReader;
    const std::size_t _chunkSize;
    boost::dynamic_bitset<> _ringState;
    const std::size_t _totalChunks = 0;

    ApiFileError _apiFileError = ApiFileError::Success;
    NativeFilePtr _bufferFile;
    std::string _bufferFilePath;
    CBufferedReader* _bufferedReader = nullptr;
    bool _disableDownloadEnd = false;
    std::size_t _headChunk = 0ull;
    std::mutex _ioMutex;
    std::condition_variable _ioNotify;
    std::deque<std::unique_ptr<IoAction>> _ioQueue;
    std::unique_ptr<std::thread> _ioThread;
    std::size_t _readChunk = 0ull;
    std::unique_ptr<std::thread> _bufferThread;
    std::mutex _readMutex;
    std::condition_variable _readNotify;
    bool _stopRequested = false;
    std::chrono::system_clock::time_point _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
    bool _timeoutActive = false;
    std::mutex _timeoutMutex;
    std::condition_variable _timeoutNotify;
    std::unique_ptr<std::thread> _timeoutThread;
    std::size_t _writeChunk = 0ull;
    std::mutex _writeMutex;

  private:
    void BufferThread(std::size_t startChunk) {
      CBufferedReader bufferedReader(_config, _fileSystemItem, _apiReader, _chunkSize, _totalChunks, startChunk);

      UniqueMutexLock writeLock(_writeMutex);
      _bufferedReader = &bufferedReader;
      _readNotify.notify_all();
      writeLock.unlock();

      const auto ringSize = _ringState.size();
      const auto halfRingSize = ringSize / 2;

      double progress = 0.0;
      const auto notifyProgress = [this, &progress] (const std::size_t& writeChunk) {
        if (_config.GetEventLevel() >= DownloadProgress::Level) {
          const double curProgress = (static_cast<double>((writeChunk + 1ull)) / static_cast<double>(_totalChunks)) * 100.0;
          if ((curProgress >= (progress + 0.2)) ||
              (curProgress <= (progress - 0.2)) ||
              ((curProgress == 100.00) && (progress != curProgress))) {
            progress = curProgress;
            EventSystem::Instance().Raise<DownloadProgress>(_fileSystemItem.ApiFilePath, _bufferFilePath, curProgress);
          }
        }
      };

      while (IsActive()) {
        writeLock.lock();
        while ((_writeChunk > _readChunk) &&
               (_writeChunk > (_headChunk + ringSize - 1ull)) &&
               IsActive()) {
          // Buffer 50% for read-ahead/read-behind
          auto buffered = false;
          while ((_writeChunk > _readChunk) &&
                 ((_writeChunk - _readChunk) > halfRingSize) &&
                 IsActive()) {
            buffered = true;
            _readNotify.wait(writeLock);
          }

          if (not buffered &&
              IsActive()) {
            _readNotify.wait(writeLock);
          }

          if ((_writeChunk > _readChunk) &&
              ((_writeChunk - _readChunk) <= halfRingSize)) {
            _headChunk++;
          }
        }

        if (IsActive()) {
          if ((_readChunk > _writeChunk) || (_readChunk < _headChunk)) {
            _headChunk = _writeChunk = _readChunk;
            _ringState.reset();
          }

          const auto fileOffset = _writeChunk * _chunkSize;
          const auto writeChunk = _writeChunk;
          const auto writePos = _writeChunk % ringSize;
          _ringState[writePos] = false;
          _readNotify.notify_all();
          writeLock.unlock();

          const auto readSize = Utils::CalculateReadSize(_fileSystemItem.Size, _chunkSize, fileOffset);
          if (readSize > 0) {
            if (((writeChunk == 0) && bufferedReader.HasFirstChunk()) ||
                ((writeChunk == (_totalChunks - 1ull)) && bufferedReader.HasLastChunk())) {
              writeLock.lock();
              _writeChunk++;
              _readNotify.notify_all();
            } else {
              std::vector<char> data;
              auto apiFileError = bufferedReader.ReadChunk(writeChunk, data);
              if (apiFileError == ApiFileError::Success) {
                std::mutex jobMutex;
                std::condition_variable jobNotify;
                UniqueMutexLock jobLock(jobMutex);

                QueueIoItem(jobMutex, jobNotify, false, [&]() {
                  std::size_t bytesWritten = 0ull;
                  if (_bufferFile->WriteBytes(&data[0], data.size(), writePos * _chunkSize, bytesWritten)) {
                    _bufferFile->Flush();
                    notifyProgress(_writeChunk);
                  } else {
                    apiFileError = ApiFileError::OSErrorCode;
                  }
                });

                jobNotify.wait(jobLock);
                jobLock.unlock();
              }

              SetApiFileError(apiFileError);

              writeLock.lock();
              if (apiFileError == ApiFileError::Success) {
                _ringState[writePos] = true;
                _writeChunk++;
                _readNotify.notify_all();
              }
            }
            writeLock.unlock();
          } else {
            writeLock.lock();
            while ((_readChunk <= _writeChunk) &&
                   (_readChunk >= _headChunk) &&
                   ((_writeChunk * _chunkSize) >= _fileSystemItem.Size) &&
                   IsActive()) {
              _readNotify.wait(writeLock);
            }
            writeLock.unlock();
          }
        } else {
          writeLock.unlock();
        }
      }

      writeLock.lock();
      _bufferedReader = nullptr;
      _readNotify.notify_all();
      writeLock.unlock();

      if (not _disableDownloadEnd) {
        EventSystem::Instance().Raise<DownloadEnd>(_fileSystemItem.ApiFilePath, _bufferFilePath, _apiFileError);
      }
    }

    void IoThread() {
      const auto processAll = [&]() {
        UniqueMutexLock ioLock(_ioMutex);
        while (not _ioQueue.empty()) {
          auto& action = _ioQueue.front();
          ioLock.unlock();

          UniqueMutexLock jobLock(action->Mutex);
          action->Action();
          action->Notify.notify_all();
          jobLock.unlock();

          ioLock.lock();
          Utils::RemoveElement(_ioQueue, action);
        }
        ioLock.unlock();
      };

      while (not _stopRequested) {
        if (_ioQueue.empty()) {
          UniqueMutexLock ioLock(_ioMutex);
          if (_ioQueue.empty()) {
            _ioNotify.wait(ioLock);
          }
        } else {
          processAll();
        }
      }

      processAll();
    }

    inline bool IsActive() const {
      return not _stopRequested && (_apiFileError == ApiFileError::Success);
    }

    inline void QueueIoItem(std::mutex& m, std::condition_variable& cv, const bool& isRead, std::function<void()> action) {
      MutexLock ioLock(_ioMutex);
      if (isRead) {
        _ioQueue.insert(_ioQueue.begin(), std::make_unique<IoAction>(m, cv, action));
      } else {
        _ioQueue.emplace_back(std::make_unique<IoAction>(m, cv, action));
      }
      _ioNotify.notify_all();
    }

    void Read(std::size_t readChunk, std::size_t readSize, std::size_t offset, std::vector<char>& data) {
      while (IsActive() &&
             (readSize > 0)) {
        UniqueMutexLock writeLock(_writeMutex);
        if ((readChunk == (_totalChunks - 1ull)) &&
            _bufferedReader &&
            _bufferedReader->HasLastChunk()) {
          std::vector<char>* buffer = nullptr;
          _bufferedReader->GetLastChunk(buffer);
          if (buffer) {
            data.insert(data.end(), buffer->begin() + offset, buffer->begin() + offset + readSize);
          }
          writeLock.unlock();
          readSize = 0;
        } else {
          const auto notifyReadLocation = [&]() {
            if (readChunk != _readChunk) {
              writeLock.lock();
              _readChunk = readChunk;
              _readNotify.notify_all();
              writeLock.unlock();
            }
          };

          if ((readChunk == 0) &&
              _bufferedReader &&
              _bufferedReader->HasFirstChunk()) {
            const auto toRead = std::min(_chunkSize - offset, readSize);
            std::vector<char> *buffer = nullptr;
            _bufferedReader->GetFirstChunk(buffer);
            if (buffer) {
              data.insert(data.end(), buffer->begin() + offset, buffer->begin() + offset + toRead);
            }
            writeLock.unlock();

            readSize -= toRead;
            offset = 0;
            readChunk++;
            notifyReadLocation();
          } else {
            while (((readChunk > _writeChunk) || (readChunk < _headChunk)) && IsActive()) {
              _readChunk = readChunk;
              _readNotify.notify_all();
              _readNotify.wait(writeLock);
            }
            writeLock.unlock();

            if (IsActive()) {
              const auto ringPos = readChunk % _ringState.size();
              if (_ringState[ringPos]) {
                notifyReadLocation();

                const auto toRead = std::min(_chunkSize - offset, readSize);
                std::mutex jobMutex;
                std::condition_variable jobNotify;
                UniqueMutexLock jobLock(jobMutex);

                QueueIoItem(jobMutex, jobNotify, true, [&]() {
                  std::vector<char> buffer(toRead);
                  std::size_t bytesRead = 0ull;
                  if (_bufferFile->ReadBytes(&buffer[0], buffer.size(), (ringPos * _chunkSize) + offset, bytesRead)) {
                    data.insert(data.end(), buffer.begin(), buffer.end());
                    buffer.clear();

                    readSize -= toRead;
                    offset = 0ull;
                  } else {
                    SetApiFileError(ApiFileError::OSErrorCode);
                  }
                });

                jobNotify.wait(jobLock);
                jobLock.unlock();

                readChunk++;
              } else {
                notifyReadLocation();
                writeLock.lock();
                while (not _ringState[ringPos] && IsActive()) {
                  _readNotify.wait(writeLock);
                }
                writeLock.unlock();
              }
            }
          }
        }
      }
    }

    inline void SetApiFileError(const ApiFileError& apiFileError) {
      if ((_apiFileError == ApiFileError::Success) && 
          (_apiFileError != apiFileError)) {
        _apiFileError = apiFileError;
      }
    }

    inline void SetTimeout() {
      _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
    }

    void Start(const std::size_t& startChunk) {
      EventSystem::Instance().Raise<DownloadBegin>(_fileSystemItem.ApiFilePath, _bufferFilePath);

      _apiFileError = ApiFileError::Success;
      _stopRequested = false;
      _headChunk = _readChunk = _writeChunk = startChunk;
      _ringState.reset();

      _ioThread = std::make_unique<std::thread>([this] {
        this->IoThread();
      });

      UniqueMutexLock writeLock(_writeMutex);
      _bufferThread = std::make_unique<std::thread>([this, startChunk]() {
        this->BufferThread(startChunk);
      });
      _readNotify.wait(writeLock);
      _readNotify.notify_all();
      writeLock.unlock();
    }

    void Stop() {
      _stopRequested = true;

      UniqueMutexLock writeLock(_writeMutex);
      _readNotify.notify_all();
      writeLock.unlock();

      if (_bufferThread) {
        _bufferThread->join();
        _bufferThread.reset();
      }

      UniqueMutexLock ioLock(_ioMutex);
      _ioNotify.notify_all();
      ioLock.unlock();

      if (_ioThread) {
        _ioThread->join();
        _ioThread.reset();

        _ioQueue.clear();
      }
    }

    void TimeoutThread() {
      UniqueMutexLock timeoutLock(_timeoutMutex);
      timeoutLock.unlock();
      while (not _stopRequested) {
        timeoutLock.lock();
        if (_timeoutActive) {
          _timeoutNotify.wait_until(timeoutLock, _timeout);
          if (IsActive() &&
              _timeoutActive &&
              (std::chrono::system_clock::now() >= _timeout)) {
            SetApiFileError(ApiFileError::DownloadTimeout);
            SetTimeout();
            timeoutLock.unlock();

            MutexLock writeLock(_writeMutex);
            _readNotify.notify_all();
          } else {
            timeoutLock.unlock();
          }
        } else if (not _stopRequested) {
          _timeoutNotify.wait(timeoutLock);
          timeoutLock.unlock();
        }
      }
    }

  public:
    inline ApiFileError DownloadAll() override {
      return ApiFileError::Unsupported;
    }

    inline ApiFileError GetResult() const override {
      return _apiFileError;
    }

    inline std::string GetSourceFilePath() const override {
      return _bufferFilePath;
    }

    inline bool GetWriteSupported() const override {
      return false;
    }

    inline void NotifyStopRequested() override {
      SetApiFileError(ApiFileError::DownloadStopped);
      _stopRequested = true;
    }

    ApiFileError ReadBytes(std::size_t readSize, const std::uint64_t &readOffset, std::vector<char> &data) override {
      data.clear();

      MutexLock readLock(_readMutex);
      if (IsActive()) {
        if (((readSize = Utils::CalculateReadSize(_fileSystemItem.Size, readSize, readOffset)) > 0)) {
          const std::size_t readChunk = readOffset / _chunkSize;
          if (not _bufferThread) {
            Start(readChunk);
          }

          Read(readChunk, readSize, readOffset % _chunkSize, data);
          SetApiFileError(((_apiFileError == ApiFileError::Success) && _stopRequested) ? ApiFileError::DownloadStopped : _apiFileError);
        }
      }

      return _apiFileError;
    }

    inline void ResetTimeout(const bool& fileClosed) override {
      MutexLock timeoutLock(_timeoutMutex);
      _timeoutActive = fileClosed;
      SetTimeout();
      _timeoutNotify.notify_all();
    }

    inline void SetDisableDownloadEnd(const bool& disable) override {
      _disableDownloadEnd = disable;
    }
};
}
#endif //REPERTORY_RINGDOWNLOAD_H