#ifndef REPERTORY_IDOWNLOADMANAGER_H
#define REPERTORY_IDOWNLOADMANAGER_H

#include <common.h>

namespace Repertory {
class IDownloadManager {
  INTERFACE_SETUP(IDownloadManager);

  public:
    virtual ApiFileError Download(const std::uint64_t &handle, FileSystemItem &fileSystemItem) = 0;
    virtual bool IsProcessing(const std::string &apiFilePath) const = 0;
    virtual ApiFileError ReadBytes(const std::uint64_t &handle, FileSystemItem &fileSystemItem, std::size_t readSize, const std::uint64_t &offset, std::vector<char> &data) = 0;
};
}
#endif //REPERTORY_IDOWNLOADMANAGER_H
