#ifndef REPERTORY_DOWNLOADMANAGER_H
#define REPERTORY_DOWNLOADMANAGER_H

#include <common.h>
#include <download/directdownload.h>
#include <download/download.h>
#include <download/events.h>
#include <download/idownload.h>
#include <download/idownloadmanager.h>
#include <download/ringdownload.h>
#include <drives/iopenfiletable.h>
#include <events/events.h>
#include <events/eventsystem.h>
#include <utils/utils.h>

namespace Repertory {
class CDownloadManager final :
  public virtual IDownloadManager {
  E_CONSUMER();
  public:
    CDownloadManager(CConfig &config, ApiReader apiReader, const bool& forceDownload = false) :
      _config(config),
      _apiReader(apiReader),
      _forceDownload(forceDownload) {
      E_SUBSCRIBE_EXACT(DownloadEnd, [this](const DownloadEnd &downloadEnd) {
        DownloadPtr download;
        UniqueRMutexLock downloadLock(_downloadMutex);
        const auto lookupPath = CreateLookupPath(downloadEnd.GetApiFilePath());
        if (_downloadLookup.find(lookupPath) != _downloadLookup.end()) {
          download = _downloadLookup[lookupPath];
        }
        downloadLock.unlock();
        if (download) {
          downloadLock.lock();
          _downloadLookup.erase(lookupPath);
          downloadLock.unlock();
        }
      });

      E_SUBSCRIBE_EXACT(FileSystemItemClosed, [this](const FileSystemItemClosed &fileClosed) {
        ResetTimeout(fileClosed.GetApiFilePath(), true);
      });

      E_SUBSCRIBE_EXACT(FileSystemItemOpened, [this](const FileSystemItemOpened &fileOpened) {
        ResetTimeout(fileOpened.GetApiFilePath(), false);
      });
    }

    ~CDownloadManager() override {
      _eventConsumers.clear();
      Stop();
    }

  private:
    CConfig &_config;
    ApiReader _apiReader;
    bool _forceDownload;
    IOpenFileTable* _openFileTable = nullptr;
    std::recursive_mutex _downloadMutex;
    std::unordered_map<std::string, DownloadPtr> _downloadLookup;
    std::recursive_mutex _startStopMutex;
    bool _stopRequested = true;

  private:
    inline std::uint64_t GetChunkSize(const FileSystemItem &fileSystemItem) const {
      return fileSystemItem.LegacyChunks ? DEFAULT_CHUNK_SIZE : (_config.GetChunkSize() * 1024ull);
    }

    DownloadPtr GetDownload(FileSystemItem &fileSystemItem, const bool& writeSupported) {
      auto createDownload = [this, &fileSystemItem, &writeSupported]() -> DownloadPtr {
        auto chunkSize = GetChunkSize(fileSystemItem);
        if (writeSupported || _forceDownload) {
          return std::dynamic_pointer_cast<IDownload>(std::make_shared<CDownload>(_config, fileSystemItem, _apiReader, chunkSize, *_openFileTable));
        } else {
          const auto downloadType = _config.GetPreferredDownloadType();
          const std::size_t ringBufferSize = _config.GetRingBufferFileSize() * 1024ull * 1024ull;
          const auto ringAllowed = not fileSystemItem.LegacyChunks && (fileSystemItem.Size > ringBufferSize);

          const auto noSpaceRemaining = (Utils::File::GetAvailableDriveSpace(_config.GetCacheDirectory()) < fileSystemItem.Size);
          if (ringAllowed &&
              (downloadType != DownloadType::Direct) &&
              (noSpaceRemaining || (downloadType == DownloadType::RingBuffer))) {
            return std::dynamic_pointer_cast<IDownload>(std::make_shared<CRingDownload>(_config, fileSystemItem, _apiReader, ringBufferSize));
          }

          if (noSpaceRemaining || (downloadType == DownloadType::Direct)) {
            return std::dynamic_pointer_cast<IDownload>(std::make_shared<CDirectDownload>(_config, fileSystemItem, _apiReader));
          } else {
            return std::dynamic_pointer_cast<IDownload>(std::make_shared<CDownload>(_config, fileSystemItem, _apiReader, chunkSize, *_openFileTable));
          }
        }
      };

      DownloadPtr download;
      const auto lookupPath = CreateLookupPath(fileSystemItem.ApiFilePath);
      {
        RMutexLock downloadLock(_downloadMutex);
        if (_downloadLookup.find(lookupPath) == _downloadLookup.end()) {
          _downloadLookup.insert({lookupPath,
                                  createDownload()});
        }
        download = _downloadLookup[lookupPath];
        if (writeSupported && not download->GetWriteSupported()) {
          download->SetDisableDownloadEnd(true);
          download = _downloadLookup[lookupPath] = createDownload();
        }
      }

      return std::move(download);
    }

    inline void ResetTimeout(const std::string& apiFilePath, const bool& fileClosed) {
      DownloadPtr download;
      UniqueRMutexLock downloadLock(_downloadMutex);
      const auto lookupPath = CreateLookupPath(apiFilePath);
      if (_downloadLookup.find(lookupPath) != _downloadLookup.end()) {
        download = _downloadLookup[lookupPath];
      }
      downloadLock.unlock();
      if (download) {
        download->ResetTimeout(fileClosed);
      }
    }

  public:
    ApiFileError Download(const std::uint64_t &handle, FileSystemItem &fileSystemItem) override {
      if (_stopRequested) {
        return ApiFileError::DownloadStopped;
      } else if (fileSystemItem.Size > 0) {
        std::uint64_t fileSize = 0;
        if (Utils::File::GetSize(fileSystemItem.SourceFilePath, fileSize)) {
          if (fileSize == fileSystemItem.Size) {
            NativeFilePtr nativeFile;
            return Utils::File::AssignAndGetNativeFile(fileSystemItem, nativeFile);
          }
        }

        auto download = GetDownload(fileSystemItem, true);
        return ((download->GetResult() == ApiFileError::Success) ? download->DownloadAll() : download->GetResult());
      } else if (fileSystemItem.Handle == INVALID_OSHANDLE_VALUE) {
        NativeFilePtr nativeFile;
        return Utils::File::AssignAndGetNativeFile(fileSystemItem, nativeFile);
      } else {
        return ApiFileError::Success;
      }
    }

    inline std::size_t GetDownloadCount() const {
      return _downloadLookup.size();
    }

    inline std::string GetSourceFilePath(const std::string &apiFilePath) const {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      RMutexLock downloadLock(const_cast<CDownloadManager *>(this)->_downloadMutex);
      if (_downloadLookup.find(lookupPath) != _downloadLookup.end()) {
        return _downloadLookup.at(lookupPath)->GetSourceFilePath();
      }
      return "";
    }

    inline bool IsProcessing(const std::string &apiFilePath) const override {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      RMutexLock downloadLock(const_cast<CDownloadManager *>(this)->_downloadMutex);
      return _downloadLookup.find(lookupPath) != _downloadLookup.end();
    }

    inline ApiFileError ReadBytes(const std::uint64_t &handle, FileSystemItem &fileSystemItem, std::size_t readSize, const std::uint64_t &offset, std::vector<char> &data) override {
      if (_stopRequested) {
        return ApiFileError::DownloadStopped;
      } else {
        readSize = Utils::CalculateReadSize(fileSystemItem.Size, readSize, offset);
        const auto readFromSource = [&]() -> ApiFileError {
          NativeFilePtr nativeFile;
          auto ret = Utils::File::AssignAndGetNativeFile(fileSystemItem, nativeFile);
          if (ret == ApiFileError::Success) {
            std::size_t bytesRead = 0;
            data.resize(readSize);
            if (not nativeFile->ReadBytes(&data[0], data.size(), offset, bytesRead)) {
              ret = ApiFileError::OSErrorCode;
            }
          }
          return ret;
        };

        if (readSize > 0) {
          if (Utils::File::IsFile(fileSystemItem.SourceFilePath)) {
            std::uint64_t fileSize = 0;
            if (Utils::File::GetSize(fileSystemItem.SourceFilePath, fileSize)) {
              if ((offset + readSize) <= fileSize) {
                return readFromSource();
              }
            } else {
              return ApiFileError::OSErrorCode;
            }
          }
        } else {
          return ApiFileError::Success;
        }

        auto download = GetDownload(fileSystemItem, false);
        const auto result = download->GetResult();
        return ((result == ApiFileError::Success) ? download->ReadBytes(readSize, offset, data) : result);
      }
    }

    void Start(IOpenFileTable* openFileTable) {
      RMutexLock startStopLock(_startStopMutex);
      if (_stopRequested) {
        _stopRequested = false;
        _openFileTable = openFileTable;
      }
    }

    void Stop() {
      UniqueRMutexLock startStopLock(_startStopMutex);
      if (not _stopRequested) {
        _stopRequested = true;
        startStopLock.unlock();

        while (not _downloadLookup.empty()) {
          UniqueRMutexLock downloadLock(_downloadMutex);
          if (not _downloadLookup.empty()) {
            DownloadPtr download;
            std::string lookupPath;
            {
              auto it = _downloadLookup.begin();
              lookupPath = it->first;
              download = it->second;
            }
            downloadLock.unlock();
            if (download) {
              downloadLock.lock();
              _downloadLookup.erase(lookupPath);
              downloadLock.unlock();

              download->NotifyStopRequested();
            }
          }
        }
      }
    }
};
}

#endif //REPERTORY_DOWNLOADMANAGER_H
