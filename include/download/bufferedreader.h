#ifndef REPERTORY_BUFFEREDREADER_H
#define REPERTORY_BUFFEREDREADER_H

#ifdef U
#undef U
#endif
#include <common.h>
#include <boost/dynamic_bitset.hpp>
#include <config.h>
#include <download/readerpool.h>

namespace Repertory {
class CBufferedReader final {
  public:
    CBufferedReader(CConfig& config, const FileSystemItem& fileSystemItem, ApiReader& apiReader, const std::size_t& chunkSize, const std::size_t& totalChunks, const std::size_t& startChunk) :
      _config(config),
      _fileSystemItem(fileSystemItem),
      _apiReader(apiReader),
      _chunkSize(chunkSize),
      _totalChunks(totalChunks),
      _ringState(config.GetReadAheadCount()) {
      _ringData.resize(_ringState.size());

      const auto readChunk = [this](const std::size_t& readOffset, std::unique_ptr<std::vector<char>>& chunkPtr) {
        const auto readSize = Utils::CalculateReadSize(_fileSystemItem.Size, _chunkSize, readOffset);
        if (readSize) {
          std::vector<char> data;
          if (_apiReader(_fileSystemItem.ApiFilePath, readSize, readOffset, data, _stopRequested) == ApiFileError::Success) {
            chunkPtr = std::make_unique<std::vector<char>>(data);
          }
        }
      };

      auto first = std::async(std::launch::async, [this, &readChunk]() {
        readChunk(0ull, _firstChunk);
      });

      if (_totalChunks > 1) {
        auto last = std::async(std::launch::async, [this, &readChunk]() {
          readChunk(((_totalChunks - 1ull) * _chunkSize), _lastChunk);
        });
        last.get();
      }

      first.get();

      _readChunk = _writeChunk = (_firstChunk ? ((startChunk == 0) ? 1ull : startChunk) : startChunk);

      if (_ringData.size() > 1) {
        _readerThread = std::make_unique<std::thread>([this]() {
          this->ReaderThread();
        });
      }
    }

    ~CBufferedReader() {
      NotifyStopRequested();

      UniqueMutexLock writeLock(_writeMutex);
      _readNotify.notify_all();
      writeLock.unlock();

      if (_readerThread) {
        _readerThread->join();
        _readerThread.reset();
      }
    }

  private:
    CConfig& _config;
    const FileSystemItem& _fileSystemItem;
    ApiReader& _apiReader;
    const std::size_t _chunkSize;
    const std::size_t _totalChunks;
    boost::dynamic_bitset<> _ringState;

    ApiFileError _apiFileError = ApiFileError::Success;
    std::unique_ptr<std::vector<char>> _firstChunk;
    std::unique_ptr<std::vector<char>> _lastChunk;
    std::size_t _readChunk = 0ull;
    std::mutex _readMutex;
    std::condition_variable _readNotify;
    bool _resetReader = false;
    std::vector<std::vector<char>> _ringData;
    std::unique_ptr<std::thread> _readerThread;
    bool _stopRequested = false;
    std::mutex _writeMutex;
    std::size_t _writeChunk = 0ull;

  private:
    inline bool IsActive() const {
      return not _stopRequested && (_apiFileError == ApiFileError::Success);
    }

    void ReaderThread() {
      CReaderPool readerPool(_ringState.size(), _apiReader);

      UniqueMutexLock writeLock(_writeMutex);
      writeLock.unlock();

      while (IsActive()) {
        writeLock.lock();
        while ((_writeChunk > _readChunk) &&
               (_writeChunk > (_readChunk + _ringState.size() - 1ull)) &&
               not _resetReader &&
               IsActive()) {
          _readNotify.wait(writeLock);
        }

        if (IsActive()) {
          if (_resetReader) {
            writeLock.unlock();
            readerPool.Restart();
            writeLock.lock();

            _writeChunk = _readChunk;
            _ringState.reset();
            _resetReader = false;
          }

          const auto fileOffset = _writeChunk * _chunkSize;
          const auto writeChunk = _writeChunk;
          const auto writePos = _writeChunk % _ringState.size();
          _ringState[writePos] = false;
          _readNotify.notify_all();
          writeLock.unlock();

          const auto readSize = Utils::CalculateReadSize(_fileSystemItem.Size, _chunkSize, fileOffset);
          if (readSize > 0) {
            if ((_firstChunk && (writeChunk == 0)) ||
                (_lastChunk && (writeChunk == (_totalChunks - 1ull)))) {
              writeLock.lock();
              _writeChunk++;
              _readNotify.notify_all();
            } else {
              const auto completedCallback = [this, writePos](const ApiFileError& apiFileError) {
                MutexLock writeLock(_writeMutex);
                if (apiFileError == ApiFileError::Success) {
                  _ringState[writePos] = true;
                } else if (not _resetReader) {
                  _apiFileError = apiFileError;
                }
                _readNotify.notify_all();
              };
              readerPool.QueueReadBytes(_fileSystemItem.ApiFilePath, readSize, writeChunk * _chunkSize, _ringData[writePos], completedCallback);

              writeLock.lock();
              _writeChunk++;
              _readNotify.notify_all();
            }
          } else {
            writeLock.lock();
            while (not _resetReader &&
                   ((_writeChunk * _chunkSize) >= _fileSystemItem.Size) &&
                   IsActive()) {
              _readNotify.wait(writeLock);
            }
          }
        }
        writeLock.unlock();
      }
    }

  public:
    inline std::size_t GetChunkSize() const {
      return _chunkSize;
    }

    inline void GetFirstChunk(std::vector<char>*& data) {
      data = _firstChunk.get();
    }

    inline void GetLastChunk(std::vector<char>*& data) {
      data = _lastChunk.get();
    }

    inline bool HasFirstChunk() const {
      return static_cast<bool>(_firstChunk);
    }

    inline bool HasLastChunk() const {
      return static_cast<bool>(_lastChunk);
    }

    inline void NotifyStopRequested() {
      _apiFileError = ApiFileError::DownloadStopped;
      _stopRequested = true;
    }

    ApiFileError ReadChunk(const std::size_t& chunk, std::vector<char>& data) {
      if (_apiFileError == ApiFileError::Success) {
        data.clear();

        if (_lastChunk && (chunk == (_totalChunks - 1ull)) ) {
          data = *_lastChunk;
        } else if (_firstChunk && (chunk == 0ull)) {
          data = *_firstChunk;
        } else if (_ringState.size() == 1ull) {
          const auto readOffset = chunk * _chunkSize;
          auto readSize = _chunkSize;
          if ((readSize = Utils::CalculateReadSize(_fileSystemItem.Size, readSize, readOffset)) > 0) {
            _apiFileError = _apiReader(_fileSystemItem.ApiFilePath, readSize, chunk * _chunkSize, data, _stopRequested);
          }
        } else {
          MutexLock readLock(_readMutex);
          const auto readPos = chunk % _ringState.size();
          UniqueMutexLock writeLock(_writeMutex);
          while ((_resetReader || ((chunk < _readChunk) || (chunk > _writeChunk))) && IsActive()) {
            _resetReader = true;
            _readChunk = _writeChunk = chunk;
            _readNotify.notify_all();
            _readNotify.wait(writeLock);
          }

          if (IsActive()) {
            _readChunk = chunk;
            _readNotify.notify_all();
            while (not _ringState[readPos] && IsActive()) {
              _readNotify.wait(writeLock);
            }
            data = _ringData[readPos];
            writeLock.unlock();
          } else {
            writeLock.unlock();
          }
        }
      }

      return _apiFileError;
    }
};
}
#endif //REPERTORY_BUFFEREDREADER_H
