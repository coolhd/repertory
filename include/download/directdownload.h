#ifndef REPERTORY_DIRECTDOWNLOAD_H
#define REPERTORY_DIRECTDOWNLOAD_H

#include <common.h>
#include <config.h>
#include <download/bufferedreader.h>
#include <download/idownload.h>
#include <download/events.h>
#include <events/eventsystem.h>

namespace Repertory {
class CDirectDownload final :
  public virtual IDownload {
  public:
    CDirectDownload(CConfig& config, FileSystemItem fileSystemItem, ApiReader& apiReader) :
      _config(config),
      _fileSystemItem(std::move(fileSystemItem)),
      _apiReader(apiReader) {
      _timeoutThread = std::make_unique<std::thread>([this]() {
        this->TimeoutThread();
      });
    }

    ~CDirectDownload() override {
      UniqueMutexLock timeoutLock(_timeoutMutex);
      _timeoutActive = false;
      _timeoutNotify.notify_all();
      timeoutLock.unlock();

      _stopRequested = true;

      timeoutLock.lock();
      _timeoutNotify.notify_all();
      timeoutLock.unlock();

      UniqueMutexLock readLock(_readMutex);
      if (_bufferedReader) {
        _bufferedReader->NotifyStopRequested();
      }
      readLock.unlock();

      if (_bufferedReader) {
        _bufferedReader.reset();
      }

      if (_timeoutThread) {
        _timeoutThread->join();
        _timeoutThread.reset();
      }
    }

  private:
    CConfig& _config;
    const FileSystemItem _fileSystemItem;
    ApiReader& _apiReader;

    ApiFileError _apiFileError = ApiFileError::Success;
    std::unique_ptr<CBufferedReader> _bufferedReader;
    bool _disableDownloadEnd = false;
    bool _downloadEndNotified = false;
    double _progress = 0.0;
    std::mutex _readMutex;
    bool _stopRequested = false;
    std::chrono::system_clock::time_point _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
    bool _timeoutActive = false;
    std::mutex _timeoutMutex;
    std::condition_variable _timeoutNotify;
    std::unique_ptr<std::thread> _timeoutThread;

  private:
    inline bool IsActive() const {
      return not _stopRequested && (_apiFileError == ApiFileError::Success);
    }

    inline void NotifyDownloadEnd() {
      UniqueMutexLock readLock(_readMutex);
      if (not _disableDownloadEnd && not _downloadEndNotified) {
        _downloadEndNotified = true;
        readLock.unlock();
        EventSystem::Instance().Raise<DownloadEnd>(_fileSystemItem.ApiFilePath, "Direct", _apiFileError);
      }
    }

    inline void SetApiFileError(const ApiFileError& apiFileError) {
      if ((_apiFileError == ApiFileError::Success) &&
          (_apiFileError != apiFileError)) {
        _apiFileError = apiFileError;
      }
    }

    inline void SetTimeout() {
      _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
    }

    void TimeoutThread() {
      UniqueMutexLock timeoutLock(_timeoutMutex);
      timeoutLock.unlock();
      while (not _stopRequested) {
        timeoutLock.lock();
        if (_timeoutActive) {
          _timeoutNotify.wait_until(timeoutLock, _timeout);
          if (not _stopRequested &&
              _timeoutActive &&
              (std::chrono::system_clock::now() >= _timeout)) {
            SetApiFileError(ApiFileError::DownloadTimeout);
            SetTimeout();
            timeoutLock.unlock();

            NotifyDownloadEnd();
          } else {
            timeoutLock.unlock();
          }
        } else if (not _stopRequested) {
          _timeoutNotify.wait(timeoutLock);
          timeoutLock.unlock();
        }
      }
    }

  public:
    inline ApiFileError DownloadAll() override {
      return ApiFileError::Unsupported;
    }

    inline ApiFileError GetResult() const override {
      return _apiFileError;
    }

    inline std::string GetSourceFilePath() const override {
      return "";
    }

    inline bool GetWriteSupported() const override {
      return false;
    }

    inline void NotifyStopRequested() override {
      SetApiFileError(ApiFileError::DownloadStopped);
      _stopRequested = true;
      NotifyDownloadEnd();
    }

    ApiFileError ReadBytes(std::size_t readSize, const std::uint64_t &readOffset, std::vector<char> &data) override {
      data.clear();

      if ((readSize = Utils::CalculateReadSize(_fileSystemItem.Size, readSize, readOffset)) > 0) {
        UniqueMutexLock readLock(_readMutex);
        if (IsActive()) {
          if (not _bufferedReader) {
            const auto chunkSize = _config.GetChunkSize() * 1024ull;
            const auto readChunk = readOffset / chunkSize;
            EventSystem::Instance().Raise<DownloadBegin>(_fileSystemItem.ApiFilePath, "Direct");
            _bufferedReader = std::make_unique<CBufferedReader>(_config, _fileSystemItem, _apiReader, chunkSize, Utils::DivideWithCeiling(_fileSystemItem.Size, static_cast<std::uint64_t>(chunkSize)), readChunk);
          }

          auto readChunk = readOffset / _bufferedReader->GetChunkSize();
          auto offset = readOffset % _bufferedReader->GetChunkSize();
          while (IsActive() &&
                 (readSize > 0)) {
            std::vector<char> buffer;
            auto ret = ApiFileError::Success;
            if ((ret = _bufferedReader->ReadChunk(readChunk, buffer)) == ApiFileError::Success) {
              const auto totalRead = std::min(static_cast<std::size_t>(_bufferedReader->GetChunkSize() - offset), readSize);
              data.insert(data.end(), buffer.begin() + offset, buffer.begin() + offset + totalRead);
              buffer.clear();
              readChunk++;
              offset = 0;
              readSize -= totalRead;
            } else {
              SetApiFileError(ret);

              readLock.unlock();
              NotifyDownloadEnd();
              readLock.lock();
            }
          }

          if (IsActive()  &&
              (_config.GetEventLevel() >= DownloadProgress::Level)) {
            const double curProgress = (static_cast<double>(readSize + readOffset) / static_cast<double>(_fileSystemItem.Size - 1ull)) * 100.0;
            if ((curProgress >= (_progress + 0.2)) || (curProgress <= (_progress - 0.2)) || ((curProgress == 100.00) && (_progress != curProgress))) {
              _progress = curProgress;
              EventSystem::Instance().Raise<DownloadProgress>(_fileSystemItem.ApiFilePath, "Direct", curProgress);
            }
          }
        }
      }

      return _apiFileError;
    }

    inline void ResetTimeout(const bool& fileClosed) override {
      MutexLock timeoutLock(_timeoutMutex);
      _timeoutActive = fileClosed;
      SetTimeout();
      _timeoutNotify.notify_all();
    }

    inline void SetDisableDownloadEnd(const bool& disable) override {
      _disableDownloadEnd = disable;
    }
};
}
#endif //REPERTORY_DIRECTDOWNLOAD_H
