#ifndef REPERTORY_CHUNK_EVENTS_H
#define REPERTORY_CHUNK_EVENTS_H

#include <common.h>
#include <events/eventsystem.h>

namespace Repertory {
E_SIMPLE2(DownloadBegin, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, DestPath, DEST, E_STRING
);

E_SIMPLE3(DownloadEnd, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, DestPath, DEST, E_STRING,
  ApiFileError, Result, RESULT, E_FROM_API_FILE_ERROR
);

E_SIMPLE3(DownloadProgress, Normal, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, DestPath, DEST, E_STRING,
  double, Progress, PROG, E_DOUBLE_PRECISE
);

E_SIMPLE2(DownloadTimeout, Warn, true,
  std::string, ApiFilePath, AP, E_STRING,
  std::string, DestPath, DEST, E_STRING
);
}
#endif