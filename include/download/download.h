#ifndef REPERTORY_DOWNLOAD_H
#define REPERTORY_DOWNLOAD_H

#include <common.h>

#ifdef U
#undef U
#endif
#include <boost/dynamic_bitset.hpp>
#include <config.h>
#include <download/events.h>
#include <download/idownload.h>
#include <drives/iopenfiletable.h>
#include <events/eventsystem.h>
#include <providers/iprovider.h>
#include <utils/throttle.h>
#include <utils/utils.h>

namespace Repertory {
class CDownload final :
  public virtual IDownload {
  private:
    typedef struct __ActiveChunk {
      explicit __ActiveChunk(std::thread worker) :
        Worker(std::move(worker)) {
      }

      std::thread Worker;
      std::mutex CompletedMutex;
      std::condition_variable CompletedNotify;
    } ActiveChunk;
    typedef std::shared_ptr<ActiveChunk> ActiveChunkPtr;

    typedef struct __ReadData {
      __ReadData(std::vector<char> &data, const std::size_t &offset) :
        Complete(false),
        Data(data),
        Offset(offset) {
      }

      bool Complete;
      std::vector<char> &Data;
      const std::uint64_t Offset;
      std::mutex ReadMutex;
      std::condition_variable ReadNotify;
    } ReadData;
    typedef std::shared_ptr<ReadData> ReadDataPtr;

    typedef struct __WriteData {
      __WriteData(const std::uint64_t &chunk, std::vector<char> &&data, const std::size_t &offset) :
        Chunk(chunk),
        Data(data),
        Offset(offset) {
      }

      const std::uint64_t Chunk;
      std::vector<char> Data;
      const std::uint64_t Offset;
    } WriteData;
    typedef std::shared_ptr<WriteData> WriteDataPtr;

  public:
    CDownload(CConfig &config, FileSystemItem &fileSystemItem, ApiReader& apiReader, const std::uint64_t &chunkSize, IOpenFileTable& openFileTable) :
      _config(config),
      _fileSystemItem(fileSystemItem),
      _apiReader(apiReader),
      _openFileTable(openFileTable),
      _chunkSize(chunkSize),
      _chunkState(Utils::DivideWithCeiling(fileSystemItem.Size, chunkSize)),
      _lastChunkSize(fileSystemItem.Size <= chunkSize ? fileSystemItem.Size : fileSystemItem.Size % chunkSize ? fileSystemItem.Size % chunkSize : chunkSize) {
      // Handle should always be invalid (just in case it's not - no leaking).
      UniqueRMutexLock itemLock(*fileSystemItem.ItemLock);
      if (fileSystemItem.Handle != INVALID_OSHANDLE_VALUE) {
        CNativeFile::Attach(fileSystemItem.Handle)->Close();
        fileSystemItem.Handle = INVALID_OSHANDLE_VALUE;
      }
      itemLock.unlock();

      // Update current open file information to non-existing source file and free open handle.
      const auto currentSourcePath = fileSystemItem.SourceFilePath;
      fileSystemItem.SourceFilePath = Utils::Path::Combine(_config.GetCacheDirectory(), Utils::CreateUUIDString());
      fileSystemItem.SourceFilePathChanged = true;

      // Update cache space if existing file size is >0 and delete
      std::uint64_t fileSize = 0;
      Utils::File::GetSize(currentSourcePath, fileSize);
      if (fileSize) {
        GlobalData::Instance().UpdateUsedSpace(fileSize, 0, true);
      }
      Utils::File::DeleteAsFile(currentSourcePath);

      // Update completed file information and create new lock mutex
      _fileSystemItem.SourceFilePath = Utils::Path::Combine(_config.GetCacheDirectory(), Utils::CreateUUIDString());
      _fileSystemItem.ItemLock = std::make_shared<std::recursive_mutex>();
      EventSystem::Instance().Raise<DownloadBegin>(_fileSystemItem.ApiFilePath, _fileSystemItem.SourceFilePath);      

      // Create new file for reading and writing
      if ((_apiFileError = CNativeFile::CreateOrOpen(_fileSystemItem.SourceFilePath, _readWriteFile)) == ApiFileError::Success) {
        // Pre-allocate full file and update used cache space
        if (_readWriteFile->Allocate(_fileSystemItem.Size)) {
          // Update used cache space
          GlobalData::Instance().UpdateUsedSpace(0, _fileSystemItem.Size, true);

          // Launch read-ahead workers
          for (std::uint8_t i = 0; i < _config.GetReadAheadCount(); i++) {
            _backgroundWorkers.emplace_back(std::thread([this]() {
              ReadAheadWorker();
            }));
          }

          // Launch read-behind worker
          _backgroundWorkers.emplace_back(std::thread([this]() {
            ReadBehindWorker();
          }));

          // Launch read-end worker
          if (fileSystemItem.Size > (_chunkSize * _config.GetReadAheadCount())) {
            _backgroundWorkers.emplace_back(std::thread([this]() {
              ReadEndWorker();
            }));
          }

          // Launch read/write IO worker
          _ioThread = std::make_unique<std::thread>([this]() {
            IoDataWorker();
          });
        } else {
          const auto tempErrorCode = Utils::GetLastErrorCode();
          Utils::File::DeleteAsFile(_fileSystemItem.SourceFilePath);
          Utils::SetLastErrorCode(tempErrorCode);
          _apiFileError = ApiFileError::OSErrorCode;
        }
      }
    }

    ~CDownload() override {
      if (not GetComplete()) {
        NotifyStopRequested();
      }

      if (_ioThread) {
        _ioThread->join();
      }
    }

  private:
    // Constructor initialization
    CConfig &_config;
    FileSystemItem _fileSystemItem;
    ApiReader& _apiReader;
    IOpenFileTable& _openFileTable;
    const std::uint64_t _chunkSize;
    boost::dynamic_bitset<> _chunkState;
    const std::uint64_t _lastChunkSize;

    // Default initialization
    std::unordered_map<std::uint64_t, ActiveChunkPtr> _activeChunks;
    ApiFileError _apiFileError = ApiFileError::Success;
    std::vector<std::thread> _backgroundWorkers;
    std::uint64_t _currentChunk = 0;
    bool _disableDownloadEnd = false;
    std::unique_ptr<std::thread> _ioThread;
    bool _processed = false;
    std::condition_variable _processedNotify;
    double _progress = 0.0;
    std::deque<ReadDataPtr> _readQueue;
    NativeFilePtr _readWriteFile;
    std::mutex _readWriteMutex;
    std::condition_variable _readWriteNotify;
    bool _stopRequested = false;
    std::chrono::system_clock::time_point _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
    CThrottle _writeThrottle;
    std::deque<WriteDataPtr> _writeQueue;

  private:
    void CreateActiveChunk(std::uint64_t chunk) {
      const auto chunkReadSize = (chunk == (_chunkState.size() - 1)) ? _lastChunkSize : _chunkSize;
      const auto chunkReadOffset = chunk * _chunkSize;

      auto reader = [this, chunk, chunkReadOffset, chunkReadSize]() {
        ApiFileError ret;
        _writeThrottle.IncrementOrWait();
        if (not GetComplete()) {
          std::vector<char> buffer;
          if ((ret = _apiReader(_fileSystemItem.ApiFilePath, chunkReadSize, chunkReadOffset, buffer, _stopRequested)) == ApiFileError::Success) {
            UniqueMutexLock readWriteLock(_readWriteMutex);
            if (not GetComplete()) {
              _writeQueue.emplace_back(std::make_shared<WriteData>(chunk, std::move(buffer), chunkReadOffset));
            }
            _readWriteNotify.notify_all();
            readWriteLock.unlock();
          } else {
            _apiFileError = ((_apiFileError == ApiFileError::Success) ? ret : _apiFileError);

            MutexLock readWriteLock(_readWriteMutex);
            _readWriteNotify.notify_all();
          }
        }
      };
      _activeChunks.insert({chunk,
                            std::make_shared<ActiveChunk>(std::thread(reader))});
    }

    void DownloadChunk(std::uint64_t chunk, bool inactiveOnly) {
      UniqueMutexLock readWriteLock(_readWriteMutex);
      if (not _chunkState[chunk] && not GetComplete()) {
        auto created = false;
        if (_activeChunks.find(chunk) == _activeChunks.end()) {
          CreateActiveChunk(chunk);
          created = true;
        }

        if (not inactiveOnly || created) {
          auto activeChunk = _activeChunks[chunk];
          if (not inactiveOnly) {
            _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
          }
          _readWriteNotify.notify_all();
          readWriteLock.unlock();
          
          Utils::SpinWaitForMutex([this, chunk, inactiveOnly, &readWriteLock]()->bool {
            return _chunkState[chunk] || GetComplete();
          }, activeChunk->CompletedNotify, activeChunk->CompletedMutex);
        }
      }
    }

    inline bool GetComplete() const {
      return _chunkState.all() || (_apiFileError != ApiFileError::Success) || _stopRequested || _processed;
    }

    inline bool GetTimeoutEnabled() const {
      return _config.GetEnableChunkDownloaderTimeout() && (_openFileTable.GetOpenCount(_fileSystemItem.ApiFilePath) == 0);
    }

    void HandleActiveChunkComplete(std::uint64_t chunk, UniqueMutexLock& readWriteLock) {
      readWriteLock.lock();
      auto activeChunk = _activeChunks[chunk];
      _activeChunks.erase(chunk);
      readWriteLock.unlock();

      UniqueMutexLock completedLock(activeChunk->CompletedMutex);
      activeChunk->CompletedNotify.notify_all();
      completedLock.unlock();

      activeChunk->Worker.join();
    }

    void IoDataWorker() {
      UniqueMutexLock readWriteLock(_readWriteMutex);
      readWriteLock.unlock();

      do {
        ProcessTimeout(readWriteLock);
        ProcessReadQueue(readWriteLock);
        ProcessWriteQueue(readWriteLock);
        NotifyProgress();
        ProcessDownloadComplete(readWriteLock);
        WaitForIo(readWriteLock);
      } while (not _processed);

      Shutdown(readWriteLock);
    }

    inline void NotifyProgress() {
      if ((_config.GetEventLevel() >= DownloadProgress::Level) && _chunkState.any()) {
        const double progress = (_chunkState.count() / static_cast<double>(_chunkState.size())) * 100.0;
        if ((_progress == 0.0) ||
            (progress >= (_progress + 0.2)) ||
            ((progress == 100.00) && (_progress != progress))) {
          _progress = progress;
          EventSystem::Instance().Raise<DownloadProgress>(_fileSystemItem.ApiFilePath, _fileSystemItem.SourceFilePath, progress);
        }
      }
    }

    void ProcessDownloadComplete(UniqueMutexLock& readWriteLock) {
      if (GetComplete() && not _processed) {
        readWriteLock.lock();
        if (not _processed) {
          if ((_apiFileError == ApiFileError::Success) && _chunkState.all()) {
            _openFileTable.PerformLockedOperation([this](IOpenFileTable& openFileTable, IProvider &provider) {
              FileSystemItem *fileSystemItem = nullptr;
              if (openFileTable.GetOpenFile(_fileSystemItem.ApiFilePath, fileSystemItem)) {
                UniqueRMutexLock itemLock(*fileSystemItem->ItemLock);
                // Handle should always be invalid (just in case it's not - no leaking).
                if (fileSystemItem->Handle != INVALID_OSHANDLE_VALUE) {
                  CNativeFile::Attach(fileSystemItem->Handle)->Close();
                  fileSystemItem->Handle = INVALID_OSHANDLE_VALUE;
                }
                fileSystemItem->Handle = _readWriteFile->GetHandle();
                itemLock.unlock();

                fileSystemItem->SourceFilePath = _fileSystemItem.SourceFilePath;
                fileSystemItem->SourceFilePathChanged = true;
                _fileSystemItem = *fileSystemItem;
              } else {
                provider.SetSourcePath(_fileSystemItem.ApiFilePath, _fileSystemItem.SourceFilePath);
                _readWriteFile->Close();
              }
            });
          } else if (_apiFileError == ApiFileError::Success) {
            _apiFileError = ApiFileError::DownloadFailed;
          }

          if (_apiFileError != ApiFileError::Success) {
            _readWriteFile->Close();

            if (Utils::File::DeleteAsFile(_fileSystemItem.SourceFilePath)) {
              GlobalData::Instance().UpdateUsedSpace(_fileSystemItem.Size, 0, true);
            }
          }

          _stopRequested = _processed = true;
          readWriteLock.unlock();
          
          _writeThrottle.Shutdown();
          
          ProcessReadQueue(readWriteLock);

          readWriteLock.lock();
          _processedNotify.notify_all();
          _readWriteNotify.notify_all();
          readWriteLock.unlock();

          if (not _disableDownloadEnd) {
            EventSystem::Instance().Raise<DownloadEnd>(_fileSystemItem.ApiFilePath, _fileSystemItem.SourceFilePath, _apiFileError);
          }
        } else {
          readWriteLock.unlock();
        }
      }
    }

    void ProcessReadQueue(UniqueMutexLock& readWriteLock) {
      readWriteLock.lock();
      while (not _readQueue.empty()) {
        auto readData = _readQueue.front();
        _readQueue.pop_front();
        readWriteLock.unlock();

        if (_apiFileError == ApiFileError::Success) {
          std::size_t bytesRead = 0;
          if (not _readWriteFile->ReadBytes(&readData->Data[0], readData->Data.size(), readData->Offset, bytesRead)) {
            _apiFileError = ((_apiFileError == ApiFileError::Success) ? ApiFileError::OSErrorCode : _apiFileError);
          }
        }

        readData->Complete = true;
        UniqueMutexLock readLock(readData->ReadMutex);
        readData->ReadNotify.notify_all();
        readLock.unlock();

        readWriteLock.lock();
      }
      readWriteLock.unlock();
    }

    inline void ProcessTimeout(UniqueMutexLock& readWriteLock) {
      if ((std::chrono::system_clock::now() >= _timeout) &&
        not GetComplete() &&
        GetTimeoutEnabled()) {
        _apiFileError = ApiFileError::DownloadTimeout;
        _stopRequested = true;
        readWriteLock.lock();
        _readWriteNotify.notify_all();
        readWriteLock.unlock();
        EventSystem::Instance().Raise<DownloadTimeout>(_fileSystemItem.ApiFilePath, _fileSystemItem.SourceFilePath);
      }
    }

    void ProcessWriteQueue(UniqueMutexLock& readWriteLock) {
      readWriteLock.lock();
      do {
        if (not _writeQueue.empty()) {
          auto writeData = _writeQueue.front();
          _writeQueue.pop_front();
          readWriteLock.unlock();

          if (not GetComplete()) {
            std::size_t bytesWritten = 0;
            if (_readWriteFile->WriteBytes(&writeData->Data[0], writeData->Data.size(), writeData->Offset, bytesWritten)) {
              _readWriteFile->Flush();
              _chunkState[writeData->Chunk] = true;
            } else {
              _apiFileError = ((_apiFileError == ApiFileError::Success) ? ApiFileError::OSErrorCode : _apiFileError);
            }
          }

          _writeThrottle.Decrement();

          HandleActiveChunkComplete(writeData->Chunk, readWriteLock);

          readWriteLock.lock();
        }
      } while (not _writeQueue.empty() && _readQueue.empty());
      readWriteLock.unlock();
    }

    void ReadAheadWorker() {
      const auto totalChunks = _chunkState.size();

      auto firstChunk = _currentChunk;
      auto getNextChunk = [&firstChunk](const std::uint64_t currentChunk, const std::uint64_t lastChunk) -> std::uint64_t {
        if (currentChunk == firstChunk) {
          return lastChunk + 1ul;
        } else {
          firstChunk = currentChunk;
          return currentChunk + 1ul;
        }
      };

      while (not GetComplete()) {
        for (auto chunk = getNextChunk(_currentChunk, 0); (chunk < totalChunks) && not GetComplete();) {
          DownloadChunk(chunk, true);
          chunk = getNextChunk(_currentChunk, chunk);
        }

        Utils::SpinWaitForMutex([this, &firstChunk]()->bool {
          return GetComplete() || (firstChunk != _currentChunk);
        }, _readWriteNotify, _readWriteMutex);
      }
    }

    void ReadBehindWorker() {
      auto firstChunk = _currentChunk;
      auto getNextChunk = [&firstChunk](const std::uint64_t currentChunk, const std::uint64_t lastChunk) -> std::uint64_t {
        if (currentChunk == firstChunk) {
          return lastChunk ? lastChunk - 1ul : 0;
        } else {
          firstChunk = currentChunk;
          return currentChunk ? currentChunk - 1ul : 0;
        }
      };

      while (not GetComplete()) {
        for (auto chunk = getNextChunk(_currentChunk, _currentChunk); chunk && not GetComplete();) {
          DownloadChunk(chunk, true);
          chunk = getNextChunk(_currentChunk, chunk);
        }

        Utils::SpinWaitForMutex([this, &firstChunk]()->bool {
          return GetComplete() || (firstChunk != _currentChunk);
        }, _readWriteNotify, _readWriteMutex);
      }
    }

    void ReadEndWorker() {
      const auto totalChunks = _chunkState.size();
      auto toRead = Utils::DivideWithCeiling(std::uint64_t(262144), _chunkSize);
      for (auto chunk = totalChunks ? totalChunks - 1ul : 0; chunk && toRead && not GetComplete(); chunk--, toRead--) {
        DownloadChunk(chunk, true);
      }
    }

    void Shutdown(UniqueMutexLock& readWriteLock) {
      _writeThrottle.Shutdown();

      readWriteLock.lock();
      while (not _activeChunks.empty()) {
        const auto chunk = _activeChunks.begin()->first;
        readWriteLock.unlock();
        HandleActiveChunkComplete(chunk, readWriteLock);
        readWriteLock.lock();
      }
      readWriteLock.unlock();

      for (auto &worker : _backgroundWorkers) {
        worker.join();
      }
      _backgroundWorkers.clear();
    }

    inline void WaitForIo(UniqueMutexLock& readWriteLock) {
      readWriteLock.lock();
      if (not GetComplete() && _readQueue.empty() && _writeQueue.empty()) {
        if (GetTimeoutEnabled()) {
          _readWriteNotify.wait_until(readWriteLock, _timeout);
        } else {
          _readWriteNotify.wait(readWriteLock);
        }
      }
      readWriteLock.unlock();
    }

  public:
    ApiFileError DownloadAll() override {
      const auto totalChunks = _chunkState.size();
      for (std::uint64_t chunk = 0; not GetComplete() && (chunk < totalChunks); chunk++) {
        DownloadChunk(chunk, false);
      }
      Utils::SpinWaitForMutex(_processed, _processedNotify, _readWriteMutex);
      return _apiFileError;
    }

    inline ApiFileError GetResult() const override {
      return _apiFileError;
    }

    inline std::string GetSourceFilePath() const override {
      return _fileSystemItem.SourceFilePath;
    }

    inline bool GetWriteSupported() const override {
      return true;
    }

    inline void NotifyStopRequested() override {
      if (not _stopRequested) {
        _apiFileError = ApiFileError::DownloadStopped;
        _stopRequested = true;
      }
    }

    ApiFileError ReadBytes(std::size_t readSize, const std::uint64_t &readOffset, std::vector<char> &data) override {
      data.clear();
      if (readSize > 0) {
        const auto readFromSource = [&]() -> ApiFileError {
          NativeFilePtr nativeFile;
          auto ret = Utils::File::AssignAndGetNativeFile(_fileSystemItem, nativeFile);
          if (ret == ApiFileError::Success) {
            std::size_t bytesRead = 0;
            data.resize(readSize);
            if (not nativeFile->ReadBytes(&data[0], data.size(), readOffset, bytesRead)) {
              ret = ApiFileError::OSErrorCode;
            }
          }
          return ret;
        };

        if (_apiFileError == ApiFileError::Success) {
          const auto startChunk = _currentChunk = readOffset / _chunkSize;
          const auto endChunk = (readSize + readOffset) / _chunkSize;
          for (std::uint64_t chunk = startChunk; not GetComplete() && (chunk <= endChunk); chunk++) {
            DownloadChunk(chunk, false);
          }

          if (_apiFileError == ApiFileError::Success) {
            UniqueMutexLock readWriteLock(_readWriteMutex);
            if (_processed) {
              readWriteLock.unlock();
              return ((_apiFileError == ApiFileError::Success) ? readFromSource() : _apiFileError);
            } else {
              data.resize(readSize);
              auto readData = std::make_shared<ReadData>(data, readOffset);
              _readQueue.emplace_front(readData);
              _readWriteNotify.notify_all();
              readWriteLock.unlock();

              ResetTimeout(false);
              Utils::SpinWaitForMutex(readData->Complete, readData->ReadNotify, readData->ReadMutex);
              ResetTimeout(false);
            }
          }
        }
      } else {
        return ApiFileError::Success;
      }

      return _apiFileError;
    }

    void ResetTimeout(const bool& fileClosed) override {
      MutexLock readWriteLock(_readWriteMutex);
      _timeout = std::chrono::system_clock::now() + std::chrono::seconds(_config.GetChunkDownloaderTimeoutSeconds());
      _readWriteNotify.notify_all();
    }

    inline void SetDisableDownloadEnd(const bool& disable) override {
      _disableDownloadEnd = disable;
    }
};
}

#endif //REPERTORY_DOWNLOAD_H
