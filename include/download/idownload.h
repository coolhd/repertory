#ifndef REPERTORY_IDOWNLOAD_H
#define REPERTORY_IDOWNLOAD_H

#include <common.h>

namespace Repertory {
class IDownload {
  INTERFACE_SETUP(IDownload);

  public:
    virtual ApiFileError DownloadAll() = 0;

    virtual ApiFileError GetResult() const = 0;

    virtual std::string GetSourceFilePath() const = 0;

    virtual bool GetWriteSupported() const = 0;

    virtual void NotifyStopRequested() = 0;

    virtual ApiFileError ReadBytes(std::size_t readSize, const std::uint64_t &readOffset, std::vector<char> &data) = 0;

    virtual void ResetTimeout(const bool& fileClosed) = 0;

    virtual void SetDisableDownloadEnd(const bool& disable) = 0;
};

typedef std::shared_ptr<IDownload> DownloadPtr;
}
#endif //REPERTORY_IDOWNLOAD_H
