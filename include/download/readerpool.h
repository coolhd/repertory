#ifndef REPERTORY_READERPOOL_H
#define REPERTORY_READERPOOL_H

#include <common.h>

namespace Repertory {
class CReaderPool final {
  public:
    typedef std::function<void(const ApiFileError& apiFileError)> CompletedCallback;

  private:
    typedef struct __WorkItem {
      __WorkItem(std::string apiFilePath, const std::size_t& readSize, const std::uint64_t& readOffset, std::vector<char>& data, CompletedCallback callback) :
        ApiFilePath(std::move(apiFilePath)),
        ReadSize(readSize),
        ReadOffset(readOffset),
        Data(data),
        Callback(callback) {
      }

      std::string ApiFilePath;
      std::size_t ReadSize;
      std::uint64_t ReadOffset;
      std::vector<char>& Data;
      CompletedCallback Callback;
    } WorkItem;

  public:
    CReaderPool(const std::size_t& poolSize, ApiReader& apiReader) :
      _poolSize(poolSize),
      _apiReader(apiReader) {
      Start();
    }

    ~CReaderPool() {
      Stop();
    }

  private:
    const std::size_t _poolSize;
    ApiReader& _apiReader;
    bool _restartActive = false;
    bool _stopRequested = false;
    std::mutex _workMutex;
    std::condition_variable _workNotify;
    std::deque<std::shared_ptr<WorkItem>> _workQueue;
    std::vector<std::thread> _workThreads;
    std::uint16_t _activeCount = 0u;

  private:
    inline void ProcessWorkItem(WorkItem& workItem) {
      const auto result = _apiReader(workItem.ApiFilePath, workItem.ReadSize, workItem.ReadOffset, workItem.Data, _restartActive);
      workItem.Callback(result);
    }

    void Start() {
      _restartActive = _stopRequested = false;
      _activeCount = 0;
      if (_poolSize > 1) {
        for (std::size_t i = 0; i < _poolSize; i++) {
          _workThreads.emplace_back(std::thread([this]() {
            UniqueMutexLock workLock(_workMutex);
            workLock.unlock();

            while (not _stopRequested) {
              workLock.lock();
              if (not _stopRequested) {
                if (_workQueue.empty()) {
                  _workNotify.wait(workLock);
                }

                if (not _workQueue.empty()) {
                  _activeCount++;
                  auto workItem = _workQueue.front();
                  _workQueue.pop_front();
                  _workNotify.notify_all();
                  workLock.unlock();

                  ProcessWorkItem(*workItem);

                  workLock.lock();
                  _activeCount--;
                  _workNotify.notify_all();
                }
              }
              workLock.unlock();
            }

            workLock.lock();
            _workNotify.notify_all();
            workLock.unlock();
          }));
        }
      }
    }

    void Stop() {
      _stopRequested = true;
      Restart();
      _restartActive = true;

      UniqueMutexLock workLock(_workMutex);
      _workNotify.notify_all();
      workLock.unlock();

      for (auto& workThread : _workThreads) {
        workThread.join();
      }
      _workThreads.clear();
    }

  public:
    void QueueReadBytes(const std::string& apiFilePath, const std::size_t& readSize, const std::uint64_t& readOffset, std::vector<char>& data,  CompletedCallback completedCallback) {
      data.clear();

      if (_poolSize == 1ull) {
        completedCallback(_apiReader(apiFilePath, readSize, readOffset, data, _restartActive));
      } else {
        auto workItem = std::make_shared<WorkItem>(apiFilePath, readSize, readOffset, data, completedCallback);
        UniqueMutexLock workLock(_workMutex);
        if (not _restartActive) {
          while ((_workQueue.size() >= _poolSize) &&
                 not _restartActive) {
            _workNotify.wait(workLock);
          }

          if (not _restartActive) {
            _workQueue.emplace_back(workItem);
            _workNotify.notify_all();
          }

          workLock.unlock();
        }
      }
    }

    inline void Restart() {
      _restartActive = true;
      UniqueMutexLock workLock(_workMutex);
      _workNotify.notify_all();
      workLock.unlock();

      while (not _workQueue.empty() || _activeCount) {
        workLock.lock();
        if (not _workQueue.empty() || _activeCount) {
          _workNotify.wait(workLock);
        }
        workLock.unlock();
      }

      workLock.lock();
      _restartActive = false;
      _workNotify.notify_all();
      workLock.unlock();
    }
};
}
#endif //REPERTORY_READERPOOL_H