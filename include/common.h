#ifndef REPERTORY_COMMON_H
#define REPERTORY_COMMON_H

#ifdef _WIN32
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <Windows.h>
#include <Shlwapi.h>
#include <ciso646>
#endif

#include <chrono>
#include <json.hpp>
#include <mutex>
#include <utils/stringutils.h>
#include <ttmath.h>
#include <unordered_map>
#include <utils/globaldata.h>

#define NANOS_PER_SECOND 1000000000L

#ifdef REPERTORY_VERSION
const std::string RepertoryVersion = REPERTORY_VERSION;
#else
const std::string RepertoryVersion = "0.0.0";
#endif

#ifdef REPERTORY_GIT_REV
const std::string RepertoryGitRev = REPERTORY_GIT_REV;
#else
const std::string RepertoryGitRev = "unknown";
#endif

#ifdef _WIN32
const std::string COLLATE = " COLLATE NOCASE ";
#define INVALID_OSHANDLE_VALUE INVALID_HANDLE_VALUE
#define OSHandle HANDLE
static inline std::string CreateLookupPath(const std::string& str) {
  return Repertory::Utils::String::ToLower(str);
}
#else
const std::string COLLATE = " ";
#define CreateLookupPath(s) s
#define INVALID_OSHANDLE_VALUE -1
#define OSHandle int
#endif

#define META_ACCESSED   "accessed"
#define META_ATTRIBUTES "attributes"
#define META_CREATION   "creation"
#define META_MODIFIED   "modified"
#define META_SOURCE     "source"
#define META_WRITTEN    "written"
#ifndef _WIN32
#define META_GID        "gid"
#define META_MODE       "mode"
#define META_UID        "uid"
#endif
#ifdef __APPLE__
#define META_OSXFLAGS   "flags"
#define META_BACKUP     "backup"
#define META_CHANGED    "changed"
#endif

#define MIN_SIA_VERSION "1.4.1"
#define MIN_SP_VERSION "1.4.0"
#define REPERTORY_CONFIG_VERSION 4ull

const auto DEFAULT_CHUNK_SIZE = (40 * 1024 * 1024ull) - 28;

#define INTERFACE_SETUP(name) \
  public:\
    name(const name&) noexcept = delete;\
    name(name&&) noexcept = delete;\
    name& operator=(const name&) noexcept = delete;\
    name& operator=(name&&) noexcept = delete;\
  protected:\
    name() = default;\
  public:\
    virtual ~name() = default

enum class ProviderType {
  Sia,
  SiaPrime
};

const std::vector<std::string> META_USED_NAMES = {
  META_ACCESSED,
  META_ATTRIBUTES,
  META_CREATION,
  META_MODIFIED,
  META_SOURCE,
  META_WRITTEN
#ifndef _WIN32
  ,
  META_GID,
  META_MODE,
  META_UID
#endif
#ifdef __APPLE__
  ,
  META_OSXFLAGS,
  META_BACKUP,
  META_CHANGED
#endif
};

#define INITIALIZE_SINGLETONS()\
namespace Repertory {\
  template<typename TEvent>\
  TEventSystem<TEvent> TEventSystem<TEvent>::_eventSystem;\
  GlobalData GlobalData::_globalData;\
  Polling Polling::_polling;\
}

using namespace std::chrono_literals;
using json = nlohmann::json;

namespace Repertory {
#ifdef _WIN32
class ComInitWrapper {
  public:
    ComInitWrapper() :
      _uninit(SUCCEEDED(::CoInitializeEx(nullptr, COINIT_APARTMENTTHREADED))) {
    }

    ~ComInitWrapper() {
      if (_uninit) {
        ::CoUninitialize();
      }
    }

  private:
    const BOOL _uninit;
};
#endif

typedef ttmath::UInt<64> Hastings;
typedef ttmath::Big<1, 30> ApiCurrency;

class StartupException :
  public virtual std::runtime_error {
  public:
    explicit StartupException(const std::string &msg) :
      std::runtime_error(msg) {
    }
};

enum class ApiError {
    Success = 0,
    CommError,
    CommApiError,
    GeneralError,
    OSErrorCode
};

enum class ApiFileError {
    Success = 0,
    AccessDenied,
    BufferTooSmall,
    BufferOverflow,
    DirectoryEndOfFiles,
    DirectoryExists,
    DirectoryNotEmpty,
    DirectoryNotFound,
    DownloadFailed,
    DownloadStopped,
    DownloadTimeout,
    Error,
    FileExists,
    FileInUse,
    InvalidOperation,
    InvalidOSHandle,
    ItemNotFound,
    OSErrorCode,
    PermissionDenied,
    Unsupported,
    UploadFailed,
    UploadQueued
#ifndef _WIN32
    ,
    XAttrBufferSmall,
    XAttrExists,
    XAttrInvalidNamespace,
    XAttrNotFound,
    XAttrTooBig
#endif
#ifdef __APPLE__
    ,
    XAttrOSXInvalid
#endif
};

enum class DownloadType {
    Direct,
    Fallback,
    RingBuffer
};

enum class ExitCode {
    Success,
    CommunicationError = -1,
    FileCreationFailed = -2,
    IncompatibleVersion = -3,
    InvalidSyntax = -4,
    LockFailed = -5,
    MountActive = -6,
    MountResult = -7,
    NotMounted = -8,
    StartupException = -9
};

enum class LockResult {
    Success,
    Locked,
    Failure
};

typedef struct {
  std::string AgentString;
  std::string ApiPassword;
  std::uint16_t ApiPort;
  std::string HostNameOrIp;
  std::uint32_t TimeoutMs;
} HostConfig;

typedef struct {
  std::uint64_t AccessedDate;
  std::string ApiFilePath;
  std::string ApiParent;
  bool Available;
  std::uint64_t ChangedDate;
  std::uint64_t CreationDate;
  std::uint64_t Expiration;
  std::uint64_t FileSize;
  bool IsOnDisk;
  bool LegacyChunks;
  std::uint64_t ModifiedDate;
  bool Recoverable;
  double Redundancy;
  bool Renewing;
  std::string SourceFilePath;
  std::uint64_t UploadedBytes;
  double UploadProgress;
} ApiFile;

#ifdef _WIN32
typedef struct {
  void* DirectoryBuffer = nullptr;
} OpenFileData;
#else
typedef int OpenFileData;
#endif

typedef struct {
  std::string ApiFilePath;
  std::string ApiParent;
  bool Directory;
} DirectoryItem;

typedef struct {
  std::string ApiFilePath;
  std::string ApiParent;
  bool Directory;
  OSHandle Handle = INVALID_OSHANDLE_VALUE;
  bool Changed = false;
  std::shared_ptr<std::recursive_mutex> ItemLock;
  bool LegacyChunks = true;
  bool MetaChanged = false;
  double Redundancy = 0.0;
  std::uint64_t Size = 0;
  std::string SourceFilePath;
  bool SourceFilePathChanged = false;
  std::unordered_map<std::uint64_t, OpenFileData> OpenData;
} FileSystemItem;

typedef std::vector<ApiFile> ApiFileList;
typedef std::unordered_map<std::string, std::string> ApiMetaMap;
typedef std::vector<DirectoryItem> DirectoryItemList;
typedef std::unordered_map<std::string, std::string> HttpParameters;
typedef std::function<void(const std::string &apiFilePath, const std::string &parent, const std::string &source, const bool &directory, const std::uint64_t &createDate, const std::uint64_t &accessDate, const std::uint64_t &modifiedDate, const std::uint64_t &changedDate)> ApiItemAdded;
typedef std::function<void(const std::string &apiFilePath, std::function<void()> operation)> OpenFileCheckOperation;

typedef struct {
  std::string Host;
  std::string Password;
  std::uint16_t Port;
  std::string User;
} RPCHostInfo;

enum class RPCResponseType {
    Success,
    ConfigValueNotFound,
    CURLError,
    HTTPError
};

typedef struct {
  RPCResponseType ResponseType;
  json Data;
} RPCResponse;

typedef std::lock_guard<std::mutex> MutexLock;
typedef std::unique_lock<std::mutex> UniqueMutexLock;
typedef std::lock_guard<std::recursive_mutex> RMutexLock;
typedef std::unique_lock<std::recursive_mutex> UniqueRMutexLock;

namespace RPCMethod {
const std::string getConfig = "getConfig";
const std::string getConfigValueByName = "getConfigValueByName";
const std::string setConfigValueByName = "setConfigValueByName";
const std::string getDriveInformation = "getDriveInformation";
const std::string getOpenFiles = "getOpenFiles";
const std::string unmount = "unmount";
}

#define ADD_CURL_LOCALHOST_RESOLVE(handle, port) \
  const auto localHost = "localhost:" + std::to_string(port) + ":127.0.0.1"; \
  struct curl_slist *hostSlist = curl_slist_append(nullptr, &localHost[0]); \
  curl_easy_setopt(handle, CURLOPT_RESOLVE, hostSlist)

#define FREE_CURL_LOCALHOST_RESOLVE() \
  curl_slist_free_all(hostSlist)

typedef std::function<ApiFileError(const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested)> ApiReader;
}
#endif //REPERTORY_COMMON_H
