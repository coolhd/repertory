#ifndef REPERTORY_CLIENT_H
#define REPERTORY_CLIENT_H
#include <common.h>
#include <curl/curl.h>
#include <utils/Base64.h>

namespace Repertory {

class CClient {
  public:
    explicit CClient(RPCHostInfo rpcHostInfo) :
      _rpcHostInfo(std::move(rpcHostInfo)) {
      _requestId = 0;
    }
    
  private:
    const RPCHostInfo _rpcHostInfo;
    std::atomic<std::uint32_t> _requestId;
    
  private:
    RPCResponse MakeRequest(const std::string& command, const std::vector<json>& args) {
      auto rpcErrorType = RPCResponseType::Success;
      
      CURL* curlHandle = curl_easy_init();
      curl_easy_reset(curlHandle);

      const auto port = static_cast<std::uint16_t>(_rpcHostInfo.Port);
      const auto url = "http://" + _rpcHostInfo.Host + ":" + std::to_string(port) + "/api";
      const auto request = json({
        {"jsonrpc", "2.0"},
        {"id", std::to_string(++_requestId)},
        {"method", command},
        {"params", args}
      }).dump();

      struct curl_slist *headers = nullptr;
      headers = curl_slist_append(headers, "Content-Type: application/json;");
      if (not (_rpcHostInfo.Password.empty() && _rpcHostInfo.User.empty())) {
        curl_easy_setopt(curlHandle, CURLOPT_USERNAME, &_rpcHostInfo.User[0]);
        curl_easy_setopt(curlHandle, CURLOPT_PASSWORD, &_rpcHostInfo.Password[0]);
      }
#if __APPLE__
      curl_easy_setopt(curlHandle, CURLOPT_NOSIGNAL, 1);
#endif
      ADD_CURL_LOCALHOST_RESOLVE(curlHandle, _rpcHostInfo.Port);

      curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT_MS, 10000);
      curl_easy_setopt(curlHandle, CURLOPT_HTTPHEADER, headers);
      curl_easy_setopt(curlHandle, CURLOPT_URL, url.c_str());
      curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, request.c_str());
      curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDSIZE, request.size());
      curl_easy_setopt(curlHandle, CURLOPT_CONNECTTIMEOUT, 5L);
      curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *outstream) -> size_t {
        (*reinterpret_cast<std::string *>(outstream)) += std::string(buffer, size * nitems);
        return size * nitems;
      }));

      std::string response;
      curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &response);

      json responseData;
      const auto res = curl_easy_perform(curlHandle);
      if (res == CURLE_OK) {
        long httpErrorCode;
        curl_easy_getinfo (curlHandle, CURLINFO_RESPONSE_CODE, &httpErrorCode);
        if (httpErrorCode == 200) {
          responseData = json::parse(response.begin(), response.end());
        } else {
          json parsed;
          try {
            parsed = json::parse(response.begin(), response.end());
          } catch (...) {
          }
          rpcErrorType = RPCResponseType::HTTPError;
          responseData = {
            { "error",
              {
                { "code", std::to_string(httpErrorCode) }
              }
            }
          };
          if (parsed.empty()) {
            responseData["error"]["response"] = response;
          } else {
            responseData["error"]["response"] = parsed;
          }
        }
      } else {
        rpcErrorType = RPCResponseType::CURLError;
        responseData = {
          { "error",
            {
              {"message", curl_easy_strerror(res)}
            }
          }
        };
      }
      curl_slist_free_all(headers);
      curl_easy_cleanup(curlHandle);

      FREE_CURL_LOCALHOST_RESOLVE();
      
      return RPCResponse({
        rpcErrorType,
        responseData
      });
    }
    
  public:
    RPCResponse GetDriveInformation() {
      auto ret = MakeRequest(RPCMethod::getDriveInformation, {});
      if (ret.ResponseType == RPCResponseType::Success) {
        ret.Data = ret.Data["result"];
      }

      return ret;
    }

    RPCResponse GetConfig() {
      auto ret = MakeRequest(RPCMethod::getConfig, {});
      if (ret.ResponseType == RPCResponseType::Success) {
        ret.Data = ret.Data["result"];
      }

      return ret;
    }

    RPCResponse GetConfigValueByName(const std::string& name) {
      auto ret = MakeRequest(RPCMethod::getConfigValueByName, {name});
      if (ret.ResponseType == RPCResponseType::Success) {
        if (ret.Data["result"]["value"].get<std::string>().empty()) {
          ret.ResponseType = RPCResponseType::ConfigValueNotFound;
        } else {
          ret.Data = ret.Data["result"];
        }
      }

      return ret;
    }

    RPCResponse GetOpenFiles() {
      auto ret = MakeRequest(RPCMethod::getOpenFiles, {});
      if (ret.ResponseType == RPCResponseType::Success) {
        ret.Data = ret.Data["result"];
      }

      return ret;
    }
    
    RPCResponse SetConfigValueByName(const std::string& name, const std::string& value) {
      auto ret = MakeRequest(RPCMethod::setConfigValueByName, {name, value});
      if (ret.ResponseType == RPCResponseType::Success) {
        if (ret.Data["result"]["value"].get<std::string>().empty()) {
          ret.ResponseType = RPCResponseType::ConfigValueNotFound;
        } else {
          ret.Data = ret.Data["result"];
        }
      }

      return ret;
    }

    RPCResponse Unmount() {
      auto ret = MakeRequest(RPCMethod::unmount, {});
      if (ret.ResponseType == RPCResponseType::Success) {
        ret.Data = ret.Data["result"];
      }

      return ret;
    }
};
}
#endif //REPERTORY_CLIENT_H
