#ifndef REPERTORY_SERVER_H
#define REPERTORY_SERVER_H
#include <common.h>
#include <config.h>
#include <cpprest/http_listener.h>
#include <drives/iopenfiletable.h>
#include <jsonrp.hpp>
#include <utils/Base64.h>

using namespace web::http;
using namespace web::http::experimental::listener;

namespace Repertory {
class CServer {
  public:
    explicit CServer(CConfig& config, IProvider& provider, IOpenFileTable& openFileTable) :
      _config(config),
      _provider(provider),
      _openFileTable(openFileTable) {
      _listenerList.push_back(std::make_unique<http_listener>(Utils::CreateHttpWebUri("http://localhost:" + Utils::String::FromUInt16(_config.GetAPIPort()) + "/api")));

      for (auto &listener : _listenerList) {
        ConfigureListener(listener.get());
      }
    }

    ~CServer() {
      Stop();
      _listenerList.clear();
    }

  private:
    typedef std::vector<std::unique_ptr<http_listener>> ListenerList;
    
  private:
    CConfig& _config;
    IProvider& _provider;
    IOpenFileTable& _openFileTable;
    ListenerList _listenerList;
    std::mutex _startStopMutex;
    bool _started = false;

  private:
    bool CheckAuthorization(const http_request& httpRequest) {
      auto ret = (_config.GetAPIAuth().empty() && _config.GetAPIUser().empty());
      if (not ret) {
#ifdef _WIN32
        const auto it = httpRequest.headers().find(L"Authorization");
#else
        const auto it = httpRequest.headers().find("Authorization");
#endif
        if (it != httpRequest.headers().end()) {
#ifdef _WIN32
          const auto authData = Utils::String::ToUtf8(it->second);
#else
          const auto authData = it->second;
#endif
          const auto authParts = Utils::String::Split(authData, ' ');
          if (not authParts.empty()) {
            const auto authType = authParts[0];
            if (authType == "Basic") {
              const auto data = macaron::Base64::Decode(authData.substr(6));
              const auto auth = Utils::String::Split(std::string(data.begin(), data.end()), ':');
              if (auth.size() == 2) {
                const auto user = auth[0];
                const auto pwd = auth[1];
                ret = (user == _config.GetAPIUser()) && (pwd == _config.GetAPIAuth());
              }
            }
          }
        }
      }

      return ret;
    }

    void ConfigureListener(http_listener* listener) {
      listener->support(methods::POST, [this](http_request httpRequest) {
        try {
          if (CheckAuthorization(httpRequest)) {
            std::unique_ptr<jsonrpcpp::Response> response;
            HandleRequest(&httpRequest, response);
            if (response) {
              httpRequest.reply(web::http::status_codes::OK, response->to_json().dump());
            }
          } else {
            httpRequest.reply(web::http::status_codes::Unauthorized);
          }
        } catch (const jsonrpcpp::RequestException &e) {
          httpRequest.reply(web::http::status_codes::BadRequest, e.to_json().dump());
          EventSystem::Instance().Raise<RPCServerException>(e.to_json().dump());
        } catch (const std::exception& e2) {
          httpRequest.reply(web::http::status_codes::InternalError, e2.what());
          EventSystem::Instance().Raise<RPCServerException>(e2.what());
        }
      });
    }

    void HandleRequest(http_request* httpRequest, std::unique_ptr<jsonrpcpp::Response>& response) {
      jsonrpcpp::entity_ptr entity = jsonrpcpp::Parser::do_parse(Utils::String::ToUtf8(httpRequest->extract_string().get()));
      if (entity->is_request()) {
        jsonrpcpp::request_ptr request = std::dynamic_pointer_cast<jsonrpcpp::Request>(entity);
        if (request->method == RPCMethod::getConfig) {
          if (request->params.param_array.empty()) {
            response = std::make_unique<jsonrpcpp::Response>(
              *request,
              _config.GetJson()
            );
          } else {
            throw jsonrpcpp::InvalidParamsException(*request);
          }
        } else if (request->method == RPCMethod::getConfigValueByName) {
          if (request->params.param_array.size() == 1) {
            response = std::make_unique<jsonrpcpp::Response>(
              *request,
              json({{"value", _config.GetValueByName(request->params.param_array[0])}})
            );
          } else {
            throw jsonrpcpp::InvalidParamsException(*request);
          }
        } else if (request->method == RPCMethod::getDriveInformation) {
          if (request->params.param_array.empty()) {
            response = std::make_unique<jsonrpcpp::Response>(
              *request,
              json({
                {"cache_space_used", GlobalData::Instance().GetUsedCacheSpace()},
                {"drive_space_total", _provider.GetTotalDriveSpace()},
                {"drive_space_used", GlobalData::Instance().GetUsedDriveSpace()},
                {"item_count", _provider.GetTotalItemCount()}
              })
            );
          } else {
            throw jsonrpcpp::InvalidParamsException(*request);
          }
        } else if (request->method == RPCMethod::getOpenFiles) {
          if (request->params.param_array.empty()) {
            const auto list = _openFileTable.GetOpenFiles();
            json openFiles = {{"file_list", std::vector<json>()}};
            for (const auto& kv : list) {
              openFiles["file_list"].emplace_back(json({
                {"path", kv.first},
                {"count", kv.second}
              }));
            }
            response = std::make_unique<jsonrpcpp::Response>(
              *request,
              openFiles
            );
          } else {
            throw jsonrpcpp::InvalidParamsException(*request);
          }
        } else if (request->method == RPCMethod::setConfigValueByName) {
          if (request->params.param_array.size() == 2) {
            response = std::make_unique<jsonrpcpp::Response>(
              *request,
              json({{"value", _config.SetValueByName(request->params.param_array[0], request->params.param_array[1])}})
            );
          } else {
            throw jsonrpcpp::InvalidParamsException(*request);
          }
        } else if (request->method == RPCMethod::unmount) {
          if (request->params.param_array.empty()) {
            EventSystem::Instance().Raise<UnmountRequested>();
            response = std::make_unique<jsonrpcpp::Response>(
              *request,
              json({{"success", true}})
            );
          } else {
            throw jsonrpcpp::InvalidParamsException(*request);
          }
        } else {
          throw jsonrpcpp::MethodNotFoundException(*request);
        }
      }
    }

  public:
    void Start() {
      MutexLock l(_startStopMutex);
      if (not _started) {
        for (auto &listener : _listenerList) {
          listener->open().wait();
        }
        _started = true;
      }
    }

    void Stop() {
      MutexLock l(_startStopMutex);
      if (_started) {
        for (auto &listener : _listenerList) {
          listener->close().wait();
        }
        _started = false;
      }
    }
};
}
#endif //REPERTORY_SERVER_H
