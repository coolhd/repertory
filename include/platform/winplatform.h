#ifndef REPERTORY_WINPLATFORM_H
#define REPERTORY_WINPLATFORM_H
#ifdef _WIN32
#include <common.h>

namespace Repertory {
// Shared data segment
#pragma data_seg("SHARED")
  char g_siaMountLocation[3] = {0};
  std::int64_t g_siaPID = -1;
  char g_siaPrimeMountLocation[3] = {0};
  std::int64_t g_siaPrimePID = -1;
#pragma data_seg()
#pragma comment(linker, "/SECTION:SHARED,RWS")

// Instance detection
static auto siaMutex = INVALID_HANDLE_VALUE;
static auto siaPrimeMutex = INVALID_HANDLE_VALUE;
static DWORD siaMutexState = WAIT_OBJECT_0;
static DWORD siaPrimeMutexState = WAIT_OBJECT_0;
static HANDLE *mtx;
static DWORD *mtxState;

static std::vector<bool> CheckStatus(std::string &siaLocation, std::string &siaPrimeLocation, std::int64_t &siaPID, std::int64_t &siaPrimePID) {
  siaMutex = ::CreateMutex(nullptr, FALSE, "repertory_sia");
  siaPrimeMutex = ::CreateMutex(nullptr, FALSE, "repertory_siaprime");

  auto threadFunc = [](HANDLE &mtx, DWORD &mtxState) {
    if (mtx != INVALID_HANDLE_VALUE) {
      for (auto i = 0; (i < 30) && ((mtxState = ::WaitForSingleObject(mtx, 100)) == WAIT_TIMEOUT); i++) {
      }

      if ((mtxState == WAIT_ABANDONED) || (mtxState == WAIT_OBJECT_0)) {
        ::ReleaseMutex(mtx);
      }
      ::CloseHandle(mtx);
    }
  };

  std::thread th1([threadFunc]() {
    threadFunc(siaMutex, siaMutexState);
  });

  std::thread th2([threadFunc]() {
    threadFunc(siaPrimeMutex, siaPrimeMutexState);
  });

  th1.join();
  th2.join();

  std::vector<bool> ret({(g_siaPID != -1) && (siaMutexState == WAIT_TIMEOUT),
                         (g_siaPrimePID != -1) && (siaPrimeMutexState == WAIT_TIMEOUT)});

  siaLocation = g_siaMountLocation;
  siaPrimeLocation = g_siaPrimeMountLocation;
  siaPID = g_siaPID;
  siaPrimePID = g_siaPrimePID;

  return std::move(ret);
}

static LockResult GrabLock(const ProviderType& providerType, std::uint8_t retryCount = 30) {
  auto ret = LockResult::Success;

  mtx = nullptr;
  mtxState = nullptr;
  if (providerType == ProviderType::SiaPrime) {
    siaPrimeMutex = CreateMutex(nullptr, FALSE, "repertory_siaprime");
    mtx = &siaPrimeMutex;
    mtxState = &siaPrimeMutexState;
  } else {
    siaMutex = CreateMutex(nullptr, FALSE, "repertory_sia");
    mtx = &siaMutex;
    mtxState = &siaMutexState;
  }

  if (*mtx == INVALID_HANDLE_VALUE) {
    ret = LockResult::Failure;
  } else {
    for (auto i = 0; (i < retryCount) && ((*mtxState = WaitForSingleObject(*mtx, 100)) == WAIT_TIMEOUT); i++);

    switch (*mtxState) {
    case WAIT_OBJECT_0: ret = LockResult::Success; break;
    case WAIT_TIMEOUT: ret = LockResult::Locked; break;
    default: ret = LockResult::Failure; break;
    }
  }

  return ret;
}

static json ReadLockedData() {
  std::string siaLocation, siaPrimeLocation;
  std::int64_t siaPID, siaPrimePID;
  const auto active = CheckStatus(siaLocation, siaPrimeLocation, siaPID, siaPrimePID);
  return {
    {"Sia", {
       {"Active", active[0]},
       {"Location", active[0] ? siaLocation : ""},
       {"PID", active[0] ? siaPID : -1}}},
    {"SiaPrime", {
      {"Active", active[1]},
      {"Location", active[1] ? siaPrimeLocation: ""},
      {"PID", active[1] ? siaPrimePID : -1}}}
  };
}

static void ReleaseLock(const LockResult& lockResult, const ProviderType& providerType) {
  if (*mtxState == WAIT_OBJECT_0) {
    ReleaseMutex(*mtx);
  }
  CloseHandle(*mtx);
}
}
#endif

#endif //REPERTORY_WINPLATFORM_H
