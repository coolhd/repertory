#ifndef REPERTORY_UNIXPLATFORM_H
#define REPERTORY_UNIXPLATFORM_H
#ifndef _WIN32
#include <common.h>
#include <config.h>
#include <fcntl.h>
#include <sys/file.h>
#include <thread>
#include <unistd.h>
#include <utils/nativefile.h>

namespace Repertory {
int g_siaLockFd = -1;
int g_siaPrimeLockFd = -1;

static std::string GetLockFile(const ProviderType& providerType) {
  auto lockFile = std::string("/tmp/repertory_") + CConfig::GetProviderName(providerType) + std::to_string(getuid());
  return std::move(lockFile);
}

static LockResult GrabLock(const ProviderType& providerType, std::uint8_t retryCount = 30) {
  const auto lockFile = GetLockFile(providerType);
  auto& lockFd = (providerType == ProviderType::SiaPrime) ?
                 g_siaPrimeLockFd :
                 g_siaLockFd;
  lockFd = open(&lockFile[0], O_CREAT | O_RDWR, S_IWUSR | S_IRUSR);

  int ret = EWOULDBLOCK;
  for (auto i = 0; (i < retryCount) && (ret == EWOULDBLOCK); i++) {
    if ((ret = flock(lockFd, LOCK_EX | LOCK_NB)) == -1) {
      if (((ret = errno) == EWOULDBLOCK) && (i != (retryCount - 1))) {
        std::this_thread::sleep_for(100ms);
      }
    }
  }

  switch (ret) {
  case 0: ftruncate(lockFd, 0); return LockResult::Success;
  case EWOULDBLOCK: return LockResult::Locked;
  default:
    return LockResult::Failure;
  }
}

static json ReadLockedData() {
  json siaData = {
    {"Active", false},
    {"Location", ""},
    {"PID", -1}
  };

  json siaPrimeData = {
    {"Active", false},
    {"Location", ""},
    {"PID", -1}
  };

  auto readLockData = [](const ProviderType& providerType, json& jsonData) {
    const auto lockFile = GetLockFile(providerType);
    auto fd = open(&lockFile[0], O_RDONLY);
    if (fd != -1) {
      auto nativeFile = CNativeFile::Attach(fd);
      std::uint64_t fileSize;
      if (nativeFile->GetFileSize(fileSize)) {
        std::string buffer;
        buffer.resize(fileSize);

        std::size_t bytesRead;
        if (nativeFile->ReadBytes(&buffer[0], fileSize, 0, bytesRead)) {
          if (bytesRead > 0) {
            try {
              jsonData = json::parse(buffer.begin(), buffer.end());
            } catch (const json::exception&) {

            }
          }
        }
      }
      nativeFile->Close();
    }
  };
  readLockData(ProviderType::Sia, siaData);
  readLockData(ProviderType::SiaPrime, siaPrimeData);

  json data;
  data["Sia"] = siaData;
  data["SiaPrime"] = siaPrimeData;

  return data;
}

static void ReleaseLock(const LockResult& lockResult, const ProviderType& providerType) {
  const auto lockFile = GetLockFile(providerType);
  auto& lockFd = (providerType == ProviderType::SiaPrime) ?
                 g_siaPrimeLockFd :
                 g_siaLockFd;
  if (lockResult == LockResult::Success) {
    unlink(&lockFile[0]);
    flock(lockFd, LOCK_UN);
  }

  if (lockFd != -1) {
    close(lockFd);
  }
}
}
#endif
#endif //REPERTORY_UNIXPLATFORM_H
