#ifndef REPERTORY_MULTIREQUEST_H
#define REPERTORY_MULTIREQUEST_H

#include <common.h>
#include <algorithm>
#include <condition_variable>
#include <curl/curl.h>
#include <curl/multi.h>
#include <mutex>
#include <thread>
#include <utils/utils.h>
#include <vector>

namespace Repertory {
class CMultiRequest {
  public:
    CMultiRequest(CURL *curlHandle, const bool &stopRequested) :
      _curlHandle(curlHandle),
      _stopRequested(stopRequested),
      _multiHandle(curl_multi_init()) {
      curl_multi_add_handle(_multiHandle, curlHandle);
    }

    ~CMultiRequest() {
      curl_multi_remove_handle(_multiHandle, _curlHandle);
      curl_easy_cleanup(_curlHandle);
      curl_multi_cleanup(_multiHandle);
    }

  private:
    CURL *_curlHandle;
    const bool &_stopRequested;
    CURLM *_multiHandle;

  public:
    void GetResult(CURLcode &curlCode, long &httpCode) {
      curlCode = CURLcode::CURLE_ABORTED_BY_CALLBACK;
      httpCode = -1;

      auto error = false;
      int runningHandles = 0;
      curl_multi_perform(_multiHandle, &runningHandles);
      while (not error & (runningHandles > 0) && not _stopRequested) {
        int ignored;
        curl_multi_wait(_multiHandle, nullptr, 0, 100, &ignored);

        const auto ret = curl_multi_perform(_multiHandle, &runningHandles);
        error = (ret != CURLM_CALL_MULTI_PERFORM) && (ret != CURLM_OK);
      }

      if (not _stopRequested) {
        int remainingMsgs = 0;
        auto *multiRes = curl_multi_info_read(_multiHandle, &remainingMsgs);
        if (multiRes && (multiRes->msg == CURLMSG_DONE)) {
          curl_easy_getinfo(multiRes->easy_handle, CURLINFO_RESPONSE_CODE, &httpCode);
          curlCode = multiRes->data.result;
        }
      }
    }
};
}
#endif //REPERTORY_MULTIREQUEST_H
