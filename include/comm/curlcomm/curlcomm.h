#ifndef REPERTORY_CURLCOMM_H
#define REPERTORY_CURLCOMM_H

#include <common.h>
#include <comm/curlcomm/multirequest.h>
#include <comm/icomm.h>
#include <config.h>
#include <curl/curl.h>
#include <curl/multi.h>

namespace Repertory {
class CCurlComm :
  public virtual IComm {
  private:
    typedef struct {
      std::vector<char>* Buffer;
      const bool& StopRequested;
    } RawWriteData;

  public:
    explicit CCurlComm(CConfig& config) :
      _config(config) {
    }

    ~CCurlComm() override = default;

  private:
    CConfig& _config;

  private:
    CURL* CommonCurlSetup(const std::string &path, const bool &post, const HttpParameters &parameters, const HostConfig &hostConfig, const bool& allowTimeout, std::string &url, std::string &fields, void *writeData) {
      auto *curlHandle = curl_easy_init();
      curl_easy_reset(curlHandle);
      url = ConstructPath(curlHandle, path, hostConfig);
      if (post) {
        for (const auto &param : parameters) {
          if (not fields.empty()) {
            fields += "&";
          }
          if (param.first == "new" + CConfig::GetProviderPathName(_config.GetProviderType())) {
            fields += (param.first + "=" + UrlEncode(curlHandle, param.second));
          } else {
            fields += (param.first + "=" + param.second);
          }
        }
        curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, &fields[0]);
      } else if (not parameters.empty()) {
        url += "?";
        for (const auto &param : parameters) {
          if (url[url.size() - 1] != '?') {
            url += "&";
          }
          url += (param.first + "=" + UrlEncode(curlHandle, param.second));
        }
      }
#if __APPLE__
      curl_easy_setopt(curlHandle, CURLOPT_NOSIGNAL, 1);
#endif
      curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, &hostConfig.AgentString[0]);
      curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, writeData);
      curl_easy_setopt(curlHandle, CURLOPT_URL, url.c_str());
      if (allowTimeout) {
        curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT_MS, hostConfig.TimeoutMs);
      }
      if (not hostConfig.ApiPassword.empty()) {
        curl_easy_setopt(curlHandle, CURLOPT_USERNAME, "");
        curl_easy_setopt(curlHandle, CURLOPT_PASSWORD, &hostConfig.ApiPassword[0]);
      }

      return curlHandle;
    }

    inline std::string ConstructPath(CURL* curlHandle, const std::string &relativePath, const HostConfig& hostConfig) const {
      auto ret = "http://" + hostConfig.HostNameOrIp + ":" + std::to_string(hostConfig.ApiPort) + UrlEncode(curlHandle, relativePath, true);
      return std::move(ret);
    }

    ApiError GetOrPost(const bool& post, const std::string &path, const HttpParameters& parameters, json &data, json &error) {
      std::string fields;
      const auto hostConfig = _config.GetHostConfig();
      std::string result;
      std::string url;
      auto* curlHandle = CommonCurlSetup(path, post, parameters, hostConfig, true, url, fields, &result);

      ADD_CURL_LOCALHOST_RESOLVE(curlHandle, hostConfig.ApiPort);

      curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *outstream) -> size_t {
        (*reinterpret_cast<std::string *>(outstream)) += std::string(buffer, size * nitems);
        return size * nitems;
      }));

      if (_config.GetEventLevel() >= EventLevel::Verbose) {
        if (post) {
          EventSystem::Instance().Raise<CommPostBegin>(url, fields);
        } else {
          EventSystem::Instance().Raise<CommGetBegin>(url);
        }
      }

      const auto curlCode = curl_easy_perform(curlHandle);

      long httpCode = -1;
      curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpCode);
      const auto ret = ProcessResponse(curlCode, httpCode, result.size(), [&]()->std::string {
          return result;
        }, [&]() {
          if (result.length()) {
            data = json::parse(result.c_str());
          }
        }, error);

      if (_config.GetEventLevel() >= EventLevel::Verbose) {
        if (post) {
          EventSystem::Instance().Raise<CommPostEnd>(url, curlCode, httpCode, ((ret == ApiError::Success) ? data.dump(2) : error.dump(2)));
        } else {
          EventSystem::Instance().Raise<CommGetEnd>(url, curlCode, httpCode, ((ret == ApiError::Success) ? data.dump(2) : error.dump(2)));
        }
      }

      curl_easy_cleanup(curlHandle);
      FREE_CURL_LOCALHOST_RESOLVE();
      return ret;
    }

    ApiError ProcessResponse(const CURLcode &res, const long &httpCode, const std::size_t& dataSize, std::function<std::string()> stringConverter, std::function<void()> successProcessor, json& error) const {
      auto ret = ApiError::Success;

      auto constructError = [&]() {
        ret = ApiError::CommError;
        const auto* curlStr = curl_easy_strerror(res);
        std::string strError(curlStr ? curlStr : "");
        error["message"] = strError + ":" + std::to_string(httpCode);
      };

      if ((res == CURLE_OK) && ((httpCode >= 200) && (httpCode < 300))) {
        if (successProcessor) {
          successProcessor();
        }
      } else if (dataSize == 0) {
        constructError();
      } else {
        try {
          const auto tmp = json::parse(stringConverter().c_str());
          if (tmp.find("message") != tmp.end()) {
            ret = ApiError::CommApiError;
            error = tmp;
          } else {
            constructError();
          }
        } catch (...) {
          constructError();
        }
      }

      return ret;
    }

    std::string UrlEncode(CURL* curlHandle, const std::string &data, const bool &allowSlash = false) const {
      auto *value = curl_easy_escape(curlHandle, data.c_str(), 0);
      std::string ret = value;
      curl_free(value);

      if (allowSlash) {
        Utils::String::Replace(ret, "%2F", "/");
      }
      return std::move(ret);
    }

  public:
    inline ApiError Get(const std::string &path, json &data, json &error) override {
      return GetOrPost(false, path, {}, data, error);
    }

    inline ApiError Get(const std::string &path, const HttpParameters& httpParameters, json &data, json &error) override {
      return GetOrPost(false, path, httpParameters, data, error);
    }

    ApiError GetRaw(const std::string& path, const HttpParameters& parameters, std::vector<char>& data, json& error, const bool& stopRequested) override {
      std::string fields;
      const auto hostConfig = _config.GetHostConfig();
      std::string url;
      RawWriteData wd = {&data, stopRequested};
      auto* curlHandle = CommonCurlSetup(path, false, parameters, hostConfig, false, url, fields, &wd);

      ADD_CURL_LOCALHOST_RESOLVE(curlHandle, hostConfig.ApiPort);

      curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *outstream) -> size_t {
        auto* writeData = reinterpret_cast<RawWriteData*>(outstream);
        std::copy(buffer, buffer + (size * nitems), std::back_inserter(*writeData->Buffer));
        return writeData->StopRequested ? 0 : size * nitems;
      }));

      if (_config.GetEventLevel() >= CommGetRawBegin::Level) {
        EventSystem::Instance().Raise<CommGetRawBegin>(url);
      }

      CMultiRequest request(curlHandle, stopRequested);
      CURLcode curlCode;
      long httpCode;
      request.GetResult(curlCode, httpCode);

      const auto ret = ProcessResponse(curlCode, httpCode, data.size(), [&]()->std::string {
          return (data.empty() ? "" : std::string(&data[0], data.size()));
        }, nullptr, error);
      if (_config.GetEventLevel() >= CommGetRawEnd::Level) {
        EventSystem::Instance().Raise<CommGetRawEnd>(url, curlCode, httpCode, ((ret == ApiError::Success) ? "" : error.dump(2)));
      }

      FREE_CURL_LOCALHOST_RESOLVE();
      return ret;
    }

    inline ApiError Post(const std::string &path, json &data, json &error) override {
      return GetOrPost(true, path, {}, data, error);
    }

    inline ApiError Post(const std::string &path, const HttpParameters& parameters, json &data, json &error) override {
      return GetOrPost(true, path, parameters, data, error);
    }

    ApiError PostFile(const std::string &path, const std::string &sourcePath, const HttpParameters& parameters, OSHandle handle, json &error, bool& cancel) override {
      auto ret = ApiError::OSErrorCode;
      std::uint64_t fileSize;
      if (Utils::File::GetSize(sourcePath, fileSize)) {
        std::string fields;
        const auto hostConfig = _config.GetHostConfig();
        std::string url;

        std::string result;
        auto *curlHandle = CommonCurlSetup(path, false, parameters, hostConfig, false, url, fields, &result);
        curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *outstream) -> size_t {
          (*reinterpret_cast<std::string *>(outstream)) += std::string(buffer, size * nitems);
          return size * nitems;
        }));
        curl_easy_setopt(curlHandle, CURLOPT_UPLOAD, 1L);
        curl_easy_setopt(curlHandle, CURLOPT_INFILESIZE_LARGE, fileSize);

        ADD_CURL_LOCALHOST_RESOLVE(curlHandle, hostConfig.ApiPort);

#ifdef _WIN32
        typedef struct {
          bool* Cancel = nullptr;
          CNativeFile* NativeFile = nullptr;
          std::uint64_t Offset = 0;
        } ReadData;

        auto nativeFile = CNativeFile::Attach(handle);
        ReadData readData{};
        readData.Cancel = &cancel;
        readData.NativeFile = nativeFile.get();
        curl_easy_setopt(curlHandle, CURLOPT_READDATA, &readData);

        curl_easy_setopt(curlHandle, CURLOPT_READFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *instream) -> size_t {
          auto* readData = reinterpret_cast<ReadData*>(instream);
          std::size_t bytesRead;
          const auto ret = readData->NativeFile->ReadBytes(buffer, size * nitems, readData->Offset, bytesRead);
          if (ret) {
            readData->Offset += bytesRead;
          }
          return ret && not *readData->Cancel ? bytesRead : 0;
        }));
#else
        curl_easy_setopt(curlHandle, CURLOPT_READDATA, handle);
#endif
        if (_config.GetEventLevel() >= CommPostFileBegin::Level) {
          EventSystem::Instance().Raise<CommPostFileBegin>(url);
        }

        CMultiRequest request(curlHandle, cancel);
        CURLcode curlCode;
        long httpCode;
        request.GetResult(curlCode, httpCode);

        ret = ProcessResponse(curlCode, httpCode, result.size(), [&]() -> std::string {
          return result;
        }, nullptr, error);

        if (_config.GetEventLevel() >= CommPostFileEnd::Level) {
          EventSystem::Instance().Raise<CommPostFileEnd>(url, curlCode, httpCode, ((ret == ApiError::Success) ? "" : error.dump(2)));
        }
        FREE_CURL_LOCALHOST_RESOLVE();
      }

      return ret;
    }
};
}

#endif //REPERTORY_CURLCOMM_H
