#ifndef REPERTORY_ICOMM_H
#define REPERTORY_ICOMM_H

#include <common.h>

namespace Repertory {
class IComm {
  INTERFACE_SETUP(IComm);

  public:
    virtual ApiError Get(const std::string &path, json &data, json &error) = 0;

    virtual ApiError Get(const std::string &path, const HttpParameters& httpParameters, json &data, json &error) = 0;

    virtual ApiError GetRaw(const std::string& path, const HttpParameters& parameters, std::vector<char>& data, json& error, const bool& stopRequested) = 0;

    virtual ApiError Post(const std::string &path, json &data, json &error) = 0;

    virtual ApiError Post(const std::string &path, const HttpParameters& parameters, json &data, json &error) = 0;

    virtual ApiError PostFile(const std::string &path, const std::string &sourcePath, const HttpParameters& parameters, OSHandle handle, json &error, bool& cancel) = 0;
};
}

#endif //REPERTORY_ICOMM_H
