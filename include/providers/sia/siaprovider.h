#ifndef REPERTORY_SIAPROVIDER_H
#define REPERTORY_SIAPROVIDER_H

#include <config.h>
#include <drives/iopenfiletable.h>
#include <events/events.h>
#include <providers/iprovider.h>
#include <rocksdb/db.h>

#ifdef _WIN32
#define GET_REAL_API_FILE_PATH(varName, apiFilePath) \
  const auto varName = GetRealApiFilePath(apiFilePath)
#else
#define GET_REAL_API_FILE_PATH(varName, apiFilePath) \
  const auto& varName = apiFilePath
#endif

namespace Repertory {
class CSiaProvider :
  public virtual IProvider {
  public:
    CSiaProvider(CConfig &config, IComm &comm) :
      _config(config),
      _comm(comm) {
#ifndef _WIN32
      rocksdb::Env::Default()->SetAllowNonOwnerAccess(false);
#endif
      rocksdb::Options options{};
      options.create_if_missing = true;
      options.db_log_dir = _config.GetLogDirectory();
      options.keep_log_file_num = 100;

      // Initialize meta database
      rocksdb::DB *db = nullptr;
      const auto status = rocksdb::DB::Open(options, Utils::Path::Combine(config.GetDataDirectory(), ROCKS_DB_NAME), &db);
      if (status.ok()) {
        _metaDb.reset(db);
      } else {
        EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, status.ToString());
        throw StartupException(status.ToString());
      }
    }

    ~CSiaProvider() override {
      _metaDb.reset();
    }

  private:
    CConfig &_config;
    IComm &_comm;
    ApiItemAdded _apiItemAdded;
    IOpenFileTable* _openFileTable = nullptr;
    std::recursive_mutex _notifyAddedMutex;
    std::uint64_t _totalDriveSpace = 0;
    bool _stopRequested = false;
    std::mutex _startStopMutex;
    std::condition_variable _startStopNotify;
    std::unique_ptr<std::thread> _driveSpaceThread;
    std::unique_ptr<rocksdb::DB> _metaDb;
    const std::string ROCKS_DB_NAME = "metadb";

  private:
    inline ApiItemAdded &GetApiItemAdded() {
      return _apiItemAdded;
    }

    inline CConfig &GetConfig() {
      return _config;
    }

    inline CConfig &GetConfig() const {
      return _config;
    }

    inline IComm &GetComm() {
      return _comm;
    }

    inline IComm &GetComm() const {
      return _comm;
    }

    inline rocksdb::DB &GetMetaDb() {
      return *_metaDb;
    }

    inline rocksdb::DB &GetMetaDb() const {
      return *_metaDb;
    }

  private:
    void CalculateTotalDriveSpace() {
      std::uint64_t ret = 0;
      json result, error;
      auto stbm = GetConfig().GetStorageByteMonth();
      const auto success = (GetComm().Get("/renter/prices", result, error) == ApiError::Success);
      if (success || (stbm > 0)) {
        if (success) {
          stbm = result["storageterabytemonth"].get<std::string>();
          GetConfig().SetStorageByteMonth(stbm);
        }
        if (GetComm().Get("/renter", result, error) == ApiError::Success) {
          const auto funds = Utils::HastingsStringToApiCurrency(result["settings"]["allowance"]["funds"].get<std::string>());
          if ((stbm > 0) && (funds > 0)) {
            const auto fundsHastings = Utils::ApiCurrencyToHastings(funds);
            ttmath::Parser<ApiCurrency> parser;
            parser.Parse(fundsHastings.ToString() + " / " + stbm.ToString() + " * 1e12");
            ret = not parser.stack.empty() ? parser.stack[0].value.ToUInt() : 0;
          }
        }
      }

      _totalDriveSpace = ret;
    }

    ApiError CalculateUsedDriveSpace(std::uint64_t &usedSpace) {
      ApiFileList apiFileList;
      const auto ret = GetFileList(apiFileList);
      usedSpace = std::accumulate(apiFileList.begin(), apiFileList.end(), std::uint64_t(0), [this](std::uint64_t a, const auto &v) {
        std::string value;
        if (not this->GetMetaDb().KeyMayExist(rocksdb::ReadOptions(), CreateLookupPath(v.ApiFilePath), &value)) {
          this->NotifyFileAdded(v.ApiFilePath, Utils::Path::GetParentApiPath(v.ApiFilePath), 0);
        }
        return a + v.FileSize;
      });
      return ret;
    }

    void DriveSpaceThread() {
      while (not _stopRequested) {
        UniqueMutexLock l(_startStopMutex);
        if (not _stopRequested) {
          _startStopNotify.wait_for(l, 5s);
        }
        l.unlock();

        if (not _stopRequested) {
          CalculateTotalDriveSpace();
        }
      }
    }

    void NotifyDirectoryAdded(const std::string &apiFilePath, const std::string &parent) const {
      auto *provider = const_cast<CSiaProvider *>(this);
      RMutexLock l(provider->_notifyAddedMutex);

      const auto dateNow = Utils::GetFileTimeNow();
      provider->GetApiItemAdded()(apiFilePath, parent, "", true, dateNow, dateNow, dateNow, dateNow);

#ifdef _WIN32
      provider->SetRealApiFilePath(apiFilePath);
#endif
    }

    ApiFileError NotifyFileAdded(const std::string &apiFilePath, const std::string &parent, const std::uint64_t& size) const {
      auto *provider = const_cast<CSiaProvider *>(this);
      RMutexLock l(provider->_notifyAddedMutex);
      
      auto ret = ApiFileError::Error;
      json data;
      json error;
      const auto apiError = GetComm().Get("/renter/file" + Utils::Path::EscapeADS(apiFilePath), data, error);
      if (apiError == ApiError::Success) {
        ret = ApiFileError::Success;
        ApiFile apiFile{};
        CreateApiFile(data["file"], CConfig::GetProviderPathName(GetConfig().GetProviderType()), apiFile);

        provider->GetApiItemAdded()(apiFilePath, parent, apiFile.SourceFilePath, false, apiFile.CreationDate, apiFile.AccessedDate, apiFile.ModifiedDate, apiFile.ChangedDate);

#ifdef _WIN32
        provider->SetRealApiFilePath(apiFilePath);
#endif
        if (size) {
          GlobalData::Instance().IncrementUsedDriveSpace(size);
        }
      } else {
        EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, error.dump(2));
      }

      return ret;
    }

    bool ProcessOrphanedFile(const std::string& sourceFilePath, const std::string& apiFilePath = "") const {
      auto ret = false;
      const auto orphanedDir = Utils::Path::Combine(GetConfig().GetDataDirectory(), "orphaned");

      EventSystem::Instance().Raise<OrphanedFileDetected>(sourceFilePath);
      const auto orphanedFile = Utils::Path::Combine(orphanedDir, Utils::Path::StripToFileName(apiFilePath.empty() ? sourceFilePath : apiFilePath));

      if (Utils::File::ResetModifiedTime(sourceFilePath) && Utils::File::MoveAsFile(sourceFilePath, orphanedFile)) {
        EventSystem::Instance().Raise<OrphanedFileProcessed>(sourceFilePath, orphanedFile);
        ret = true;
      } else {
        EventSystem::Instance().Raise<OrphanedFileProcessingFailed>(sourceFilePath, orphanedFile, std::to_string(Utils::GetLastErrorCode()));
      }

      return ret;
    }

    void RemoveDeletedFiles(const bool& recalculateSpace) {
      auto dbIter = std::unique_ptr<rocksdb::Iterator>(GetMetaDb().NewIterator(rocksdb::ReadOptions()));
      for (dbIter->SeekToFirst(); not _stopRequested && dbIter->Valid(); dbIter->Next()) {
        const auto apiFilePath = dbIter->key().ToString();
#ifdef _WIN32
        if (not (Utils::String::BeginsWith(apiFilePath, "source:") || Utils::String::BeginsWith(apiFilePath, "real:"))) {
#else
        if (not Utils::String::BeginsWith(apiFilePath, "source:")) {
#endif
          if (apiFilePath.empty()) {
            RemoveItem(apiFilePath);
          } else {
            std::string sourceFilePath;
            if (not IsDirectory(apiFilePath) &&
              (CheckFileExists(apiFilePath) == ApiFileError::ItemNotFound) &&
              (_GetItemMeta(apiFilePath, META_SOURCE, sourceFilePath) == ApiFileError::Success)) {
              if (not sourceFilePath.empty()) {
                _openFileTable->PerformLockedOperation([this, &recalculateSpace, &apiFilePath, &sourceFilePath](IOpenFileTable&, IProvider&) {
                  if (_openFileTable->CheckNoOpenFileHandles()) {
                    std::uint64_t usedSpace = 0;
                    if (not recalculateSpace || (CalculateUsedDriveSpace(usedSpace) == ApiError::Success)) {
                      RemoveItem(apiFilePath);
                      EventSystem::Instance().Raise<FileRemovedExternally>(apiFilePath, sourceFilePath);

                      if (recalculateSpace) {
                        std::uint64_t fileSize = 0;
                        if (Utils::File::GetSize(sourceFilePath, fileSize) && ProcessOrphanedFile(sourceFilePath, apiFilePath)) {
                          GlobalData::Instance().UpdateUsedSpace(fileSize, 0, true);
                        }

                        GlobalData::Instance().InitializeUsedDriveSpace(usedSpace);
                      }
                    }
                  }
                  });
              }
            }
          }
        }
      }
    }

    void RemoveExpiredOrphanedFiles() {
      const auto orphanedDir = Utils::Path::Combine(GetConfig().GetDataDirectory(), "orphaned");
      const auto files = Utils::File::GetDirectoryFiles(orphanedDir, true);
      for (const auto& file : files) {
        if (Utils::File::IsModifiedDateOlderThan(file, std::chrono::hours(GetConfig().GetOrphanedFileRetentionDays() * 24))) {
          if (Utils::File::DeleteAsFile(file)) {
            EventSystem::Instance().Raise<OrphanedFileDeleted>(file);
          }
        }
      }
    }

    void RemoveUnknownSourceFiles() {
      auto files = Utils::File::GetDirectoryFiles(GetConfig().GetCacheDirectory(), true);
      while (not _stopRequested && not files.empty()) {
        const auto file = files.front();
        files.pop_front();

        std::string apiFilePath;
        if (GetMetaDb().Get(rocksdb::ReadOptions(), "source:" + CreateLookupPath(file), &apiFilePath).IsNotFound()) {
          ProcessOrphanedFile(file);
        }
      }
    }

  protected:
    ApiFileError CheckFileExists(const std::string &apiFilePath) const {
      auto ret = ApiFileError::Error;
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);

      json data, error;
      const auto apiError = GetComm().Get("/renter/file" + Utils::Path::EscapeADS(realApiFilePath), data, error);
      if (apiError == ApiError::Success) {
        ret = ApiFileError::Success;
      } else if (CheckNotFound(error)) {
        ret = ApiFileError::ItemNotFound;
      }

      return ret;
    }

    inline bool CheckDirectoryFound(const json &error) const {
      return ((error.find("message") != error.end()) && (Utils::String::Contains(error["message"].get<std::string>(), "a siadir already exists at that location")));
    }

    inline bool CheckNotFound(const json &error) const {
      return ((error.find("message") != error.end()) && 
        (Utils::String::Contains(error["message"].get<std::string>(), "no file known") ||
          Utils::String::Contains(error["message"].get<std::string>(), "no such file or directory") ||
          Utils::String::Contains(error["message"].get<std::string>(), "cannot find the file") ||
          Utils::String::Contains(error["message"].get<std::string>(), "no siadir known with that path") ||
          Utils::String::Contains(error["message"].get<std::string>(), "cannot find the path")));
    }

    virtual void Cleanup() {
      RemoveDeletedFiles(false);
      RemoveUnknownSourceFiles();
      RemoveExpiredOrphanedFiles();
    }

    void CreateApiFile(const json &file, const std::string &pathName, ApiFile &apiFile) const {
      apiFile.ApiFilePath = Utils::Path::UnEscapeADS(Utils::Path::CreateApiPath(file[pathName].get<std::string>()));
      apiFile.ApiParent = Utils::Path::GetParentApiPath(apiFile.ApiFilePath);
      apiFile.Available = file["available"].get<bool>();
      apiFile.Expiration = file["expiration"].get<std::uint64_t>();
      apiFile.FileSize = file["filesize"].get<std::uint64_t>();
      apiFile.IsOnDisk = file["ondisk"].get<bool>();
      apiFile.LegacyChunks = GetUsesLegacyChunks();
      apiFile.Recoverable = file["recoverable"].get<bool>();
      apiFile.Redundancy = file["redundancy"].get<double>();
      apiFile.Renewing = file["renewing"].get<bool>();
      apiFile.SourceFilePath = file["localpath"].get<std::string>();
      apiFile.UploadedBytes = file["uploadedbytes"].get<std::uint64_t>();
      apiFile.UploadProgress = file["uploadprogress"].get<double>();

      SetApiFileDates(file, apiFile);
    }

    ApiFileError _GetItemMeta(const std::string &apiFilePath, ApiMetaMap &meta) const {
      auto ret = ApiFileError::ItemNotFound;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      std::string keyValue;
      if (GetMetaDb().Get(rocksdb::ReadOptions(), lookupPath, &keyValue).ok()) {
        const auto jsonData = json::parse(keyValue);
        for (auto it = jsonData.begin(); it != jsonData.end(); it++) {
          meta.insert({it.key(),
                       it.value().get<std::string>()});
        }
        ret = ApiFileError::Success;
      }

      return ret;
    }

    ApiFileError _GetItemMeta(const std::string &apiFilePath, const std::string &key, std::string &value) const {
      auto ret = ApiFileError::ItemNotFound;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      std::string keyValue;
      if (GetMetaDb().Get(rocksdb::ReadOptions(), lookupPath, &keyValue).ok()) {
        const auto jsonData = json::parse(keyValue);
        if (jsonData.find(key) != jsonData.end()) {
          value = jsonData[key].get<std::string>();
        }
        ret = ApiFileError::Success;
      }

      return ret;
    }
#ifdef _WIN32
    inline std::string GetRealApiFilePath(const std::string &apiFilePath) const {
      std::string realApiFilePath;
      if (GetMetaDb().Get(rocksdb::ReadOptions(), "real:" + CreateLookupPath(apiFilePath), &realApiFilePath).ok()) {
        if (not realApiFilePath.empty()) {
          return realApiFilePath;
        }
      }
      return apiFilePath;
    }
#endif
    virtual bool GetUsesLegacyChunks() const {
      static auto ret = (Utils::CompareVersionStrings(MIN_SIA_VERSION, "1.4.1") < 0);
      return ret;
    }

    void RemoveItem(const std::string &apiFilePath) {
      std::string source;
      GetItemMeta(apiFilePath, META_SOURCE, source);
      GetMetaDb().Delete(rocksdb::WriteOptions(), "source:" + CreateLookupPath(source));
      GetMetaDb().Delete(rocksdb::WriteOptions(), CreateLookupPath(apiFilePath));
#ifdef _WIN32
      GetMetaDb().Delete(rocksdb::WriteOptions(), "real:" + CreateLookupPath(apiFilePath));
#endif
    }

    void SetApiFileDates(const json &file, ApiFile &apiFile) const {
      apiFile.AccessedDate = Utils::ConvertApiDate(file["accesstime"].get<std::string>());
      apiFile.ChangedDate = Utils::ConvertApiDate(file["changetime"].get<std::string>());
      apiFile.CreationDate = Utils::ConvertApiDate(file["createtime"].get<std::string>());
      apiFile.ModifiedDate = Utils::ConvertApiDate(file["modtime"].get<std::string>());
    }
#ifdef _WIN32
    ApiFileError SetRealApiFilePath(const std::string &apiFilePath) {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      GetMetaDb().Put(rocksdb::WriteOptions(), "real:" + lookupPath, apiFilePath);
      return ApiFileError::Success;
    }
#endif
  public:
    ApiFileError CreateDirectory(const std::string &apiFilePath, const ApiMetaMap &meta) override {
#ifdef _WIN32
      auto ret = IsDirectory(apiFilePath) ? ApiFileError::DirectoryExists : IsFile(apiFilePath) ? ApiFileError::FileExists : ApiFileError::Success;
#else
      auto ret = ApiFileError::Success;
#endif
      if (ret == ApiFileError::Success) {
        json result, error;
        auto apiError = GetComm().Post("/renter/dir" + Utils::Path::EscapeADS(apiFilePath), {{"action", "create"}}, result, error);
        if (apiError == ApiError::Success) {
#ifdef _WIN32
          SetRealApiFilePath(apiFilePath);
#endif
          ret = SetItemMeta(apiFilePath, meta);
        } else if (CheckDirectoryFound(error)) {
          ret = ApiFileError::DirectoryExists;
        } else {
          EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, error.dump(2));
        }
      }

      return ret;
    }

    ApiFileError CreateDirectoryCloneSourceMeta(const std::string &sourceApiPath, const std::string &apiFilePath) override {
      ApiMetaMap meta;
      auto ret = GetItemMeta(sourceApiPath, meta);
      if (ret == ApiFileError::Success) {
        ret = CreateDirectory(apiFilePath, meta);
      }
      return ret;
    }

    ApiFileError CreateFile(const std::string &apiFilePath, const ApiMetaMap &meta) override {
      const auto isDir = IsDirectory(apiFilePath);
      const auto isFile = IsFile(apiFilePath);
      auto ret = isDir ? ApiFileError::DirectoryExists : isFile ? ApiFileError::FileExists : ApiFileError::Success;
      if (ret == ApiFileError::Success) {
        const auto source = Utils::Path::Combine(GetConfig().GetCacheDirectory(), Utils::CreateUUIDString());

        NativeFilePtr nativeFile;
        if ((ret = CNativeFile::CreateOrOpen(source, nativeFile)) == ApiFileError::Success) {
          nativeFile->Close();
        }

        if (ret == ApiFileError::Success) {
          if (((ret = SetItemMeta(apiFilePath, meta)) != ApiFileError::Success) || ((ret = SetSourcePath(apiFilePath, source)) != ApiFileError::Success) || ((ret = UploadFile(apiFilePath, source)) != ApiFileError::Success)) {
            RemoveItem(apiFilePath);
            Utils::File::DeleteAsFile(source);
#ifdef _WIN32
          } else {
            SetRealApiFilePath(apiFilePath);
          }
#else
          }
#endif
        }
      }

      return ret;
    }

    std::uint64_t GetDirectoryItemCount(const std::string &apiFilePath) const override {
      std::int64_t ret = 0;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      auto *provider = const_cast<CSiaProvider *>(this);
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);
      json result, error;
      const auto apiError = GetComm().Get("/renter/dir" + Utils::Path::EscapeADS(realApiFilePath), result, error);
      if (apiError == ApiError::Success) {
        const auto dirCount = result["directories"].size() - 1;
        const auto fileCount = result["files"].size();
        ret = fileCount + dirCount;
      }

      return ret;
    }

    ApiFileError GetDirectoryItems(const std::string &apiFilePath, DirectoryItemList &directoryItemList) const override {
      auto ret = ApiFileError::Error;

      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);

      json result, error;
      auto apiError = GetComm().Get("/renter/dir" + Utils::Path::EscapeADS(realApiFilePath), result, error);
      if (apiError == ApiError::Success) {
        auto processItem = [&](const bool &directory, const json &item) {
          const auto itemPath = Utils::Path::CreateApiPath(item[CConfig::GetProviderPathName(GetConfig().GetProviderType())].get<std::string>());
          if (itemPath != realApiFilePath) {
            DirectoryItem directoryItem{};
            directoryItem.ApiFilePath = Utils::Path::UnEscapeADS(itemPath);
            directoryItem.ApiParent = realApiFilePath;
            directoryItem.Directory = directory;

            directoryItemList.emplace_back(directoryItem);
          }
        };

        for (const auto &dir : result["directories"]) {
          processItem(true, dir);
        }

        for (const auto &file : result["files"]) {
          processItem(false, file);
        }
        ret = ApiFileError::Success;
      }

      return ret;
    }

    ApiError GetFile(const std::string& apiFilePath, ApiFile &file) const override {
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);

      json data, error;
      const auto apiError = GetComm().Get("/renter/file" + Utils::Path::EscapeADS(realApiFilePath), data, error);
      if (apiError == ApiError::Success) {
        const std::string pathName = CConfig::GetProviderPathName(GetConfig().GetProviderType());
        CreateApiFile(data["file"], pathName, file);
      } else {
        EventSystem::Instance().Raise<FileSizeGetFailed>(realApiFilePath, error.dump(2));
      }

      return apiError;
    }

    ApiError GetFileList(ApiFileList &fileList) const override {
      auto ret = ApiError::Success;
      json data;
      json error;
      if ((ret = GetComm().Get("/renter/files", data, error)) == ApiError::Success) {
        for (const auto &file : data["files"]) {
          const std::string pathName = CConfig::GetProviderPathName(GetConfig().GetProviderType());
          ApiFile apiFile{};
          CreateApiFile(file, pathName, apiFile);

          fileList.emplace_back(apiFile);
        }
      } else {
        EventSystem::Instance().Raise<FileGetApiListFailed>(error.dump(2));
      }

      return ret;
    }

    ApiError GetFileSize(const std::string &apiFilePath, std::uint64_t &fileSize) const override {
      fileSize = 0;
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);

      json data, error;
      const auto apiError = GetComm().Get("/renter/file" + Utils::Path::EscapeADS(realApiFilePath), data, error);
      if (apiError == ApiError::Success) {
        fileSize = data["file"]["filesize"].get<std::uint64_t>();
      } else {
        EventSystem::Instance().Raise<FileSizeGetFailed>(realApiFilePath, error.dump(2));
      }

      return apiError;
    }

    ApiFileError GetFileSystemItem(const std::string &apiFilePath, const bool &directory, FileSystemItem &fileSystemItem) const override {
      auto ret = ApiFileError::Error;
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);
      if (directory) {
        json result, error;
        const auto apiError = GetComm().Get("/renter/dir" + Utils::Path::EscapeADS(realApiFilePath), result, error);
        if (apiError == ApiError::Success) {
          ret = ApiFileError::Success;
        } else if (CheckNotFound(error)) {
          ret = ApiFileError::ItemNotFound;
        } else {
          EventSystem::Instance().Raise<FileSystemItemGetFailed>(realApiFilePath, error.dump(2));
          ret = ApiFileError::Error;
        }
      } else if ((ret = GetItemMeta(apiFilePath, META_SOURCE, fileSystemItem.SourceFilePath)) == ApiFileError::Success) {
        json data, error;
        const auto apiError = GetComm().Get("/renter/file" + Utils::Path::EscapeADS(realApiFilePath), data, error);
        if (apiError == ApiError::Success) {
          ApiFile apiFile{};
          CreateApiFile(data["file"], CConfig::GetProviderPathName(GetConfig().GetProviderType()), apiFile);
          fileSystemItem.LegacyChunks = apiFile.LegacyChunks;
          fileSystemItem.Redundancy = apiFile.Redundancy;
          fileSystemItem.Size = apiFile.FileSize;
        } else if (CheckNotFound(error)) {
          ret = ApiFileError::ItemNotFound;
        } else {
          EventSystem::Instance().Raise<FileSystemItemGetFailed>(realApiFilePath, error.dump(2));
          ret = ApiFileError::Error;
        }
      }

      if (ret == ApiFileError::Success) {
        fileSystemItem.Directory = directory;
        fileSystemItem.ItemLock = fileSystemItem.ItemLock ? fileSystemItem.ItemLock : std::make_shared<std::recursive_mutex>();
        fileSystemItem.ApiFilePath = realApiFilePath;
        fileSystemItem.ApiParent = Utils::Path::GetParentApiPath(realApiFilePath);
      }

      return ret;
    }

    ApiFileError GetFileSystemItemFromSourcePath(const std::string &sourceFilePath, FileSystemItem &fileSystemItem) const override {
      auto ret = ApiFileError::ItemNotFound;
      if (not sourceFilePath.empty()) {
        const auto lookupPath = "source:" + CreateLookupPath(sourceFilePath);
        std::string apiFilePath;
        if (GetMetaDb().Get(rocksdb::ReadOptions(), lookupPath, &apiFilePath).ok()) {
          ret = GetFileSystemItem(apiFilePath, false, fileSystemItem);
        }
      }

      return ret;
    }

    ApiFileError GetItemMeta(const std::string &apiFilePath, ApiMetaMap &meta) const override {
      auto ret = _GetItemMeta(apiFilePath, meta);
      if (ret == ApiFileError::ItemNotFound) {
        auto getMeta = false;
        if (IsDirectory(apiFilePath)) {
          NotifyDirectoryAdded(apiFilePath, Utils::Path::GetParentApiPath(apiFilePath));
          getMeta = true;
        } else if (IsFile(apiFilePath)) {
          std::uint64_t fileSize = 0;
          if ((ret = ((GetFileSize(apiFilePath, fileSize) == ApiError::Success) ? ApiFileError::Success : ApiFileError::Error)) == ApiFileError::Success) {
            getMeta = ((ret = NotifyFileAdded(apiFilePath, Utils::Path::GetParentApiPath(apiFilePath), fileSize)) == ApiFileError::Success);
          }
        }
        if (getMeta) {
          ret = _GetItemMeta(apiFilePath, meta);
        }
      }

      return ret;
    }

    ApiFileError GetItemMeta(const std::string &apiFilePath, const std::string &key, std::string &value) const override {
      auto ret = _GetItemMeta(apiFilePath, key, value);
      if (ret == ApiFileError::ItemNotFound) {
        auto getMeta = false;
        if (IsDirectory(apiFilePath)) {
          NotifyDirectoryAdded(apiFilePath, Utils::Path::GetParentApiPath(apiFilePath));
          getMeta = true;
        } else if (IsFile(apiFilePath)) {
          std::uint64_t fileSize = 0;
          if ((ret = ((GetFileSize(apiFilePath, fileSize) == ApiError::Success) ? ApiFileError::Success : ApiFileError::Error)) == ApiFileError::Success) {
            getMeta = ((ret = NotifyFileAdded(apiFilePath, Utils::Path::GetParentApiPath(apiFilePath), fileSize)) == ApiFileError::Success);
          }
        }
        if (getMeta) {
          ret = _GetItemMeta(apiFilePath, key, value);
        }
      }

      return ret;
    }

    inline std::uint64_t GetTotalDriveSpace() const override {
      return _totalDriveSpace;
    }

    std::uint64_t GetTotalItemCount() const override {
      std::uint64_t ret = 0;

      json result, error;
      const auto apiError = GetComm().Get("/renter/dir/", result, error);
      if (apiError == ApiError::Success) {
        ret = result["directories"][0]["aggregatenumfiles"].get<std::uint64_t>();
      }

      return ret;
    }

    inline std::uint64_t GetUsedDriveSpace() const override {
      return GlobalData::Instance().GetUsedDriveSpace();
    }

    bool IsDirectory(const std::string &apiFilePath) const override {
      auto ret = false;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      auto *provider = const_cast<CSiaProvider *>(this);

      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);
      json result, error;
      const auto apiError = GetComm().Get("/renter/dir" + Utils::Path::EscapeADS(realApiFilePath), result, error);
      if (apiError == ApiError::Success) {
        ret = true;
      } else if (not CheckNotFound(error)) {
        EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, error.dump(2));
      }

      return ret;
    }

    bool IsFile(const std::string &apiFilePath) const override {
      return (CheckFileExists(apiFilePath) == ApiFileError::Success);
    }

    bool IsOnline() const override {
      // TODO Expand here for true online detection (i.e. wallet unlocked)
      // TODO Return error string for display
      json data, error;
      return (GetComm().Get("/wallet", data, error) == ApiError::Success);
    }

    ApiFileError ReadFileBytes(const std::string &apiFilePath, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &buffer, const bool &stopRequested) override {
      auto ret = ApiFileError::DownloadFailed;
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);
      for (std::uint8_t i = 0; not stopRequested && (ret != ApiFileError::Success) && (i < 2); i++) {
        json error;
        const auto apiError = GetComm().GetRaw("/renter/download" + Utils::Path::EscapeADS(realApiFilePath), {{"httpresp", "true"},
                                                                                                              {"async",    "false"},
                                                                                                              {"offset",   Utils::String::FromInt64(offset)},
                                                                                                              {"length",   Utils::String::FromInt64(size)}}, buffer, error, stopRequested);
        ret = (apiError == ApiError::Success) ? ApiFileError::Success : ApiFileError::DownloadFailed;
        if (ret != ApiFileError::Success) {
          EventSystem::Instance().Raise<FileReadBytesFailed>(realApiFilePath, error.dump(2), i + 1);
        }
      }

      return ret;
    }

    ApiFileError RemoveDirectory(const std::string &apiFilePath) override {
      auto ret = ApiFileError::DirectoryNotEmpty;

      if (GetDirectoryItemCount(apiFilePath) == 0) {
        ret = ApiFileError::DirectoryNotFound;

        GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);

        json result, error;
        auto apiError = GetComm().Post("/renter/dir" + Utils::Path::EscapeADS(realApiFilePath), {{"action", "delete"}}, result, error);
        if (apiError == ApiError::Success) {
          RemoveItem(apiFilePath);
          EventSystem::Instance().Raise<DirectoryRemoved>(realApiFilePath);
          ret = ApiFileError::Success;
        } else if (CheckNotFound(error)) {
          RemoveItem(apiFilePath);
          ret = ApiFileError::DirectoryNotFound;
        } else {
          EventSystem::Instance().Raise<DirectoryRemoveFailed>(realApiFilePath, error.dump(2));
        }
      }

      return ret;
    }

    ApiFileError RemoveFile(const std::string &apiFilePath) override {
      auto ret = ApiFileError::Error;

      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);

      json result, error;
      const auto apiError = GetComm().Post("/renter/delete" + Utils::Path::EscapeADS(realApiFilePath), result, error);
      auto notFound = false;
      if ((apiError == ApiError::Success) || (notFound = CheckNotFound(error))) {
        if (not notFound) {
          EventSystem::Instance().Raise<FileRemoved>(realApiFilePath);
        }
        ret = notFound ? ApiFileError::ItemNotFound : ApiFileError::Success;

        RemoveItem(apiFilePath);
      } else {
        EventSystem::Instance().Raise<FileRemoveFailed>(realApiFilePath, error.dump(2));
      }

      return ret;
    }

    ApiFileError RemoveItemMeta(const std::string &apiFilePath, const std::string &key) override {
      auto ret = ApiFileError::Success;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      json jsonData;
      std::string keyValue;
      if (GetMetaDb().Get(rocksdb::ReadOptions(), lookupPath, &keyValue).ok()) {
        jsonData = json::parse(keyValue);
      }

      jsonData.erase(key);
      GetMetaDb().Put(rocksdb::WriteOptions(), lookupPath, jsonData.dump());

      return ret;
    }

    ApiFileError RenameFile(const std::string &fromApiPath, const std::string &toApiPath) override {
      std::string curSource;
      auto ret = GetItemMeta(fromApiPath, META_SOURCE, curSource);
      if (ret == ApiFileError::Success) {
        GET_REAL_API_FILE_PATH(realFromApiFilePath, fromApiPath);
        GET_REAL_API_FILE_PATH(realToApiFilePath, toApiPath);

        json result, error;
        const auto propertyName = "new" + CConfig::GetProviderPathName(GetConfig().GetProviderType());
        const auto destPath = Utils::Path::EscapeADS(realToApiFilePath).substr(1);
        const auto apiError = GetComm().Post("/renter/rename" + Utils::Path::EscapeADS(realFromApiFilePath), {{propertyName, destPath}}, result, error);
        if (apiError == ApiError::Success) {
          auto lookupPathFrom = CreateLookupPath(fromApiPath);
          auto lookupPathTo = CreateLookupPath(toApiPath);
          std::string keyValue;
          if (GetMetaDb().Get(rocksdb::ReadOptions(), lookupPathFrom, &keyValue).ok()) {
            GetMetaDb().Delete(rocksdb::WriteOptions(), lookupPathFrom);
            GetMetaDb().Put(rocksdb::WriteOptions(), lookupPathTo, keyValue);
          }

          GetMetaDb().Put(rocksdb::WriteOptions(), "source:" + CreateLookupPath(curSource), realToApiFilePath);
#ifdef _WIN32
          SetRealApiFilePath(realToApiFilePath);
          GetMetaDb().Delete(rocksdb::WriteOptions(), "real:" + CreateLookupPath(realFromApiFilePath));
#endif
        } else if (CheckNotFound(error)) {
          ret = ApiFileError::ItemNotFound;
        } else {
          EventSystem::Instance().Raise<FileRenameFailed>(realFromApiFilePath, realToApiFilePath, error.dump(2));
          ret = ApiFileError::Error;
        }
      }

      return ret;
    }

    ApiFileError SetItemMeta(const std::string &apiFilePath, const std::string &key, const std::string &value) override {
      //TODO Detect if path exists
      //TODO Handle error on Put()
      auto ret = ApiFileError::Success;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      json jsonData;
      std::string keyValue;
      if (GetMetaDb().Get(rocksdb::ReadOptions(), lookupPath, &keyValue).ok()) {
        jsonData = json::parse(keyValue);
      }

      jsonData[key] = value;
      GetMetaDb().Put(rocksdb::WriteOptions(), lookupPath, jsonData.dump());

      return ret;
    }

    ApiFileError SetItemMeta(const std::string &apiFilePath, const ApiMetaMap &meta) override {
      auto ret = ApiFileError::Success;
      auto it = meta.begin();
      for (size_t i = 0; (ret == ApiFileError::Success) && (i < meta.size()); i++) {
        ret = SetItemMeta(apiFilePath, it->first, it->second);
        it++;
      }

      return ret;
    }

    ApiFileError SetSourcePath(const std::string &apiFilePath, const std::string &sourcePath) override {
      std::string curSource;
      GetItemMeta(apiFilePath, META_SOURCE, curSource);
      if (not curSource.empty()) {
        GetMetaDb().Delete(rocksdb::WriteOptions(), "source:" + CreateLookupPath(curSource));
      }

      auto ret = SetItemMeta(apiFilePath, META_SOURCE, sourcePath);
      if (ret == ApiFileError::Success) {
        GetMetaDb().Put(rocksdb::WriteOptions(), "source:" + CreateLookupPath(sourcePath), apiFilePath);
      }

      return ret;
    }

    void Start(ApiItemAdded apiItemAdded, IOpenFileTable* openFileTable) override {
      auto unmountRequested = false;
      {
        Repertory::EventConsumer ec("UnmountRequested", [&unmountRequested](const Event &e) {
          unmountRequested = true;
        });
        for (std::uint16_t i = 0; not unmountRequested && not IsOnline() && (i < _config.GetOnlineCheckRetrySeconds()); i++) {
          EventSystem::Instance().Raise<ProviderIsOffline>(_config.GetHostConfig().HostNameOrIp, _config.GetHostConfig().ApiPort);
          std::this_thread::sleep_for(1s);
        }
      }

      if (not unmountRequested && IsOnline()) {
        {
          json data, error;
          const auto res = _comm.Get("/daemon/version", data, error);
          if (res == ApiError::Success) {
            if (Utils::CompareVersionStrings(data["version"].get<std::string>(), CConfig::GetProviderMinimumVersion(_config.GetProviderType())) < 0) {
              throw StartupException("incompatible daemon version: " + data["version"].get<std::string>());
            }
          } else {
            throw StartupException("failed to get version from daemon");
          }
        }

        MutexLock l(_startStopMutex);
        if (not _driveSpaceThread) {
          _stopRequested = false;
          _apiItemAdded = apiItemAdded;
          _openFileTable = openFileTable;

          Cleanup();

          CalculateTotalDriveSpace();

          std::uint64_t usedSpace = 0;
          if (CalculateUsedDriveSpace(usedSpace) != ApiError::Success) {
            throw StartupException("failed to determine used space");
          }
          GlobalData::Instance().InitializeUsedDriveSpace(usedSpace);

          _driveSpaceThread = std::make_unique<std::thread>([this] {
            DriveSpaceThread();
          });

          Polling::Instance().SetCallback({"check_deleted", true, [this]() {
            RemoveDeletedFiles(true);
            RemoveExpiredOrphanedFiles();
          }});
        }
      } else {
        throw StartupException("failed to connect to api");
      }
    }

    void Stop() override {
      UniqueMutexLock l(_startStopMutex);
      if (_driveSpaceThread) {
        _stopRequested = true;

        Polling::Instance().RemoveCallback("check_deleted");

        _startStopNotify.notify_all();
        l.unlock();

        _driveSpaceThread->join();
        _driveSpaceThread.reset();
      }
    }

    ApiFileError UploadFile(const std::string &apiFilePath, const std::string &sourcePath) override {
      GET_REAL_API_FILE_PATH(realApiFilePath, apiFilePath);
      EventSystem::Instance().Raise<FileUploadBegin>(realApiFilePath, sourcePath);

      auto ret = SetSourcePath(apiFilePath, sourcePath);
      if (ret == ApiFileError::Success) {
        std::uint64_t fileSize;
        if (Utils::File::GetSize(sourcePath, fileSize)) {
          json result, error;
          const auto apiError = GetComm().Post("/renter/upload" + Utils::Path::EscapeADS(realApiFilePath), {{"source", &sourcePath[0]},
                                                                                                            {"force",  "true"}}, result, error);
          ret = ((apiError == ApiError::Success) ? ApiFileError::Success : ApiFileError::UploadFailed);
          if (ret != ApiFileError::Success) {
            EventSystem::Instance().Raise<FileUploadFailed>(realApiFilePath, sourcePath, error.dump(2));
          }
        } else {
          ret = ApiFileError::OSErrorCode;
          EventSystem::Instance().Raise<FileUploadFailed>(realApiFilePath, sourcePath, "Failed to get source file size");
        }
      } else {
        EventSystem::Instance().Raise<FileUploadFailed>(realApiFilePath, sourcePath, "Failed to set source path");
      }

      EventSystem::Instance().Raise<FileUploadEnd>(realApiFilePath, sourcePath, ret);
      return ret;
    }
};
}

#endif //REPERTORY_SIAPROVIDER_H
