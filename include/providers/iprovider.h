#ifndef REPERTORY_IPROVIDER_H
#define REPERTORY_IPROVIDER_H

#include <common.h>
#include <string>

namespace Repertory {
class IOpenFileTable;
class IProvider {
  INTERFACE_SETUP(IProvider);

  public:
    virtual ApiFileError CreateDirectory(const std::string& apiFilePath, const ApiMetaMap& meta) = 0;

    virtual ApiFileError CreateDirectoryCloneSourceMeta(const std::string& sourceApiPath, const std::string& apiFilePath) = 0;

    virtual ApiFileError CreateFile(const std::string &apiFilePath, const ApiMetaMap &meta) = 0;

    virtual ApiError GetFileList(ApiFileList& fileList) const = 0;

    virtual std::uint64_t GetDirectoryItemCount(const std::string& apiFilePath) const = 0;

    virtual ApiFileError GetDirectoryItems(const std::string& apiFilePath, DirectoryItemList& directoryItemList) const = 0;

    virtual ApiError GetFile(const std::string& apiFilePath, ApiFile &file) const = 0;

    virtual ApiError GetFileSize(const std::string& apiFilePath, std::uint64_t& fileSize) const = 0;

    virtual ApiFileError GetFileSystemItem(const std::string &apiFilePath, const bool& directory, FileSystemItem& fileSystemItem) const = 0;

    virtual ApiFileError GetFileSystemItemFromSourcePath(const std::string& sourceFilePath, FileSystemItem& fileSystemItem) const = 0;

    virtual ApiFileError GetItemMeta(const std::string &apiFilePath, ApiMetaMap &meta) const = 0;

    virtual ApiFileError GetItemMeta(const std::string &apiFilePath, const std::string &key, std::string &value) const = 0;

    virtual std::uint64_t GetTotalDriveSpace() const = 0;

    virtual std::uint64_t GetTotalItemCount() const = 0;

    virtual std::uint64_t GetUsedDriveSpace() const = 0;

    virtual bool IsDirectory(const std::string& apiFilePath) const = 0;

    virtual bool IsFile(const std::string& apiFilePath) const = 0;

    virtual bool IsOnline() const = 0;

    virtual ApiFileError ReadFileBytes(const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested) = 0;

    virtual ApiFileError RemoveDirectory(const std::string& apiFilePath) = 0;

    virtual ApiFileError RemoveFile(const std::string& apiFilePath) = 0;

    virtual ApiFileError RenameFile(const std::string& fromApiPath, const std::string& toApiPath) = 0;

    virtual ApiFileError RemoveItemMeta(const std::string &apiFilePath, const std::string &key) = 0;

    virtual ApiFileError SetItemMeta(const std::string &apiFilePath, const std::string &key, const std::string &value) = 0;

    virtual ApiFileError SetItemMeta(const std::string &apiFilePath, const ApiMetaMap &meta) = 0;

    virtual ApiFileError SetSourcePath(const std::string& apiFilePath, const std::string& sourcePath) = 0;

    virtual void Start(ApiItemAdded apiItemAdded, IOpenFileTable* openFileTable) = 0;

    virtual void Stop() = 0;

    virtual ApiFileError UploadFile(const std::string& apiFilePath, const std::string& sourcePath) = 0;
};
}

#endif //REPERTORY_IPROVIDER_H
