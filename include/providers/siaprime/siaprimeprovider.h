#ifndef REPERTORY_SIAPRIMEPROVIDER_H
#define REPERTORY_SIAPRIMEPROVIDER_H

#include <config.h>
#include <providers/iprovider.h>
#include <providers/sia/siaprovider.h>

namespace Repertory {
class CSiaPrimeProvider final :
  public virtual CSiaProvider {
  public:
    CSiaPrimeProvider(CConfig &config, IComm &comm) :
      CSiaProvider(config, comm) {
    }

  protected:
    bool GetUsesLegacyChunks() const override {
      static auto ret = (Utils::CompareVersionStrings(MIN_SP_VERSION, "1.4.1") < 0);
      return ret;
    }
};
}

#endif //REPERTORY_SIAPRIMEPROVIDER_H
