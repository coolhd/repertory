#ifndef REPERTORY_PROVIDER_H
#define REPERTORY_PROVIDER_H

#include <comm/icomm.h>
#include <config.h>
#include <events/events.h>
#include <providers/sia/siaprovider.h>
#include <providers/siaprime/siaprimeprovider.h>

namespace Repertory {
std::unique_ptr<IProvider> CreateProvider(const ProviderType& providerType, CConfig& config, IComm& comm) {
  switch (providerType) {
  case ProviderType::Sia:
    return std::move(std::unique_ptr<IProvider>(dynamic_cast<IProvider*>(new CSiaProvider(config, comm))));

  case ProviderType::SiaPrime:
    return std::move(std::unique_ptr<IProvider>(dynamic_cast<IProvider*>(new CSiaPrimeProvider(config, comm))));
  }

  return nullptr;
}
}

#endif //REPERTORY_PROVIDER_COMMON_H