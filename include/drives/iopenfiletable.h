#ifndef REPERTORY_IOPENFILETABLE_H
#define REPERTORY_IOPENFILETABLE_H

#include <common.h>

namespace Repertory {
class IProvider;
class IOpenFileTable {
  INTERFACE_SETUP(IOpenFileTable);

  public:
    typedef std::function<void(IOpenFileTable& openFileTable, IProvider& provider)> LockedOperation;

  public:
    virtual bool CheckNoOpenFileHandles() const = 0;

    virtual bool EvictFile(const std::string &apiFilePath) = 0;

    virtual std::uint64_t GetOpenCount(const std::string& apiFilePath) const = 0;

    virtual bool GetOpenFile(const std::string& apiFilePath, FileSystemItem*& fileSystemItem) = 0;

    virtual std::unordered_map<std::string, std::size_t> GetOpenFiles() = 0;

    virtual void PerformLockedOperation(LockedOperation operation) = 0;
};
}
#endif //REPERTORY_IOPENFILETABLE_H
