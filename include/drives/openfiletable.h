#ifndef REPERTORY_OPENFILETABLE_H
#define REPERTORY_OPENFILETABLE_H

#include <config.h>
#include <download/idownloadmanager.h>
#include <drives/iopenfiletable.h>
#include <mutex>
#include <utils/polling.h>
#include <providers/iprovider.h>
#include <SQLiteCpp/Database.h>
#include <utils/nativefile.h>

namespace Repertory {
template <typename Flags>
class COpenFileTable final :
  public virtual IOpenFileTable {
  public:
    COpenFileTable(IProvider &provider, CConfig &config, IDownloadManager& downloadManager) :
      _provider(provider),
      _config(config),
      _downloadManager(downloadManager),
      _database(Utils::Path::Combine(config.GetDataDirectory(), "uploads.db3"), SQLite::OPEN_CREATE | SQLite::OPEN_READWRITE) {
#ifndef _WIN32
      chmod(Utils::Path::Combine(config.GetDataDirectory(), "uploads.db3").c_str(), 0600u);
#endif
      SQLite::Statement(_database, "PRAGMA journal_mode=WAL;").executeStep();
#ifndef _DEBUG
      SQLite::Statement(_database, "PRAGMA locking_mode=EXCLUSIVE;").executeStep();
#endif
      SQLite::Statement(_database, "PRAGMA case_sensitive_like=true;").executeStep();

      auto sql = std::string() +
        "create table if not exists retry_table (" +
        "id integer primary key autoincrement, " +
        "path text unique not null, " +
        "date integer not null);";
      SQLite::Statement(_database, sql.c_str()).exec();

      // Set initial value for used cache space
      GlobalData::Instance().InitializeUsedCacheSpace(Utils::File::CalculateUsedSpace(_config.GetCacheDirectory(), false));
      Polling::Instance().SetCallback({"last_close_clear", false, [this] {
        std::vector<std::string> keys;
        {
          MutexLock l(_lastCloseMutex);
          for (const auto &kv : _lastCloseLookup) {
            keys.push_back(kv.first);
          }
        }

        for (const auto& key : keys) {
          MutexLock l(_lastCloseMutex);
          RemoveIfExpired(key, _lastCloseLookup[key]);
        }
      }});
    }

    ~COpenFileTable() override {
      Polling::Instance().RemoveCallback("last_close_clear");
    }

  private:
    typedef struct {
      FileSystemItem Item;
      ApiMetaMap Meta;
    } OpenFileInfo;

  private:
    IProvider &_provider;
    CConfig &_config;
    IDownloadManager& _downloadManager;
    SQLite::Database _database;
    std::unordered_map<std::string, std::shared_ptr<OpenFileInfo>> _openFileLookup;
    std::recursive_mutex _openFileMutex;
    std::unordered_map<std::uint64_t, OpenFileInfo*> _openHandleLookup;
    std::uint64_t _nextHandle = 1;
    bool _stopRequested = false;
    std::mutex _retryMutex;
    std::mutex _retryActiveMutex;
    std::unique_ptr<std::thread> _retryThread;
    std::condition_variable _retryNotify;
    bool _retryPaused = false;
    std::mutex _startStopMutex;
    std::mutex _lastCloseMutex;
    std::unordered_map<std::string, std::uint64_t> _lastCloseLookup;

  private:
    ApiFileError GetFileSystemItem(const std::string& apiFilePath, const bool& directory, FileSystemItem& fileSystemItem) {
      auto ret = ApiFileError::ItemNotFound;

      // NTFS is case-insensitive - lookup's must use common case
      const auto lookupPath = CreateLookupPath(fileSystemItem.ApiFilePath);

      const auto it = _openFileLookup.find(lookupPath);
      if (it != _openFileLookup.end()) {
        Utils::CopyFileSystemItem(fileSystemItem, it->second->Item);
        ret = ApiFileError::Success;
      } else {
        ret = _provider.GetFileSystemItem(apiFilePath, directory, fileSystemItem);
      }

      return ret;
    }

    inline std::uint64_t GetNextHandle() {
      std::uint64_t ret = 0;
      while ((ret = _nextHandle++) == 0);
      return ret;
    }

    ApiFileError HandleFileRename(const std::string& fromApiPath, const std::string& toApiPath) {
      auto ret = _provider.RenameFile(fromApiPath, toApiPath);
      if (ret == ApiFileError::Success) {
        SwapRenamedItems(fromApiPath, toApiPath);
        const auto lookupPath = CreateLookupPath(toApiPath);
        // Update retry database
        try {
          const auto sql = "update retry_table set path=@new_path where path=@path" + COLLATE + ";";
          SQLite::Statement update(_database, sql);
          update.bind("@new_path", toApiPath);
          update.bind("@path", fromApiPath);
          update.exec();
        } catch (const SQLite::Exception& e) {
          EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, e.what());
        }
      }

      return ret;
    }

    void PauseRetry() {
      MutexLock l(_retryActiveMutex);
      _retryPaused = true;
    }

    bool RemoveIfExpired(const std::string &lookupPath, const std::uint64_t &time) {
      auto ret = false;
#ifdef _WIN32
      const auto delay = std::chrono::minutes(_config.GetEvictionDelayMinutes());
      const auto lastCheck = std::chrono::system_clock::from_time_t(time);
      if ((ret = ((lastCheck + delay) <= std::chrono::system_clock::now()))) {
#else
        if ((ret = (time + ((_config.GetEvictionDelayMinutes() * 60L) * NANOS_PER_SECOND)) <= Utils::GetTimeNow())) {
#endif
        _lastCloseLookup.erase(lookupPath);
      }
      return ret;
    }

    void ResumeRetry() {
      MutexLock l(_retryActiveMutex);
      _retryPaused = false;
    }

    bool RetryDeleteFile(const std::string& file) {
      auto deleted = false;
      for (auto i = 0; not (deleted = Utils::File::DeleteAsFile(file)) && (i < 100); i++) {
        std::this_thread::sleep_for(10ms);
      }
      return deleted;
    }

    void SwapRenamedItems(std::string fromApiPath, std::string toApiPath) {
      fromApiPath = CreateLookupPath(fromApiPath);
      toApiPath = CreateLookupPath(toApiPath);
      const auto it = _openFileLookup.find(fromApiPath);
      if (it != _openFileLookup.end()) {
        _openFileLookup[toApiPath] = _openFileLookup[fromApiPath];
        _openFileLookup.erase(fromApiPath);
        auto& fileSystemItem = _openFileLookup[toApiPath]->Item;
        fileSystemItem.ApiFilePath = toApiPath;
        fileSystemItem.ApiParent = Utils::Path::GetParentApiPath(toApiPath);
      }
    }

    void UploadRetryThread() {
      while (not _stopRequested) {
        auto processed = false;
        {
          UniqueMutexLock l(_retryActiveMutex);
          if (not _retryPaused) {
            try {
              std::deque<std::string> removedItems;

              const auto sql = "select * from retry_table order by date asc;";
              SQLite::Statement query(_database, sql);
              while (query.executeStep()) {
                const auto apiFilePath = query.getColumn("path").getString();
                const auto lookupFilePath = CreateLookupPath(apiFilePath);
                EventSystem::Instance().Raise<FailedUploadRetry>(apiFilePath);
                {
                  RMutexLock l2(_openFileMutex);
                  if (_openFileLookup.find(lookupFilePath) == _openFileLookup.end()) {
                    l.unlock();

                    auto success = false;
                    FileSystemItem fileSystemItem{};
                    const auto res = _provider.GetFileSystemItem(apiFilePath, false, fileSystemItem);
                    if ((res == ApiFileError::Success) || ((res == ApiFileError::ItemNotFound) && _provider.IsFile(apiFilePath))) {
                      if (_provider.UploadFile(apiFilePath, fileSystemItem.SourceFilePath) == ApiFileError::Success) {
                        removedItems.emplace_back(apiFilePath);
                        success = true;
                      }
                    }

                    // Remove deleted files
                    if (not success && not _provider.IsFile(apiFilePath)) {
                      removedItems.emplace_back(apiFilePath);
                    }
                  } else {
                    // File is open, so force re-upload on close
                    _openFileLookup[lookupFilePath]->Item.Changed = true;
                    l.unlock();
                    removedItems.emplace_back(apiFilePath);
                  }
                }
              }

              // Delete removed items from table
              while (not removedItems.empty()) {
                processed = true;
                const auto sql2 = "delete from retry_table where path=@path" + COLLATE + ";";
                SQLite::Statement del(_database, sql2);
                del.bind("@path", removedItems.front());
                if (del.exec() > 0) {
                  EventSystem::Instance().Raise<FailedUploadRemoved>(removedItems.front());
                }
                removedItems.pop_front();
              }
            } catch (const SQLite::Exception& e) {
              EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, e.what());
            }
          }
        }

        UniqueMutexLock l(_retryMutex);
        if (not processed) {
          _retryNotify.wait_for(l, 5s);
        }
      }
    }

  public:
    bool CheckNoOpenFileHandles() const override {
      auto* oft = const_cast<COpenFileTable*>(this);
      RMutexLock l(oft->_openFileMutex);
      return std::find_if(_openFileLookup.cbegin(), _openFileLookup.cend(), [](const auto& kv) {
        return not kv.second->Item.Directory;
      }) == _openFileLookup.cend();
    }

    void Close(const std::uint64_t& handle) {
      RMutexLock l(_openFileMutex);
      const auto it = _openHandleLookup.find(handle);
      if (it != _openHandleLookup.end()) {
        auto *openItem = it->second;
        _openHandleLookup.erase(handle);

        auto &fileSystemItem = openItem->Item;
        const auto wasChanged = fileSystemItem.Changed;

        // Handle meta change
        if (fileSystemItem.MetaChanged) {
          if (_provider.SetItemMeta(fileSystemItem.ApiFilePath, openItem->Meta) == ApiFileError::Success) {
            fileSystemItem.MetaChanged = false;
          } else {
            EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, "failed to set file meta: " + fileSystemItem.ApiFilePath);
          }
        }

        // Handle source path change
        if (not fileSystemItem.Directory && fileSystemItem.SourceFilePathChanged) {
          if (_provider.SetSourcePath(fileSystemItem.ApiFilePath, fileSystemItem.SourceFilePath) == ApiFileError::Success) {
            fileSystemItem.SourceFilePathChanged = false;
          } else {
            EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, "failed to set source path: " + fileSystemItem.ApiFilePath + "|" + fileSystemItem.SourceFilePath);
          }
        }

        // NTFS is case-insensitive - lookup's must use common case
        const auto lookupPath = CreateLookupPath(fileSystemItem.ApiFilePath);

        // Update last close time in lookup table
        if (not fileSystemItem.Directory) {
          MutexLock l2(_lastCloseMutex);
          _lastCloseLookup[lookupPath] = Utils::GetTimeNow();
        }

        // Handle file change
#ifdef __APPLE__
        // Special handling for OS X - only upload if handle being closed is writable
        if (not fileSystemItem.Directory && wasChanged && (fileSystemItem.OpenData[handle] & O_WRONLY)) {
#else
        if (not fileSystemItem.Directory && wasChanged) {
#endif
          // Remove from retry queue, if present
          {
            MutexLock l2(_retryActiveMutex);
            const auto sql = "delete from retry_table where path=@path" + COLLATE + ";";
            SQLite::Statement del(_database, sql);
            del.bind("@path", fileSystemItem.ApiFilePath);
            if (del.exec() > 0) {
              EventSystem::Instance().Raise<FailedUploadRemoved>(fileSystemItem.ApiFilePath);
            }
          }

          // Upload file and add to retry queue on failure
          auto nativeFile = CNativeFile::Attach(fileSystemItem.Handle);
          nativeFile->Flush();
          if (_provider.UploadFile(fileSystemItem.ApiFilePath, fileSystemItem.SourceFilePath) == ApiFileError::Success) {
            fileSystemItem.Changed = false;
          } else {
            try {
              const auto sql = "insert into retry_table (path, date) values (@path, @date);";
              SQLite::Statement insert(_database, sql);
              insert.bind("@path", fileSystemItem.ApiFilePath);
              insert.bind("@date", (long long) Utils::GetTimeNow());
              insert.exec();
              fileSystemItem.Changed = false;

              EventSystem::Instance().Raise<FailedUploadQueued>(fileSystemItem.ApiFilePath);

              _retryNotify.notify_all();
            } catch (const SQLite::Exception &e) {
              EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, e.what());
            }
          }
        }

        // Close internal handle if no more open files
        auto &openData = fileSystemItem.OpenData;
        openData.erase(handle);
        EventSystem::Instance().Raise<FileSystemItemHandleClosed>(fileSystemItem.ApiFilePath, handle, fileSystemItem.SourceFilePath, fileSystemItem.Directory, wasChanged);
        if (openData.empty()) {
          auto nativeFile = CNativeFile::Attach(fileSystemItem.Handle);
          nativeFile->Close();

          EventSystem::Instance().Raise<FileSystemItemClosed>(fileSystemItem.ApiFilePath, fileSystemItem.SourceFilePath, fileSystemItem.Directory, wasChanged);
          _openFileLookup.erase(lookupPath);
        }
      }
    }

#ifdef _WIN32
    void CloseAll(const std::string& apiFilePath) {
      const auto lookupPath = CreateLookupPath(apiFilePath);

      RMutexLock l(_openFileMutex);
      const auto it = _openFileLookup.find(lookupPath);
      if (it != _openFileLookup.end()) {
        auto* openItem = it->second.get();
        std::vector<std::uint64_t> handles;
        for (const auto& kv : _openHandleLookup) {
          if (kv.second == openItem) {
            handles.emplace_back(kv.first);
          }
        }

        while (!handles.empty()) {
          Close(handles.back());
          handles.pop_back();
        }
      }
    }
#endif

    ApiFileError DeriveItemData(const std::string& apiFilePath, ApiMetaMap& meta) {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      auto ret = ApiFileError::Success;
      meta.clear();

      UniqueRMutexLock l(_openFileMutex);
      const auto it = _openFileLookup.find(lookupPath);
      if (it == _openFileLookup.end()) {
        l.unlock();
        ret = _provider.GetItemMeta(apiFilePath, meta);
      } else {
        meta = _openFileLookup[lookupPath]->Meta;
      }

      return ret;
    }

    inline ApiFileError DeriveItemData(const DirectoryItem& directoryItem, std::uint64_t& fileSize, ApiMetaMap& meta) {
      return DeriveItemData(directoryItem.ApiFilePath, directoryItem.Directory, fileSize, meta);
    }

    ApiFileError DeriveItemData(const std::string& apiFilePath, const bool& directory, std::uint64_t& fileSize, ApiMetaMap& meta) {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      auto ret = ApiFileError::Success;
      meta.clear();
      fileSize = 0;

      UniqueRMutexLock l(_openFileMutex);
      const auto it = _openFileLookup.find(lookupPath);
      if (it == _openFileLookup.end()) {
        l.unlock();
        ret = _provider.GetItemMeta(apiFilePath, meta);
        if ((ret == ApiFileError::Success) && not directory) {
          const auto apiError = _provider.GetFileSize(apiFilePath, fileSize);
          if (apiError != ApiError::Success) {
            ret = ApiFileError::Error;
          }
        }
      } else {
        meta = _openFileLookup[lookupPath]->Meta;
        if (not directory) {
          fileSize = _openFileLookup[lookupPath]->Item.Size;
        }
      }

      return ret;
    }

    bool EvictFile(const std::string &apiFilePath) override {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      auto ret = false;
      auto allowEviction = true;
      // Ensure enough time has passed since file was closed
      {
        MutexLock l(_lastCloseMutex);
        const auto it = _lastCloseLookup.find(lookupPath);
        if (it != _lastCloseLookup.end()) {
          allowEviction = RemoveIfExpired(lookupPath, it->second);
        }
      }

      if (allowEviction) {
        RMutexLock l(_openFileMutex);
        try {
          // Ensure item is not in upload retry queue
          SQLite::Statement query(_database, "select COUNT(id) from retry_table where path=@path" + COLLATE + "limit 1;");
          query.bind("@path", apiFilePath);
          query.executeStep();
          if ((query.getColumn(0).getInt64() == 0) && (GetOpenCount(apiFilePath) == 0)) {
            // Ensure item is not currently downloading
            if (not _downloadManager.IsProcessing(apiFilePath)) {
              FileSystemItem fileSystemItem{};
              if (_provider.GetFileSystemItem(apiFilePath, false, fileSystemItem) == ApiFileError::Success) {
                std::uint64_t fileSize = 0;
                if ((ret = (Utils::File::GetSize(fileSystemItem.SourceFilePath, fileSize) && RetryDeleteFile(fileSystemItem.SourceFilePath)))) {
                  GlobalData::Instance().UpdateUsedSpace(fileSize, 0, true);
                  EventSystem::Instance().Raise<FileSystemItemEvicted>(fileSystemItem.ApiFilePath, fileSystemItem.SourceFilePath);
                }
              }
            }
          }
        } catch (const SQLite::Exception &e) {
          EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, e.what());
        }
      }

      return ret;
    }

    std::uint64_t GetOpenCount(const std::string& apiFilePath) const override {
      auto openFileTable = const_cast<COpenFileTable*>(this);

      std::uint64_t ret = 0;
      const auto lookupPath = CreateLookupPath(apiFilePath);
      RMutexLock l(openFileTable->_openFileMutex);
      const auto it = openFileTable->_openFileLookup.find(lookupPath);
      if (it != openFileTable->_openFileLookup.end()) {
        ret = it->second->Item.OpenData.size();
      }
      return ret;
    }

    bool GetOpenFile(const std::string& apiFilePath, FileSystemItem*& fileSystemItem) override {
      auto ret = false;
      const auto lookupPath = CreateLookupPath(apiFilePath);

      RMutexLock l(_openFileMutex);
      const auto it = _openFileLookup.find(lookupPath);
      if (it != _openFileLookup.end()) {
        fileSystemItem = &it->second->Item;
        ret = true;
      }
      return ret;
    }

    bool GetOpenFile(const std::uint64_t& handle, FileSystemItem*& fileSystemItem) {
      auto ret = false;
      RMutexLock l(_openFileMutex);
      const auto it = _openHandleLookup.find(handle);
      if (it != _openHandleLookup.end()) {
        fileSystemItem = &it->second->Item;
        ret = true;
      }
      return ret;
    }

    std::unordered_map<std::string, std::size_t> GetOpenFiles() override {
      std::unordered_map<std::string, std::size_t> ret;
      UniqueRMutexLock l(_openFileMutex);
      for (const auto& kv : _openFileLookup) {
        ret.insert({kv.first, kv.second->Item.OpenData.size()});
      }
      l.unlock();
      return std::move(ret);
    }

    ApiFileError Open(const std::string& apiFilePath, const bool& directory, const Flags& flags, std::uint64_t& handle) {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      auto ret = ApiFileError::Success;

      RMutexLock l(_openFileMutex);
      if (_openFileLookup.find(lookupPath) == _openFileLookup.end()) {
        ApiMetaMap meta;
        if ((ret = _provider.GetItemMeta(apiFilePath, meta)) == ApiFileError::Success) {
          auto openItem = std::make_shared<OpenFileInfo>();
          openItem->Meta = meta;
          if ((ret = _provider.GetFileSystemItem(apiFilePath, directory, openItem->Item)) == ApiFileError::Success) {
            _openFileLookup.insert({lookupPath, openItem});
            EventSystem::Instance().Raise<FileSystemItemOpened>(openItem->Item.ApiFilePath, openItem->Item.SourceFilePath, openItem->Item.Directory);
          }
        }
      }

      if (ret == ApiFileError::Success) {
        auto* openItem = _openFileLookup[lookupPath].get();
        auto& fileSystemItem = openItem->Item;
        if (fileSystemItem.Directory == directory) {
          handle = GetNextHandle();
          fileSystemItem.OpenData.insert({handle,
                                           flags});
          _openHandleLookup.insert({handle, openItem});
        } else {
          ret = directory ? ApiFileError::FileExists : ApiFileError::DirectoryExists;
        }
      }

      return ret;
    }

    inline void PerformLockedOperation(LockedOperation operation) override {
      RMutexLock l(_openFileMutex);
      operation(*this, _provider);
    }

    ApiFileError RemoveFile(const std::string& apiFilePath) {
      RMutexLock l(_openFileMutex);

      FileSystemItem fileSystemItem{};
      auto ret = ApiFileError::FileInUse;
      if ((GetOpenCount(apiFilePath) == 0) &&
        ((ret = _provider.GetFileSystemItem(apiFilePath, false, fileSystemItem)) == ApiFileError::Success) &&
        ((ret = _provider.RemoveFile(apiFilePath)) == ApiFileError::Success)) {
        std::uint64_t fileSize = 0;
        Utils::File::GetSize(fileSystemItem.SourceFilePath, fileSize);
        if (RetryDeleteFile(fileSystemItem.SourceFilePath) && fileSize) {
          GlobalData::Instance().UpdateUsedSpace(fileSize, 0, false);
        }
      }

      return ret;
    }
#ifdef HAS_SETXATTR
    ApiFileError RemoveXAttrMeta(const std::string& apiFilePath, const std::string& name) {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      auto ret = ApiFileError::XAttrNotFound;
      if (std::find(META_USED_NAMES.begin(), META_USED_NAMES.end(), name) == META_USED_NAMES.end()) {
        UniqueRMutexLock l(_openFileMutex);
        if (_openFileLookup.find(lookupPath) == _openFileLookup.end()) {
          l.unlock();
          ret = _provider.RemoveItemMeta(apiFilePath, name);
        } else if (_openFileLookup[lookupPath]->Meta.find(name) != _openFileLookup[lookupPath]->Meta.end()) {
          _openFileLookup[lookupPath]->Item.MetaChanged = true;
          _openFileLookup[lookupPath]->Meta.erase(name);
          ret = ApiFileError::Success;
        }
      }

      return ret;
    }
#endif
    ApiFileError RenameDirectory(const std::string& fromApiPath, const std::string& toApiPath) {
      UniqueRMutexLock l(_openFileMutex);
      auto ret = ApiFileError::DirectoryNotFound;
      // Ensure source directory exists
      if (_provider.IsDirectory(fromApiPath)) {
        ret = ApiFileError::DirectoryExists;
        // Ensure destination directory does not exist
        if (not _provider.IsDirectory(toApiPath)) {
          ret = ApiFileError::FileExists;
          // Ensure destination is not a file
          if (not _provider.IsFile(fromApiPath)) {
            ret = ApiFileError::DirectoryNotFound;
            // Ensure parent destination directory exists
            DirectoryItemList directoryItems;
            if (_provider.IsDirectory(Utils::Path::GetParentApiPath(toApiPath)) &&
              ((ret = _provider.CreateDirectoryCloneSourceMeta(fromApiPath, toApiPath)) == ApiFileError::Success) &&
              ((ret = _provider.GetDirectoryItems(fromApiPath, directoryItems)) == ApiFileError::Success)) {
              // Rename all items - directories MUST BE returned first
              for (size_t i = 0; (ret == ApiFileError::Success) && (i < directoryItems.size()); i++) {
                const auto oldPath = directoryItems[i].ApiFilePath;
                const auto newPath = Utils::Path::CreateApiPath(Utils::Path::Combine(toApiPath, oldPath.substr(fromApiPath.size())));
                if (directoryItems[i].Directory) {
                  ret = RenameDirectory(oldPath, newPath);
                } else {
                  ret = RenameFile(oldPath, newPath);
                }
              }

              if (ret == ApiFileError::Success) {
                SwapRenamedItems(fromApiPath, toApiPath);
                ret = _provider.RemoveDirectory(fromApiPath);
              }
            }
          }
        }
      }

      return ret;
    }

    ApiFileError RenameFile(const std::string& fromApiPath, const std::string& toApiPath, const bool& overwrite = true) {
      // Don't rename if paths are the same
      auto ret = (CreateLookupPath(fromApiPath) == CreateLookupPath(toApiPath)) ? ApiFileError::FileExists : ApiFileError::Success;
      if (ret == ApiFileError::Success) {
        PauseRetry();

        UniqueRMutexLock l(_openFileMutex);
        // Check allow overwrite if file exists
        if (not overwrite && _provider.IsFile(toApiPath)) {
          l.unlock();
          ret = ApiFileError::FileExists;
        } else {
          // Don't rename if source does not exist
          if ((ret = _provider.IsFile(fromApiPath) ? ApiFileError::Success : ApiFileError::ItemNotFound) == ApiFileError::Success) {
            // Don't rename if files are downloading
            if ((ret = (_downloadManager.IsProcessing(fromApiPath) || _downloadManager.IsProcessing(toApiPath)) ? ApiFileError::FileInUse : ApiFileError::Success) == ApiFileError::Success) {
              // Don't rename if destination file has open handles
              ret = ApiFileError::FileInUse;
              if (GetOpenCount(toApiPath) == 0) {
                if (_provider.IsFile(toApiPath)) { // Handle destination file exists (should overwrite)
                  FileSystemItem fsi{};
                  if ((ret = GetFileSystemItem(toApiPath, false, fsi)) == ApiFileError::Success) {
                    ret = ApiFileError::OSErrorCode;
                    std::uint64_t fileSize = 0;
                    if (Utils::File::GetSize(fsi.SourceFilePath, fileSize)) {
                      ret = _provider.RemoveFile(toApiPath);
                      if ((ret == ApiFileError::Success) || (ret == ApiFileError::ItemNotFound)) {
                        if (RetryDeleteFile(fsi.SourceFilePath) && fileSize) {
                          GlobalData::Instance().UpdateUsedSpace(fileSize, 0, false);
                        }
                        ret = HandleFileRename(fromApiPath, toApiPath);
                      }
                    }
                  }
                  l.unlock();
                } else if (_provider.IsDirectory(toApiPath)) { // Handle destination is directory
                  l.unlock();
                  ret = ApiFileError::DirectoryExists;
                } else if (_provider.IsDirectory(Utils::Path::GetParentApiPath(toApiPath))) { // Handle rename if destination directory exists
                  ret = HandleFileRename(fromApiPath, toApiPath);
                  l.unlock();
                } else { // Destination directory not found
                  l.unlock();
                  ret = ApiFileError::DirectoryNotFound;
                }
              }
            } else if (_provider.IsDirectory(fromApiPath)) {
              l.unlock();
              ret = ApiFileError::DirectoryExists;
            }
          }
        }

        ResumeRetry();
      }

      return ret;
    }

    ApiFileError SetItemMeta(const std::string &apiFilePath, const std::string &key, const std::string &value) {
      const auto lookupPath = CreateLookupPath(apiFilePath);
      auto ret = ApiFileError::Success;

      UniqueRMutexLock l(_openFileMutex);
      if (_openFileLookup.find(lookupPath) == _openFileLookup.end()) {
        l.unlock();
        ret = _provider.SetItemMeta(apiFilePath, key, value);
      } else if (_openFileLookup[lookupPath]->Meta[key] != value) {
        _openFileLookup[lookupPath]->Item.MetaChanged = true;
        _openFileLookup[lookupPath]->Meta[key] = value;
      }

      return ret;
    }

    ApiFileError SetItemMeta(const std::string &apiFilePath, const ApiMetaMap& meta) {
      auto ret = ApiFileError::Success;
      auto it = meta.begin();
      for (size_t i = 0; (ret == ApiFileError::Success) && (i < meta.size()); i++) {
        ret = SetItemMeta(apiFilePath, it->first, it->second);
        it++;
      }

      return ret;
    }

    void Start() {
      MutexLock l(_startStopMutex);
      if (not _retryThread) {
        _stopRequested = false;
        _retryThread = std::make_unique<std::thread>([this]{
          UploadRetryThread();
        });
      }
    }

    void Stop() {
      MutexLock l(_startStopMutex);
      if (_retryThread) {
        _stopRequested = true;
        _retryNotify.notify_all();
        _retryThread->join();
        _retryThread.reset();
      }
    }
};
}

#endif //REPERTORY_OPENFILETABLE_H
