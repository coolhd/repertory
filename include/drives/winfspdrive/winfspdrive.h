#ifndef REPERTORY_WINFSPDRIVE_H
#define REPERTORY_WINFSPDRIVE_H
#ifdef _WIN32

#include <config.h>
#include <download/downloadmanager.h>
#include <drives/eviction.h>
#include <drives/iopenfiletable.h>
#include <drives/openfiletable.h>
#include <events/consumers/consoleconsumer.h>
#include <events/consumers/loggingconsumer.h>
#include <utils/polling.h>
#include <providers/iprovider.h>
#include <rpc/server/server.h>
#include <sddl.h>
#include <string>
#include <unordered_map>
#include <utils/utils.h>
#include <vector>
#include <winfsp/winfsp.hpp>

extern char g_siaMountLocation[];
extern std::int64_t g_siaPID;
extern char g_siaPrimeMountLocation[];
extern std::int64_t g_siaPrimePID;

using namespace Fsp;

namespace Repertory {
E_SIMPLE3(WinFSPEvent, Debug, true, std::string, Function, FUNC, E_STRING, std::string, ApiFilePath, AP, E_STRING, NTSTATUS, Result, RES, E_FROM_INT32);

#define RAISE_WINFSP_EVENT(func, file, ret) \
  if (_config.GetEnableDriveEvents() && (((_config.GetEventLevel() >= WinFSPEvent::Level) && (ret != STATUS_SUCCESS)) || (_config.GetEventLevel() >= EventLevel::Verbose))) EventSystem::Instance().Raise<WinFSPEvent>(func, file, ret)

class CWinFSPDrive final :
  public virtual FileSystemBase {
  E_CONSUMER();
    const std::uint16_t ALLOCATION_UNIT = 4096U;

  public:
    CWinFSPDrive(IProvider &provider, CConfig &config) :
      FileSystemBase(),
      _provider(provider),
      _config(config) {
      E_SUBSCRIBE_EXACT(UnmountRequested, [this](const UnmountRequested &e) {
        this->Shutdown();
      });
    }

    ~CWinFSPDrive() override = default;

  private:
    class CDirectoryIterator {
      public:
        CDirectoryIterator(IProvider &provider, const FileSystemItem &fileSystemItem, std::string pattern) :
          _provider(provider),
          _fileSystemItem(fileSystemItem),
          _pattern(std::move(pattern)) {
          _result = provider.GetDirectoryItems(fileSystemItem.ApiFilePath, _directoryItems);
          _iter = _directoryItems.begin();
        }

      private:
        IProvider &_provider;
        const FileSystemItem &_fileSystemItem;
        const std::string _pattern;
        ApiFileError _result = ApiFileError::Success;
        DirectoryItemList _directoryItems;
        DirectoryItemList::const_iterator _iter;

      public:
        inline ApiFileError GetResult() const {
          return _result;
        }

        inline void Reset() {
          _iter = _directoryItems.begin();
        }

        ApiFileError Next(DirectoryItem &directoryItem) {
          auto ret = ApiFileError::DirectoryEndOfFiles;
          if (_iter != _directoryItems.end()) {
            directoryItem = *_iter;
            _iter++;
            ret = ApiFileError::Success;
          }
          return ret;
        }
    };

    class CWinFSPService :
      virtual public Service {
      public:
        CWinFSPService(CWinFSPDrive &winFSPDrive, std::vector<std::string> driveArgs, CConfig &config) :
          Service((PWSTR) L"Repertory"),
          _winFSPDrive(winFSPDrive),
          _driveArgs(std::move(driveArgs)),
          _fileSystemHost(winFSPDrive),
          _config(config) {
        }

        ~CWinFSPService() override = default;

      private:
        CWinFSPDrive &_winFSPDrive;
        const std::vector<std::string> _driveArgs;
        FileSystemHost _fileSystemHost;
        CConfig &_config;

      private:
        void ResetMountState() const {
          auto &pid = (_config.GetProviderType() == ProviderType::SiaPrime) ? g_siaPrimePID : g_siaPID;

          auto *mountLocation = (_config.GetProviderType() == ProviderType::SiaPrime) ? &g_siaPrimeMountLocation[0] : &g_siaMountLocation[0];
          pid = -1;
          ::ZeroMemory(mountLocation, 3);
        }

      protected:
        NTSTATUS OnStart(ULONG, PWSTR *) override {
          const auto mountPoint = Utils::Path::Absolute((_driveArgs.size() > 1) ? _driveArgs[1] : "");
          auto &pid = (_config.GetProviderType() == ProviderType::SiaPrime) ? g_siaPrimePID : g_siaPID;

          auto *mountLocation = (_config.GetProviderType() == ProviderType::SiaPrime) ? &g_siaPrimeMountLocation[0] : &g_siaMountLocation[0];

          strcpy_s(mountLocation, 3, &mountPoint[0]);
          pid = ::GetCurrentProcessId();

          const auto driveLetter = ((mountPoint.size() == 2) || ((mountPoint.size() == 3) && (mountPoint[2] == '\\'))) && (mountPoint[1] == ':');

          auto ret = driveLetter ? STATUS_DEVICE_BUSY : STATUS_NOT_SUPPORTED;
          if ((driveLetter && not Utils::File::IsDirectory(mountPoint))) {
            auto unicodeMountPoint = Utils::String::FromUtf8(mountPoint);
            ret = _fileSystemHost.Mount(&unicodeMountPoint[0]);
          } else {
            std::cerr << (driveLetter ? "Mount location in use: " : "Mount location not supported: ") << mountPoint << std::endl;
          }

          if (ret != STATUS_SUCCESS) {
            ResetMountState();
            EventSystem::Instance().Raise<DriveMountFailed>(mountPoint, std::to_string(ret));
          }

          return ret;
        }

        NTSTATUS OnStop() override {
          if (_winFSPDrive._downloadManager) {
            _winFSPDrive._downloadManager->Stop();
          }
          _fileSystemHost.Unmount();
          ResetMountState();

          return STATUS_SUCCESS;
        }
    };

  private:
    IProvider &_provider;
    CConfig &_config;
    std::unique_ptr<CServer> _server;
    std::unique_ptr<COpenFileTable<OpenFileData>> _openFileTable;
    std::unique_ptr<CDownloadManager> _downloadManager;
    std::unique_ptr<CEviction> _eviction;

  private:
    inline std::uint64_t GetMetaAccessedTime(const ApiMetaMap &meta) {
      return Utils::String::ToUInt64(meta.at(META_ACCESSED));
    }

    inline DWORD GetMetaAttributes(const ApiMetaMap &meta) {
      return static_cast<DWORD>(Utils::String::ToUInt32(meta.at(META_ATTRIBUTES)));
    }

    inline std::uint64_t GetMetaChangedTime(const ApiMetaMap &meta) {
      return Utils::String::ToUInt64(meta.at(META_MODIFIED));
    }

    inline std::uint64_t GetMetaCreationTime(const ApiMetaMap &meta) {
      return Utils::String::ToUInt64(meta.at(META_CREATION));
    }

    inline std::uint64_t GetMetaWrittenTime(const ApiMetaMap &meta) {
      return Utils::String::ToUInt64(meta.at(META_WRITTEN));
    }

    void PopulateFileInfo(const std::string& apiFilePath, const std::uint64_t &fileSize, const ApiMetaMap &meta, FSP_FSCTL_OPEN_FILE_INFO& openFileInfo) {
      const auto filePath = Utils::String::FromUtf8(Utils::String::ReplaceCopy(apiFilePath, '/', '\\'));
      
      wcscpy_s(openFileInfo.NormalizedName, openFileInfo.NormalizedNameSize / sizeof(WCHAR), &filePath[0]);
      openFileInfo.NormalizedNameSize = (UINT16)(wcslen(&filePath[0]) * sizeof(WCHAR));
      PopulateFileInfo(fileSize, meta, openFileInfo.FileInfo);
    }

    void PopulateFileInfo(const std::uint64_t &fileSize, const ApiMetaMap &meta, FSP_FSCTL_FILE_INFO &fileInfo) {
      fileInfo.FileSize = fileSize;
      fileInfo.AllocationSize = (fileSize + ALLOCATION_UNIT - 1) / ALLOCATION_UNIT * ALLOCATION_UNIT;
      fileInfo.ChangeTime = GetMetaChangedTime(meta);
      fileInfo.CreationTime = GetMetaCreationTime(meta);
      fileInfo.FileAttributes = GetMetaAttributes(meta);
      fileInfo.HardLinks = 0;
      fileInfo.IndexNumber = 0;
      fileInfo.LastAccessTime = GetMetaAccessedTime(meta);
      fileInfo.LastWriteTime = GetMetaWrittenTime(meta);
      fileInfo.ReparseTag = 0;
      fileInfo.EaSize = 0;
    }

  public:
    NTSTATUS CanDelete(PVOID fileNode, PVOID fileDesc, PWSTR fileName) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          if (fileSystemItem->Directory) {
            apiFileError = _provider.GetDirectoryItemCount(fileSystemItem->ApiFilePath) ? ApiFileError::DirectoryNotEmpty : ApiFileError::Success;
          } else {
            apiFileError = _downloadManager->IsProcessing(fileSystemItem->ApiFilePath) ? ApiFileError::FileInUse : ApiFileError::Success;
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    VOID Cleanup(PVOID fileNode, PVOID fileDesc, PWSTR fileName, ULONG flags) override {
      std::string apiFilePath;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;

          const auto directory = fileSystemItem->Directory;
          if ((flags & FspCleanupSetArchiveBit) && not directory) {
            ApiMetaMap meta;
            if (_openFileTable->DeriveItemData(apiFilePath, meta) == ApiFileError::Success) {
              _openFileTable->SetItemMeta(apiFilePath, META_ATTRIBUTES, std::to_string(GetMetaAttributes(meta) | FILE_ATTRIBUTE_ARCHIVE));
            }
          }

          if (flags & (FspCleanupSetLastAccessTime | FspCleanupSetLastWriteTime | FspCleanupSetChangeTime)) {
            const auto fileTimeNow = Utils::GetFileTimeNow();
            if (flags & FspCleanupSetLastAccessTime) {
              _openFileTable->SetItemMeta(apiFilePath, META_ACCESSED, std::to_string(fileTimeNow));
            }

            if (flags & FspCleanupSetLastWriteTime) {
              _openFileTable->SetItemMeta(apiFilePath, META_WRITTEN, std::to_string(fileTimeNow));
            }

            if (flags & FspCleanupSetChangeTime) {
              _openFileTable->SetItemMeta(apiFilePath, META_MODIFIED, std::to_string(fileTimeNow));
            }
          }

          if (flags & FspCleanupSetAllocationSize) {
            UINT64 allocationSize = (fileSystemItem->Size + ALLOCATION_UNIT - 1) / ALLOCATION_UNIT * ALLOCATION_UNIT;
            SetFileSize(fileNode, fileDesc, allocationSize, TRUE, nullptr);
          }

          if (flags & FspCleanupDelete) {
            const auto sourceFilePath = fileSystemItem->SourceFilePath;
            auto *dirBuffer = fileSystemItem->OpenData[handle].DirectoryBuffer;
            fileSystemItem = nullptr;

            _openFileTable->CloseAll(apiFilePath);

            if (dirBuffer) {
              FspFileSystemDeleteDirectoryBuffer(&dirBuffer);
            }

            if (directory) {
              _provider.RemoveDirectory(apiFilePath);
            } else {
              _openFileTable->RemoveFile(apiFilePath);
            }
          }
        }
      }
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, 0);
    }

    VOID Close(PVOID fileNode, PVOID fileDesc) override {
      std::string apiFilePath;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        PVOID dirBuffer = nullptr;
        FileSystemItem* fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          dirBuffer = fileSystemItem->OpenData[handle].DirectoryBuffer;
          fileSystemItem = nullptr;
        }
        _openFileTable->Close(handle);
        if (dirBuffer) {
          FspFileSystemDeleteDirectoryBuffer(&dirBuffer);
        }
      }
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, 0);
    }

    NTSTATUS Create(PWSTR fileName, UINT32 createOptions, UINT32 grantedAccess, UINT32 fileAttributes, PSECURITY_DESCRIPTOR securityDescriptor, UINT64 allocationSize, PVOID *fileNode, PVOID *fileDesc, OpenFileInfo *openFileInfo) override {
      //TODO Need to revisit this (ConvertSecurityDescriptorToStringSecurityDescriptor/ConvertStringSecurityDescriptorToSecurityDescriptor)
      auto apiFileError = ApiFileError::Error;

      if (createOptions & FILE_DIRECTORY_FILE) {
        fileAttributes |= FILE_ATTRIBUTE_DIRECTORY;
      } else {
        fileAttributes &= ~FILE_ATTRIBUTE_DIRECTORY;
      }

      if (not fileAttributes) {
        fileAttributes = FILE_ATTRIBUTE_NORMAL;
      }

      const auto createDate = Utils::GetFileTimeNow();
      auto meta = ApiMetaMap({{META_ATTRIBUTES, std::to_string(fileAttributes)},
                              {META_CREATION,   std::to_string(createDate)},
                              {META_WRITTEN,    std::to_string(createDate)},
                              {META_MODIFIED,   std::to_string(createDate)},
                              {META_ACCESSED,   std::to_string(createDate)}});

      const auto apiFilePath = Utils::Path::CreateApiPath(Utils::String::ToUtf8(fileName));
      if (fileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        apiFileError = _provider.CreateDirectory(apiFilePath, meta);
      } else {
        apiFileError = _provider.CreateFile(apiFilePath, meta);
      }
      if (apiFileError == ApiFileError::Success) {
        OpenFileData openFileData{};
        std::uint64_t handle;
        apiFileError = _openFileTable->Open(apiFilePath, static_cast<bool>(fileAttributes & FILE_ATTRIBUTE_DIRECTORY), openFileData, handle);
        if (apiFileError == ApiFileError::Success) {
          PopulateFileInfo(apiFilePath, 0, meta, *openFileInfo);
          *fileDesc = reinterpret_cast<PVOID>(handle);
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS Flush(PVOID fileNode, PVOID fileDesc, FileInfo *fileInfo) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::Success;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        apiFileError = ApiFileError::InvalidOSHandle;
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          apiFileError = ApiFileError::Success;
          if (fileSystemItem->Handle != INVALID_OSHANDLE_VALUE) {
            if (not::FlushFileBuffers(fileSystemItem->Handle)) {
              apiFileError = ApiFileError::OSErrorCode;
            }
          }

          // Populate file information
          std::uint64_t fileSize = 0;
          ApiMetaMap meta;
          if (_openFileTable->DeriveItemData(apiFilePath, false, fileSize, meta) == ApiFileError::Success) {
            PopulateFileInfo(fileSize, meta, *fileInfo);
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS GetFileInfo(PVOID fileNode, PVOID fileDesc, FileInfo *fileInfo) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          std::uint64_t fileSize = 0;
          ApiMetaMap meta;
          if ((apiFileError = _openFileTable->DeriveItemData(fileSystemItem->ApiFilePath, fileSystemItem->Directory, fileSize, meta)) == ApiFileError::Success) {
            PopulateFileInfo(fileSize, meta, *fileInfo);
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS GetSecurityByName(PWSTR fileName, PUINT32 fileAttributes, PSECURITY_DESCRIPTOR securityDescriptor, SIZE_T *securityDescriptorSize) override {
      const auto apiFilePath = Utils::Path::CreateApiPath(Utils::String::ToUtf8(fileName));

      ApiMetaMap meta;
      auto apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta);
      if (apiFileError == ApiFileError::Success) {
        if (fileAttributes) {
          *fileAttributes = GetMetaAttributes(meta);
        }

        if (securityDescriptorSize) {
          ULONG sz = 0;
          PSECURITY_DESCRIPTOR sd = nullptr;
          if (::ConvertStringSecurityDescriptorToSecurityDescriptor("O:BAG:BAD:P(A;;FA;;;SY)(A;;FA;;;BA)(A;;FA;;;WD)", SDDL_REVISION_1, &sd, &sz)) {
            if (sz > *securityDescriptorSize) {
              apiFileError = ApiFileError::BufferTooSmall;
            } else {
              ::CopyMemory(securityDescriptor, sd, sz);
            }
            *securityDescriptorSize = sz;
            ::LocalFree(sd);
          } else {
            apiFileError = ApiFileError::OSErrorCode;
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS GetVolumeInfo(VolumeInfo *volumeInfo) override {
      const std::wstring volumeLabel = Utils::String::FromUtf8("Repertory" + CConfig::GetProviderDisplayName(_config.GetProviderType()));
      const auto totalBytes = _provider.GetTotalDriveSpace();
      const auto totalUsed = _provider.GetUsedDriveSpace();
      volumeInfo->FreeSize = totalBytes - totalUsed;
      volumeInfo->TotalSize = totalBytes;
      wcscpy_s(&volumeInfo->VolumeLabel[0], 32, &volumeLabel[0]);
      volumeInfo->VolumeLabelLength = std::min(static_cast<UINT16>(64), static_cast<UINT16>(volumeLabel.size() * sizeof(WCHAR)));

      RAISE_WINFSP_EVENT(__FUNCTION__, Utils::String::ToUtf8(volumeLabel), 0);
      return STATUS_SUCCESS;
    }

    NTSTATUS Init(PVOID host) override {
      auto *fileSystemHost = reinterpret_cast<FileSystemHost *>(host);
      fileSystemHost->SetFlushAndPurgeOnCleanup(TRUE);
      fileSystemHost->SetReparsePoints(FALSE);
      fileSystemHost->SetReparsePointsAccessCheck(FALSE);
      fileSystemHost->SetSectorSize(ALLOCATION_UNIT);
      fileSystemHost->SetSectorsPerAllocationUnit(1);
      fileSystemHost->SetFileInfoTimeout(1000);
      fileSystemHost->SetCaseSensitiveSearch(TRUE);
      fileSystemHost->SetCasePreservedNames(TRUE);
      fileSystemHost->SetNamedStreams(FALSE);
      fileSystemHost->SetUnicodeOnDisk(TRUE);
      fileSystemHost->SetPersistentAcls(FALSE);
      fileSystemHost->SetPostCleanupWhenModifiedOnly(TRUE);
      fileSystemHost->SetPassQueryDirectoryPattern(FALSE);
      fileSystemHost->SetVolumeCreationTime(Utils::GetFileTimeNow());
      fileSystemHost->SetVolumeSerialNumber(0);
      return STATUS_SUCCESS;
    }

    NTSTATUS Mounted(PVOID host) override {
      auto ret = STATUS_SUCCESS;
      auto *fileSystemHost = reinterpret_cast<FileSystemHost *>(host);
      Polling::Instance().Start(&_config);
      EventSystem::Instance().Raise<DriveMounted>(Utils::String::ToUtf8(fileSystemHost->MountPoint()));
      _downloadManager = std::make_unique<CDownloadManager>(_config, [this](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool &stopRequested) -> ApiFileError {
        return _provider.ReadFileBytes(path, size, offset, data, stopRequested);
      });
      _openFileTable = std::make_unique<COpenFileTable<OpenFileData>>(_provider, _config, *_downloadManager);
      _server = std::make_unique<CServer>(_config, _provider, *_openFileTable);
      _eviction = std::make_unique<CEviction>(_provider, _config, *_openFileTable);
      try {
        _provider.Start([this](const std::string &apiFilePath, const std::string &parent, const std::string &source, const bool &directory, const std::uint64_t &createDate, const std::uint64_t &accessDate, const std::uint64_t &modifiedDate, const std::uint64_t &changedDate) -> void {
          EventSystem::Instance().Raise<FileSystemItemAdded>(apiFilePath, parent, directory);
          _provider.SetItemMeta(apiFilePath, {{META_ATTRIBUTES, std::to_string(directory ? FILE_ATTRIBUTE_DIRECTORY : FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_ARCHIVE)},
                                              {META_CREATION,   std::to_string(createDate)},
                                              {META_WRITTEN,    std::to_string(modifiedDate)},
                                              {META_MODIFIED,   std::to_string(modifiedDate)},
                                              {META_ACCESSED,   std::to_string(accessDate)}});          
          if (not directory) {
            _provider.SetSourcePath(apiFilePath, source);
          }
        }, _openFileTable.get());
        _downloadManager->Start(_openFileTable.get());
        _openFileTable->Start();
        _eviction->Start();
        _server->Start();

        // Force root creation
        ApiMetaMap metaMap{};
        _provider.GetItemMeta("/", metaMap);
      } catch (const StartupException &e) {
        EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, e.what());
        _server->Stop();
        Polling::Instance().Stop();
        _eviction->Stop();
        _openFileTable->Stop();
        _downloadManager->Stop();
        _provider.Stop();
        ret = STATUS_INTERNAL_ERROR;
      }

      return ret;
    }

    NTSTATUS Open(PWSTR fileName, UINT32 createOptions, UINT32 grantedAccess, PVOID *fileNode, PVOID *fileDesc, OpenFileInfo *openFileInfo) override {
      const auto apiFilePath = Utils::Path::CreateApiPath(Utils::String::ToUtf8(fileName));
      const auto directory = _provider.IsDirectory(apiFilePath);

      OpenFileData openFileData{};
      std::uint64_t handle;
      auto apiFileError = _openFileTable->Open(apiFilePath, directory, openFileData, handle);
      if (apiFileError == ApiFileError::Success) {
        std::uint64_t fileSize = 0;
        ApiMetaMap meta;
        if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, directory, fileSize, meta)) == ApiFileError::Success) {
          PopulateFileInfo(apiFilePath, fileSize, meta, *openFileInfo);
          *fileDesc = reinterpret_cast<PVOID>(handle);
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS Overwrite(PVOID fileNode, PVOID fileDesc, UINT32 fileAttributes, BOOLEAN replaceFileAttributes, UINT64 allocationSize, FileInfo *fileInfo) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          std::uint64_t fileSize;
          ApiMetaMap meta{};
          if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, false, fileSize, meta)) == ApiFileError::Success) {
            apiFileError = ApiFileError::FileInUse;
            if (not _downloadManager->IsProcessing(apiFilePath)) {
              apiFileError = ApiFileError::Success;
              auto updateCacheSpace = true;
              if (fileSystemItem->Handle != INVALID_OSHANDLE_VALUE) {
                RMutexLock l(*fileSystemItem->ItemLock);
                auto nativeFile = CNativeFile::Attach(fileSystemItem->Handle);
                if (nativeFile->Truncate(0)) {
                  GlobalData::Instance().UpdateUsedSpace(fileSize, 0, true);
                  nativeFile->Close();
                  fileSystemItem->Handle = INVALID_OSHANDLE_VALUE;
                  updateCacheSpace = false;
                } else {
                  apiFileError = ApiFileError::OSErrorCode;
                }
              }

              Utils::File::DeleteAsFile(fileSystemItem->SourceFilePath);

              if ((apiFileError == ApiFileError::Success) &&
                  ((apiFileError = _provider.RemoveFile(apiFilePath)) == ApiFileError::Success)) {
                if (updateCacheSpace) {
                  GlobalData::Instance().UpdateUsedSpace(fileSize, 0, false);
                } else {
                  GlobalData::Instance().DecrementUsedDriveSpace(fileSize);
                }

                fileSystemItem->Changed = false;
                fileSize = fileSystemItem->Size = 0;

                if ((apiFileError = _provider.CreateFile(apiFilePath, meta)) == ApiFileError::Success) {
                  FileSystemItem fsi{};
                  if ((apiFileError = _provider.GetFileSystemItem(apiFilePath, false, fsi)) == ApiFileError::Success) {
                    fileSystemItem->SourceFilePath = fsi.SourceFilePath;
                    fileSystemItem->SourceFilePathChanged = false;
                    Utils::File::DeleteAsFile(fileSystemItem->SourceFilePath);

                    // Handle replace attributes
                    if (replaceFileAttributes) {
                      if (not fileAttributes) {
                        fileAttributes = FILE_ATTRIBUTE_NORMAL;
                      }
                      meta[META_ATTRIBUTES] = std::to_string(fileAttributes);
                      apiFileError = _openFileTable->SetItemMeta(apiFilePath, meta);
                    } else if (fileAttributes) { // Handle merge attributes
                      const auto currentAttributes = GetMetaAttributes(meta);
                      const auto mergedAttributes = fileAttributes | currentAttributes;
                      if (mergedAttributes != currentAttributes) {
                        meta[META_ATTRIBUTES] = std::to_string(mergedAttributes);
                        apiFileError = _openFileTable->SetItemMeta(apiFilePath, meta);
                      }
                    }
                  }
                }
              }
            }

            // Populate file information
            PopulateFileInfo(fileSize, meta, *fileInfo);
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS Read(PVOID fileNode, PVOID fileDesc, PVOID buffer, UINT64 offset, ULONG length, PULONG bytesTransferred) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          std::vector<char> data;
          if ((apiFileError = _downloadManager->ReadBytes(handle, *fileSystemItem, length, offset, data)) == ApiFileError::Success) {
            *bytesTransferred = static_cast<ULONG>(data.size());
            if ((length > 0) && (data.size() != length)) {
              ::SetLastError(ERROR_HANDLE_EOF);
            }

            if (not data.empty()) {
              ::CopyMemory(buffer, &data[0], data.size());
              data.clear();
            }
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      if (ret != STATUS_SUCCESS) {
        RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      }
      return ret;
    }

    NTSTATUS ReadDirectory(PVOID fileNode, PVOID fileDesc, PWSTR pattern, PWSTR marker, PVOID buffer, ULONG bufferLength, PULONG bytesTransferred) override {
      std::string apiFilePath;
      const auto strPattern = pattern ? std::wstring(pattern) : L"*";
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem* fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          CDirectoryIterator directoryIterator(_provider, *fileSystemItem, Utils::String::ToUtf8(strPattern));
          if ((apiFileError = directoryIterator.GetResult()) == ApiFileError::Success) {
            apiFilePath = fileSystemItem->ApiFilePath;
            auto ntStatusResult = STATUS_SUCCESS;
            auto *dirBuffer = fileSystemItem->OpenData[handle].DirectoryBuffer;
            if (FspFileSystemAcquireDirectoryBuffer(&dirBuffer, static_cast<BOOLEAN>(nullptr == marker), &ntStatusResult)) {
              DirectoryItem directoryItem{};
              while ((apiFileError = directoryIterator.Next(directoryItem)) == ApiFileError::Success) {
                if (not Utils::Path::IsADS(directoryItem.ApiFilePath)) {
                  const auto displayName = Utils::String::FromUtf8(Utils::Path::StripToFileName(directoryItem.ApiFilePath));
                  union {
                    UINT8 B[FIELD_OFFSET(FSP_FSCTL_DIR_INFO, FileNameBuf) + ((MAX_PATH + 1) * sizeof(WCHAR))];
                    FSP_FSCTL_DIR_INFO D;
                  } DirInfoBuf;

                  auto *dirInfo = &DirInfoBuf.D;
                  ::ZeroMemory(dirInfo, sizeof(*dirInfo));
                  dirInfo->Size = static_cast<UINT16>(FIELD_OFFSET(FSP_FSCTL_DIR_INFO, FileNameBuf) + (std::min((size_t) MAX_PATH, displayName.size()) * sizeof(WCHAR)));

                  std::uint64_t fileSize = 0;
                  ApiMetaMap meta;
                  if (_openFileTable->DeriveItemData(directoryItem, fileSize, meta) == ApiFileError::Success) {
                    PopulateFileInfo(fileSize, meta, dirInfo->FileInfo);
                  }
                  ::wcscpy_s(&dirInfo->FileNameBuf[0], MAX_PATH, &displayName[0]);

                  FspFileSystemFillDirectoryBuffer(&dirBuffer, dirInfo, &ntStatusResult);
                }
              }
            }

            FspFileSystemReleaseDirectoryBuffer(&dirBuffer);

            if (ntStatusResult == STATUS_SUCCESS) {
              FspFileSystemReadDirectoryBuffer(&dirBuffer, marker, buffer, bufferLength, bytesTransferred);
              if (apiFileError == ApiFileError::DirectoryEndOfFiles) {
                apiFileError = ApiFileError::Success;
              }
            } else {
              RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ntStatusResult);
              return ntStatusResult;
            }
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS Rename(PVOID fileNode, PVOID fileDesc, PWSTR fileName, PWSTR newFileName, BOOLEAN replaceIfExists) override {
      const auto fromApiPath = Utils::Path::CreateApiPath(Utils::String::ToUtf8(fileName));
      const auto toApiPath = Utils::Path::CreateApiPath(Utils::String::ToUtf8(newFileName));

      auto apiFileError = ApiFileError::ItemNotFound;
      if (_provider.IsFile(fromApiPath)) {
        apiFileError = _openFileTable->RenameFile(fromApiPath, toApiPath, replaceIfExists);
      } else if (_provider.IsDirectory(fromApiPath)) {
        apiFileError = _openFileTable->RenameDirectory(fromApiPath, toApiPath);
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, fromApiPath + "|" + toApiPath, ret);
      return ret;
    }

    NTSTATUS SetBasicInfo(PVOID fileNode, PVOID fileDesc, UINT32 fileAttributes, UINT64 creationTime, UINT64 lastAccessTime, UINT64 lastWriteTime, UINT64 changeTime, FileInfo *fileInfo) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          if (fileAttributes == INVALID_FILE_ATTRIBUTES) {
            fileAttributes = 0;
          } else if (fileAttributes == 0) {
            fileAttributes = FILE_ATTRIBUTE_NORMAL;
          }

          ApiMetaMap meta;
          if (fileAttributes) {
            meta[META_ATTRIBUTES] = std::to_string(fileAttributes);
          }
          if (creationTime && (creationTime != 0xFFFFFFFFFFFFFFFF)) {
            meta[META_CREATION] = std::to_string(creationTime);
          }
          if (lastAccessTime && (lastAccessTime != 0xFFFFFFFFFFFFFFFF)) {
            meta[META_ACCESSED] = std::to_string(lastAccessTime);
          }
          if (lastWriteTime && (lastWriteTime != 0xFFFFFFFFFFFFFFFF)) {
            meta[META_WRITTEN] = std::to_string(lastWriteTime);
          }
          if (changeTime && (changeTime != 0xFFFFFFFFFFFFFFFF)) {
            meta[META_MODIFIED] = std::to_string(changeTime);
          }

          apiFileError = _openFileTable->SetItemMeta(apiFilePath, meta);

          // Populate file information
          std::uint64_t fileSize = 0;
          if (_openFileTable->DeriveItemData(apiFilePath, false, fileSize, meta) == ApiFileError::Success) {
            PopulateFileInfo(fileSize, meta, *fileInfo);
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    NTSTATUS SetFileSize(PVOID fileNode, PVOID fileDesc, UINT64 newSize, BOOLEAN setAllocationSize, FileInfo *fileInfo) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          if ((apiFileError = _downloadManager->Download(handle, *fileSystemItem)) == ApiFileError::Success) {
            const auto origSize = fileSystemItem->Size;
            if (setAllocationSize) {
              FILE_ALLOCATION_INFO allocationInfo{};
              allocationInfo.AllocationSize.QuadPart = newSize;
              if (::SetFileInformationByHandle(fileSystemItem->Handle, FileAllocationInfo, &allocationInfo, sizeof(allocationInfo))) {
                if (newSize != origSize) {
                  fileSystemItem->Changed = true;
                  Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                  GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);
                }
              } else {
                apiFileError = ApiFileError::OSErrorCode;
              }
            } else {
              FILE_END_OF_FILE_INFO endOfFileInfo{};
              endOfFileInfo.EndOfFile.QuadPart = newSize;
              if (::SetFileInformationByHandle(fileSystemItem->Handle, FileEndOfFileInfo, &endOfFileInfo, sizeof(endOfFileInfo))) {
                if (newSize != origSize) {
                  fileSystemItem->Changed = true;
                  Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                  GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);
                }
              } else {
                apiFileError = ApiFileError::OSErrorCode;
              }
            }
          }

          if (fileInfo) {
            // Populate file information
            std::uint64_t fileSize = 0;
            ApiMetaMap meta;
            if (_openFileTable->DeriveItemData(apiFilePath, false, fileSize, meta) == ApiFileError::Success) {
              PopulateFileInfo(fileSize, meta, *fileInfo);
            }
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      return ret;
    }

    VOID Unmounted(PVOID host) override {
      auto *fileSystemHost = reinterpret_cast<FileSystemHost *>(host);
      const auto mountPoint = Utils::String::ToUtf8(fileSystemHost->MountPoint());
      EventSystem::Instance().Raise<DriveUnMountPending>(mountPoint);
      _server->Stop();
      Polling::Instance().Stop();
      _eviction->Stop();
      _openFileTable->Stop();
      _downloadManager->Stop();
      _provider.Stop();
      _server.reset();
      _downloadManager.reset();
      _openFileTable.reset();
      _eviction.reset();
      EventSystem::Instance().Raise<DriveUnMounted>(mountPoint);
      _config.SaveConfiguration();
    }

    NTSTATUS Write(PVOID fileNode, PVOID fileDesc, PVOID buffer, UINT64 offset, ULONG length, BOOLEAN writeToEndOfFile, BOOLEAN constrainedIo, PULONG bytesTransferred, FileInfo *fileInfo) override {
      std::string apiFilePath;
      auto apiFileError = ApiFileError::InvalidOSHandle;
      auto handle = reinterpret_cast<std::uint64_t>(fileDesc);
      if (handle) {
        FileSystemItem *fileSystemItem = nullptr;
        if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
          apiFilePath = fileSystemItem->ApiFilePath;
          auto shouldWrite = true;
          if (constrainedIo) {
            if (offset >= fileSystemItem->Size) {
              apiFileError = ApiFileError::Success;
              shouldWrite = false;
            } else if (offset + length > fileSystemItem->Size) {
              length = static_cast<ULONG>(fileSystemItem->Size - offset);
            }
          }

          if (shouldWrite) {
            if (Utils::File::IsFile(fileSystemItem->SourceFilePath)) {
              std::uint64_t fileSize;
              if (Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSize)) {
                if (fileSize == fileSystemItem->Size) {
                  apiFileError = ApiFileError::Success;
                } else {
                  apiFileError = _downloadManager->Download(handle, *fileSystemItem);
                }
              } else {
                apiFileError = ApiFileError::OSErrorCode;
              }
            } else {
              apiFileError = _downloadManager->Download(handle, *fileSystemItem);
            }

            if (apiFileError == ApiFileError::Success) {
              NativeFilePtr nativeFile;
              if ((apiFileError = Utils::File::AssignAndGetNativeFile(*fileSystemItem, nativeFile)) == ApiFileError::Success) {
                std::size_t bytesWritten = 0;
                if (nativeFile->WriteBytes(static_cast<const char *>(buffer), length, offset, bytesWritten)) {
                  *bytesTransferred = static_cast<ULONG>(bytesWritten);
                  fileSystemItem->Changed = true;
                  const auto origSize = fileSystemItem->Size;
                  Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                  GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);

                  // Populate file information
                  ApiMetaMap meta;
                  if (_openFileTable->DeriveItemData(apiFilePath, meta) == ApiFileError::Success) {
                    PopulateFileInfo(fileSystemItem->Size, meta, *fileInfo);
                  }
                } else {
                  apiFileError = ApiFileError::OSErrorCode;
                }
              }
            }
          }
        }
      }

      const auto ret = Utils::TranslateApiFileError(apiFileError);
      if (ret != STATUS_SUCCESS) {
        RAISE_WINFSP_EVENT(__FUNCTION__, apiFilePath, ret);
      }
      return ret;
    }

  public:
    inline int Main(const std::vector<std::string> &driveArgs) {
      std::vector<std::string> parsedDriveArgs;

      auto forceNoConsole = false;
      for (const auto &arg : driveArgs) {
        if (arg == "-nc") {
          forceNoConsole = true;
          break;
        }
      }

      auto enableConsole = false;
      if (not forceNoConsole) {
        for (const auto &arg : driveArgs) {
          if (arg == "-f") {
            enableConsole = true;
          } else {
            parsedDriveArgs.emplace_back(arg);
          }
        }
      }

      CLoggingConsumer loggingConsumer(_config.GetLogDirectory(), _config.GetEventLevel());
      std::unique_ptr<CConsoleConsumer> consoleConsumer;
      if (enableConsole) {
        consoleConsumer = std::make_unique<CConsoleConsumer>();
      }
      EventSystem::Instance().Start();
      const auto ret = CWinFSPService(*this, parsedDriveArgs, _config).Run();
      EventSystem::Instance().Raise<DriveMountResult>(std::to_string(ret));
      EventSystem::Instance().Stop();
      consoleConsumer.reset();
      return static_cast<int>(ret);
    }

    inline void Shutdown() {
      ::GenerateConsoleCtrlEvent(CTRL_C_EVENT, 0);
    }

    static inline void DisplayOptions(int argc, char *argv[]) {
    }

    static inline void DisplayVersionInfo(int argc, char *argv[]) {
    }
};
}

#endif
#endif //REPERTORY_WINFSPDRIVE_H
