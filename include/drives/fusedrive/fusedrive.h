#ifndef REPERTORY_FUSEDRIVE_H
#define REPERTORY_FUSEDRIVE_H
#ifndef _WIN32

#define FUSE_USE_VERSION 29
#include <common.h>
#include <atomic>
#include <config.h>
#include <download/downloadmanager.h>
#include <drives/eviction.h>
#include <drives/iopenfiletable.h>
#include <drives/openfiletable.h>
#include <events/eventsystem.h>
#include <events/consumers/consoleconsumer.h>
#include <events/consumers/loggingconsumer.h>
#include <providers/iprovider.h>
#include <rpc/server/server.h>
#include <string>
#include <sys/stat.h>
#ifdef HAS_SETXATTR
#include <sys/types.h>
#include <sys/xattr.h>
#endif
#include <unordered_map>
#include <utils/Base64.h>
#include <utils/fileutils.h>
#include <utils/polling.h>
#include <utils/utils.h>
#include <vector>
#if !IS_DEBIAN9_DISTRO && HAS_STD_OPTIONAL
#include <optional>
#else
#include <utils/optional.h>
#endif
#ifdef __APPLE__
#include <osxfuse/fuse.h>
#include <sys/vnode.h>
#else
#include <fuse/fuse.h>
#endif

#ifdef __APPLE__
#define G_PREFIX                       "org"
#define G_KAUTH_FILESEC_XATTR G_PREFIX ".apple.system.Security"
#define A_PREFIX                       "com"
#define A_KAUTH_FILESEC_XATTR A_PREFIX ".apple.system.Security"
#define XATTR_APPLE_PREFIX "com.apple."
#endif

namespace Repertory {
extern int g_siaLockFd;
extern int g_siaPrimeLockFd;

E_SIMPLE3(FuseEvent, Debug, true,
  std::string, Function, FUNC, E_STRING,
  std::string, ApiFilePath, AP, E_STRING,
  int, Result, RES, E_FROM_INT32
);

E_SIMPLE1(FuseArgsParsed, Normal, true,
  std::string, Arguments, ARGS, E_STRING
);

#define RAISE_FUSE_EVENT(func, file, ret) \
  if (_config->GetEnableDriveEvents() && (((_config->GetEventLevel() >= FuseEvent::Level) && (ret != 0)) || (_config->GetEventLevel() >= EventLevel::Verbose))) EventSystem::Instance().Raise<FuseEvent>(func, file, ret)

class CFuseDrive final {
  E_CONSUMER();
  public:
    CFuseDrive(IProvider &provider, CConfig &config) :
      _provider(provider),
      _config(config) {
      E_SUBSCRIBE_EXACT(UnmountRequested, [this](const UnmountRequested& e) {
#ifdef __APPLE__
        std::thread([this]() {
          CFuseDrive::Shutdown(_mountPoint);
        }).detach();
#else
        CFuseDrive::Shutdown(_mountPoint);
#endif
      });
    }

  private:
    IProvider &_provider;
    CConfig &_config;
    std::string _mountPoint;

  private:
    static inline void Shutdown(std::string mountPoint) {
#if __APPLE__ || IS_FREEBSD
      const auto unmount = "umount \"" + mountPoint + "\" >/dev/null 2>&1";
#else
      const auto unmount = "fusermount -u \"" + mountPoint + "\" >/dev/null 2>&1";
#endif
      const auto ret = system(&unmount[0]);
      EventSystem::Instance().Raise<UnmountResult>(mountPoint, std::to_string(ret));
    }

  private:
    class CDirectoryIterator final {
      public:
        explicit CDirectoryIterator(const std::string &apiFilePath, IProvider &provider) :
          _apiFilePath(apiFilePath) {
          _result = provider.GetDirectoryItems(apiFilePath, _directoryItemList);
        }

        ~CDirectoryIterator() = default;

      private:
        const std::string _apiFilePath;
        DirectoryItemList _directoryItemList;
        ApiFileError _result = ApiFileError::Success;
        off_t _offset = 0;

      public:
        int FillBuffer(fuse_fill_dir_t fillerFunc, void *buffer) {
          auto ret = 0;
          if (_offset >= 0) {
            std::string itemName;
            switch (_offset) {
            case 0: {
              itemName = ".";
            }
              break;

            case 1: {
              itemName = "..";
            }
              break;

            default: {
              const auto &item = _directoryItemList[_offset - 2];
              itemName = Utils::Path::StripToFileName(item.ApiFilePath);
            }
            }

            if (fillerFunc(buffer, &itemName[0], nullptr, _offset + 1) != 0) {
              ret = -1;
              _offset = -1;
              errno = ENOMEM;
            }
          }

          return ret;
        }

        inline ApiFileError GetResult() const {
          return _result;
        }

        inline void SeekOffset(const off_t &offset) {
          _offset = (((offset >= 0) && (offset < (_directoryItemList.size() + 2))) ? offset : -1);
        }
    };

    class FuseImpl final {
      public:
        static IProvider *_provider;
        static CConfig *_config;
        static std::string *_mountPoint;
        static std::unique_ptr<CConsoleConsumer> _consoleConsumer;
        static std::unique_ptr<CLoggingConsumer> _loggingConsumer;
        static std::unique_ptr<COpenFileTable<OpenFileData>> _openFileTable;
        static std::unique_ptr<CDownloadManager> _downloadManager;
        static std::unique_ptr<CEviction> _eviction;
        static std::unique_ptr<CServer> _server;
        static std::optional<gid_t> _forcedGid;
        static std::optional<uid_t> _forcedUid;
        static std::optional<mode_t> _forcedUMask;
        static bool _consoleEnabled;
        static bool _wasMounted;

      private:
        static ApiFileError CheckAccess(const std::string& apiFilePath, int checkMask) {
          ApiMetaMap meta;
          auto ret = _openFileTable->DeriveItemData(apiFilePath, meta);
          if (ret == ApiFileError::Success) {
            // Always allow root
            if (fuse_get_context()->uid != 0) {
              // Always allow forced user
              if (not _forcedUid.has_value() || (fuse_get_context()->uid != GetCurrentUID())) {
                // Always allow if checking file exists
                if (F_OK != checkMask) {
                  const auto effectiveUid = (_forcedUid.has_value() ? _forcedUid.value() : ParseMetaUID(meta));
                  const auto effectiveGid = (_forcedGid.has_value() ? _forcedGid.value() : ParseMetaGID(meta));

                  // Create file mode
                  mode_t effectiveMode;
                  if (_forcedUMask.has_value()) {
                    effectiveMode = ((S_IRWXU | S_IRWXG | S_IRWXO) & (~_forcedUMask.value()));
                  } else {
                    effectiveMode = ParseMetaMode(meta);
                  }

                  // Create access mask
                  mode_t activeMask = S_IRWXO;
                  if (fuse_get_context()->uid == effectiveUid) {
                    activeMask |= S_IRWXU;
                  }
                  if (fuse_get_context()->gid == effectiveGid) {
                    activeMask |= S_IRWXG;
                  }
                  if (Utils::IsUidMemberOfGroup(fuse_get_context()->uid, effectiveGid)) {
                    activeMask |= S_IRWXG;
                  }

                  // Calculate effective file mode
                  effectiveMode &= activeMask;

                  // Check allow execute
                  if ((checkMask & X_OK) == X_OK) {
                    if ((effectiveMode & (S_IXUSR | S_IXGRP | S_IXOTH)) == 0) {
                      ret = ApiFileError::PermissionDenied;
                    }
                  }
                  if (ret == ApiFileError::Success) {
                    // Check allow write
                    if ((checkMask & W_OK) == W_OK) {
                      if ((effectiveMode & (S_IWUSR | S_IWGRP | S_IWOTH)) == 0) {
                        ret = ApiFileError::AccessDenied;
                      }
                    }
                    if (ret == ApiFileError::Success) {
                      // Check allow read
                      if ((checkMask & R_OK) == R_OK) {
                        if ((effectiveMode & (S_IRUSR | S_IRGRP | S_IROTH)) == 0) {
                          ret = ApiFileError::AccessDenied;
                        }
                      }

                      if ((ret == ApiFileError::Success) && (effectiveMode == 0)) {
                        // Deny access if effective mode is 0
                        ret = ApiFileError::AccessDenied;
                      }
                    }
                  }
                }
              }
            }
          }

          return ret;
        }

        static ApiFileError CheckParentAccess(const std::string& apiFilePath, int mask) {
          auto ret = ApiFileError::Success;

          // Ignore root
          if (apiFilePath != "/") {
            if ((mask & X_OK) == X_OK) {
              for (auto parent = Utils::Path::GetParentDirectory(apiFilePath); (ret == ApiFileError::Success) && not parent.empty(); parent = Utils::Path::GetParentDirectory(parent)) {
                if (((ret = CheckAccess(parent, X_OK)) == ApiFileError::Success) && (parent == "/")) {
                  break;
                }
              }
            }

            if (ret == ApiFileError::Success) {
              mask &= (~X_OK);
              if (mask != 0) {
                ret = CheckAccess(Utils::Path::GetParentDirectory(apiFilePath), mask);
              }
            }
          }

          return ret;
        }

        static inline ApiFileError CheckOwner(const ApiMetaMap &meta) {
          auto ret = ApiFileError::Success;

          // Always allow root
          if ((fuse_get_context()->uid != 0) &&
            // Always allow forced UID
            (not _forcedUid.has_value() || (fuse_get_context()->uid != GetCurrentUID())) &&
            // Check if current uid matches file uid
            (ParseMetaUID(meta) != GetCurrentUID()))  {
            ret = ApiFileError::PermissionDenied;
          }

          return ret;
        }

        static inline gid_t GetCurrentGID() {
          return _forcedGid.has_value() ? _forcedGid.value() : fuse_get_context()->gid;
        }

        static inline uid_t GetCurrentUID() {
          return _forcedUid.has_value() ? _forcedUid.value() : fuse_get_context()->uid;
        }

        static inline void ParseMetaDate(const ApiMetaMap &meta, const std::string& name, struct timespec &ts) {
          const auto t = Utils::String::ToUInt64(meta.at(name));
          ts.tv_nsec = t % NANOS_PER_SECOND;
          ts.tv_sec = t / NANOS_PER_SECOND;
        }
#ifdef __APPLE__
        static inline __uint32_t ParseMetaFlags(const ApiMetaMap& meta) {
          return Utils::String::ToUInt32(meta.at(META_OSXFLAGS));
        }
#endif
        static inline gid_t ParseMetaGID(const ApiMetaMap &meta) {
          return static_cast<gid_t>(Utils::String::ToUInt32(meta.at(META_GID)));
        }

        static inline mode_t ParseMetaMode(const ApiMetaMap &meta) {
          return static_cast<mode_t>(Utils::String::ToUInt32(meta.at(META_MODE)));
        }

        static inline uid_t ParseMetaUID(const ApiMetaMap &meta) {
          return static_cast<uid_t>(Utils::String::ToUInt32(meta.at(META_UID)));
        }

      private:
        static void PopulateStat(const std::string &apiFilePath, const std::uint64_t &fileSize, const ApiMetaMap &meta, const bool &directory, struct stat *st) {
          memset(st, 0, sizeof(struct stat));
          st->st_nlink = (directory ? 2 + _provider->GetDirectoryItemCount(apiFilePath) : 1);
          if (directory) {
            st->st_blocks = 0;
          } else {
            st->st_size = fileSize;
            const auto blockSizeStat = static_cast<std::uint64_t>(512);
            const auto blockSize = static_cast<std::uint64_t>(4096);
            const auto size = Utils::DivideWithCeiling(static_cast<std::uint64_t>(st->st_size), blockSize) * blockSize;
            st->st_blocks = std::max(blockSize / blockSizeStat, Utils::DivideWithCeiling(size, blockSizeStat));
          }
#ifdef __APPLE__
          ParseMetaDate(meta, META_MODIFIED, st->st_mtimespec);
          ParseMetaDate(meta, META_CREATION, st->st_birthtimespec);
          ParseMetaDate(meta, META_CHANGED, st->st_ctimespec);
          ParseMetaDate(meta, META_ACCESSED, st->st_atimespec);
#else
          ParseMetaDate(meta, META_MODIFIED, st->st_mtim);
          ParseMetaDate(meta, META_CREATION, st->st_ctim);
          ParseMetaDate(meta, META_ACCESSED, st->st_atim);
#endif
          st->st_gid = ParseMetaGID(meta);
          st->st_mode = (directory ? S_IFDIR : S_IFREG) | ParseMetaMode(meta);
          st->st_uid = ParseMetaUID(meta);
#ifdef __APPLE__
          st->st_blksize = 0;
#else
          st->st_blksize = 4096;
#endif
#ifdef __APPLE__
          st->st_flags = ParseMetaFlags(meta);
#endif
        }

      public:
        static void TearDown(const int& ret) {
          if (_wasMounted) {
            EventSystem::Instance().Raise<DriveMountResult>(std::to_string(ret));
            EventSystem::Instance().Stop();
            _loggingConsumer.reset();
            _consoleConsumer.reset();
          }
        }

      public:
        static int repertory_access(const char *path, int mask) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          const auto apiFileError = CheckAccess(apiFilePath, mask);

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#ifdef __APPLE__
        static int repertory_chflags(const char *path, uint32_t flags) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          ApiMetaMap meta;
          ApiFileError apiFileError;
          if ((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
            if ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) {
              if ((apiFileError = CheckOwner(meta)) == ApiFileError::Success) {
                apiFileError = _openFileTable->SetItemMeta(apiFilePath, META_OSXFLAGS, Utils::String::FromUInt32(flags));
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#endif
        static int repertory_chmod(const char *path, mode_t mode) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          ApiMetaMap meta;
          ApiFileError apiFileError;
          if (((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) &&
            ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) &&
            ((apiFileError = CheckOwner(meta)) == ApiFileError::Success)) {
            apiFileError = _openFileTable->SetItemMeta(apiFilePath, META_MODE, Utils::String::FromUInt32(mode));
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_chown(const char *path, uid_t uid, gid_t gid) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          ApiMetaMap meta;
          ApiFileError apiFileError;
          if (((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) &&
            ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) &&
            ((apiFileError = CheckOwner(meta)) == ApiFileError::Success)) {
            meta.clear();
            if (uid != -1) {
              meta.insert({META_UID, Utils::String::FromUInt32(uid)});
            }

            if (gid != -1) {
              meta.insert({META_GID, Utils::String::FromUInt32(gid)});
            }

            if (not meta.empty()) {
              apiFileError = _openFileTable->SetItemMeta(apiFilePath, meta);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          fi->fh = static_cast<std::uint64_t>(0);

          const bool directoryOp = ((fi->flags & O_DIRECTORY) == O_DIRECTORY);
          const bool createOp = ((fi->flags & O_CREAT) == O_CREAT);
          const bool truncateOp = ((fi->flags & O_TRUNC) && ((fi->flags & O_WRONLY) || (fi->flags & O_RDWR)));

          std::uint64_t handle = 0;
          auto apiFileError = CheckParentAccess(apiFilePath, X_OK);
          if (apiFileError == ApiFileError::Success) {
            if (createOp) {
              if ((apiFileError = CheckAccess(apiFilePath, W_OK)) == ApiFileError::ItemNotFound) {
                apiFileError = CheckParentAccess(apiFilePath, W_OK);
              }
            } else if ((apiFileError = CheckAccess(apiFilePath, R_OK)) == ApiFileError::ItemNotFound) {
              apiFileError = CheckParentAccess(apiFilePath, R_OK);
            }

            if (apiFileError == ApiFileError::Success) {
              if (createOp && directoryOp) {
                apiFileError = ApiFileError::InvalidOperation;
              } else if (not createOp && not(directoryOp ? _provider->IsDirectory(apiFilePath) : _provider->IsFile(apiFilePath))) {
                apiFileError = (directoryOp ? ApiFileError::DirectoryNotFound : ApiFileError::ItemNotFound);
              }
              if (apiFileError == ApiFileError::Success) {
                if (createOp) {
                  const auto createDate = std::to_string(Utils::GetFileTimeNow());
                  const ApiMetaMap meta = {{META_CREATION,
                                            createDate},
                                           {META_MODIFIED,
                                            createDate},
                                           {META_ACCESSED,
                                            createDate},
#ifdef __APPLE__
                  {META_OSXFLAGS, Utils::String::FromUInt32(fi->flags)},
                  {META_BACKUP,   "0"},
                  {META_CHANGED,  createDate},
#endif
                                           {META_MODE,
                                            Utils::String::FromUInt32(mode)},
                                           {META_UID,
                                            Utils::String::FromUInt32(GetCurrentUID())},
                                           {META_GID,
                                            Utils::String::FromUInt32(GetCurrentGID())}};
                  apiFileError = _provider->CreateFile(apiFilePath, meta);
                  if (apiFileError == ApiFileError::FileExists) {
                    apiFileError = ApiFileError::Success;
                  }
                }

                if ((apiFileError == ApiFileError::Success) && ((apiFileError = _openFileTable->Open(apiFilePath, directoryOp, fi->flags, handle)) == ApiFileError::Success)) {
                  fi->fh = handle;
                  if (truncateOp) {
                    const auto res = repertory_ftruncate(path, 0, fi);
                    if (res != 0) {
                      _openFileTable->Close(handle);
                      fi->fh = 0;
                      errno = std::abs(res);
                      apiFileError = ApiFileError::OSErrorCode;
                    }
                  }
                }
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static void repertory_destroy(void *ptr) {
          EventSystem::Instance().Raise<DriveUnMountPending>(*_mountPoint);
          _server->Stop();
          _downloadManager->Stop();
          Polling::Instance().Stop();
          _eviction->Stop();
          _openFileTable->Stop();
          _provider->Stop();
          _openFileTable.reset();
          _downloadManager.reset();
          _eviction.reset();
          _server.reset();
          ftruncate((_config->GetProviderType() == ProviderType::SiaPrime) ?
                    g_siaPrimeLockFd :
                    g_siaLockFd, 0);
          EventSystem::Instance().Raise<DriveUnMounted>(*_mountPoint);
          _config->SaveConfiguration();
        }

        static int repertory_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;

          FileSystemItem *fileSystemItem = nullptr;
          if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
            if ((apiFileError = _downloadManager->Download(fi->fh, *fileSystemItem)) == ApiFileError::Success) {
              auto nativeFile = CNativeFile::Attach(fileSystemItem->Handle);
#ifdef __APPLE__
              apiFileError = ApiFileError::OSErrorCode;
              fstore_t fstore = {0};
              errno = ENOTSUP;
              if (not(mode & PREALLOCATE)) {
                if (mode & ALLOCATECONTIG) {
                  fstore.fst_flags |= F_ALLOCATECONTIG;
                }

                if (mode & ALLOCATEALL) {
                  fstore.fst_flags |= F_ALLOCATEALL;
                }

                if (mode & ALLOCATEFROMPEOF) {
                  fstore.fst_posmode = F_PEOFPOSMODE;
                } else if (mode & ALLOCATEFROMVOL) {
                  fstore.fst_posmode = F_VOLPOSMODE;
                }

                fstore.fst_offset = offset;
                fstore.fst_length = length;

                if (fcntl(nativeFile->GetHandle(), F_PREALLOCATE, &fstore) >= 0) {
                  apiFileError = ApiFileError::Success;
                }
              }
#else
#ifdef IS_FREEBSD
              if (posix_fallocate(nativeFile->GetHandle(), offset, length) == -1) {
#else
              if (fallocate(nativeFile->GetHandle(), mode, offset, length) == -1) {
#endif
                apiFileError = ApiFileError::OSErrorCode;
              }
#endif
              if ((apiFileError == ApiFileError::Success) && ((offset + length) != fileSystemItem->Size)) {
                fileSystemItem->Changed = true;
                const auto origSize = fileSystemItem->Size;
                Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);
                const auto modified = std::to_string(Utils::GetFileTimeNow());
                _openFileTable->SetItemMeta(apiFilePath, META_MODIFIED, modified);
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_fgetattr(const char *path, struct stat *st, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;

          FileSystemItem *fileSystemItem = nullptr;
          if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
            std::uint64_t fileSize;
            ApiMetaMap meta;
            if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, fileSystemItem->Directory, fileSize, meta)) == ApiFileError::Success) {
              PopulateStat(apiFilePath, fileSize, meta, fileSystemItem->Directory, st);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#ifdef __APPLE__
        static int repertory_fsetattr_x(const char *path, struct setattr_x *attr, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;
          FileSystemItem *fileSystemItem = nullptr;
          if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
            const auto res = repertory_setattr_x(path, attr);
            if (res == 0) {
              apiFileError = ApiFileError::Success;
            } else {
              errno = std::abs(res);
              apiFileError = ApiFileError::OSErrorCode;
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#endif
        static int repertory_fsync(const char *path, int datasync, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;

          FileSystemItem *fileSystemItem = nullptr;
          if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
            apiFileError = ApiFileError::Success;
            if (fileSystemItem->Handle != INVALID_OSHANDLE_VALUE) {
#ifdef __APPLE__
              if ((datasync ? fcntl(fileSystemItem->Handle, F_FULLFSYNC) : fsync(fileSystemItem->Handle)) == -1) {
#else
              if ((datasync ? fdatasync(fileSystemItem->Handle) : fsync(fileSystemItem->Handle)) == -1) {
#endif
                apiFileError = ApiFileError::OSErrorCode;
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_ftruncate(const char *path, off_t size, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;

          FileSystemItem *fileSystemItem = nullptr;
          if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
            if ((apiFileError = _downloadManager->Download(fi->fh, *fileSystemItem)) == ApiFileError::Success) {
              auto nativeFile = CNativeFile::Attach(fileSystemItem->Handle);
              if (nativeFile->Truncate(static_cast<uint64_t>(size))) {
                if (fileSystemItem->Size != size) {
                  fileSystemItem->Changed = true;
                  const auto origSize = fileSystemItem->Size;
                  Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                  GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);
                  const auto modified = std::to_string(Utils::GetFileTimeNow());
                  _openFileTable->SetItemMeta(apiFilePath, META_MODIFIED, modified);
                }
              } else {
                apiFileError = ApiFileError::OSErrorCode;
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_getattr(const char *path, struct stat *st) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          auto apiFileError = CheckParentAccess(apiFilePath, X_OK);
          if (apiFileError == ApiFileError::Success) {
            std::uint64_t fileSize;
            ApiMetaMap meta;
            const auto directory = _provider->IsDirectory(apiFilePath);
            if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, directory, fileSize, meta)) == ApiFileError::Success) {
              PopulateStat(apiFilePath, fileSize, meta, directory, st);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#ifdef __APPLE__
        static int repertory_getxtimes(const char* path, struct timespec *bkuptime, struct timespec *crtime) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          auto apiFileError = CheckParentAccess(apiFilePath, X_OK);
          if (apiFileError == ApiFileError::Success) {
            ApiMetaMap meta;
            if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
              ParseMetaDate(meta, META_CREATION, *crtime);
              ParseMetaDate(meta, META_BACKUP, *bkuptime);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#endif
        static void *repertory_init(struct fuse_conn_info *conn) {
#ifdef __APPLE__
          conn->want |= FUSE_CAP_VOL_RENAME;
          conn->want |= FUSE_CAP_XTIMES;
#endif
          conn->want |= FUSE_CAP_BIG_WRITES;

          auto lockFd = (_config->GetProviderType() == ProviderType::SiaPrime) ?
                        g_siaPrimeLockFd :
                        g_siaLockFd;
          ftruncate(lockFd, 0);
          const auto name = CConfig::GetProviderDisplayName(_config->GetProviderType());
          json data = {
            {"Active", true},
            {"Location", *_mountPoint},
            {"PID", (std::int64_t)getpid()}
          };
          const std::string lockData = data.dump(2);
          pwrite(lockFd, &lockData[0], lockData.size(), 0);

          if (_consoleEnabled) {
            _consoleConsumer = std::make_unique<CConsoleConsumer>();
          }
          _loggingConsumer = std::make_unique<CLoggingConsumer>(_config->GetLogDirectory(), _config->GetEventLevel());
          EventSystem::Instance().Start();
          _wasMounted = true;

          Polling::Instance().Start(_config);
          _downloadManager = std::make_unique<CDownloadManager>(*_config, [](const std::string &path, const std::size_t &size, const std::uint64_t &offset, std::vector<char> &data, const bool& stopRequested) -> ApiFileError {
            return _provider->ReadFileBytes(path, size, offset, data, stopRequested);
          });
          _openFileTable = std::make_unique<COpenFileTable<OpenFileData>>(*_provider, *_config, *_downloadManager);
          _server = std::make_unique<CServer>(*_config, *_provider, *_openFileTable);
          _eviction = std::make_unique<CEviction>(*_provider, *_config, *_openFileTable);
          try {
            _provider->Start([](const std::string &apiFilePath, const std::string &parent, const std::string& source, const bool &directory, const std::uint64_t &createDate, const std::uint64_t& accessDate, const std::uint64_t& modifiedDate, const std::uint64_t& changedDate)->void {
              EventSystem::Instance().Raise<FileSystemItemAdded>(apiFilePath, parent, directory);
              _provider->SetItemMeta(apiFilePath, {{META_CREATION, std::to_string(createDate)},
                                                   {META_MODIFIED, std::to_string(modifiedDate)},
                                                   {META_ACCESSED, std::to_string(accessDate)},
#ifdef __APPLE__
                                                   {META_OSXFLAGS, "0"},
                                                   {META_BACKUP,   "0"},
                                                   {META_CHANGED,  std::to_string(changedDate)},
#endif
                                                   {META_MODE, Utils::String::FromUInt32(directory ? S_IRUSR | S_IWUSR | S_IXUSR : S_IRUSR | S_IWUSR)},
                                                   {META_UID, Utils::String::FromUInt32(getuid())},
                                                   {META_GID, Utils::String::FromUInt32(getgid())}});
              if (not directory) {
                _provider->SetSourcePath(apiFilePath, source);
              }
            }, _openFileTable.get());
            _downloadManager->Start(_openFileTable.get());
            _openFileTable->Start();
            _eviction->Start();
            _server->Start();

            // Force root creation
            ApiMetaMap metaMap{};
            _provider->GetItemMeta("/", metaMap);
            EventSystem::Instance().Raise<DriveMounted>(*_mountPoint);
          } catch (const StartupException &e) {
            EventSystem::Instance().Raise<RepertoryException>(__FUNCTION__, e.what());
            _server->Stop();
            ftruncate(lockFd, 0);
            Polling::Instance().Stop();
            _eviction->Stop();
            _openFileTable->Stop();
            _downloadManager->Stop();
            _provider->Stop();
            EventSystem::Instance().Stop();
            _consoleConsumer.reset();
            _loggingConsumer.reset();
            fuse_exit(fuse_get_context()->fuse);
          }

          return nullptr;
        }

        static int repertory_mkdir(const char *path, mode_t mode) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = CheckParentAccess(apiFilePath, W_OK|X_OK);
          if (apiFileError == ApiFileError::Success) {
            const auto createDate = std::to_string(Utils::GetFileTimeNow());
            const ApiMetaMap meta = {{META_CREATION, createDate},
                                     {META_MODIFIED, createDate},
                                     {META_ACCESSED, createDate},
#ifdef __APPLE__
                                     {META_OSXFLAGS, "0"},
                                     {META_BACKUP,   "0"},
                                     {META_CHANGED,  createDate},
#endif
                                     {META_MODE,     Utils::String::FromUInt32(mode)},
                                     {META_UID,      Utils::String::FromUInt32(GetCurrentUID())},
                                     {META_GID,      Utils::String::FromUInt32(GetCurrentGID())}};
            if (((apiFileError = _provider->CreateDirectory(apiFilePath, meta)) == ApiFileError::Success) && (apiFilePath != "/")) {
              _openFileTable->SetItemMeta(Utils::Path::GetParentApiPath(apiFilePath), META_MODIFIED, createDate);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_open(const char *path, struct fuse_file_info *fi) {
          fi->flags &= (~O_CREAT);
          return repertory_create(path, 0, fi);
        }

        static int repertory_opendir(const char *path, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          auto mask = (O_RDONLY != (fi->flags & O_ACCMODE) ? W_OK : R_OK) | X_OK;
          auto apiFileError = CheckAccess(apiFilePath, mask);
          if (apiFileError == ApiFileError::Success) {
            apiFileError = CheckParentAccess(apiFilePath, mask);
          }

          if (apiFileError == ApiFileError::Success) {
            auto* directoryIterator = new CDirectoryIterator(apiFilePath, *_provider);
            if ((apiFileError = directoryIterator->GetResult()) == ApiFileError::Success) {
              fi->fh = reinterpret_cast<std::uint64_t>(directoryIterator);
            } else {
              delete directoryIterator;
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_read(const char *path, char *buffer, size_t readSize, off_t readOffset, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          auto apiFileError = ApiFileError::ItemNotFound;

          FileSystemItem *fileSystemItem = nullptr;
          auto bytesRead = 0;
          if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
            std::vector<char> data;
            apiFileError = _downloadManager->ReadBytes(fi->fh, *fileSystemItem, readSize, static_cast<std::uint64_t>(readOffset), data);
            bytesRead = static_cast<int>(data.size());
            memcpy(buffer, &data[0], data.size());
            data.clear();
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          if (ret != 0) {
            RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          }
          return ((ret == 0) ? bytesRead : ret);
        }

        static int repertory_readdir(const char *path, void *buf, fuse_fill_dir_t fuseFillDir, off_t offset, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;

          auto *directoryIterator = reinterpret_cast<CDirectoryIterator *>(fi->fh);
          if (directoryIterator &&
            ((apiFileError = CheckAccess(apiFilePath, X_OK)) == ApiFileError::Success)) {
            directoryIterator->SeekOffset(offset);
            apiFileError = (directoryIterator->FillBuffer(fuseFillDir, buf) == 0) ? ApiFileError::Success : ApiFileError::OSErrorCode;
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_release(const char *path, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          _openFileTable->Close(fi->fh);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, 0);
          return 0;
        }

        static int repertory_releasedir(const char *path, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::InvalidOSHandle;

          auto *directoryIterator = reinterpret_cast<CDirectoryIterator *>(fi->fh);
          if (directoryIterator) {
            delete directoryIterator;
            apiFileError = ApiFileError::Success;
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_rename(const char *from, const char *to) {
          const auto fromApiPath = Utils::Path::CreateApiPath(from);
          const auto toApiPath = Utils::Path::CreateApiPath(to);

          auto apiFileError = CheckParentAccess(toApiPath, W_OK|X_OK);
          if ((apiFileError == ApiFileError::Success) &&
            ((apiFileError = CheckParentAccess(fromApiPath, W_OK|X_OK)) == ApiFileError::Success)) {
            apiFileError = ApiFileError::ItemNotFound;
            if (_provider->IsFile(fromApiPath)) {
              apiFileError = _openFileTable->RenameFile(fromApiPath, toApiPath);
            } else if (_provider->IsDirectory(fromApiPath)) {
              apiFileError = _openFileTable->RenameDirectory(fromApiPath, toApiPath);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, fromApiPath + "|" + toApiPath, ret);
          return ret;
        }

        static int repertory_rmdir(const char *path) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = CheckParentAccess(apiFilePath, W_OK|X_OK);
          if (apiFileError == ApiFileError::Success) {
            apiFileError = _provider->RemoveDirectory(apiFilePath);
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#ifdef HAS_SETXATTR
#ifdef __APPLE__
        static int repertory_getxattr(const char *path, const char *name, char *value, size_t size, uint32_t position) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::Success;
          std::string attrName = name ? name : "";
          if (attrName == A_KAUTH_FILESEC_XATTR) {
            char newName[MAXPATHLEN] = {0};
            memcpy(newName, A_KAUTH_FILESEC_XATTR, sizeof(A_KAUTH_FILESEC_XATTR));
            memcpy(newName, G_PREFIX, sizeof(G_PREFIX) - 1);
            attrName = newName;
          } else if (attrName.empty() || ((attrName != XATTR_RESOURCEFORK_NAME) && (position != 0))) {
            apiFileError = ApiFileError::XAttrOSXInvalid;
          }

          int retSize = 0;
          if (apiFileError == ApiFileError::Success) {
            if ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) {
              ApiMetaMap meta;
              if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
                apiFileError = ApiFileError::XAttrNotFound;
                if (meta.find(attrName) != meta.end()) {
                  const auto data = macaron::Base64::Decode(meta[attrName]);
                  if (position < data.size()) {
                    apiFileError = ApiFileError::Success;
                    retSize = static_cast<int>(data.size() - position);
                    if (size) {
                      apiFileError = ApiFileError::XAttrBufferSmall;
                      if (size >= (data.size() - position)) {
                        memcpy(value, &data[position], data.size() - position);
                        apiFileError = ApiFileError::Success;
                      }
                    }
                  }
                }
              }
            }
          }

          const auto ret = (apiFileError == ApiFileError::Success) ? retSize : Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#else
        static int repertory_getxattr(const char *path, const char *name, char *value, size_t size) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          int retSize = 0;
          auto apiFileError = CheckParentAccess(apiFilePath, X_OK);
          if (apiFileError == ApiFileError::Success) {
            ApiMetaMap meta;
            if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
              apiFileError = ApiFileError::XAttrNotFound;
              if (meta.find(name) != meta.end()) {
                apiFileError = ApiFileError::Success;
                const auto data = macaron::Base64::Decode(meta[name]);
                retSize = static_cast<int>(data.size());
                if (size) {
                  apiFileError = ApiFileError::XAttrBufferSmall;
                  if (size >= data.size()) {
                    memcpy(value, &data[0], data.size());
                    apiFileError = ApiFileError::Success;
                  }
                }
              }
            }
          }

          const auto ret = (apiFileError == ApiFileError::Success) ? retSize : Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, name ? std::string(name) + ":" + apiFilePath : apiFilePath, ret);
          return ret;
        }
#endif
        static int repertory_listxattr(const char *path, char *buffer, size_t size) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          const auto checkSize = (size == 0);
          int requiredSize = 0;

          auto apiFileError = CheckParentAccess(apiFilePath, X_OK);
          if (apiFileError == ApiFileError::Success) {
            ApiMetaMap meta;
            if ((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
              for (const auto& kv : meta) {
                if (std::find(META_USED_NAMES.begin(), META_USED_NAMES.end(), kv.first) == META_USED_NAMES.end()) {
                  auto attrName = kv.first;
#ifdef __APPLE__
                  if (attrName != G_KAUTH_FILESEC_XATTR) {
#endif
                    const auto nameSize = attrName.size();
                    if (size >= nameSize) {
                      strncpy(&buffer[requiredSize], &attrName[0], nameSize);
                      size -= nameSize;
                    } else {
                      apiFileError = ApiFileError::XAttrBufferSmall;
                    }
                    requiredSize += nameSize;
#ifdef __APPLE__
                  }
#endif
                }
              }
            }
          }

          const auto ret =
            ((apiFileError == ApiFileError::Success) ||
             ((apiFileError == ApiFileError::XAttrBufferSmall) && checkSize)) ?
            requiredSize :
            Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_removexattr(const char *path, const char *name) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          std::string attrName = name ? name : "";
          auto apiFileError = ApiFileError::Success;
#ifdef __APPLE__
          if (attrName == A_KAUTH_FILESEC_XATTR) {
            char newName[MAXPATHLEN];
            memcpy(newName, A_KAUTH_FILESEC_XATTR, sizeof(A_KAUTH_FILESEC_XATTR));
            memcpy(newName, G_PREFIX, sizeof(G_PREFIX) - 1);
            attrName = newName;
          } else if (attrName.empty()) {
            apiFileError = ApiFileError::XAttrOSXInvalid;
          }
#endif
          if (apiFileError == ApiFileError::Success) {
            ApiMetaMap meta;
            if (((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) &&
              ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) &&
              ((apiFileError = CheckOwner(meta)) == ApiFileError::Success)) {
              apiFileError = ApiFileError::XAttrNotFound;
              if (meta.find(name) != meta.end()) {
                apiFileError = _openFileTable->RemoveXAttrMeta(apiFilePath, attrName);
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, name ? std::string(name) + ":" + apiFilePath : apiFilePath, ret);
          return ret;
        }
#ifdef __APPLE__
        static int repertory_setxattr(const char *path, const char *name, const char *value, size_t size, int flags, uint32_t position) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          std::string attrName = name ? name : "";
          auto apiFileError = ApiFileError::Success;
          if (attrName == A_KAUTH_FILESEC_XATTR) {
            char newName[MAXPATHLEN] = {0};
            memcpy(newName, A_KAUTH_FILESEC_XATTR, sizeof(A_KAUTH_FILESEC_XATTR));
            memcpy(newName, G_PREFIX, sizeof(G_PREFIX) - 1);
            attrName = newName;
          } else if (attrName.empty() || ((attrName != XATTR_RESOURCEFORK_NAME) && (position != 0))) {
            apiFileError = ApiFileError::XAttrOSXInvalid;
          }

          if (apiFileError == ApiFileError::Success) {
            apiFileError = ApiFileError::XAttrTooBig;
            if (attrName.size() <= XATTR_MAXNAMELEN) {
              ApiMetaMap meta;
              apiFileError = ApiFileError::XAttrInvalidNamespace;

              if (not(Utils::String::Contains(attrName, " .") || Utils::String::Contains(attrName, ". "))) {
                if ((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
                  if ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) {
                    if ((apiFileError = CheckOwner(meta)) == ApiFileError::Success) {
                      apiFileError = ApiFileError::Success;
                      if (flags == XATTR_CREATE) {
                        if (meta.find(attrName) != meta.end()) {
                          apiFileError = ApiFileError::XAttrExists;
                        }
                      } else if (flags == XATTR_REPLACE) {
                        if (meta.find(attrName) == meta.end()) {
                          apiFileError = ApiFileError::XAttrNotFound;
                        }
                      }

                      if (apiFileError == ApiFileError::Success) {
                        const auto attrValue = macaron::Base64::Encode(value, size);
                        apiFileError = _openFileTable->SetItemMeta(apiFilePath, attrName, attrValue);
                      }
                    }
                  }
                }
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#else
        static int repertory_setxattr(const char *path, const char *name, const char *value, size_t size, int flags) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          const std::string attrName(name ? name : "");
          const auto attrNamespace = Utils::String::Contains(attrName, ".") ? Utils::String::Split(attrName, '.', false)[0] : "";
          auto apiFileError = ApiFileError::XAttrTooBig;
          if ((attrName.size() <= XATTR_NAME_MAX) && (size <= XATTR_SIZE_MAX)) {
            ApiMetaMap meta;
            apiFileError = ApiFileError::XAttrInvalidNamespace;

            if (not(Utils::String::Contains(attrName, " .") || Utils::String::Contains(attrName, ". ")) &&
              std::find(Utils::AttributeNamespaces.begin(), Utils::AttributeNamespaces.end(), attrNamespace) != Utils::AttributeNamespaces.end()) {
              if (((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) &&
                ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) &&
                ((apiFileError = CheckOwner(meta)) == ApiFileError::Success)) {
                apiFileError = ApiFileError::Success;
                if (flags == XATTR_CREATE) {
                  if (meta.find(attrName) != meta.end()) {
                    apiFileError = ApiFileError::XAttrExists;
                  }
                } else if (flags == XATTR_REPLACE) {
                  if (meta.find(attrName) == meta.end()) {
                    apiFileError = ApiFileError::XAttrNotFound;
                  }
                }

                if (apiFileError == ApiFileError::Success) {
                  const auto attrValue = macaron::Base64::Encode(value, size);
                  apiFileError = _openFileTable->SetItemMeta(apiFilePath, attrName, attrValue);
                }
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, name ? std::string(name) + ":" + apiFilePath : apiFilePath, ret);
          return ret;
        }
#endif
#endif
#ifdef __APPLE__
        static int repertory_setattr_x(const char *path, struct setattr_x *attr) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::ItemNotFound;

          if (_provider->IsFile(apiFilePath) || _provider->IsDirectory(apiFilePath)) {
            apiFileError = ApiFileError::Success;

            int res = 0;
            if (SETATTR_WANTS_MODE(attr)) {
              res = repertory_chmod(path, attr->mode);
            }

            if (res == 0) {
              uid_t uid = -1;
              if (SETATTR_WANTS_UID(attr)) {
                uid = attr->uid;
              }

              gid_t gid = -1;
              if (SETATTR_WANTS_GID(attr)) {
                gid = attr->gid;
              }

              if ((uid != -1) || (gid != -1)) {
                res = repertory_chown(path, uid, gid);
              }

              if (res == 0) {
                if (SETATTR_WANTS_SIZE(attr)) {
                  res = repertory_truncate(path, attr->size);
                }

                if (res == 0) {
                  if (SETATTR_WANTS_MODTIME(attr)) {
                    struct timespec ts[2];
                    if (SETATTR_WANTS_ACCTIME(attr)) {
                      ts[0].tv_sec = attr->acctime.tv_sec;
                      ts[0].tv_nsec = attr->acctime.tv_nsec;
                    } else {
                      struct timeval tv{};
                      gettimeofday(&tv, NULL);
                      ts[0].tv_sec = tv.tv_sec;
                      ts[0].tv_nsec = tv.tv_usec * 1000;
                    }
                    ts[1].tv_sec = attr->modtime.tv_sec;
                    ts[1].tv_nsec = attr->modtime.tv_nsec;
                    res = repertory_utimens(path, ts);
                  }

                  if (res == 0) {
                    if (SETATTR_WANTS_CRTIME(attr)) {
                      res = repertory_setcrtime(path, &attr->crtime);
                    }

                    if (res == 0) {
                      if (SETATTR_WANTS_CHGTIME(attr)) {
                        res = repertory_setchgtime(path, &attr->chgtime);
                      }

                      if (res == 0) {
                        if (SETATTR_WANTS_BKUPTIME(attr)) {
                          res = repertory_setbkuptime(path, &attr->bkuptime);
                        }

                        if (res == 0) {
                          if (SETATTR_WANTS_FLAGS(attr)) {
                            res = repertory_chflags(path, attr->flags);
                          }
                        }
                      }
                    }
                  }
                }
              }
            }

            if (res != 0) {
              apiFileError = ApiFileError::OSErrorCode;
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_setbkuptime(const char *path, const struct timespec *bkuptime) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          ApiMetaMap meta;
          ApiFileError apiFileError;
          if ((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
            if ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) {
              if ((apiFileError = CheckOwner(meta)) == ApiFileError::Success) {
                const auto nanos = bkuptime->tv_nsec + (bkuptime->tv_nsec * NANOS_PER_SECOND);
                apiFileError = _openFileTable->SetItemMeta(apiFilePath, META_BACKUP, Utils::String::FromUInt64(nanos));
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_setchgtime(const char *path, const struct timespec *chgtime) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          ApiMetaMap meta;
          ApiFileError apiFileError;
          if ((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
            if ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) {
              if ((apiFileError = CheckOwner(meta)) == ApiFileError::Success) {
                const auto nanos = chgtime->tv_nsec + (chgtime->tv_nsec * NANOS_PER_SECOND);
                apiFileError = _openFileTable->SetItemMeta(apiFilePath, META_CHANGED, Utils::String::FromUInt64(nanos));
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_setcrtime(const char *path, const struct timespec *crtime) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          ApiMetaMap meta;
          ApiFileError apiFileError;
          if ((apiFileError =_openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) {
            if ((apiFileError = CheckParentAccess(apiFilePath, X_OK)) == ApiFileError::Success) {
              if ((apiFileError = CheckOwner(meta)) == ApiFileError::Success) {
                const auto nanos = crtime->tv_nsec + (crtime->tv_nsec * NANOS_PER_SECOND);
                apiFileError = _openFileTable->SetItemMeta(apiFilePath, META_CREATION, Utils::String::FromUInt64(nanos));
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_setvolname(const char *volname) {
          (void)volname;
          return 0;
        }

        static int repertory_statfs_x(const char *path, struct statfs *stbuf) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          auto ret = statfs(&_config->GetCacheDirectory()[0], stbuf);
          if (ret == 0) {
            const auto totalBytes = _provider->GetTotalDriveSpace();
            const auto totalUsed = _provider->GetUsedDriveSpace();
            const auto usedBlocks = Utils::DivideWithCeiling(totalUsed, static_cast<std::uint64_t>(stbuf->f_bsize));
            stbuf->f_blocks = Utils::DivideWithCeiling(totalBytes, static_cast<std::uint64_t>(stbuf->f_bsize));
            stbuf->f_bavail = stbuf->f_bfree = stbuf->f_blocks ? (stbuf->f_blocks - usedBlocks) : 0;
            stbuf->f_files = 4294967295;
            stbuf->f_ffree = stbuf->f_files - _provider->GetTotalItemCount();
            stbuf->f_owner = getuid();
            strncpy(&stbuf->f_mntonname[0], _mountPoint->c_str(), MNAMELEN);
            strncpy(&stbuf->f_mntfromname[0], ("repertory" + CConfig::GetProviderName(_config->GetProviderType())).c_str(), MNAMELEN);
          } else {
            ret = -errno;
          }

          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#else
        static int repertory_statfs(const char *path, struct statvfs *stbuf) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          auto ret = statvfs(&_config->GetCacheDirectory()[0], stbuf);
          if (ret == 0) {
            const auto totalBytes = _provider->GetTotalDriveSpace();
            const auto totalUsed = _provider->GetUsedDriveSpace();
            const auto usedBlocks = Utils::DivideWithCeiling(totalUsed, static_cast<std::uint64_t>(stbuf->f_frsize));
            stbuf->f_blocks = Utils::DivideWithCeiling(totalBytes, static_cast<std::uint64_t>(stbuf->f_frsize));
            stbuf->f_bavail = stbuf->f_bfree = stbuf->f_blocks ? (stbuf->f_blocks - usedBlocks) : 0;
            stbuf->f_ffree = stbuf->f_files - _provider->GetTotalItemCount();
          } else {
            ret = -errno;
          }

          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }
#endif
        static int repertory_truncate(const char *path, off_t size) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = CheckParentAccess(apiFilePath, X_OK);
          if ((apiFileError == ApiFileError::Success) &&
            ((apiFileError = CheckAccess(apiFilePath, W_OK)) == ApiFileError::Success)) {
            OpenFileData openFileData = O_RDWR;
            std::uint64_t handle = 0;
            if ((apiFileError = _openFileTable->Open(apiFilePath, false, openFileData, handle)) == ApiFileError::Success) {
              FileSystemItem *fileSystemItem = nullptr;
              if (_openFileTable->GetOpenFile(handle, fileSystemItem)) {
                if ((apiFileError = _downloadManager->Download(handle, *fileSystemItem)) == ApiFileError::Success) {
                  auto nativeFile = CNativeFile::Attach(fileSystemItem->Handle);
                  if (nativeFile->Truncate(static_cast<uint64_t>(size))) {
                    if (fileSystemItem->Size != size) {
                      fileSystemItem->Changed = true;
                      const auto origSize = fileSystemItem->Size;
                      Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                      GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);
                      const auto modified = std::to_string(Utils::GetFileTimeNow());
                      _openFileTable->SetItemMeta(apiFilePath, META_MODIFIED, modified);
                    }
                  } else {
                    apiFileError = ApiFileError::OSErrorCode;
                  }
                }
              } else {
                apiFileError = ApiFileError::ItemNotFound;
              }
              _openFileTable->Close(handle);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_unlink(const char *path) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          auto apiFileError = ApiFileError::DirectoryExists;
          if (not _provider->IsDirectory(apiFilePath) &&
            ((apiFileError = CheckParentAccess(apiFilePath, W_OK|X_OK)) == ApiFileError::Success)) {
            apiFileError = _openFileTable->RemoveFile(apiFilePath);
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_utimens(const char *path, const struct timespec tv[2]) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);
          ApiMetaMap meta;
          ApiFileError apiFileError;
          if (((apiFileError = _openFileTable->DeriveItemData(apiFilePath, meta)) == ApiFileError::Success) &&
            ((apiFileError = CheckOwner(meta)) == ApiFileError::Success)) {
            meta.clear();
            if (not tv || (tv[0].tv_nsec == UTIME_NOW)) {
              meta.insert({META_ACCESSED, std::to_string(Utils::GetFileTimeNow())});
            } else if (tv[0].tv_nsec != UTIME_OMIT) {
              const auto val = tv[0].tv_nsec + (tv[0].tv_sec * NANOS_PER_SECOND);
              meta.insert({META_ACCESSED, std::to_string(val)});
            }

            if (not tv || (tv[1].tv_nsec == UTIME_NOW)) {
              meta.insert({META_MODIFIED, std::to_string(Utils::GetFileTimeNow())});
            } else if (tv[1].tv_nsec != UTIME_OMIT) {
              const auto val = tv[1].tv_nsec + (tv[1].tv_sec * NANOS_PER_SECOND);
              meta.insert({META_MODIFIED, std::to_string(val)});
            }

            if (not meta.empty()) {
              apiFileError = _openFileTable->SetItemMeta(apiFilePath, meta);
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          return ret;
        }

        static int repertory_write(const char *path, const char *buffer, size_t writeSize, off_t writeOffset, struct fuse_file_info *fi) {
          const auto apiFilePath = Utils::Path::CreateApiPath(path);

          std::size_t bytesWritten = 0;
          auto apiFileError = ApiFileError::Success;
          if (writeSize > 0) {
            apiFileError = ApiFileError::ItemNotFound;

            FileSystemItem *fileSystemItem = nullptr;
            if (_openFileTable->GetOpenFile(fi->fh, fileSystemItem)) {
              if (Utils::File::IsFile(fileSystemItem->SourceFilePath)) {
                std::uint64_t fileSize;
                if (Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSize)) {
                  if (fileSize == fileSystemItem->Size) {
                    apiFileError = ApiFileError::Success;
                  } else {
                    apiFileError = _downloadManager->Download(fi->fh, *fileSystemItem);
                  }
                } else {
                  apiFileError = ApiFileError::OSErrorCode;
                }
              } else {
                apiFileError = _downloadManager->Download(fi->fh, *fileSystemItem);
              }

              if (apiFileError == ApiFileError::Success) {
                NativeFilePtr nativeFile;
                if ((apiFileError = Utils::File::AssignAndGetNativeFile(*fileSystemItem, nativeFile)) == ApiFileError::Success) {
                  if (nativeFile->WriteBytes(buffer, writeSize, static_cast<const std::uint64_t>(writeOffset), bytesWritten)) {
                    fileSystemItem->Changed = true;
                    const auto origSize = fileSystemItem->Size;
                    Utils::File::GetSize(fileSystemItem->SourceFilePath, fileSystemItem->Size);
                    GlobalData::Instance().UpdateUsedSpace(origSize, fileSystemItem->Size, false);
                    const auto modified = std::to_string(Utils::GetFileTimeNow());
                    _openFileTable->SetItemMeta(apiFilePath, META_MODIFIED, modified);
                  } else {
                    apiFileError = ApiFileError::OSErrorCode;
                  }
                }
              }
            }
          }

          const auto ret = Utils::TranslateApiFileError(apiFileError);
          if (ret != 0) {
            RAISE_FUSE_EVENT(__FUNCTION__, apiFilePath, ret);
          }
          return ((ret == 0) ? static_cast<int>(bytesWritten) : ret);
        }
    };

  private:
    struct fuse_operations _fuseOps{
      .getattr            = FuseImpl::repertory_getattr,
      .readlink           = nullptr, //int (*readlink) (const char *, char *, size_t);
      .getdir             = nullptr, //int (*getdir) (const char *, fuse_dirh_t, fuse_dirfil_t);
      .mknod              = nullptr, //int (*mknod) (const char *, mode_t, dev_t);
      .mkdir              = FuseImpl::repertory_mkdir,
      .unlink             = FuseImpl::repertory_unlink,
      .rmdir              = FuseImpl::repertory_rmdir,
      .symlink            = nullptr, //int (*symlink) (const char *, const char *);
      .rename             = FuseImpl::repertory_rename,
      .link               = nullptr, //int (*link) (const char *, const char *);
      .chmod              = FuseImpl::repertory_chmod,
      .chown              = FuseImpl::repertory_chown,
      .truncate           = FuseImpl::repertory_truncate,
      .utime              = nullptr, //int (*utime) (const char *, struct utimbuf *);
      .open               = FuseImpl::repertory_open,
      .read               = FuseImpl::repertory_read,
      .write              = FuseImpl::repertory_write,
#ifdef __APPLE__
      .statfs             = nullptr,
#else
      .statfs             = FuseImpl::repertory_statfs,
#endif
      .flush              = nullptr, //int (*flush) (const char *, struct fuse_file_info *);
      .release            = FuseImpl::repertory_release,
      .fsync              = FuseImpl::repertory_fsync,
#if HAS_SETXATTR
      .setxattr           = FuseImpl::repertory_setxattr,
      .getxattr           = FuseImpl::repertory_getxattr,
      .listxattr          = FuseImpl::repertory_listxattr,
      .removexattr        = FuseImpl::repertory_removexattr,
#else
      .setxattr           = nullptr,
      .getxattr           = nullptr,
      .listxattr          = nullptr,
      .removexattr        = nullptr,
#endif
      .opendir            = FuseImpl::repertory_opendir,
      .readdir            = FuseImpl::repertory_readdir,
      .releasedir         = FuseImpl::repertory_releasedir,
      .fsyncdir           = nullptr, //int (*fsyncdir) (const char *, int, struct fuse_file_info *);
      .init               = FuseImpl::repertory_init,
      .destroy            = FuseImpl::repertory_destroy,
      .access             = FuseImpl::repertory_access,
      .create             = FuseImpl::repertory_create,
      .ftruncate          = FuseImpl::repertory_ftruncate,
      .fgetattr           = FuseImpl::repertory_fgetattr,
      .lock               = nullptr, //int (*lock) (const char *, struct fuse_file_info *, int cmd, struct flock *);
      .utimens            = FuseImpl::repertory_utimens,
      .bmap               = nullptr, //int (*bmap) (const char *, size_t blocksize, uint64_t *idx);
      .flag_nullpath_ok   = 0,
      .flag_nopath        = 0,
      .flag_utime_omit_ok = 1,
      .flag_reserved      = 0,
      .ioctl              = nullptr, //int (*ioctl) (const char *, int cmd, void *arg, struct fuse_file_info *, unsigned int flags, void *data);
      .poll               = nullptr, //int (*poll) (const char *, struct fuse_file_info *, struct fuse_pollhandle *ph, unsigned *reventsp);
      .write_buf          = nullptr, //int (*write_buf) (const char *, struct fuse_bufvec *buf, off_t off, struct fuse_file_info *);
      .read_buf           = nullptr, //int (*read_buf) (const char *, struct fuse_bufvec **bufp, size_t size, off_t off, struct fuse_file_info *);
      .flock              = nullptr, //int (*flock) (const char *, struct fuse_file_info *, int op);
      .fallocate          = FuseImpl::repertory_fallocate
    };

  public:
    int Main(std::vector<std::string> driveArgs) {
      FuseImpl::_provider = &_provider;
      FuseImpl::_config = &_config;
      FuseImpl::_mountPoint = &_mountPoint;

#ifdef __APPLE__
      _fuseOps.chflags      = FuseImpl::repertory_chflags;
      _fuseOps.fsetattr_x   = FuseImpl::repertory_fsetattr_x;
      _fuseOps.getxtimes    = FuseImpl::repertory_getxtimes;
      _fuseOps.setattr_x    = FuseImpl::repertory_setattr_x;
      _fuseOps.setbkuptime  = FuseImpl::repertory_setbkuptime;
      _fuseOps.setchgtime   = FuseImpl::repertory_setchgtime;
      _fuseOps.setcrtime    = FuseImpl::repertory_setcrtime;
      _fuseOps.setvolname   = FuseImpl::repertory_setvolname;
      _fuseOps.statfs_x     = FuseImpl::repertory_statfs_x;
#endif
      auto forceNoConsole = false;
      for (int i = 1; !forceNoConsole && (i < driveArgs.size()); i++) {
        if (driveArgs[i] == "-nc") {
          forceNoConsole = true;
        }
      }
      Utils::RemoveElement(driveArgs, "-nc");

      for (int i = 1; i < driveArgs.size(); i++) {
        if (driveArgs[i] == "-f") {
          FuseImpl::_consoleEnabled = not forceNoConsole;
        } else if (driveArgs[i].find("-o") == 0) {
          std::string options = "";
          if (driveArgs[i].size() == 2) {
            if ((i + 1) < driveArgs.size()) {
              options = driveArgs[++i];
            }
          } else {
            options = driveArgs[i].substr(2);
          }

          const auto optionList = Utils::String::Split(options, ',');
          for (const auto &option : optionList) {
            if (option.find("gid") == 0) {
              const auto parts = Utils::String::Split(option, '=');
              if (parts.size() == 2) {
                auto gid = getgrnam(parts[1].c_str());
                if (not gid) {
                  gid = getgrgid(Utils::String::ToUInt32(parts[1]));
                }
                if ((getgid() != 0) && (gid->gr_gid == 0)) {
                  std::cerr << "'gid=0' requires running as root" << std::endl;
                  return -1;
                } else {
                  FuseImpl::_forcedGid = gid->gr_gid;
                }
              }
            } else if (option.find("uid") == 0) {
              const auto parts = Utils::String::Split(option, '=');
              if (parts.size() == 2) {
                auto uid = getpwnam(parts[1].c_str());
                if (not uid) {
                  uid = getpwuid(Utils::String::ToUInt32(parts[1]));
                }
                if ((getuid() != 0) && (uid->pw_uid == 0)) {
                  std::cerr << "'uid=0' requires running as root" << std::endl;
                  return -1;
                } else {
                  FuseImpl::_forcedUid = uid->pw_uid;
                }
              }
            } else if (option.find("umask") == 0) {
              const auto parts = Utils::String::Split(option, '=');
              if (parts.size() == 2) {
                try {
                  if (not std::regex_match(parts[1], std::regex("[0-9]+"))) {
                    throw "invalid syntax";
                  } else {
                    FuseImpl::_forcedUMask = Utils::String::ToUInt32(parts[1]);
                  }
                } catch (...) {
                  std::cerr << ("'" + option + "' invalid syntax") << std::endl;
                  return -1;
                }
              }
            }
          }
        }
      }

      std::vector<const char *> fuseArgv(driveArgs.size());
      for (size_t i = 0; i < driveArgs.size(); i++) {
        fuseArgv[i] = driveArgs[i].c_str();
      }

      struct fuse_args args = FUSE_ARGS_INIT(static_cast<int>(fuseArgv.size()), (char **) &fuseArgv[0]);
      char* mountPoint = nullptr;
      fuse_parse_cmdline(&args, &mountPoint, nullptr, nullptr);
      if (mountPoint) {
        _mountPoint = mountPoint;
        free(mountPoint);
      }

      std::string argString;
      for (const auto* arg : fuseArgv) {
        if (argString.empty()) {
          argString += arg;
        } else {
          argString += (" " + std::string(arg));
        }
      }

      EventSystem::Instance().Raise<FuseArgsParsed>(argString);

      umask(0);
      const auto ret = fuse_main(static_cast<int>(fuseArgv.size()), (char **) &fuseArgv[0], &_fuseOps, &_mountPoint);
      FuseImpl::TearDown(ret);
      return ret;
    }

    static inline void DisplayOptions(int argc, char *argv[]) {
      struct fuse_operations fuseOps{};
      fuse_main(argc, argv, &fuseOps, nullptr);
      std::cout << std::endl;
    }

    static inline void DisplayVersionInfo(int argc, char *argv[]) {
      struct fuse_operations fuseOps{};
      fuse_main(argc, argv, &fuseOps, nullptr);
    }
};

IProvider *CFuseDrive::FuseImpl::_provider = nullptr;
CConfig *CFuseDrive::FuseImpl::_config = nullptr;
std::string* CFuseDrive::FuseImpl::_mountPoint = nullptr;
std::unique_ptr<CLoggingConsumer> CFuseDrive::FuseImpl::_loggingConsumer;
std::unique_ptr<CConsoleConsumer> CFuseDrive::FuseImpl::_consoleConsumer;
std::unique_ptr<COpenFileTable<OpenFileData>> CFuseDrive::FuseImpl::_openFileTable;
std::unique_ptr<CDownloadManager> CFuseDrive::FuseImpl::_downloadManager;
std::unique_ptr<CEviction> CFuseDrive::FuseImpl::_eviction;
std::unique_ptr<CServer> CFuseDrive::FuseImpl::_server;
std::optional<gid_t> CFuseDrive::FuseImpl::_forcedGid;
std::optional<uid_t> CFuseDrive::FuseImpl::_forcedUid;
std::optional<mode_t> CFuseDrive::FuseImpl::_forcedUMask;
bool CFuseDrive::FuseImpl::_consoleEnabled = false;
bool CFuseDrive::FuseImpl::_wasMounted = false;
}

#endif
#endif //REPERTORY_FUSEDRIVE_H
