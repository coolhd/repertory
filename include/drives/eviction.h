#ifndef REPERTORY_EVICTION_H
#define REPERTORY_EVICTION_H

#include <common.h>
#include <drives/iopenfiletable.h>
#include <providers/iprovider.h>

namespace Repertory {
class CEviction final {
  public:
    CEviction(IProvider &provider, CConfig& config, IOpenFileTable& openFileTable) :
      _provider(provider),
      _config(config),
      _openFileTable(openFileTable) {
    }

    ~CEviction() = default;

  private:
    IProvider &_provider;
    CConfig& _config;
    IOpenFileTable& _openFileTable;
    bool _stopRequested = false;
    std::mutex _startStopMutex;
    std::condition_variable _stopCondition;
    std::unique_ptr<std::thread> _evictionThread;
    std::mutex _evictionMutex;

  private:
    void CheckItemsThread() {
      while (not _stopRequested) {
        auto shouldEvict = true;
        auto evicted = false;

        // Handle maximum cache size eviction
        auto usedBytes = GlobalData::Instance().GetUsedCacheSpace();
        if (_config.GetEnableMaxCacheSize()) {
          shouldEvict = (usedBytes > _config.GetMaxCacheSizeBytes());
        }

        // Evict all items if minimum redundancy eviction is enabled; otherwise, evict
        //  until required space is reclaimed.
        if (shouldEvict) {
          auto files = Utils::File::GetDirectoryFiles(_config.GetCacheDirectory(), true);
          while (shouldEvict && not files.empty()) {
            // Ensure enough time has passed since last file modification
            if (CheckModifiedRequirement(files.front())) {
              FileSystemItem fileSystemItem{};
              if (_provider.GetFileSystemItemFromSourcePath(files.front(), fileSystemItem) == ApiFileError::Success) {
                ApiFile apiFile{};
                if ((_provider.GetFile(fileSystemItem.ApiFilePath, apiFile) == ApiError::Success)) {
                  // Ensure minimum file redundancy has been met or source path is not being used for local recovery
                  const auto differentSource = CreateLookupPath(apiFile.SourceFilePath) != CreateLookupPath(fileSystemItem.SourceFilePath);
                  if (apiFile.Recoverable && ((fileSystemItem.Redundancy >= _config.GetMinimumRedundancy()) || differentSource)) {
                    std::uint64_t fileSize;
                    if (Utils::File::GetSize(fileSystemItem.SourceFilePath, fileSize)) {
                      // Only evict files that are > 0 and match expected size
                      if ((fileSize > 0) && (fileSize == fileSystemItem.Size)) {
                        // Try to evict file
                        if ((evicted = _openFileTable.EvictFile(fileSystemItem.ApiFilePath)) && _config.GetEnableMaxCacheSize()) {
                          // Restrict number of items evicted if maximum cache size is enabled
                          usedBytes -= fileSize;
                          shouldEvict = (usedBytes > _config.GetMaxCacheSizeBytes());
                        }
                      }
                    }
                  }
                }
              }
            }
            files.pop_front();
          }
        }

        if (not evicted && not _stopRequested) {
          UniqueMutexLock l(_evictionMutex);
          if (not _stopRequested) {
            _stopCondition.wait_for(l, _config.GetEnableMaxCacheSize() ? 5s : 60s);
          }
        }
      }
    }

    bool CheckModifiedRequirement(const std::string& filePath) {
      auto ret = false;
      std::uint64_t modifiedTime = 0;
      if ((ret = Utils::File::GetModifiedTime(filePath, modifiedTime))) {
#ifdef _WIN32
        const auto now = std::chrono::system_clock::now();
        const auto delay = std::chrono::minutes(_config.GetEvictionDelayMinutes());
        ret = ((std::chrono::system_clock::from_time_t(modifiedTime) + delay) <= now);
#else
        const auto now = Utils::GetTimeNow();
        const auto delay = (_config.GetEvictionDelayMinutes() * 60L) * NANOS_PER_SECOND;
        ret = ((modifiedTime + delay) <= now);
#endif
      }
      return ret;
    }

  public:
    void Start() {
      MutexLock l(_startStopMutex);
      if (not _evictionThread) {
        _stopRequested = false;
        _evictionThread = std::make_unique<std::thread>([this] {
          CheckItemsThread();
        });
      }
    }

    void Stop() {
      MutexLock l(_startStopMutex);
      if (_evictionThread) {
        _stopRequested = true;
        {
          MutexLock l2(_evictionMutex);
          _stopCondition.notify_all();
        }
        _evictionThread->join();
        _evictionThread.reset();
      }
    }
};
}

#endif //REPERTORY_EVICTION_H
