#ifndef REPERTORY_FILEUTILS_H
#define REPERTORY_FILEUTILS_H

#include <common.h>
#include <fstream>
#include <utils/pathutils.h>
#include <openssl/sha.h>
#if __APPLE__ || IS_FREEBSD
#include <sys/statvfs.h>
#endif
#ifndef _WIN32
#include <fcntl.h>
#include <sys/stat.h>
#endif
namespace Repertory {
namespace Utils {
namespace File {

// Prototypes
static ApiFileError AssignAndGetNativeFile(FileSystemItem &fileSystemItem, NativeFilePtr &nativeFile);
static std::uint64_t CalculateUsedSpace(std::string folder, const bool &recursive);
static inline bool CreateFullDirectoryPath(const std::string &path);
static inline bool DeleteAsDirectory(std::string path);
static inline bool DeleteAsFile(std::string path);
static std::string GenerateSha256(const std::string& filePath);
static std::uint64_t GetAvailableDriveSpace(const std::string& path);
static std::deque<std::string> GetDirectoryFiles(std::string sourcePath, const bool& oldestFirst);
static bool GetModifiedTime(const std::string& path, std::uint64_t& modified);
static bool GetSize(std::string filePath, std::uint64_t &fileSize);
static bool IsDirectory(const std::string& path);
static inline bool IsFile(const std::string &path);
static inline bool IsModifiedDateOlderThan(const std::string& path, const std::chrono::hours& hours);
static bool MoveAsFile(std::string from, std::string to);
template <typename T1, typename T2>
bool ReadFromStream(T1* inFile, T2& buf, size_t toRead);
static bool ReadJsonFile(const std::string& filePath, json& data);
static std::vector<std::string> ReadLines(const std::string& filePath);
static bool RecursiveDeleteDirectory(const std::string& directoryPath);
static bool ResetModifiedTime(const std::string& path);
static bool WriteJsonToFile(const std::string& path, const json& j);

// Implementations
static ApiFileError AssignAndGetNativeFile(FileSystemItem &fileSystemItem, NativeFilePtr &nativeFile) {
  auto apiFileError = ApiFileError::Success;
  RMutexLock l(*fileSystemItem.ItemLock);
  if (fileSystemItem.Handle == INVALID_OSHANDLE_VALUE) {
    if ((apiFileError = CNativeFile::CreateOrOpen(fileSystemItem.SourceFilePath, nativeFile)) == ApiFileError::Success) {
      fileSystemItem.Handle = nativeFile->GetHandle();
    }
  } else {
    nativeFile = CNativeFile::Attach(fileSystemItem.Handle);
  }

  return apiFileError;
}

static std::uint64_t CalculateUsedSpace(std::string folder, const bool &recursive) {
  folder = Utils::Path::Absolute(folder);
  std::uint64_t ret = 0;
#ifdef _WIN32
  WIN32_FIND_DATA fd = {0};
  const auto search = Utils::Path::Combine(folder, "*.*");
  auto find = ::FindFirstFile(&search[0], &fd);
  if (find != INVALID_HANDLE_VALUE) {
    do {
      const auto fileName = std::string(fd.cFileName);
      if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        if (recursive && (fileName != ".") && (fileName != "..")) {
          ret += CalculateUsedSpace(Utils::Path::Combine(folder, fileName), recursive);
        }
      } else {
        std::uint64_t fileSize;
        if (Utils::File::GetSize(Utils::Path::Combine(folder, fileName), fileSize)) {
          ret += fileSize;
        }
      }
    } while (::FindNextFile(find, &fd) != 0);
    ::FindClose(find);
  }
#else
  auto *root = opendir(&folder[0]);
  if (root) {
    struct dirent *de;
    while ((de = readdir(root)) != nullptr) {
      if (de->d_type == DT_DIR) {
        if (recursive && (strcmp(de->d_name, ".") != 0) && (strcmp(de->d_name, "..") != 0)) {
          ret += CalculateUsedSpace(Utils::Path::Combine(folder, de->d_name), recursive);
        }
      } else {
        std::uint64_t fileSize;
        if (Utils::File::GetSize(Utils::Path::Combine(folder, de->d_name), fileSize)) {
          ret += fileSize;
        }
      }
    }
    closedir(root);
  }
#endif

  return ret;
}

static inline bool CreateFullDirectoryPath(const std::string &path) {
#ifdef _WIN32
  const auto convString = String::FromUtf8(Utils::Path::Absolute(path));
  return IsDirectory(path) || (::SHCreateDirectory(nullptr, &convString[0]) == ERROR_SUCCESS);
#else
  auto ret = true;
  const auto paths = String::Split(Utils::Path::Absolute(path), Utils::Path::DirSep[0]);
  std::string curPath;
  for (size_t i = 0; ret && (i < paths.size()); i++) {
    if (paths[i].empty()) { // Skip root
      curPath = Utils::Path::DirSep;
    } else {
      curPath = Utils::Path::Combine(curPath, paths[i]);
      const auto status = mkdir(&curPath[0], S_IRWXU);
      ret = ((status == 0) || (errno == EEXIST));
    }
  }

  return ret;
#endif
}

static inline bool DeleteAsDirectory(std::string path) {
#ifdef _WIN32
  return (not IsDirectory(path) || ::RemoveDirectory(&path[0]));
#else
  return not IsDirectory(path) || (rmdir(&path[0]) == 0);
#endif
}

static inline bool DeleteAsFile(std::string path) {
#ifdef _WIN32
  return (not IsFile(path) || ::DeleteFile(&path[0]));
#else
  return (not IsFile(path) || (unlink(&path[0]) == 0));
#endif
}

std::string GenerateSha256(const std::string& filePath) {
  std::string ret;
  SHA256_CTX ctx{};
  if (SHA256_Init(&ctx)) {
    std::ifstream inFile(&filePath[0], std::ios::in | std::ios::binary);
    inFile.seekg(0, std::ios::beg);

    std::vector<unsigned char> hash(SHA256_DIGEST_LENGTH);

    std::vector<char> buf;
    buf.resize(40960);

    std::uint64_t remain;
    GetSize(filePath, remain);
    while (remain > 0) {
      const auto toRead = std::min(buf.size(), (size_t)remain);
      if (not ReadFromStream(&inFile, buf, toRead))
        throw std::runtime_error("read failed");
      remain -= toRead;
      SHA256_Update(&ctx, &buf[0], toRead);
    }
    SHA256_Final(&hash[0], &ctx);

    ret = Utils::ToHexString(hash);
  }

  return ret;
}

static std::uint64_t GetAvailableDriveSpace(const std::string& path) {
#ifdef _WIN32
  ULARGE_INTEGER li = { 0 };
  ::GetDiskFreeSpaceEx(&path[0], &li, nullptr, nullptr);
  return li.QuadPart;
#endif
#ifdef __linux__
  std::uint64_t ret = 0;
  struct statfs64 st{};
  if (statfs64(&path[0], &st) == 0) {
    ret = st.f_bfree * st.f_bsize;
  }
  return ret;
#endif
#if __APPLE__ || IS_FREEBSD
  struct statvfs st{};
  statvfs(&path[0], &st);
  return st.f_bavail * st.f_frsize;
#endif
}

static std::deque<std::string> GetDirectoryFiles(std::string sourcePath, const bool& oldestFirst) {
  sourcePath = Utils::Path::Absolute(sourcePath);
  std::deque<std::string> ret;
  std::unordered_map<std::string, std::uint64_t> lookup;
#ifdef _WIN32
  WIN32_FIND_DATA fd = { 0 };
  const auto search = Utils::Path::Combine(sourcePath, "*.*");
  auto find = ::FindFirstFile(&search[0], &fd);
  if (find != INVALID_HANDLE_VALUE) {
    do {
      if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) {        
        ULARGE_INTEGER li{};
        li.HighPart = fd.ftLastWriteTime.dwHighDateTime;
        li.LowPart = fd.ftLastWriteTime.dwLowDateTime;
        const auto fullPath = Utils::Path::Combine(sourcePath, fd.cFileName);
        lookup[fullPath] = li.QuadPart;
        ret.emplace_back(fullPath);
      }
    } while (::FindNextFile(find, &fd) != 0);
    ::FindClose(find);

    std::sort(ret.begin(), ret.end(), [&](const auto& p1, const auto& p2)->bool {
      return (oldestFirst != 0) == (lookup[p1] < lookup[p2]);
    });
  }
#else
  auto *root = opendir(&sourcePath[0]);
  if (root) {
    struct dirent *de;
    while ((de = readdir(root)) != nullptr) {
      if (de->d_type != DT_DIR) {
        ret.emplace_back(Utils::Path::Combine(sourcePath, de->d_name));
      }
    }
    closedir(root);

    auto addToLookup = [&](const std::string& path) {
      if (lookup.find(path) == lookup.end()) {
        struct stat st{};
        stat(&path[0], &st);
#ifdef __APPLE__
        lookup[path] = static_cast<std::uint64_t>((st.st_mtimespec.tv_sec * NANOS_PER_SECOND) + st.st_mtimespec.tv_nsec);
#else
        lookup[path] = static_cast<std::uint64_t>((st.st_mtim.tv_sec * NANOS_PER_SECOND) + st.st_mtim.tv_nsec);
#endif
      }
    };

    std::sort(ret.begin(), ret.end(), [&](const auto& p1, const auto& p2)->bool {
      addToLookup(p1);
      addToLookup(p2);
      return (oldestFirst != 0) == (lookup[p1] < lookup[p2]);
    });
  }
#endif
  return std::move(ret);
}

static bool GetSize(std::string filePath, std::uint64_t &fileSize) {
  fileSize = 0;
  filePath = Utils::Path::Finalize(filePath);
#ifdef _WIN32
  struct _stat64 st{};
  if (_stat64(&filePath[0], &st) != 0) {
#else
#if __APPLE__ || IS_FREEBSD
  struct stat st{};
  if (stat(&filePath[0], &st) != 0) {
#else
  struct stat64 st{};
  if (stat64(&filePath[0], &st) != 0) {
#endif
#endif
    return false;
  }

  if (st.st_size >= 0) {
    fileSize = static_cast<std::uint64_t>(st.st_size);
  }
  return (st.st_size >= 0);
}

static bool GetModifiedTime(const std::string& path, std::uint64_t& modified) {
  auto ret = false;
  modified = 0;
#ifdef _WIN32
  struct _stat64 st{};
  if (_stat64(&path[0], &st) != -1) {
    modified = static_cast<uint64_t>(st.st_mtime);
#else
  struct stat st{};
  if (stat(&path[0], &st) != -1) {
#ifdef __APPLE__
    modified = static_cast<uint64_t>(st.st_mtimespec.tv_nsec + (st.st_mtimespec.tv_sec * NANOS_PER_SECOND));
#else
    modified = static_cast<uint64_t>(st.st_mtim.tv_nsec + (st.st_mtim.tv_sec * NANOS_PER_SECOND));
#endif
#endif
    ret = true;
  }

  return ret;
}

static inline bool IsDirectory(const std::string& path) {
#ifdef _WIN32
  return !!::PathIsDirectory(&path[0]) ? true : false;
#else
  struct stat st{};
  return (not stat(&path[0], &st) && S_ISDIR(st.st_mode));
#endif
}

static inline bool IsFile(const std::string &path) {
#ifdef _WIN32
  return (::PathFileExists(&path[0]) && not::PathIsDirectory(&path[0]));
#else
  struct stat st{};
  return (not stat(&path[0], &st) && not S_ISDIR(st.st_mode));
#endif
}

static inline bool IsModifiedDateOlderThan(const std::string& path, const std::chrono::hours& hours) {
  auto ret = false;
  std::uint64_t modified = 0;
  if (Utils::File::GetModifiedTime(path, modified)) {
    const auto seconds = std::chrono::duration_cast<std::chrono::seconds>(hours);
#ifdef _WIN32
    return (std::chrono::system_clock::from_time_t(modified) + seconds) < std::chrono::system_clock::now();
#else
    return (modified + (seconds.count() * NANOS_PER_SECOND)) < Utils::GetTimeNow();
#endif
  }
  return ret;
}

static bool MoveAsFile(std::string from, std::string to) {
  from = Utils::Path::Finalize(from);
  to = Utils::Path::Finalize(to);

  const auto directory = Utils::Path::RemoveFileName(to);
  CreateFullDirectoryPath(directory);
#ifdef _WIN32
  const bool ret = !!::MoveFile(&from[0], &to[0]) ? true : false;
#else
  const bool ret = (rename(&from[0], &to[0]) == 0);
#endif

  return ret;
}

template <typename T1, typename T2>
bool ReadFromStream(T1* inFile, T2& buf, size_t toRead) {
  std::uint64_t offset = 0;
  do {
    inFile->read((char*)&buf[offset], toRead);
    toRead -= inFile->gcount();
    offset += inFile->gcount();
  } while (not inFile->fail() && (toRead > 0));

  return (toRead == 0);
}

static bool ReadJsonFile(const std::string &filePath, json& data) {
  auto ret = false;
  try {
    std::ifstream myfile(&filePath[0]);
    if (myfile.is_open()) {
      std::stringstream ss;
      ss << myfile.rdbuf();
      std::string jsonTxt = ss.str();
      if (jsonTxt.empty()) {
        myfile.close();
      } else {
        data = json::parse(jsonTxt.c_str());
        ret = true;
        myfile.close();
      }
    }
  } catch (const std::exception&) {
  }

  return ret;
}

static std::vector<std::string> ReadLines(const std::string& filePath) {
  std::vector<std::string> ret;
  if (Utils::File::IsFile(filePath)) {
    std::ifstream fs(filePath);
    std::string curLine;
    while (not fs.eof() && std::getline(fs, curLine)) {
      ret.emplace_back(Utils::String::RightTrim(curLine, '\r'));
    }
    fs.close();
  }

  return std::move(ret);
}

static bool RecursiveDeleteDirectory(const std::string& directoryPath) {
#ifdef _WIN32
  auto ret = false;

  WIN32_FIND_DATA fd = {0};
  const auto search = Utils::Path::Combine(directoryPath, "*.*");
  auto find = ::FindFirstFile(&search[0], &fd);
  if (find != INVALID_HANDLE_VALUE) {
    ret = true;
    do {
      if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
        if ((std::string(fd.cFileName) != ".") && (std::string(fd.cFileName) != "..")) {
          ret = RecursiveDeleteDirectory(Utils::Path::Combine(directoryPath, fd.cFileName));
        }
      } else {
        ret = DeleteAsFile(Utils::Path::Combine(directoryPath, fd.cFileName));
      }
    } while (ret && (::FindNextFile(find, &fd) != 0));

    ::FindClose(find);
  }
#else
  auto ret = true;
  const auto fp = Utils::Path::Absolute(directoryPath);
  DIR *root = opendir(&fp[0]);
  if (root) {
    struct dirent *de;
    while (ret && (de = readdir(root))) {
      if (de->d_type == DT_DIR) {
        if ((strcmp(de->d_name, ".") != 0) && (strcmp(de->d_name, "..") != 0)) {
          ret = RecursiveDeleteDirectory(Utils::Path::Combine(fp, de->d_name));
        }
      } else {
        ret = DeleteAsFile(Utils::Path::Combine(fp, de->d_name));
      }
    }

    closedir(root);
  }
#endif

  return ret && DeleteAsDirectory(directoryPath);
}

static bool ResetModifiedTime(const std::string& path) {
  auto ret = false;
#ifdef _WIN32
  SYSTEMTIME st{};
  ::GetSystemTime(&st);

  FILETIME ft{};
  if ((ret = !!::SystemTimeToFileTime(&st, &ft))) {
    auto handle = ::CreateFile(&path[0], FILE_READ_ATTRIBUTES|FILE_WRITE_ATTRIBUTES, FILE_SHARE_DELETE|FILE_SHARE_READ|FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, 0, nullptr);
    if ((ret = (handle != INVALID_HANDLE_VALUE))) {
      ret = !!::SetFileTime(handle, nullptr, &ft, &ft);
      ::CloseHandle(handle);
    }
  }
#else
  auto fd = open(&path[0], O_RDWR);
  if ((ret = (fd != -1))) {
    ret = not futimens(fd, nullptr);
    close(fd);
  }
#endif
  return ret;
}

static bool WriteJsonToFile(const std::string& path, const json& j) {
  std::string data;
  {
    std::stringstream ss;
    ss << std::setw(2) << j << std::endl;
    data = ss.str();
  }
  NativeFilePtr nativeFile;
  auto ret = (CNativeFile::CreateOrOpen(path, nativeFile) == ApiFileError::Success);
  if (ret) {
    std::size_t bytesWritten;
    ret = nativeFile->Truncate(0) && nativeFile->WriteBytes(&data[0], data.size(), 0, bytesWritten);
    nativeFile->Close();
  }

  return ret;
}
}
}
}

#endif //REPERTORY_FILEUTILS_H
