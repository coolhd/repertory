#ifndef REPERTORY_PATHUTILS_H
#define REPERTORY_PATHUTILS_H

#include <common.h>
#include <utils/utils.h>

namespace Repertory {
namespace Utils {
namespace Path {

#ifdef _WIN32
static const std::string DirSep = "\\";
static const std::string NDirSep = "/";
#else
static const std::string DirSep = "/";
static const std::string NDirSep = "\\";
#endif

// Prototypes
static std::string Absolute(std::string path);
static inline std::string Combine(const std::string &path1, const std::string &path2);
static std::string CreateApiPath(std::string path);
static inline std::string EscapeADS(std::string path);
static std::string Finalize(const std::string &path);
static inline std::string GetParentApiPath(const std::string& apiFilePath);
#ifndef _WIN32
static std::string GetParentDirectory(std::string path);
#endif
static inline bool IsADS(const std::string &path);
std::string RemoveFileName(std::string path);
#ifndef _WIN32
static inline std::string Resolve(std::string path);
#endif
static inline std::string StripToFileName(std::string path);
static inline std::string UnEscapeADS(std::string path);

// Implementations
static std::string Absolute(std::string path) {
#ifdef _WIN32
  if (not path.empty() && ::PathIsRelative(&path[0])) {
    std::string temp;
    temp.resize(MAX_PATH + 1);
    path = _fullpath(&temp[0], &path[0], MAX_PATH);
  }
#else
  if (not path.empty() && (path[0] != '/')) {
    auto found = false;
    auto tmp = path;
    do {
      auto *res = realpath(&tmp[0], nullptr);
      if (res) {
        path = Combine(res, path.substr(tmp.size()));
        free(res);
        found = true;
      } else if (tmp == ".") {
        found = true;
      } else {
        tmp = dirname(&tmp[0]);
      }
    } while (not found);
  }
#endif
  return path;
}

static inline std::string Combine(const std::string &path1, const std::string &path2) {
  return Finalize(path1 + DirSep + path2);
}

static std::string CreateApiPath(std::string path) {
  if (path.empty() || (path == ".")) {
    path = "/";
  } else {
    std::replace(path.begin(), path.end(), '\\', '/');
    if (path.find("./") == 0) {
      path = path.substr(1);
    }
    std::regex r("/+");
    path = std::regex_replace(path, r, "/");
    if (path[0] != '/') {
      path = "/" + path;
    }
  }
  return std::move(path);
}

static inline std::string EscapeADS(std::string path) {
#ifdef _WIN32
  if (Utils::String::Contains(path, ":")) {
    Utils::String::Replace(path, ":", ".__ads__.");
  }
#endif
  return std::move(path);
}

static std::string Finalize(const std::string &path) {
  std::regex r("\\" + NDirSep + "+");
  const auto str = std::regex_replace(path, r, DirSep);
  std::regex r2(DirSep + DirSep + "+");
  auto ret = std::regex_replace(str, r2, DirSep);
  if ((ret.size() > 1) && (ret[ret.size() - 1] == DirSep[0])) {
    ret = ret.substr(0, ret.size() - 1);
  }
  return std::move(ret);
}

static inline std::string GetParentApiPath(const std::string& apiFilePath) {
  std::string ret;
  if (apiFilePath != "/") {
    ret = apiFilePath.substr(0, apiFilePath.rfind('/') + 1);
    if (ret != "/") {
      ret = String::RightTrim(ret, '/');
    }
  }
  return ret;
}

#ifndef _WIN32
static std::string GetParentDirectory(std::string path) {
  auto ret = std::string(dirname((char*)path.c_str()));
  if (ret == ".") {
    ret = "/";
  }

  return std::move(ret);
}
#endif

static inline bool IsADS(const std::string &path) {
#ifdef _WIN32
  return Utils::String::Contains(path, ":");
#else
  return false;
#endif
}

std::string RemoveFileName(std::string path) {
  path = Finalize(path);

#ifdef _WIN32
  ::PathRemoveFileSpec(&path[0]);
  path = path.c_str();
#else
  if (path != "/") {
    auto i = path.size() - 1;
    while ((i != 0) && (path[i] != '/')) {
      i--;
    }

    if (i > 0) {
      path = Finalize(path.substr(0, i));
    } else {
      path = '/';
    }
  }
#endif

  return std::move(path);
}

#ifndef _WIN32
static inline std::string Resolve(std::string path) {
  struct passwd *pw = getpwuid(getuid());
  std::string home = (pw->pw_dir ? pw->pw_dir : "");
  if (home.empty() || ((home == "/") && (getuid() != 0))) {
    home = Combine("/home", pw->pw_name);
  }
  return Finalize(String::Replace(path, "~", home));
}
#endif

static inline std::string StripToFileName(std::string path) {
#ifdef _WIN32
  path = ::PathFindFileName(&path[0]);
#else
  path = basename(&path[0]);
#endif
  return std::move(path);
}

static inline std::string UnEscapeADS(std::string path) {
#ifdef _WIN32
  if (Utils::String::Contains(path, ".__ads__.")) {
    Utils::String::Replace(path, ".__ads__.", ":");
  }
#endif
  return std::move(path);
}

}
}
}

#endif //REPERTORY_PATHUTILS_H
