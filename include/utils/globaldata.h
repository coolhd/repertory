#ifndef REPERTORY_GLOBALDATA_H
#define REPERTORY_GLOBALDATA_H

#include <atomic>
#include <cstdint>

namespace Repertory {
class GlobalData final {
  public:
    GlobalData(const GlobalData&) = delete;
    GlobalData(GlobalData&&) = delete;
    GlobalData& operator=(const GlobalData&) = delete;
    GlobalData& operator=(GlobalData&&) = delete;

  private:
    GlobalData() :
      _usedDriveSpace(0),
      _usedCacheSpace(0) {
    }
    ~GlobalData() = default;

  private:
    static GlobalData _globalData;

  public:
    static inline GlobalData& Instance() {
      return _globalData;
    }

  private:
    std::atomic<std::uint64_t> _usedCacheSpace;
    std::atomic<std::uint64_t> _usedDriveSpace;

  public:
    inline void DecrementUsedDriveSpace(const std::uint64_t& val) {
      _usedDriveSpace -= val;
    }

    inline std::uint64_t GetUsedCacheSpace() const {
      return _usedCacheSpace;
    }

    inline std::uint64_t GetUsedDriveSpace() const {
      return _usedDriveSpace;
    }

    inline void IncrementUsedDriveSpace(const std::uint64_t& val) {
      _usedDriveSpace += val;
    }

    inline void InitializeUsedCacheSpace(const std::uint64_t& val) {
      _usedCacheSpace = val;
    }

    inline void InitializeUsedDriveSpace(const std::uint64_t& val) {
      _usedDriveSpace = val;
    }

    inline void UpdateUsedSpace(const std::uint64_t &fileSize, const std::uint64_t &newFileSize, const bool& cacheOnly) {
      if (fileSize > newFileSize) {
        const auto diff = (fileSize - newFileSize);
        _usedCacheSpace -= diff;
        if (not cacheOnly) {
          _usedDriveSpace -= diff;
        }
      } else if (fileSize < newFileSize) {
        const auto diff = (newFileSize - fileSize);
        _usedCacheSpace += diff;
        if (not cacheOnly) {
          _usedDriveSpace += diff;
        }
      }
    }
};
}
#endif //REPERTORY_GLOBALDATA_H
