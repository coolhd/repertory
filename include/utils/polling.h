#ifndef REPERTORY_POLLING_H
#define REPERTORY_POLLING_H

#include <common.h>
#include <config.h>
#include <future>

namespace Repertory {
class Polling final {
  public:
    typedef struct {
      std::string Name;
      bool LowFrequency;
      std::function<void()> Action;
    } PollingItem;

  public:
    Polling(const Polling&) = delete;
    Polling(Polling&&) = delete;
    Polling& operator=(const Polling&) = delete;
    Polling& operator=(Polling&&) = delete;
    
  private:
    Polling() = default;
    ~Polling() {
      Stop();
    }

  private:
    static Polling _polling;

  public:
    static inline Polling& Instance() {
      return _polling;
    }
    
  private:
    CConfig* _config = nullptr;
    std::unordered_map<std::string, PollingItem> _pollingItems;
    std::mutex _pollingMutex;
    bool _stopRequested = false;
    std::condition_variable _pollingNotify;
    std::mutex _startStopMutex;
    std::unique_ptr<std::thread> _highFreqThread;
    std::unique_ptr<std::thread> _lowFreqThread;
    
  private:
    void FreqThread(std::function<std::uint32_t()> frequencySeconds, const bool& lowFrequency) {
      while (not _stopRequested) {
        std::deque<std::future<void>> futures;
        UniqueMutexLock l(_pollingMutex);
        if (not _stopRequested &&
          _pollingNotify.wait_for(l, std::chrono::seconds(frequencySeconds())) == std::cv_status::timeout) {
          for (auto &kv : _pollingItems) {
            if (kv.second.LowFrequency == lowFrequency) {
              futures.emplace_back(std::async(std::launch::async, [kv]() -> void {
                EventSystem::Instance().Raise<PollingItemBegin>(kv.first);
                kv.second.Action();
                EventSystem::Instance().Raise<PollingItemEnd>(kv.first);
              }));
            }
          }
          l.unlock();
          while (not futures.empty()) {
            futures.front().wait();
            futures.pop_front();
          }
        }
      }
    }
    
  public:
    inline void RemoveCallback(const std::string& name) {
      MutexLock l(_pollingMutex);
      _pollingItems.erase(name);
    }

    inline void SetCallback(const PollingItem& pollingItem) {
      MutexLock l(_pollingMutex);
      _pollingItems[pollingItem.Name] = pollingItem;
    }
    
    void Start(CConfig* config) {
      MutexLock l(_startStopMutex);
      if (not _highFreqThread) {
        _config = config;
        _stopRequested = false;
        _highFreqThread = std::make_unique<std::thread>([this]()->void {
          this->FreqThread([this]()->std::uint32_t {return _config->GetHighFreqIntervalSeconds();}, false);
        });
        _lowFreqThread = std::make_unique<std::thread>([this]()->void {
          this->FreqThread([this]()->std::uint32_t {return _config->GetLowFreqIntervalSeconds();}, true);
        });
      }
    }

    void Stop() {
      MutexLock l(_startStopMutex);
      if (_highFreqThread) {
        {
          MutexLock l2(_pollingMutex);
          _stopRequested = true;
          _pollingNotify.notify_all();
        }
        _highFreqThread->join();
        _lowFreqThread->join();
        _highFreqThread.reset();
        _lowFreqThread.reset();
      }
    }
};
}

#endif //REPERTORY_POLLING_H
