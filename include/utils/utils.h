#ifndef REPERTORY_UTILS_H
#define REPERTORY_UTILS_H

#include <algorithm>
#include <chrono>
#include <cpprest/http_listener.h>
#include <mutex>
#include <providers/iprovider.h>
#include <regex>
#include <utils/nativefile.h>
#include <utils/stringutils.h>

#ifdef _WIN32
#include <codecvt>
#include <ShlObj.h>
#include <winfsp/winfsp.h>
#else
#include <climits>
#include <dirent.h>
#include <grp.h>
#include <libgen.h>
#include <pwd.h>
#include <sys/stat.h>
#ifdef __linux__
#include <sys/statfs.h>
#endif
#include <sys/types.h>
#include <unistd.h>
#if __linux__ || IS_FREEBSD
#include <utils/uuid++.hh>
#else
#include <uuid/uuid.h>
#endif
#endif

namespace Repertory {
namespace Utils {
#if __linux__ || IS_FREEBSD
const std::vector<std::string> AttributeNamespaces = {
  "security",
  "system",
  "trusted",
  "user"};
#endif

// Prototypes
static Hastings ApiCurrencyToHastings(const ApiCurrency &currency);
static std::string ApiCurrencyToHastingsString(const ApiCurrency &currency);
static inline std::size_t CalculateReadSize(const uint64_t &totalSize, const std::size_t& readSize, const uint64_t &offset);
static inline int CompareVersionStrings(std::string verA, std::string verB);
static inline std::uint64_t ConvertApiDate(const std::string& date);
static inline void CopyFileSystemItem(FileSystemItem& dest, const FileSystemItem& src);
static inline web::http::uri CreateHttpWebUri(const std::string& str);
static inline std::string CreateUUIDString();
template<typename T>
static inline T DivideWithCeiling(const T &n, const T &d);
static DownloadType DownloadTypeFromString(std::string downloaderType, const DownloadType& defaultType);
static std::string DownloadTypeToString(const DownloadType& downloaderType);
static std::string EnvironmentVariable(const std::string& variable);
static inline std::uint64_t GetFileTimeNow();
#ifdef _WIN32
static inline DWORD GetLastErrorCode();
#else
static inline int GetLastErrorCode();
#endif
#ifdef _WIN32
static std::string GetLocalAppDataDirectory();
#endif
static void GetLocalTime(struct tm& localTime);
static std::string GenerateRandomString(const std::uint16_t& length);
static inline std::uint64_t GetTimeNow();
static bool HasOption(const int &argc, char *argv[], const std::string &option);
static ApiCurrency HastingsStringToApiCurrency(const std::string &hastings);
#ifndef _WIN32
static bool IsUidMemberOfGroup(const uid_t& uid, const gid_t& gid);
#endif
static auto ParseOption(const int &argc, char *argv[], const std::string &option, std::uint8_t count);
template <typename T>
inline void RemoveElement(T& t, const typename T::value_type& value );
#ifdef _WIN32
inline void SetLastErrorCode(DWORD errorCode);
#else
inline void SetLastErrorCode(int errorCode);
#endif
inline void SpinWaitForMutex(std::function<bool()> complete, std::condition_variable& cv, std::mutex& mtx);
inline void SpinWaitForMutex(bool& complete, std::condition_variable& cv, std::mutex& mtx);
template <typename T>
std::string ToHexString(const T& t);

// Implementations
static Hastings ApiCurrencyToHastings(const ApiCurrency &currency) {
  ttmath::Parser<ApiCurrency> parser;
  parser.Parse(currency.ToString() + " * (10 ^ 24)");
  ttmath::Conv conv;
  conv.scient_from = 256;
  conv.base = 10;
  conv.round = 0;
  return parser.stack[0].value.ToString(conv);
}

static std::string ApiCurrencyToHastingsString(const ApiCurrency &currency) {
  ttmath::Parser<ApiCurrency> parser;
  parser.Parse(currency.ToString() + " * (10 ^ 24)");
  ttmath::Conv conv;
  conv.scient_from = 256;
  conv.base = 10;
  conv.round = 0;
  return parser.stack[0].value.ToString(conv);
}

static inline std::size_t CalculateReadSize(const uint64_t &totalSize, const std::size_t& readSize, const uint64_t &offset) {
  return ((offset + readSize) > totalSize) ? ((offset < totalSize) ? totalSize - offset : 0) : readSize;
}

static inline int CompareVersionStrings(std::string verA, std::string verB) {
  if (Utils::String::Contains(verA, "-")) {
    verA = Utils::String::Split(verA, '-')[0];
  }
  if (Utils::String::Contains(verB, "-")) {
    verB = Utils::String::Split(verB, '-')[0];
  }
  auto aNums = Utils::String::Split(verA, '.');
  auto bNums = Utils::String::Split(verB, '.');
  while (aNums.size() > bNums.size()) {
    bNums.emplace_back("0");
  }
  while (bNums.size() > aNums.size()) {
    aNums.emplace_back("0");
  }

  for (size_t i = 0; i < aNums.size(); i++) {
    const auto aInt = Utils::String::ToUInt32(aNums[i]);
    const auto bInt = Utils::String::ToUInt32(bNums[i]);
    const auto res = std::memcmp(&aInt, &bInt, sizeof(aInt));
    if (res) {
      return res;
    }
  }

  return 0;
}

static inline std::uint64_t ConvertApiDate(const std::string& date) {
  //"2019-02-21T02:24:37.653091916-06:00"
  const auto parts = String::Split(date, '.');
  const auto dt = parts[0];
  const auto nanos = String::ToUInt64(String::Split(parts[1], '-')[0]);

  struct tm tm1{};
#ifdef _WIN32
  auto convertTime = [](time_t t)->std::uint64_t {
    const auto ll = Int32x32To64(t, 10000000) + 116444736000000000;
    ULARGE_INTEGER ft{};
    ft.LowPart = static_cast<DWORD>(ll);
    ft.HighPart = static_cast<DWORD>(ll >> 32);
    return ft.QuadPart;
  };

  const auto parts2 = String::Split(dt, 'T');
  const auto dateParts = String::Split(parts2[0], '-');
  const auto timeParts = String::Split(parts2[1], ':');
  tm1.tm_year = String::ToInt32(dateParts[0]) - 1900;
  tm1.tm_mon = String::ToInt32(dateParts[1]) - 1;
  tm1.tm_mday = String::ToInt32(dateParts[2]);
  tm1.tm_hour = String::ToUInt32(timeParts[0]);
  tm1.tm_min = String::ToUInt32(timeParts[1]);
  tm1.tm_sec = String::ToUInt32(timeParts[2]);
  tm1.tm_wday = -1;
  tm1.tm_yday = -1;
  tm1.tm_isdst = -1;
  return (nanos / 100) + convertTime(mktime(&tm1));
#else
  strptime(&dt[0], "%Y-%m-%dT%T", &tm1);
  return nanos + (mktime(&tm1) * NANOS_PER_SECOND);
#endif
}

static inline void CopyFileSystemItem(FileSystemItem& dest, const FileSystemItem& src) {
  dest = src;
}

#ifdef _WIN32
static inline web::http::uri CreateHttpWebUri(const std::string& str) {
  std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
  return converter.from_bytes(str);
}
#else
static inline web::http::uri CreateHttpWebUri(const std::string& str) {
  return web::http::uri(&str[0]);
}
#endif

static inline std::string CreateUUIDString() {
#ifdef _WIN32
  UUID guid{};
  UuidCreate(&guid);

  unsigned char* s;
  UuidToStringA(&guid, &s);

  std::string ret(reinterpret_cast<char*>(s));
  RpcStringFreeA(&s);

  return ret;
#else
#if __linux__ || IS_FREEBSD
  uuid id;
  id.make(UUID_MAKE_V4);
  return id.string();
#else
  uuid_t guid;
  uuid_generate_random(guid);

  std::string ret;
  ret.resize(37);
  uuid_unparse(guid, &ret[0]);

  return ret.c_str();
#endif
#endif
}

template<typename T>
static inline T DivideWithCeiling(const T &n, const T &d) {
  return n ? (n / d) + (n % d != 0) : 0;
}

static DownloadType DownloadTypeFromString(std::string downloaderType, const DownloadType& defaultType) {
  downloaderType = Utils::String::ToLower(Utils::String::Trim(downloaderType));
  if (downloaderType == "direct") {
    return DownloadType::Direct;
  } else if (downloaderType == "fallback") {
    return DownloadType::Fallback;
  } else if (downloaderType == "ringbuffer") {
    return DownloadType::RingBuffer;
  }

  return defaultType;
}

static std::string DownloadTypeToString(const DownloadType& downloaderType) {
  switch (downloaderType) {
  case DownloadType::Direct: return "Direct";
  case DownloadType::Fallback: return "Fallback";
  case DownloadType::RingBuffer: return "RingBuffer";
  default: return "Fallback";
  }
}

static std::string EnvironmentVariable(const std::string& variable) {
#ifdef _WIN32
  std::string value;
  auto sz = ::GetEnvironmentVariable(&variable[0], nullptr, 0);
  if (sz > 0) {
    value.resize(sz);
    ::GetEnvironmentVariable(&variable[0], &value[0], sz);
  }
  return &value[0];
#else
  const auto* v = getenv(&variable[0]);
  return std::string(v ? v : "");
#endif
}

static std::string GenerateRandomString(const std::uint16_t& length) {
  srand(static_cast<unsigned int>(GetTimeNow()));

  std::string ret;
  ret.resize(length);
  for (auto i = 0; i < length; i++) {
    do {
      ret[i] = static_cast<char>(rand() % 74 + 48);
    } while (((ret[i] >= 91) && (ret[i] <= 96)) || ((ret[i] >= 58) && (ret[i] <= 64)));
  }

  return ret;
}

static inline std::uint64_t GetFileTimeNow() {
#ifdef _WIN32
  SYSTEMTIME st{};
  ::GetSystemTime(&st);
  FILETIME ft{};
  ::SystemTimeToFileTime(&st, &ft);
  return static_cast<std::uint64_t>(((LARGE_INTEGER*)&ft)->QuadPart);
#else
  return GetTimeNow();
#endif
}
#ifdef _WIN32
static inline DWORD GetLastErrorCode() {
  return ::GetLastError();
}
#else
static inline int GetLastErrorCode() {
  return errno;
}
#endif
#ifdef _WIN32
std::string GetLocalAppDataDirectory() {
  static std::string appData;
  if (appData.empty()) {
    ComInitWrapper cw;
    PWSTR localAppData = nullptr;
    if (SUCCEEDED(::SHGetKnownFolderPath(FOLDERID_LocalAppData, 0, nullptr, &localAppData))) {
      appData = Utils::String::ToUtf8(localAppData);
      ::CoTaskMemFree(localAppData);
    } else {
      throw StartupException("Unable to detect application data folder");
    }
  }

  return appData;
}
#endif
static void GetLocalTime(struct tm& localTime) {
  memset(&localTime, 0, sizeof(localTime));

  const auto now = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
#ifdef _WIN32
  localtime_s(&localTime, &now);
#else
  const auto* tmp = std::localtime(&now);
  if (tmp) {
    memcpy(&localTime, tmp, sizeof(localTime));
  }
#endif
}

static inline std::uint64_t GetTimeNow() {
#ifdef _WIN32
  return static_cast<std::uint64_t>(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
#else
#if __APPLE__ || IS_FREEBSD
  return std::chrono::nanoseconds(std::chrono::system_clock::now().time_since_epoch()).count();
#else
  return static_cast<std::uint64_t>(std::chrono::nanoseconds(std::chrono::high_resolution_clock::now().time_since_epoch()).count());
#endif
#endif
}

static bool HasOption(const int &argc, char *argv[], const std::string &option) {
  auto ret = false;
  for (int i = 0; not ret && (i < argc); i++) {
    ret = (option == argv[i]);
  }
  return ret;
}

static ApiCurrency HastingsStringToApiCurrency(const std::string &hastings) {
  ttmath::Parser<ApiCurrency> parser;
  parser.Parse(hastings + " / (10 ^ 24)");
  return parser.stack[0].value;
}

#ifndef _WIN32
static bool IsUidMemberOfGroup(const uid_t& uid, const gid_t& gid) {
  auto *pw = getpwuid(uid);
  std::vector<gid_t> groups;

  int groupCount = 0;
  if (getgrouplist(pw->pw_name, pw->pw_gid, nullptr, &groupCount) < 0) {
    groups.resize(static_cast<unsigned long>(groupCount));
#ifdef __APPLE__
    getgrouplist(pw->pw_name, pw->pw_gid, reinterpret_cast<int*>(&groups[0]), &groupCount);
#else
    getgrouplist(pw->pw_name, pw->pw_gid, &groups[0], &groupCount);
#endif
  }

  return (std::find(groups.begin(), groups.end(), gid) != groups.end());
}
#endif

static auto ParseOption(const int &argc, char *argv[], const std::string &option, std::uint8_t count) {
  std::vector<std::string> ret;
  auto found = false;
  for (auto i = 0; not found && (i < argc); i++) {
    if ((found = (option == argv[i]))) {
      if ((++i + count) <= argc) {
        while (count--) {
          ret.emplace_back(argv[i++]);
        }
      }
    }
  }

  return ret;
}

#ifdef _WIN32
static NTSTATUS TranslateApiFileError(const ApiFileError &error) {
  switch (error) {
  case ApiFileError::AccessDenied:
    return FspNtStatusFromWin32(ERROR_ACCESS_DENIED);
  case ApiFileError::BufferTooSmall:
    return STATUS_BUFFER_TOO_SMALL;
  case ApiFileError::BufferOverflow:
    return STATUS_BUFFER_OVERFLOW;
  case ApiFileError::DirectoryEndOfFiles:
    return STATUS_NO_MORE_FILES;
  case ApiFileError::DirectoryExists:
    return STATUS_OBJECT_NAME_EXISTS;
  case ApiFileError::DirectoryNotEmpty:
    return FspNtStatusFromWin32(ERROR_DIR_NOT_EMPTY);
  case ApiFileError::DirectoryNotFound:
    return STATUS_OBJECT_NAME_NOT_FOUND;
  case ApiFileError::DownloadFailed:
    return FspNtStatusFromWin32(ERROR_INTERNAL_ERROR);
  case ApiFileError::Error:
    return FspNtStatusFromWin32(ERROR_INTERNAL_ERROR);
  case ApiFileError::FileExists:
    return FspNtStatusFromWin32(ERROR_FILE_EXISTS);
  case ApiFileError::FileInUse:
    return FspNtStatusFromWin32(ERROR_BUSY);
  case ApiFileError::InvalidOperation:
    return FspNtStatusFromWin32(ERROR_INTERNAL_ERROR);
  case ApiFileError::ItemNotFound:
    return STATUS_OBJECT_NAME_NOT_FOUND;
  case ApiFileError::OSErrorCode:
    return FspNtStatusFromWin32(::GetLastError());
  case ApiFileError::PermissionDenied:
    return FspNtStatusFromWin32(ERROR_ACCESS_DENIED);
  case ApiFileError::Success:
    return 0;
  case ApiFileError::Unsupported:
    return STATUS_INVALID_DEVICE_REQUEST;
  case ApiFileError::UploadFailed:
    return FspNtStatusFromWin32(ERROR_INTERNAL_ERROR);
  default:
    return FspNtStatusFromWin32(ERROR_INTERNAL_ERROR);
  }
}
#else
static int TranslateApiFileError(const ApiFileError &error) {
  switch (error) {
  case ApiFileError::AccessDenied:
    return -EACCES;
  case ApiFileError::DirectoryEndOfFiles:
    return -EOF;
  case ApiFileError::DirectoryExists:
    return -EISDIR;
  case ApiFileError::DirectoryNotEmpty:
    return -ENOTEMPTY;
  case ApiFileError::DirectoryNotFound:
    return -ENOTDIR;
  case ApiFileError::DownloadFailed:
#if __APPLE__ || IS_FREEBSD
    return -EBADMSG;
#else
    return -EREMOTEIO;
#endif
  case ApiFileError::Error:
    return -EIO;
  case ApiFileError::FileExists:
    return -EEXIST;
  case ApiFileError::FileInUse:
    return -EBUSY;
  case ApiFileError::InvalidOperation:
    return -EINVAL;
  case ApiFileError::ItemNotFound:
    return -ENOENT;
  case ApiFileError::OSErrorCode:
    return -errno;
  case ApiFileError::PermissionDenied:
    return -EPERM;
  case ApiFileError::Success:
    return 0;
  case ApiFileError::Unsupported:
    return -ENOSYS;
  case ApiFileError::UploadFailed:
#if __APPLE__ || IS_FREEBSD
    return -EBADMSG;
#else
    return -EREMOTEIO;
#endif
  case ApiFileError::XAttrBufferSmall:
    return -ERANGE;
  case ApiFileError::XAttrExists:
    return -EEXIST;
  case ApiFileError::XAttrInvalidNamespace:
    return -ENOTSUP;
  case ApiFileError::XAttrNotFound:
#ifdef __APPLE__
    return -ENOATTR;
#else
    return -ENODATA;
#endif
  case ApiFileError::XAttrTooBig:
#ifdef __APPLE__
    return -ENAMETOOLONG;
#else
    return -E2BIG;
#endif
#ifdef __APPLE__
  case ApiFileError::XAttrOSXInvalid:
    return -EINVAL;
#endif
  default:
    return -EIO;
  }
}
#endif

template <typename T>
inline void RemoveElement(T& t, const typename T::value_type& value) {
  t.erase(std::remove(t.begin(), t.end(), value), t.end());
}
#ifdef _WIN32
inline void SetLastErrorCode(DWORD errorCode) {
  ::SetLastError(errorCode);
}
#else
inline void SetLastErrorCode(int errorCode) {
  errno = errorCode;
}
#endif
inline void SpinWaitForMutex(std::function<bool()> complete, std::condition_variable& cv, std::mutex& mtx) {
  while (not complete()) {
    UniqueMutexLock l(mtx);
    if (not complete()) {
      cv.wait_for(l, 1s);
    }
    l.unlock();
  }
}

inline void SpinWaitForMutex(bool& complete, std::condition_variable& cv, std::mutex& mtx) {
  while (not complete) {
    UniqueMutexLock l(mtx);
    if (not complete) {
      cv.wait_for(l, 1s);
    }
    l.unlock();
  }
}
template <typename T>
std::string ToHexString(const T& t) {
  std::string ret;

  for (const auto& v : t) {
    char h[3] = { 0 };
#ifdef _WIN32
    sprintf_s(&h[0], sizeof(h), "%x", static_cast<std::uint8_t>(v));
#else
    sprintf(&h[0], "%x", static_cast<std::uint8_t>(v));
#endif
    ret += ((strlen(h) == 1) ? std::string("0") + h : h);
  }

  return ret;
}
}
}

#endif //REPERTORY_UTILS_H