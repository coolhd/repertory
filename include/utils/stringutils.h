#ifndef REPERTORY_STRINGUTILS_H
#define REPERTORY_STRINGUTILS_H

#include <algorithm>
#include <codecvt>
#include <locale>
#include <sstream>
#include <string>
#include <vector>

namespace Repertory {
namespace Utils {
namespace String {

// Prototypes
static inline bool BeginsWith(const std::string& str, const std::string& val);
static inline bool Contains(const std::string& str, const std::string& search);
static inline bool EndsWith(const std::string& str, const std::string& val);
static inline std::string FromBool(const bool& val);
static inline std::string FromDouble(const double &value);
static inline std::string FromInt32(const std::int32_t& val);
static inline std::string FromInt64(const std::int64_t& val);
static inline std::string FromUInt8(const std::uint8_t& val);
static inline std::string FromUInt16(const std::uint16_t& val);
static inline std::string FromUInt32(const std::uint32_t& val);
static inline std::string FromUInt64(const std::uint64_t& val);
static inline std::wstring FromUtf8(const std::string& str);
static inline std::string &LeftTrim(std::string &s);
static inline std::string &LeftTrim(std::string &s, const char& c);
static inline std::string& Replace(std::string& src, const char &character, const char &with);
static std::string& Replace(std::string &src, const std::string& find, const std::string& with, size_t startPos = 0);
static inline std::string ReplaceCopy(std::string src, const char &character, const char &with);
static std::string ReplaceCopy(std::string src, const std::string& find, const std::string& with, size_t startPos = 0);
static inline std::string &RightTrim(std::string &s);
static inline std::string &RightTrim(std::string &s, const char& c);
static std::vector<std::string> Split(const std::string& str, const char& delim, const bool& trim = true);
static inline bool ToBool(const std::string& val);
static inline double ToDouble(const std::string &str);
static inline std::string ToLower(std::string str);
static inline std::int32_t ToInt32(const std::string& val);
static inline std::int64_t ToInt64(const std::string& val);
static inline std::uint8_t ToUInt8(const std::string& val);
static inline std::uint16_t ToUInt16(const std::string& val);
static inline std::uint32_t ToUInt32(const std::string& val);
static inline std::uint64_t ToUInt64(const std::string& val);
static inline std::string ToUpper(std::string str);
static inline const std::string& ToUtf8(const std::string& str);
static inline std::string ToUtf8(const std::wstring& str);
static inline std::string &Trim(std::string& str);
static inline std::string &Trim(std::string& str, const char& c);

// Implementations
static inline bool BeginsWith(const std::string& str, const std::string& val) {
  return (str.find(val) == 0);
}

static inline bool Contains(const std::string& str, const std::string& search) {
  return (str.find(search) != std::string::npos);
}

static inline bool EndsWith(const std::string& str, const std::string& val) {
  if (val.size() > str.size()) {
    return false;
  }
  return std::equal(val.rbegin(), val.rend(), str.rbegin());
}

static inline std::string FromBool(const bool& val) {
  return std::move(std::to_string(val));
}

static inline std::string FromDouble(const double &value) {
  return std::move(std::to_string(value));
}

static inline std::string FromInt32(const std::int32_t& val) {
  return std::move(std::to_string(val));
}

static inline std::string FromInt64(const std::int64_t& val) {
  return std::to_string(val);
}

static inline std::string FromUInt8(const std::uint8_t& val) {
  return std::move(std::to_string(val));
}

static inline std::string FromUInt16(const std::uint16_t& val) {
  return std::move(std::to_string(val));
}

static inline std::string FromUInt32(const std::uint32_t& val) {
  return std::move(std::to_string(val));
}

static inline std::string FromUInt64(const std::uint64_t& val) {
  return std::to_string(val);
}

static inline std::wstring FromUtf8(const std::string& str) {
  return str.empty() ? L"" : std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().from_bytes(str);
}

static inline std::string &LeftTrim(std::string &s) {
  return LeftTrim(s, ' ');
}

static inline std::string &LeftTrim(std::string &s, const char& c) {
  s.erase(0, s.find_first_not_of(c));
  return s;
}

static inline std::string& Replace(std::string& src, const char &character, const char &with) {
  std::replace(src.begin(), src.end(), character, with);
  return src;
}

static std::string& Replace(std::string &src, const std::string& find, const std::string& with, size_t startPos) {
  if (!src.empty() && (startPos < src.size())) {
    while ((startPos = src.find(find, startPos)) != std::string::npos) {
      src.replace(startPos, find.size(), with);
      startPos += with.size();
    }
  }
  return src;
}

static inline std::string ReplaceCopy(std::string src, const char &character, const char &with) {
  std::replace(src.begin(), src.end(), character, with);
  return std::move(src);
}

static std::string ReplaceCopy(std::string src, const std::string& find, const std::string& with, size_t startPos) {
  if (!src.empty() && (startPos < src.size())) {
    while ((startPos = src.find(find, startPos)) != std::string::npos) {
      src.replace(startPos, find.size(), with);
      startPos += with.size();
    }
  }
  return std::move(src);
}

static inline std::string &RightTrim(std::string &s) {
  return RightTrim(s, ' ');
}

static inline std::string &RightTrim(std::string &s, const char& c) {
  s.erase(s.find_last_not_of(c) + 1);
  return s;
}

static std::vector<std::string> Split(const std::string& str, const char& delim, const bool& trim) {
  std::vector<std::string> ret;
  std::stringstream ss(str);
  std::string item;
  while (std::getline(ss, item, delim)) {
    ret.push_back(trim ? Trim(item) : item);
  }
  return std::move(ret);
}

static inline bool ToBool(const std::string& val) {
  std::istringstream is(ToLower(val));
  auto b = false;
  is >> std::boolalpha >> b;
  return b;
}

static inline double ToDouble(const std::string &str) {
  return std::stod(str);
}

static inline std::int32_t ToInt32(const std::string& val) {
  return std::stoi(val);
}

static inline std::int64_t ToInt64(const std::string& val) {
  return std::stoll(val);
}

static inline std::string ToLower(std::string str) {
  std::transform(str.begin(), str.end(), str.begin(), ::tolower);
  return std::move(str);
}

static inline std::uint8_t ToUInt8(const std::string& val) {
  return static_cast<std::uint8_t>(std::stoul(val));
}

static inline std::uint16_t ToUInt16(const std::string& val) {
  return static_cast<std::uint16_t>(std::stoul(val));
}

static inline std::uint32_t ToUInt32(const std::string& val) {
  return static_cast<std::uint32_t>(std::stoul(val));
}

static inline std::uint64_t ToUInt64(const std::string& val) {
  return std::stoull(val);
}

static inline std::string ToUpper(std::string str) {
  std::transform(str.begin(), str.end(), str.begin(), ::toupper);
  return std::move(str);
}

static inline const std::string& ToUtf8(const std::string& str) {
  return str;
}

static inline std::string ToUtf8(const std::wstring& str) {
  return str.empty() ? "" : std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t>().to_bytes(str);
}

static inline std::string &Trim(std::string& str) {
  return RightTrim(LeftTrim(str));
}

static inline std::string &Trim(std::string& str, const char& c) {
  return RightTrim(LeftTrim(str, c), c);
}
}
}
}

#endif //REPERTORY_STRINGUTILS_H
