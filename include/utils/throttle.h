#ifndef REPERTORY_THROTTLE_H
#define REPERTORY_THROTTLE_H

#include <common.h>

namespace Repertory {
class CThrottle final {
  public:
    CThrottle() :
      _maxSize(10) {
    }

    explicit CThrottle(const std::size_t& maxSize) :
      _maxSize(maxSize) {
    }

  public:
    CThrottle(const CThrottle&) noexcept = delete;
    CThrottle(CThrottle&&) noexcept = delete;
    CThrottle& operator=(const CThrottle&) = delete;
    CThrottle& operator=(CThrottle&&) = delete;

  public:
    ~CThrottle() {
      Shutdown();
    }

  private:
    const std::size_t _maxSize;
    std::size_t _count = 0;
    bool _isShutdown = false;
    std::mutex _throttleMutex;
    std::condition_variable _throttleNotify;

  public:
    void Decrement() {
      MutexLock l(_throttleMutex);
      if (not _isShutdown) {
        if (_count > 0) {
          _count--;
        }
        _throttleNotify.notify_one();
      }
    }

    void IncrementOrWait() {
      UniqueMutexLock l(_throttleMutex);
      if (not _isShutdown) {
        if (_count >= _maxSize) {
          _throttleNotify.wait(l);
        }
        if (not _isShutdown) {
          _count++;
        }
      }
    }

    void Reset() {
      UniqueMutexLock l(_throttleMutex);
      if (_isShutdown) {
        _count = 0;
        _isShutdown = false;
      }
    }

    void Shutdown() {
      if (not _isShutdown) {
        UniqueMutexLock l(_throttleMutex);
        _isShutdown = true;
        _throttleNotify.notify_all();
      }
    }
};
}

#endif //REPERTORY_THROTTLE_H
