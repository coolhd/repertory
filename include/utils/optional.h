#ifndef REPERTORY_OPTIONAL_H
#define REPERTORY_OPTIONAL_H
#if IS_DEBIAN9_DISTRO || !HAS_STD_OPTIONAL
#include <memory>
namespace std {
class bad_optional_access :
  std::runtime_error {
  public:
    bad_optional_access() :
      std::runtime_error("bad optional access") {
    }
};

template<typename t>
class optional {
  public:
    optional() = default;

    explicit optional(const t& v) :
      _val(new t(v)) {
    }

    explicit optional(t&& v) noexcept :
      _val(new t(std::move(v))) {
    }

    optional(const optional& o) noexcept :
      _val(o.has_value() ? new t(o._val) : nullptr) {
    }

    optional(optional&& o) noexcept :
      _val(o.has_value() ? std::move(o._val) : nullptr) {
    }

  private:
    std::unique_ptr<t> _val;

  public:
    inline bool has_value() const {
      return (_val != nullptr);
    }

    inline t& value() {
      if (not has_value()) {
        throw bad_optional_access();
      }
      return *_val;
    }

    inline optional& operator=(const t& v) {
      if (&v != _val.get()) {
        _val.reset(new t(v));
      }
      return *this;
    }

    inline optional& operator=(const optional& v) {
      if (&v != this) {
        _val.reset(new t(v.has_value() ? v.value() : nullptr));
      }
      return *this;
    }

    inline optional& operator=(t&& v) noexcept {
      if (&v != _val.get()) {
        _val.reset(new t(std::move(v)));
      }
      return *this;
    }

    inline optional& operator=(optional&& v) noexcept {
      if (&v != this) {
        _val = std::move(v);
      }
      return *this;
    }

    inline bool operator==(const optional& v) {
      return (&v == this) || ((has_value() && v.has_value()) && (*_val == *v._val));
    }

    inline bool operator==(const t& v) {
      return has_value() && (v == *_val);
    }

    inline bool operator!=(const optional& v) {
      return (has_value() != v.has_value) || ((has_value() && v.has_value()) && (*_val != *v._val));
    }

    inline bool operator!=(const t& v) {
      return not has_value() || (v != *_val);
    }
};
}
#endif
#endif //REPERTORY_OPTIONAL_H
