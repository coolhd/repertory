#ifndef REPERTORY_NATIVEFILE_H
#define REPERTORY_NATIVEFILE_H

#include <common.h>
#ifndef _WIN32
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

#endif
#if __APPLE__ || IS_FREEBSD
#define pread64 pread
#define pwrite64 pwrite
#endif

namespace Repertory {
class CNativeFile final {
  public:
    typedef std::shared_ptr<CNativeFile> NativeFilePtr;

  public:
    static inline NativeFilePtr Attach(OSHandle handle) {
      return NativeFilePtr(new CNativeFile(handle));
    }

    static ApiFileError CreateOrOpen(const std::string &sourcePath, NativeFilePtr &nativeFile) {
#ifdef _WIN32
      auto handle = ::CreateFile(&sourcePath[0], GENERIC_READ|GENERIC_WRITE, FILE_SHARE_READ|FILE_SHARE_WRITE, nullptr, OPEN_ALWAYS, FILE_FLAG_RANDOM_ACCESS, nullptr);
#else
      auto handle = open(&sourcePath[0], O_CREAT | O_RDWR | O_CLOEXEC, 0600u);
      chmod(&sourcePath[0], 0600u);
#endif
      nativeFile = CNativeFile::Attach(handle);
      return ((handle == INVALID_OSHANDLE_VALUE) ? ApiFileError::OSErrorCode : ApiFileError::Success);
    }

  private:
    explicit CNativeFile(const OSHandle &handle) :
      _handle(handle) {
    }

  public:
    ~CNativeFile() = default;

  private:
    OSHandle _handle;
#ifdef _WIN32
    std::recursive_mutex _readWriteMutex;
#endif

  public:
    inline bool Allocate(const std::uint64_t &fileSize) {
#ifdef _WIN32
      LARGE_INTEGER li{};
      li.QuadPart = fileSize;
      return (::SetFilePointerEx(_handle, li, nullptr, FILE_BEGIN) && ::SetEndOfFile(_handle));
#endif
#ifdef __linux__
      return (fallocate(_handle, 0, 0, fileSize) >= 0);
#endif
#ifdef IS_FREEBSD
      return (posix_fallocate(_handle, 0, fileSize) >= 0);
#endif
#ifdef __APPLE__
      return (ftruncate(_handle, fileSize) >= 0);
#endif
    }

    inline void Close() {
      if (_handle != INVALID_OSHANDLE_VALUE) {
#ifdef WIN32
        ::CloseHandle(_handle);
#else
        close(_handle);
#endif
        _handle = INVALID_OSHANDLE_VALUE;
      }
    }

    bool CopyFrom(const NativeFilePtr& source) {
      auto ret = false;
      std::uint64_t fileSize;
      if ((ret = source->GetFileSize(fileSize))) {
        std::vector<char> buffer;
        buffer.resize(65536 * 2);
        std::uint64_t offset = 0;
        while (ret && (fileSize > 0)) {
          std::size_t bytesRead;
          if ((ret = source->ReadBytes(&buffer[0], buffer.size(), offset, bytesRead))) {
            std::size_t bytesWritten;
            ret = WriteBytes(&buffer[0], bytesRead, offset, bytesWritten);
            fileSize -= bytesRead;
            offset += bytesRead;
          }
        }
        Flush();
      }

      return ret;
    }

    bool CopyFrom(const std::string &sourceFile) {
      auto ret = false;
      NativeFilePtr nativeFile;
      if (CNativeFile::CreateOrOpen(sourceFile, nativeFile) == ApiFileError ::Success) {
        ret = CopyFrom(nativeFile);
        nativeFile->Close();
      }

      return ret;
    }

    inline void Flush() {
#ifdef _WIN32
      RMutexLock l(_readWriteMutex);
      ::FlushFileBuffers(_handle);
#else
      fsync(_handle);
#endif
    }

    inline bool GetFileSize(std::uint64_t &fileSize) {
      auto ret = false;
#ifdef _WIN32
      LARGE_INTEGER li{};
      if ((ret = ::GetFileSizeEx(_handle, &li) && (li.QuadPart >= 0))) {
        fileSize = static_cast<std::uint64_t>(li.QuadPart);
      }
#else
#if __APPLE__ || IS_FREEBSD
      struct stat st{};
      if (fstat(_handle, &st) >= 0) {
#else
      struct stat64 st{};
      if (fstat64(_handle, &st) >= 0) {
#endif
        if ((ret = (st.st_size >= 0))) {
          fileSize = static_cast<uint64_t>(st.st_size);
        }
      }
#endif
      return ret;
    }

    inline OSHandle GetHandle() {
      return _handle;
    }

#ifdef _WIN32
    bool ReadBytes(char *buffer, const std::size_t& readSize, const std::uint64_t& readOffset, std::size_t &bytesRead) {
      RMutexLock l(_readWriteMutex);
      
      auto ret = false;
      bytesRead = 0;
      LARGE_INTEGER li{};
      li.QuadPart = readOffset;
      if ((ret = !!::SetFilePointerEx(_handle, li, nullptr, FILE_BEGIN))) {
        DWORD dwRead = 0;
        do {
          dwRead = 0;
          ret = !!::ReadFile(_handle, &buffer[bytesRead], static_cast<DWORD>(readSize - bytesRead), &dwRead, nullptr);
          bytesRead += dwRead;
        } while (ret && (bytesRead < readSize) && (dwRead != 0));
      }

      if (ret && (readSize != bytesRead)) {
        ::SetLastError(ERROR_HANDLE_EOF);
      }

      return ret;
    }
#else
    bool ReadBytes(char *buffer, const std::size_t &readSize, const std::uint64_t &readOffset, std::size_t &bytesRead) {
      bytesRead = 0;
      ssize_t result = 0;
      do {
        result = pread64(_handle, &buffer[bytesRead], readSize - bytesRead, readOffset + bytesRead);
        if (result > 0) {
          bytesRead = static_cast<size_t>(result);
        }
      } while ((result > 0) && (bytesRead < readSize));

      return (result >= 0);
    }
#endif
    inline bool Truncate(const std::uint64_t &fileSize) {
#ifdef _WIN32
      RMutexLock l(_readWriteMutex);
      LARGE_INTEGER li{};
      li.QuadPart = fileSize;
      return (::SetFilePointerEx(_handle, li, nullptr, FILE_BEGIN) &&
              ::SetEndOfFile(_handle));
#else
      return (ftruncate(_handle, fileSize) >= 0);
#endif
    }

#ifdef _WIN32
    bool WriteBytes(const char *buffer, const std::size_t& writeSize, const std::uint64_t& writeOffset, std::size_t& bytesWritten) {
      RMutexLock l(_readWriteMutex);
      
      bytesWritten = 0;
      auto ret = true;
    
      LARGE_INTEGER li{};
      li.QuadPart = writeOffset;
      if ((ret = !!::SetFilePointerEx(_handle, li, nullptr, FILE_BEGIN))) {
        do {
          DWORD dwWrite = 0;
          ret = !!::WriteFile(_handle, &buffer[bytesWritten], static_cast<DWORD>(writeSize - bytesWritten), &dwWrite, nullptr);
          bytesWritten += dwWrite;
        } while (ret && (bytesWritten < writeSize));
      }
    
      return ret;
    }
#else
    bool WriteBytes(const char *buffer, const std::size_t &writeSize, const std::uint64_t &writeOffset, std::size_t &bytesWritten) {
      bytesWritten = 0;
      ssize_t result = 0;
      do {
        result = pwrite64(_handle, &buffer[bytesWritten], writeSize - bytesWritten, writeOffset + bytesWritten);
        if (result > 0) {
          bytesWritten += static_cast<size_t>(result);
        }
      } while ((result >= 0) && (bytesWritten < writeSize));

      return (bytesWritten == writeSize);
    }
#endif
};
typedef CNativeFile::NativeFilePtr NativeFilePtr;
}
#endif //REPERTORY_NATIVEFILE_H
