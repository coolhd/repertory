if (LINUX OR FREEBSD)
  set(LIBUUID_PROJECT_NAME libuuid_${LIBUUID_VERSION})
  set(LIBUUID_BUILD_ROOT ${EXTERNAL_BUILD_ROOT}/builds/${LIBUUID_PROJECT_NAME})
  ExternalProject_Add(libuuid_project
    DOWNLOAD_NO_PROGRESS 1
    URL "https://www.mirrorservice.org/sites/ftp.ossp.org/pkg/lib/uuid/uuid-${LIBUUID_VERSION}.tar.gz"
    PREFIX ${LIBUUID_BUILD_ROOT}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ./configure --enable-static --prefix=${EXTERNAL_BUILD_ROOT}
    BUILD_COMMAND make
    INSTALL_COMMAND make install
  )
  set(LIBUUID_LIBRARIES ${EXTERNAL_BUILD_ROOT}/lib/libuuid.a)
endif()