if (UNIX)
  set(BOOST_PROJECT_NAME boost_${BOOST_VERSION})
  set(BOOST_BUILD_ROOT ${EXTERNAL_BUILD_ROOT}/builds/${BOOST_PROJECT_NAME})
  if (FREEBSD)
    set(BOOST_TOOLSET --with-toolset=clang)
  else ()
    set(BOOST_OPENSSL_DIR "--openssldir=${EXTERNAL_BUILD_ROOT}")
  endif()
  ExternalProject_Add(boost_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://dl.bintray.com/boostorg/release/${BOOST_VERSION}/source/boost_${BOOST_VERSION_DL}.tar.gz
    PREFIX ${BOOST_BUILD_ROOT}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND ./bootstrap.sh ${BOOST_TOOLSET} --with-libraries=atomic,chrono,date_time,filesystem,random,regex,system,thread
    BUILD_COMMAND ./b2 ${BOOST_OPENSSL_DIR} link=static threading=multi address-model=64 architecture=x86 cxxstd=${CMAKE_CXX_STANDARD} cxxflags=-std=c++${CMAKE_CXX_STANDARD} linkflags=-std=c++${CMAKE_CXX_STANDARD} --prefix=${EXTERNAL_BUILD_ROOT} define=BOOST_SYSTEM_NO_DEPRECATED define=BOOST_ASIO_HAS_STD_STRING_VIEW
    INSTALL_COMMAND ./b2 install ${BOOST_OPENSSL_DIR} threading=multi link=static address-model=64 architecture=x86 cxxstd=${CMAKE_CXX_STANDARD} cxxflags=-std=c++${CMAKE_CXX_STANDARD} linkflags=-std=c++${CMAKE_CXX_STANDARD} --prefix=${EXTERNAL_BUILD_ROOT} define=BOOST_SYSTEM_NO_DEPRECATED define=BOOST_ASIO_HAS_STD_STRING_VIEW
  )

  if (NOT FREEBSD)
    add_dependencies(boost_project openssl_project)
  endif()

  set(BOOST_ROOT ${BOOST_BUILD_ROOT}/src/boost_project)

  set(Boost_LIBRARIES
    libboost_system.a
    libboost_atomic.a
    libboost_chrono.a
    libboost_date_time.a
    libboost_filesystem.a
    libboost_random.a
    libboost_regex.a
    libboost_thread.a
  )
elseif(MSVC)
  set(BOOST_PROJECT_NAME boost_${BOOST_VERSION})
  set(BOOST_BUILD_ROOT ${EXTERNAL_BUILD_ROOT}/builds/${BOOST_PROJECT_NAME})
  ExternalProject_Add(boost_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://dl.bintray.com/boostorg/release/${BOOST_VERSION}/source/boost_${BOOST_VERSION_DL}.tar.gz
    PREFIX ${BOOST_BUILD_ROOT}
    BUILD_IN_SOURCE 1
    CONFIGURE_COMMAND bootstrap.bat --with-date_time --with-regex --with-system
    BUILD_COMMAND b2.exe --with-date_time --with-regex --with-system --prefix=${EXTERNAL_BUILD_ROOT} runtime-link=shared threading=multi address-model=64 architecture=x86 toolset=msvc-14.1 variant=${CMAKE_BUILD_TYPE_LOWER} -sZLIB_BINARY=zlibstatic${DEBUG_EXTRA} -sZLIB_LIBPATH="${EXTERNAL_BUILD_ROOT}/lib" -sZLIB_INCLUDE="${EXTERNAL_BUILD_ROOT}/include"
    INSTALL_COMMAND b2.exe install --with-date_time --with-regex --with-system --prefix=${EXTERNAL_BUILD_ROOT} runtime-link=shared threading=multi address-model=64 architecture=x86 toolset=msvc-14.1 variant=${CMAKE_BUILD_TYPE_LOWER} -sZLIB_BINARY=zlibstatic${DEBUG_EXTRA} -sZLIB_LIBPATH="${EXTERNAL_BUILD_ROOT}/lib" -sZLIB_INCLUDE="${EXTERNAL_BUILD_ROOT}/include"
  )
  add_dependencies(boost_project zlib_project)
  set(BOOST_ROOT ${BOOST_BUILD_ROOT}/src/boost_project)
  set(Boost_INCLUDE_DIR ${EXTERNAL_BUILD_ROOT}/include/boost-${BOOST_VERSION_DLL})
  set(Boost_LIBRARIES
    libboost_date_time-vc141-mt-${BOOST_DEBUG_EXTRA}x64-${BOOST_VERSION_DLL}.lib
    libboost_regex-vc141-mt-${BOOST_DEBUG_EXTRA}x64-${BOOST_VERSION_DLL}.lib
    libboost_system-vc141-mt-${BOOST_DEBUG_EXTRA}x64-${BOOST_VERSION_DLL}.lib
  )
endif()
