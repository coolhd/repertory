# Testing
enable_testing()

set(GTEST_PROJECT_NAME gtest_${GTEST_VERSION})
set(GTEST_BUILD_ROOT ${EXTERNAL_BUILD_ROOT}/builds/${GTEST_PROJECT_NAME})
if (MSVC)
  ExternalProject_Add(gtest_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://github.com/google/googletest/archive/release-${GTEST_VERSION}.tar.gz
    PREFIX ${GTEST_BUILD_ROOT}
    CMAKE_ARGS -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_BUILD_TYPE=${EXTERNAL_BUILD_TYPE} -Dgtest_force_shared_crt=OFF -DBUILD_SHARED_LIBS=ON -DCMAKE_CXX_FLAGS=/D_SILENCE_TR1_NAMESPACE_DEPRECATION_WARNING
    INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step."
  )
else()
  ExternalProject_Add(gtest_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://github.com/google/googletest/archive/release-${GTEST_VERSION}.tar.gz
    PREFIX ${GTEST_BUILD_ROOT}
    CMAKE_ARGS -DCMAKE_C_FLAGS=${CMAKE_C_FLAGS} -DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS} -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_BUILD_TYPE=${EXTERNAL_BUILD_TYPE}
    INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step."
  )
endif()

set(GTEST_INCLUDE_DIRS
  ${GTEST_BUILD_ROOT}/src/gtest_project/googletest/include
  ${GTEST_BUILD_ROOT}/src/gtest_project/googlemock/include
)

if (MSVC)
  if (CMAKE_GENERATOR_LOWER STREQUAL "nmake makefiles")
    set(GTEST_LIBRARIES
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock${DEBUG_EXTRA}.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock_main${DEBUG_EXTRA}.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest${DEBUG_EXTRA}.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest_main${DEBUG_EXTRA}.lib
    )
  else()
    set(GTEST_LIBRARIES
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock${DEBUG_EXTRA}.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock_main${DEBUG_EXTRA}.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest${DEBUG_EXTRA}.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest_main${DEBUG_EXTRA}.lib
    )
  endif()
elseif (UNIX)
  set(GTEST_LIBRARIES
    ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/libgmock${DEBUG_EXTRA}.a
    ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/libgtest${DEBUG_EXTRA}.a
    ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/libgtest_main${DEBUG_EXTRA}.a
  )
endif()

file(GLOB_RECURSE UNITTEST_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/tests/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/tests/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/tests/fixtures/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/tests/mocks/*.h
  ${CMAKE_CURRENT_SOURCE_DIR}/tests/utils/*.h
)

add_executable(unittests
  ${CMAKE_CURRENT_SOURCE_DIR}/3rd_party/jsonrpcpp-1.1.1/lib/jsonrp.cpp
  ${REPERTORY_HEADERS}
  ${UNITTEST_SOURCES}
)

target_link_libraries(unittests PRIVATE ${GTEST_LIBRARIES})

target_compile_definitions(unittests PUBLIC GTEST_LINKED_AS_SHARED_LIBRARY=1)

if (MSVC)
  if (CMAKE_GENERATOR_LOWER STREQUAL "nmake makefiles")
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}
    )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock_main${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}
    )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}
    )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest_main${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}
    )
  else()
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
    )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock_main${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
    )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
    )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest_main${DEBUG_EXTRA}.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
    )
  endif()
endif()
target_include_directories(unittests PUBLIC
  ${GTEST_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}/tests
)
add_dependencies(unittests
  gtest_project
  sqlitecpp_project
  rocksdb_project
  cpprestsdk_project
)
if (LINUX OR FREEBSD)
  add_dependencies(unittests
    libuuid_project
    curl_project
    boost_project
  )
  if (NOT FREEBSD)
    add_dependencies(unittests openssl_project)
  endif()
elseif (MACOS)
  add_dependencies(unittests
    openssl_project
    boost_project
  )
  target_link_libraries(unittests PRIVATE -W1,-rpath,@executable_path)
  set_target_properties(unittests PROPERTIES COMPILE_FLAGS -fvisibility=hidden)
elseif (MSVC)
  add_dependencies(unittests
    curl_project
    zlib_project
    boost_project
  )
  CopySupportDLLs(unittests)
  CodeSignFile(unittests unittests.exe)
endif()

if (CMAKE_GENERATOR_LOWER STREQUAL "nmake makefiles")
  add_test(NAME AllTests COMMAND unittests WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
else()
  add_test(NAME AllTests COMMAND unittests WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE})
endif()