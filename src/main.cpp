#include <common.h>
#include <algorithm>
#include <comm/curlcomm/curlcomm.h>
#include <events/eventsystem.h>
#include <iostream>
#include <memory>
#include <platform/unixplatform.h>
#include <platform/winplatform.h>
#include <utils/polling.h>
#include <providers/provider.h>
#include <rpc/client/client.h>
#include <sqlite3.h>
#include <utils/fileutils.h>

INITIALIZE_SINGLETONS()

#ifdef _WIN32
#include <drives/winfspdrive/winfspdrive.h>
typedef Repertory::CWinFSPDrive RepertoryDrive;
#else
#include <drives/fusedrive/fusedrive.h>
typedef Repertory::CFuseDrive RepertoryDrive;
#endif

using namespace Repertory;

static void ProcessApiAuthData(std::string& user, std::string& password, const ProviderType& providerType) {
  if (user.empty() && password.empty()) {
    const auto configFile = Utils::Path::Combine(CConfig::GetDefaultDataDirectory(providerType), "config.json");
    json data;
    auto success = false;
    for (auto i = 0; not(success = Utils::File::ReadJsonFile(configFile, data)) && (i < 20); i++) {
      std::this_thread::sleep_for(100ms);
    }

    if (success) {
      password = data["ApiAuth"].get<std::string>();
      user = data["ApiUser"].get<std::string>();
    }
  }
}

static std::vector<std::string> ProcessDriveArgs(const int &argc, char *argv[], ProviderType& providerType, std::string& dataDir) {
  const std::vector<std::string> appArgs = {
                                            "-cv",
                                            "--check_version",
                                            "-dc",
                                            "--display_config",
                                            "-dd",
                                            "--data_directory",
                                            "-di",
                                            "--drive_information",
                                            "-gc",
                                            "--generate_config",
                                            "-get",
                                            "-gt",
                                            "--generate_template",
                                            "-hidden",
                                            "-of",
                                            "--open_files",
                                            "-pw",
                                            "--password",
                                            "-sp",
                                            "--siaprime",
                                            "-set",
                                            "-status",
                                            "-unmount",
                                            "-us",
                                            "--user"};

  // Strip out options from command line
  std::vector<std::string> driveArgs;
  for (int i = 0; i < argc; i++) {
    if (std::find(appArgs.begin(), appArgs.end(), argv[i]) == appArgs.end()) {
      driveArgs.emplace_back(argv[i]);
    }
  }

#ifndef _WIN32
  std::vector<std::string> fuseFlagsList;
  for (int i = 1; i < driveArgs.size(); i++) {
    if (driveArgs[i].find("-o") == 0) {
      std::string options = "";
      if (driveArgs[i].size() == 2) {
        if ((i + 1) < driveArgs.size()) {
          options = driveArgs[++i];
        }
      } else {
        options = driveArgs[i].substr(2);
      }

      const auto optionList = Utils::String::Split(options, ',');
      for (const auto &option : optionList) {
        if ((option.find("sp") == 0) || (option.find("siaprime") == 0)) {
          providerType = ProviderType::SiaPrime;
        } else if ((option.find("dd") == 0) || (option.find("data_directory") == 0)) {
          const auto data = Utils::String::Split(option, '=');
          if (data.size() == 2) {
            dataDir = data[1];
          } else {
            std::cerr << "Invalid syntax for 'dd,data_directory'" << std::endl;
            exit(3);
          }
        } else {
          fuseFlagsList.push_back(option);
        }
      }
    }
  }

  {
    std::vector<std::string> newDriveArgs(driveArgs);
    for (int i = 1; i < newDriveArgs.size(); i++) {
      if (newDriveArgs[i].find("-o") == 0) {
        if (newDriveArgs[i].size() == 2) {
          if ((i + 1) < driveArgs.size()) {
            Utils::RemoveElement(newDriveArgs, newDriveArgs[i]);
            Utils::RemoveElement(newDriveArgs, newDriveArgs[i]);
            i--;
          }
        } else {
          Utils::RemoveElement(newDriveArgs, newDriveArgs[i]);
          i--;
        }
      }
    }

#ifdef __APPLE__
    auto it = std::remove_if(fuseFlagsList.begin(), fuseFlagsList.end(), [](const auto& option)->bool {
      return option.find("volname") == 0;
    });
    if (it != fuseFlagsList.end()) {
      fuseFlagsList.erase(it, fuseFlagsList.end());
    }
    fuseFlagsList.emplace_back("volname=Repertory" + CConfig::GetProviderDisplayName(providerType));

    it = std::remove_if(fuseFlagsList.begin(), fuseFlagsList.end(), [](const auto& option)->bool {
      return option.find("daemon_timeout") == 0;
    });
    if (it != fuseFlagsList.end()) {
      fuseFlagsList.erase(it, fuseFlagsList.end());
    }
    fuseFlagsList.emplace_back("daemon_timeout=300");

    it = std::remove_if(fuseFlagsList.begin(), fuseFlagsList.end(), [](const auto& option)->bool {
      return option.find("noappledouble") == 0;
    });
    if (it == fuseFlagsList.end()) {
      fuseFlagsList.emplace_back("noappledouble");
    }
#endif

    if (not fuseFlagsList.empty()) {
      std::string fuseOptions;
      for (const auto &flag: fuseFlagsList) {
        if (fuseOptions.empty()) {
          fuseOptions += flag;
        } else {
          fuseOptions += (',' + flag);
        }
      }
      newDriveArgs.emplace_back("-o");
      newDriveArgs.emplace_back(fuseOptions);
    }
    driveArgs = std::move(newDriveArgs);
  }
#endif

  return std::move(driveArgs);
}

int main(int argc, char *argv[]) {
  int mountResult = 0;
  if (argc == 1) {
    argc++;
    static std::string cmd(argv[0]);
    static std::vector<const char*> v({&cmd[0], "-h"});
    argv = (char**)&v[0];
  }

  auto ret = ExitCode::Success;
  auto providerType = (Utils::HasOption(argc, argv, "--siaprime") || Utils::HasOption(argc, argv, "-sp")) ?
                      ProviderType::SiaPrime :
                      ProviderType::Sia;
  std::string dataDir;
  if (Utils::HasOption(argc, argv, "-dd") || Utils::HasOption(argc, argv, "--data_directory")) {
    auto data = Utils::ParseOption(argc, argv, "-dd", 1);
    if (data.empty()) {
      data = Utils::ParseOption(argc, argv, "--data_directory", 1);
      if (data.empty()) {
        std::cerr << "Invalid syntax for '-dd,--data_directory'" << std::endl;
        ret = ExitCode::InvalidSyntax;
      }
    }

    if (not data.empty()) {
      dataDir = data[0];
    }
  }

  std::string password;
  if (Utils::HasOption(argc, argv, "-pw") || Utils::HasOption(argc, argv, "--password")) {
    auto data = Utils::ParseOption(argc, argv, "-pw", 1);
    if (data.empty()) {
      data = Utils::ParseOption(argc, argv, "--password", 1);
    }
    if (not data.empty()) {
      password = data[0];
    }
  }

  std::string user;
  if (Utils::HasOption(argc, argv, "-us") || Utils::HasOption(argc, argv, "--user")) {
    auto data = Utils::ParseOption(argc, argv, "-us", 1);
    if (data.empty()) {
      data = Utils::ParseOption(argc, argv, "--user", 1);
    }
    if (not data.empty()) {
      user = data[0];
    }
  }
  
  if (ret == ExitCode::Success) {
    if (Utils::HasOption(argc, argv, "-cv") || Utils::HasOption(argc, argv, "--check_version")) {
      CConfig config(providerType, dataDir);
      CCurlComm curlComm(config);
      json data, err;

      if (curlComm.Get("/daemon/version", data, err) == ApiError::Success) {
        const auto res = Utils::CompareVersionStrings(data["version"].get<std::string>(), CConfig::GetProviderMinimumVersion(providerType));
        if (res < 0) {
          ret = ExitCode::IncompatibleVersion;
          std::cerr << "Failed!" << std::endl;
          std::cerr << "   Actual: " << data["version"].get<std::string>() << std::endl;
          std::cerr << "  Minimum: " << CConfig::GetProviderMinimumVersion(providerType) << std::endl;
        } else {
          std::cout << "Success!" << std::endl;
          std::cout << "   Actual: " << data["version"].get<std::string>() << std::endl;
          std::cout << "  Minimum: " << CConfig::GetProviderMinimumVersion(providerType) << std::endl;
        }
      } else {
        std::cerr << "Failed!" << std::endl;
        std::cerr << err.dump(2) << std::endl;
        ret = ExitCode::CommunicationError;
      }
    } else if (Utils::HasOption(argc, argv, "-di") || Utils::HasOption(argc, argv, "--drive_information")) {
      const auto lockResult = GrabLock(providerType, 1);
      if (lockResult == LockResult::Locked) {
        ProcessApiAuthData(user, password, providerType);
        const auto rpcResponse = CClient({"localhost",
                                          password,
                                          CConfig::GetDefaultRPCPort(providerType),
                                          user}).GetDriveInformation();
        std::cout << static_cast<int>(rpcResponse.ResponseType) << std::endl;
        std::cout << rpcResponse.Data.dump(2) << std::endl;
      } else {
        std::cerr << CConfig::GetProviderDisplayName(providerType) << " is not mounted." << std::endl;
        ret = ExitCode::NotMounted;
      }
      ReleaseLock(lockResult, providerType);
    } else if (Utils::HasOption(argc, argv, "-gt")) {
      std::cout << std::setw(2) << CConfig::CreateTemplate(providerType) << std::endl;
    } else if (Utils::HasOption(argc, argv, "-dc") || Utils::HasOption(argc, argv, "--display_config")) {
      const auto lockResult = GrabLock(providerType, 1);
      if (lockResult == LockResult::Success) {
        CConfig config(providerType, dataDir);
        const auto cfg = config.GetJson();
        std::cout << 0 << std::endl;
        std::cout << cfg.dump(2) << std::endl;
      } else if (lockResult == LockResult::Locked) {
        ProcessApiAuthData(user, password, providerType);
        const auto rpcResponse = CClient({"localhost",
                                          password,
                                          CConfig::GetDefaultRPCPort(providerType),
                                          user}).GetConfig();
        std::cout << static_cast<int>(rpcResponse.ResponseType) << std::endl;
        std::cout << rpcResponse.Data.dump(2) << std::endl;
      }
      ReleaseLock(lockResult, providerType);
    } else if (Utils::HasOption(argc, argv, "-get")) {
      auto data = Utils::ParseOption(argc, argv, "-get", 1);
      if (data.empty()) {
        ret = ExitCode::InvalidSyntax;
        std::cerr << "Invalid syntax for '-get'" << std::endl;
      } else {
        const auto lockResult = GrabLock(providerType, 1);
        if (lockResult == LockResult::Success) {
          CConfig config(providerType, dataDir);
          const auto value = config.GetValueByName(data[0]);
          std::cout << (value.empty() ? static_cast<int>(RPCResponseType::ConfigValueNotFound) : 0) << std::endl;
          std::cout << json({{"value", value}}).dump(2) << std::endl;
        } else if (lockResult == LockResult::Locked) {
          ProcessApiAuthData(user, password, providerType);
          const auto rpcResponse = CClient({"localhost",
                                            password,
                                            CConfig::GetDefaultRPCPort(providerType),
                                            user}).GetConfigValueByName(data[0]);
          std::cout << static_cast<int>(rpcResponse.ResponseType) << std::endl;
          std::cout << rpcResponse.Data.dump(2) << std::endl;
        }
        ReleaseLock(lockResult, providerType);
      }
    } else if (Utils::HasOption(argc, argv, "-of") || Utils::HasOption(argc, argv, "--open_files")) {
      const auto lockResult = GrabLock(providerType, 1);
      if (lockResult == LockResult::Locked) {
        ProcessApiAuthData(user, password, providerType);
        const auto rpcResponse = CClient({"localhost",
                                          password,
                                          CConfig::GetDefaultRPCPort(providerType),
                                          user}).GetOpenFiles();
        std::cout << static_cast<int>(rpcResponse.ResponseType) << std::endl;
        std::cout << rpcResponse.Data.dump(2) << std::endl;
      } else {
        std::cerr << CConfig::GetProviderDisplayName(providerType) << " is not mounted." << std::endl;
        ret = ExitCode::NotMounted;
      }
      ReleaseLock(lockResult, providerType);
    } else if (Utils::HasOption(argc, argv, "-set")) {
      auto data = Utils::ParseOption(argc, argv, "-set", 2);
      if (data.empty()) {
        ret = ExitCode::InvalidSyntax;
        std::cerr << "Invalid syntax for '-set'" << std::endl;
      } else {
        const auto lockResult = GrabLock(providerType, 1);
        if (lockResult == LockResult::Success) {
          CConfig config(providerType, dataDir);
          const auto value = config.SetValueByName(data[0], data[1]);
          std::cout << (value.empty() ? static_cast<int>(RPCResponseType::ConfigValueNotFound) : 0) << std::endl;
          std::cout << json({{"value", value}}).dump(2) << std::endl;
        } else if (lockResult == LockResult::Locked) {
          ProcessApiAuthData(user, password, providerType);
          const auto rpcResponse = CClient({"localhost",
                                            password,
                                            CConfig::GetDefaultRPCPort(providerType),
                                            user}).SetConfigValueByName(data[0], data[1]);
          std::cout << static_cast<int>(rpcResponse.ResponseType) << std::endl;
          std::cout << rpcResponse.Data.dump(2) << std::endl;
        }
        ReleaseLock(lockResult, providerType);
      }
    } else if (Utils::HasOption(argc, argv, "-status")) {
      const auto lockResult = GrabLock(providerType, 10);
      std::cout << std::setw(2) << ReadLockedData() << std::endl;
      ReleaseLock(lockResult, providerType);
    } else if (Utils::HasOption(argc, argv, "-unmount")) {
      ProcessApiAuthData(user, password, providerType);
      const auto rpcResponse = CClient({"localhost",
                                        password,
                                        CConfig::GetDefaultRPCPort(providerType),
                                        user}).Unmount();
      std::cout << static_cast<int>(rpcResponse.ResponseType) << std::endl;
      if (rpcResponse.ResponseType == RPCResponseType::Success) {
        std::cout << rpcResponse.Data.dump(2) << std::endl;
      } else {
        std::cerr << rpcResponse.Data.dump(2) << std::endl;
        ret = ExitCode::CommunicationError;
      }
    } else if (Utils::HasOption(argc, argv, "-V") || Utils::HasOption(argc, argv, "--version")) {
      std::cout << "Repertory core version: " << RepertoryVersion << std::endl;
      std::cout << "Repertory Git revision: " << RepertoryGitRev << std::endl;
      RepertoryDrive::DisplayVersionInfo(argc, argv);
    } else if (Utils::HasOption(argc, argv, "-h") || Utils::HasOption(argc, argv, "--help")) {
      RepertoryDrive::DisplayOptions(argc, argv);
      std::cout << "Repertory options:" << std::endl;
      std::cout << "    -cv,--check_version              Check daemon version compatibility" << std::endl;
      std::cout << "    -dc,--display_config             Display configuration" << std::endl;
      std::cout << "    -dd,--data_directory [directory] Override data directory" << std::endl;
      std::cout << "    -di,--drive_information          Display mounted drive information" << std::endl;
      std::cout << "    -gc,--generate_config            Generate initial configuration" << std::endl;
      std::cout << "    -get [name]                      Get configuration value" << std::endl;
      std::cout << "    -gt,--generate_template          Generate configuration template" << std::endl;
      std::cout << "    -nc                              Force disable console output" << std::endl;
      std::cout << "    -of,--open_files                 List all open files and count" << std::endl;
      std::cout << "    -pw,--password                   Specify API password" << std::endl;
      std::cout << "    -sp,--siaprime                   Enables SiaPrime mode" << std::endl;
#ifndef _WIN32
      std::cout << "    -o sp,-o siaprime                Enables SiaPrime mode for 'fstab' mounts" << std::endl;
#endif
      std::cout << "    -set [name] [value]              Set configuration value" << std::endl;
      std::cout << "    -status                          Display mount status" << std::endl;
      std::cout << "    -unmount                         Unmount and shutdown" << std::endl;
      std::cout << "    -us,--user                       Specify API user name" << std::endl;
    } else {
      const auto lockResult = GrabLock(providerType);
      if (lockResult == LockResult::Locked) {
        ret = ExitCode::MountActive;
        std::cerr << CConfig::GetProviderDisplayName(providerType) << " mount is already active" << std::endl;
      } else if (lockResult == LockResult::Success) {
        sqlite3_initialize();
        curl_global_init(CURL_GLOBAL_DEFAULT);
        {
          const auto generateConfig = Utils::HasOption(argc, argv, "--generate_config") || Utils::HasOption(argc, argv, "-gc");
          if (generateConfig) {
            CConfig config(providerType, dataDir);
            std::cout << "Generated " << CConfig::GetProviderDisplayName(providerType) << " Configuration" << std::endl;
            std::cout << config.GetConfigFilePath() << std::endl;
            ret = Utils::File::IsFile(config.GetConfigFilePath()) ? ExitCode::Success : ExitCode::FileCreationFailed;
          } else {
#ifdef _WIN32
            if (Utils::HasOption(argc, argv, "-hidden")) {
              ::ShowWindow(::GetConsoleWindow(), SW_HIDE);
            }
#endif
            std::cout << "Initializing " << CConfig::GetProviderDisplayName(providerType) << " Drive" << std::endl;
            const auto driveArgs = ProcessDriveArgs(argc, argv, providerType, dataDir);
            CConfig config(providerType, dataDir);
            auto comm = std::make_unique<CCurlComm>(config);

            try {
              auto provider = CreateProvider(providerType, config, *comm);
              RepertoryDrive drive(*provider, config);
              mountResult = drive.Main(driveArgs);
              ret = ExitCode::MountResult;
            } catch (const SQLite::Exception &) {
              std::cerr << CConfig::GetProviderDisplayName(providerType) << " mount is already active" << std::endl;
              ret = ExitCode::MountActive;
            } catch (const StartupException &e) {
              std::cerr << "FATAL: " << e.what() << std::endl;
              ret = ExitCode::StartupException;
            }
          }
        }
        curl_global_cleanup();
        sqlite3_shutdown();

        ReleaseLock(lockResult, providerType);
      } else {
        ret = ExitCode::LockFailed;
      }
    }
  }

  return ((ret == ExitCode::MountResult) ? mountResult : static_cast<int>(ret));
}


